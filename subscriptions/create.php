<?
include "../configuration.php";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../stripe/init.php');

$publishable_kay = "pk_test_132a6XiEWdvKD0IFjlIRXQsF";
$stripe_secret_key = "sk_test_51D9fjEDeozc14F99C2bzP28NWyZW2QJFm74d01lWzfYfeM4Eur16flRq4zWLe8lwGVv2jC7NerHDDTT2mLf4aYcE0043rCwJMI";


$stripe = new \Stripe\StripeClient($stripe_secret_key);


$st = $db->prepare("SELECT * FROM auto_repeat WHERE id = ?");
$st->execute(array(170));
$x = $st->fetchObject();


$products = array(array("prod_INRonpm9iJQqLI", "20", "9182"), array("prod_INQn9ckIlmmXvG", "30", "87265"), array("prod_INQcbhrWn3WY6n", "40", "02983"));

$frequency = "bi-monthly";
$num = 2;

$parts = array();

foreach($products as $k){
    $parts[] = array('metadata' => 
                     array('sku' => $k[2]),
                            'price_data' => array(
                                'currency' => 'GBP',
                                'product' => $k[0],
                                'recurring' => array(
                                    'interval' => 'month',
                                    'interval_count' => $num
                                ),
                                'unit_amount' => round(($k[1]*100))
                            ),
                            'quantity' => 1);
}

// Create
$due = strtotime($x->next_due);
                                
$subs = $stripe->subscriptions->create([
  'customer' => "cus_INRzH0nIs3lVjY",
  'items' => $parts,
  'off_session' => true,
  'collection_method' => 'charge_automatically',
  'billing_cycle_anchor' => $due,
  'trial_end' => $due

]);

$su = $db->prepare("UPDATE auto_repeat SET subscription_id = ? WHERE id = ?");
$su->execute(array($subs->id, $x->id));


?>