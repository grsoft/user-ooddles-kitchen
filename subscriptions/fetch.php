<?
include "../configuration.php";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../stripe/init.php');

$publishable_kay = "pk_test_132a6XiEWdvKD0IFjlIRXQsF";
$stripe_secret_key = "sk_test_51D9fjEDeozc14F99C2bzP28NWyZW2QJFm74d01lWzfYfeM4Eur16flRq4zWLe8lwGVv2jC7NerHDDTT2mLf4aYcE0043rCwJMI";


$stripe = new \Stripe\StripeClient($stripe_secret_key);


$st = $db->prepare("SELECT * FROM auto_repeat WHERE id = ?");
$st->execute(array(170));
$x = $st->fetchObject();

$products = array(array("prod_INRonpm9iJQqLI", "20", "9182", "true"), array("prod_INQn9ckIlmmXvG", "30", "87265", "false"), array("prod_INQcbhrWn3WY6n", "40", "02983", "false"));

$frequency = "monthly";
$num = 1;

$parts = array();

foreach($products as $k){
    $parts[] = array('metadata' => 
                     array('sku' => $k[2]),
                            'price_data' => array(
                                'currency' => 'GBP',
                                'product' => $k[0],
                                'recurring' => array(
                                    'interval' => 'month',
                                    'interval_count' => $num
                                ),
                                'unit_amount' => round(($k[1]*100))
                            ),
                    'quantity' => 1,
                    'deleted' => $k[3]);
}

$due = strtotime($x->next_due);

// Get all items and remove them.
$parts = $stripe->subscriptions->retrieve($x->subscription_id);


$su = $db->prepare("DELETE FROM auto_repeat_items WHERE auto_id = ?");
//$su->execute(array(170));

$si = $db->prepare("INSERT INTO auto_repeat_items (auto_id, price, quantity, stripe_id, stripe_product) VALUES (?,?,?,?,?)");

foreach($parts->items->data as $d){
    $si->execute(array(170, ($d->price->unit_amount/100), $d->quantity, $d->id, $d->price->product));
    echo $d->id." ".$d->quantity." ".$d->price->product." ".($d->price->unit_amount/100)." ".$d->price->recurring->interval_count."<br />";
    
    $frequency = $d->price->recurring->interval_count;
}

if($frequency == 2){
    $freq = "bi-monthly";
}else{
    $freq = "monthly";
}
// Update the subscription
$sf = $db->prepare("UPDATE auto_repeat SET frequency = ?, status = ? WHERE subscription_id = ?");
$sf->execute(array($freq, $parts->status, $x->subscription_id));

?>