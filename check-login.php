<?
include "configuration.php";

include "classes/login.hash.php";
	
// Check the email session and password to clarify
	
	if(!isset($_SESSION['a_email']) || !isset($_SESSION['password']) || !isset($_SESSION['unique_id'])){
		$logged_in = 0;
		$sc = $db->prepare("SELECT * FROM customers WHERE session_id = ?");
        $sc->execute(array(session_id()));
        $customer = $sc->fetchObject();  
		
		//return;
	}else{
		$active = check_login($_SESSION['a_email'], $_SESSION['password'], $db);
		
		
		if($active == 1){
			
			$st = $db->prepare("SELECT * FROM customers WHERE email = :email AND unique_id = :id AND tmp = :tmp AND unique_id != :check");
			$st->execute(array("email" => $_SESSION['a_email'], "id" => $_SESSION['unique_id'], "tmp" => 0, "check" => ""));
			$customer = $st->fetchObject();
            
            $logged_in = 2;
            
            // How many pets do we have?
                $sp = $db->prepare("SELECT * FROM pets WHERE customer_id = ? AND inactive = ?");
                $sp->execute(array($customer->id,0));
                $pets = $sp->rowCount();
               
            
		}else{
            $sc = $db->prepare("SELECT * FROM customers WHERE session_id = ?");
            $sc->execute(array(session_id()));
            $customer = $sc->fetchObject();  
            
            if(empty($customer->unique_id)){
                $date = new DateTime();
                $hash =  $date->getTimestamp();
                
                $su = $db->prepare("UPDATE customers SET unique_id = ? WHERE session_id = ?");
                $su->executer(array($hash, session_id()));
            }
            
			$logged_in = 0;
			
			
			unset($_SESSION['a_name']);
			unset($_SESSION['a_email']);
			unset($_SESSION['password']);
			unset($_SESSION['unique_id']);
            unset($_SESSION['customer_id']);
		}
        
    }
    

if(PAGE == "/summary.php" || PAGE == "/actions.php" || PAGE == "/basket.php" || PAGE == "/addresses.php" || PAGE == "/order.php"){
	
    $address = $customer->address.", ";
    if(!empty($customer->town)){
      $address .= $customer->town.", ";
    }
    $address .= $customer->city.", <br />".$customer->county." ".$customer->postcode."<br />".$customer->country;
    
    // The delivery address
        $st = $db->prepare("SELECT *
                            FROM deliveries
                            WHERE session_id = :session");
        $st->execute(array("session" => session_id()));
        if($st->rowCount() == 0){
            $st = $db->prepare("SELECT *
                                FROM deliveries
                                WHERE order_id = :session");
            $st->execute(array("session" => session_id()));
        }
        $delivery = $st->fetchObject();

    // The delivery address
        $delivery_address = $delivery->d_address.", ";
        if(!empty($delivery->d_address2)){
            $delivery_address .= $delivery->d_address2.", ";
        }
        $delivery_address .= $delivery->d_city.", <br />".$delivery->d_county." ".strtoupper($delivery->d_postcode)."<br />".$delivery->d_country;

        $delivery_price = 0;
    
		// Get ISO
        $sf = $db->prepare("SELECT surcharge, iso FROM countries WHERE country = ?");
        $sf->execute(array($delivery->d_country));
        $f = $sf->fetchObject();
        $surcharge = $f->surcharge;
        $iso = $f->iso;
    
        
        
}

if(!empty($_SESSION['referrer'])){
    if($logged_in == 2){
        // If they are logged in = have they ordered before?
        $so = $db->prepare("SELECT * FROM orders WHERE customer_id = ? AND status = ?");
        $so->execute(array($customer->id, "Paid"));
        if($cart->qty > 0 && $logged_in == 2 && $so->rowCount() == 0){
            $code_percent = 50;
            $discount = ((($cart->total-$cart->discount)/100)*50);
        }
    }else{
        $code_percent = 50;
        $discount = ((($cart->total-$cart->discount)/100)*50);    
    }
    
}else{
    

    if(!empty($_SESSION['code'])){
        
        
        
        // Get the code
        $rf = $db->prepare("SELECT * FROM discount_codes WHERE discount_code = ? AND redeemed != ?");
        $rf->execute(array($_SESSION['code'],1));
        
        if($rf->rowCount() > 0){
            
            $f = $rf->fetchObject();
            
            $minimum_spend = $f->min_spend;
            
            // Make sure we haven't used this code previously.
            $so = $db->prepare("SELECT * FROM orders WHERE customer_id = ? AND status = ? AND discount_code = ?");
            $so->execute(array($customer->id, "Paid", $f->discount_code));
            if($so->rowCount() > 0){
                $discount = 0;
                $code_percent = 0;
            }else{
                
                
                if($f->product_id > 0){
                    if($f->limited > 0){
                        $group = "GROUP BY c.id";
                        $limit = "LIMIT ".$f->limited;
                        $sl = $db->prepare("SELECT price AS total 
                                            FROM cart c 
                                            LEFT JOIN orders o ON c.order_id = o.id 
                                            WHERE o.status = ? AND c.sku_id = ? AND o.session_id = ? {$limit}");
                        //select sum(price) from (select items.price from items order by items.price desc limit 3) as subt;
                    }else{
                        $sl = $db->prepare("SELECT SUM(price) AS total FROM cart c LEFT JOIN orders o ON c.order_id = o.id WHERE o.status = ? AND c.sku_id = ? AND o.session_id = ?");
                    }
                    
                    // We are only product specific
                    //$sl = $db->prepare("SELECT SUM(price) AS total FROM cart c LEFT JOIN orders o ON c.order_id = o.id WHERE o.status = ? AND c.sku_id = ? AND (customer_id = ? OR o.session_id = ?) {$limit}");
                    $sl->execute(array("Unpaid", $f->product_id, session_id()));
                    
                    
                    //echo $counter." ".session_id()." ".$f->product_id;
                    
                    $l = $sl->fetchObject();
                    
                    
                    if(($f->min_spend > 0 && $cart->total > $f->min_spend) || $f->min_spend == 0){
                        if($f->percentage > 0){
                                $discount = round((($l->total/100)*$f->percentage),2);
                                $code_percent = $f->percentage;
                                $code_type = "percent";
                        }else{
                            $discount = $f->amount; 
                            $code_type = "amount";
                        }    
                    }
                    
                }elseif($f->range_id > 0){
                    
                    
                    //echo $customer->id;
                    // We are only product specific
                    if($f->limited > 0){
                        $group = "GROUP BY c.id";
                        $limit = "LIMIT ".$f->limited;
                    }
                    $sl = $db->prepare("SELECT SUM(price) AS total 
                                        FROM cart c 
                                        LEFT JOIN orders o ON c.order_id = o.id 
                                        LEFT JOIN skus s ON c.sku_id = s.id
                                        LEFT JOIN products p ON s.product_id = p.id
                                        WHERE o.status = ? AND p.range_id = ? AND o.session_id = ?
                                        {$group}
                                        {$limit}");
                    $sl->execute(array("Unpaid", $f->range_id, session_id()));
                    $l = $sl->fetchObject();
                    
                    if(($f->min_spend > 0 && $cart->total > $f->min_spend) || $f->min_spend == 0){
                        if($f->percentage > 0){
                                $discount = round((($l->total/100)*$f->percentage),2);
                                $code_percent = $f->percentage;
                                $code_type = "percent";
                        }else{
                            $discount = $f->amount; 
                            $code_type = "amount";
                        }    
                    }   
                }elseif(!empty($f->categories)){
                    $where = "AND (";
                    $bits = explode(",", $f->categories);
                    $bobs = array();
                    foreach($bits as $b){
                        $bobs[] = "p.category_id = '{$b}'";
                    }
                    $where .= implode(" OR ", $bobs);
                    $where .= ")";
                    $sl = $db->prepare("SELECT SUM(price) AS total 
                                        FROM cart c 
                                        LEFT JOIN orders o ON c.order_id = o.id 
                                        LEFT JOIN skus s ON c.sku_id = s.id
                                        LEFT JOIN products p ON s.product_id = p.id
                                        WHERE o.status = ? AND o.session_id = ? {$where}");
                    $sl->execute(array("Unpaid", session_id())); 
                    
                    $l = $sl->fetchObject();
                    if(($f->min_spend > 0 && $cart->total > $f->min_spend) || $f->min_spend == 0){
                        if($f->percentage > 0){
                                $discount = round((($l->total/100)*$f->percentage),2);
                                $code_percent = $f->percentage;
                                $code_type = "percent";
                        }else{
                            $discount = $f->amount; 
                            $code_type = "amount";
                        }    
                    }
                    
                }else{
                    if($f->new_customer == 1 && $logged_in == 2){
                        $discount = 0;
                        $code_percent = 0;
                    }else{
                        if(($f->min_spend > 0 && $cart->total > $f->min_spend) || $f->min_spend == 0){
                            if($f->percentage > 0){
                                    $discount = round((($cart->total/100)*$f->percentage),2);
                                    $code_percent = $f->percentage;
                                    $code_type = "percent";
                            }else{
                                $discount = $f->amount; 
                                $code_type = "amount";
                            }    
                        }      
                    }
                         
                    
                    
                }
                
            }
            
            
            
            
        }else{
           
            if($cart->qty > 0 && $logged_in != 2 && $company->order_discount > 0 && $_SESSION['code'] == $company->order_code){
                $discount = ((($cart->total-$cart->discount)/100)*$company->order_discount);
                $code_percent = $company->order_discount;
            }else{
                if($f->type_id == "First Order"){
                    // If they are logged in = have they ordered before?
                    $so = $db->prepare("SELECT * FROM orders WHERE customer_id = ? AND status = ?");
                    $so->execute(array($customer->id, "Paid"));
                    if($cart->qty > 0 && $logged_in == 2 && $company->order_discount > 0 && $so->rowCount() == 0){
                        $discount = ((($cart->total-$cart->discount)/100)*$company->order_discount);
                        $code_percent = $company->order_discount;
                    }    
                }
                

            }
        }
    }
}
    
// Delivery cost
if(($cart->total-$cart->discount-$discount) < $min_spend){
    $delivery_price = ($delivery_price + $ship_rate);
}
$delivery_price = ($delivery_price + $surcharge);

// Remove the shipping if the shipping discount code is effective
if($f->shipping_discount == 1 && ($l->total > 0 || (empty($f->categories) && empty($f->product_id) && empty($f->range_id)))){
    $delivery_price = 0;
}

?>