<?
include "configuration.php";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
    
require_once('stripe/init.php');
$stripe = new \Stripe\StripeClient($stripe_secret_key);

$st = $db->prepare("SELECT s.quantity AS qty, s.unit, s.id, p.title AS product, s.weight, stripe_product 
                    FROM skus s
                    LEFT JOIN products p ON s.product_id = p.id
                    WHERE p.title != ? AND s.status = ? AND p.status = ?");
$st->execute(array("", "Published", "Published"));

$su = $db->prepare("UPDATE skus SET stripe_product = ? WHERE id = ?");

while($l = $st->fetchObject()){
    
    if(empty($l->stripe_product)){
        $product = $stripe->products->create([
            'name' => "{$l->product}: {$l->qty} x {$l->weight}{$l->unit}"       
        ]);

        $su->execute(array($product->id, $l->id));
    }else{
        // Does the product exist?
        
        try {
            $prod = $stripe->products->retrieve(
                $l->stripe_product,
                []
            );    
        } catch (Exception $e){
            $product = $stripe->products->create([
                'name' => "{$l->product}: {$l->qty} x {$l->weight}{$l->unit}"       
            ]);

            $su->execute(array($product->id, $l->id));
        }
        
        
        
        
    }
    
}
?>
