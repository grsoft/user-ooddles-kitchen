<?
include "check-login.php";

require_once('stripe/init.php');
$stripe = new \Stripe\StripeClient($stripe_secret_key);

include "vendor/autoload.php";
// reference the Dompdf namespace
    use Dompdf\Dompdf;
    use Dompdf\Options;

switch($_GET['option']){
    case "paypal":
        require('classes/PaypalIPN.php'); 
        $ipn = new PaypalIPN();
        $verified = $ipn->verifyIPN();
        
        if ($verified){
            
            $payment_status = $_POST['payment_status'];
            switch($payment_status){
		        case "Completed":
                    $item_number = $_POST['custom'];   
                    
                    // Successful payment
                      $su = $db->prepare("UPDATE orders SET status = ?, ipn_received = ?, paypal_transaction = ? WHERE unique_id = ?");
                      $su->execute(array("Paid", date('Y-m-d H:i:s'), !empty($_POST['txn_id']) ? $_POST['txn_id'] : "", $item_number));
                    
                    // Is the unique cusatomer ID present?
                      $st = $db->prepare("SELECT c.unique_id, o.customer_id
                                          FROM orders o 
                                          LEFT JOIN customers c ON o.customer_id = c.id
                                          WHERE o.unique_id = ?");
                      $st->execute(array($item_number));
                      $order = $st->fetchObject();
                    
                      if(empty($order->unique_id)){
                            $date = new DateTime();
                            $hash =  $date->getTimestamp();
                            $su = $db->prepare("UPDATE customers SET unique_id = ? WHERE id = ?");
                            $su->execute(array($hash, $order->customer_id));
                      }
                    
                    // Get the order
                      $st = $db->prepare("SELECT o.id, c.name, c.surname, c.email, c.tmp, o.customer_id, o.unique_id, o.referrer, c.unique_id AS customer_code, o.total, o.discount_total, o.confirmation, c.stripe_id, o.shipping, c.tmp_password
                                          FROM orders o 
                                          LEFT JOIN customers c ON o.customer_id = c.id
                                          WHERE o.unique_id = ?");
                      $st->execute(array($item_number));
                      $order = $st->fetchObject();
                    
                    // Handle the transaction
                      $st = $db->prepare("SELECT *
                                          FROM deliveries
                                          WHERE order_id = ?");
                      $st->execute(array($order->id));
                      $delivery = $st->fetchObject();

                  // The delivery address
                      $delivery_address = $delivery->d_address.", ";
                      if(!empty($delivery->d_address2)){
                          $delivery_address .= $delivery->d_address2.", ";
                      }
                      $delivery_address .= $delivery->d_city.", ".$delivery->d_county." ".strtoupper($delivery->d_postcode);

                      if($delivery->d_country != "United Kingdom"){
                          $delivery_address .= " ".$delivery->d_country;
                      }
                    
                        include "classes/class.phpmailer.php";


                        // Select the pet
                        $sp = $db->prepare("SELECT name FROM cart s LEFT JOIN pets p ON s.pet_id = p.unique_id WHERE s.order_id = ? AND p.name != ?");
                        $sp->execute(array($order->id, ""));
                        $pet = $sp->fetchObject();

                        include "includes/mail-headers.php";



                        // Are they a new customer?
                        if(!empty($order->tmp_password)){
                            // Create a password and send them a welcome email
                            $secure = new SecureHash();

                            // Password
                            $date = new DateTime();
                            $hash =  $date->getTimestamp();
                            $password = $order->tmp_password;

                            // Salt
                            $salt = uniqid(mt_rand(), true);

                            // Encrypted password
                            $encrypted = $secure->create_hash($password, $salt);


                            $url = 'https://api-ssl.bitly.com/v4/shorten';
                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['long_url' => MAIN_SITE."/refer/".$order->customer_code]));
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                                "Authorization: Bearer ".$bitly,
                                "Content-Type: application/json"
                            ]);

                            $arr_result = json_decode(curl_exec($ch));



                            // Unset the tmp flag and set the password
                            $su = $db->prepare("UPDATE customers SET salt = ?, encryption = ?, tmp = ?, referral_code = ?, tmp_password = ? WHERE id = ?");
                            $su->execute(array($salt, $encrypted, 0, !empty($arr_result->link) ? $arr_result->link : "", "", $order->customer_id));


                            // The email

                            $mailer = $mail_header;

                            $mailer .= "<tr>
                                        <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: Arial, Gill Sans, Geneva, Museo Sans,  museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Welcome to Ooddles</td>
                                        </tr>
                                        <tr>
                                        <td style='padding:20px; background-color: #fff;'>
                                        <p style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;; font-size:12px; line-height:150%;'>Hello {$order->name}";
                            if(!empty($pet->name)){
                                $mailer .= " and {$pet->name}";
                            }
                            $mailer .= ",<br /><br />
                                        Welcome to Ooddles. <br /><br />

                                        To manage your account, please log into our O Hub, <a href='".MAIN_SITE."/o-hub' title='O Hub' style='color:#000; text-decoration:none; font-weight:bold;'>here</a> via the Ooddles website. Please use the email and password you set up.<br /><br />
                                        You can view your orders, track, update your details, add a dog and lots more. <br /><br />
                                        We hope you enjoy our refer a dog rewards programme, when you are logged in you will see your unique referral code, your friends will receive a discount and you can rack up your promo codes to use on future orders.<br /><br />
                                        We look forward to you sharing your dog Ooddling with us at <a href='{$company->facebook}' title='Facebook' style='color:#000; text-decoration:none; font-weight:bold;'>Facebook</a> or <a href='{$company->instagram}' title='Instagram' style='color:#000; text-decoration:none; font-weight:bold;'>Instagram</a> and do keep up to date with Oode and his antics.<br /><br />
                                        If you need any help, please contact customer services <a href='mailto:{$company->email}' title='Email' style='color:#000; text-decoration:none; font-weight:bold;'>{$company->email}</a>, or on live chat.<br /><br />

                                        Happy Ooddling.


                                        </p>
                                        </td>
                                        </tr>";

                            $mailer .= $mail_footer;


                            $email = new PHPMailer();
                            $email->From = $company->email;
                            $email->FromName = $company->name;

                            $email->Subject = "Welcome to Ooddles";
                            $email->Body = $mailer;
                            $email->IsHTML(true);

                            $email->AddAddress($order->email);
                            //$email->AddAddress("david.oldfield@brainstormdesign.co.uk");
                            $email->Send();

                            // If there is a referral - make the refferer a discount code.
                            if(!empty($order->referrer)){
                                // Make them a code.
                                $sh = $db->prepare("SELECT id FROM customers WHERE unique_id = ?");
                                $sh->execute(array($order->referrer));
                                if($sh->rowCount() > 0){
                                    $h = $sh->fetchObject();
                                    $discount_code = $hash.$h->id;

                                    $sq = $db->prepare("INSERT INTO discount_codes (discount_code, customer_id, percentage) VALUES (?,?,?)");
                                    $sq->execute(array($discount_code, $h->id, $company->referral_percent));

                                }
                            }



                        } // End of welcome email

                        // Order email
                        $mailer = $mail_header;
                        if(!empty($pet->name)){
                            $tag = $pet->name;
                        }else{
                            $tag = "your dog";
                        }
                        $mailer .= "<tr>
                                    <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: Arial, Gill Sans, Geneva, Museo Sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Your Ooddles order</td>
                                    </tr>
                                    <tr>
                                    <td style='padding:20px; background-color: #fff;'>
                                    <p style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; text-align:center;'>Hello ".ucwords(strtolower($order->name))."<br />
                                    Thank you for your order, please let {$tag} know it will be arriving shortly.<br />
                                    Your Order Number is {$order->unique_id}.</p>
                                    <table cellpadding='0' cellspacing='0' width='100%'>
                                    <tr>
                                    <th width='25%' style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>PRODUCT</th>
                                    <th width='50%' style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>DESCRIPTION</th>
                                    <th style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>QTY</th>
                                    <th style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>PRICE</th>
                                    </tr>";
                        $sd = $db->prepare("SELECT s.title, c.price, SUM(c.price) AS subtotal, COUNT(c.id) AS qty, p.title AS product_title, i.category, p.image, c.auto_repeat, s.quantity, s.weight, s.unit, c.discount, SUM(c.discount) AS overall_discount
                                            FROM cart c
                                            LEFT JOIN skus s ON c.sku_id = s.id
                                            LEFT JOIN products p ON s.product_id = p.id
                                            LEFT JOIN categories i ON p.category_id = i.id
                                            LEFT JOIN orders o ON c.order_id = o.id
                                            WHERE c.order_id = ?
                                            GROUP BY c.sku_id, c.auto_repeat");            
                        $sd->execute(array($order->id));


                        while($d = $sd->fetchObject()){
                            $mailer .= "<tr>
                                  <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'>";
                            if(!empty($d->image)){
                                $mailer .= "<img src='".MAIN_SITE."/thumb.php?src=/images/products/{$d->image}&w=80&h=80&zc=2' alt='{$d->title}' />";
                            }
                            $mailer .= "</td>
                                        <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'><strong>{$d->product_title}</strong><br />
                                        {$d->quantity} x {$d->weight}{$d->unit} {$d->title}";
                            if(!empty($d->auto_repeat)){
                                // Set the auto repeat flag
                                $rerun = 1;
                                $mailer .= "<br /><span style='font-style:italic;'>Auto Repeat {$d->auto_repeat}</span>";
                            }
                            $mailer.=  "</td>
                                        <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'>{$d->qty}</td>
                                        <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format(($d->subtotal-$d->overall_discount),2)."</td>
                                        </tr>";
                        }

                        $mailer .= "<tr>
                                    <td  colspan='2'></td>
                                    <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Subtotal</td>
                                    <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format($order->total,2)."</td>
                                    </tr>
                                    <tr>
                                    <td  colspan='2'></td>
                                    <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Discount</td>
                                    <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format($order->discount_total,2)."</td>
                                    </tr>
                                    <tr>
                                    <td  colspan='2'></td>
                                    <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Delivery</td>
                                    <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format($order->shipping,2)."</td>
                                    </tr>
                                    <tr>
                                    <td  colspan='2'></td>
                                    <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Total</td>
                                    <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format(($order->total-$order->discount_total+$order->shipping),2)."</td>
                                    </tr>
                                    </table>

                                    <p style='padding:10px 0px;  text-align:left;'><strong style='text-transform:uppercase;'>Delivery Details</strong><br />
                                    {$delivery_address}</p>
                                    <p style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; text-align:left;'>Please do log into your O Hub account, using your accounts details you created. <br />
                                    Any problems, please contact customer services at <a href='mailto:{$company->email}' title='Email {$company->name}' style='color:#000; text-decoration:none; font-weight:bold;'>{$company->email}</a> or jump onto live chat.<br /><br />
                                    Happy Ooddling<br />
                                    Ooddles Team</p>


                                    </td>
                                    </tr>";

                        $mailer .= $mail_footer;



                        $email = new PHPMailer();
                        $email->From = $company->email;
                        $email->FromName = $company->name;

                        $email->Subject = "Your Ooddling";
                        $email->Body = $mailer;
                        $email->IsHTML(true);

                        $email->AddAddress($order->email);

                        //$email->AddAddress("david.oldfield@brainstormdesign.co.uk");

                    // The invoice		
                        $html = file_get_contents(MAIN_SITE."/invoice.php?id=".$order->unique_id);

                        if(!empty($html)){
                            // instantiate and use the dompdf class
                            $options = new Options();
                            $options->setIsRemoteEnabled(true);
                            $options->setDefaultFont('Helvetica');

                            $dompdf = new Dompdf($options);


                            $dompdf->loadHtml($html);

                        // (Optional) Setup the paper size and orientation
                            $dompdf->setPaper('A4', 'portrait');

                        // Render the HTML as PDF
                            $dompdf->render();

                            $link = $order->unique_id."-invoice.pdf";

                            $output = $dompdf->output();

                            file_put_contents($link, $output);


                            // move the file
                            if(file_exists(ROOT.'/'.$link)){
                                rename(ROOT.'/'.$link, INVOICES.'/'.$link);    
                            }

                            $invoice = INVOICES.'/'.$link;
                        }
                        if(file_exists($invoice)){
                            $email->AddAttachment($invoice);
                        }

                        $email->Send();

                        // Update the order with confirmation sent.
                        $su = $db->prepare("UPDATE orders SET confirmation = ? WHERE unique_id = ?");
                        $su->execute(array(date("Y-m-d H:i:s"), $order->unique_id));

                        if($rerun == 1){
                            $sf = $db->prepare("SELECT SUM(price) AS auto_value, SUM(discount) AS auto_discount, pet_id, auto_repeat FROM cart WHERE auto_repeat != ? AND order_id = ? GROUP BY auto_repeat, pet_id");
                            $sf->execute(array("", $order->id));

                            // Prepare
                            $sw = $db->prepare("INSERT INTO auto_repeat (customer_id, pet_id, frequency, start_date, next_due, status, order_id, total) VALUES (?,?,?,?,?,?,?,?)");

                            $sl = $db->prepare("INSERT INTO auto_repeat_items (auto_id, sku_id, price, quantity) VALUES (?,?,?,?)");

                            $sg = $db->prepare("SELECT COUNT(id) AS qty, sku_id, price, discount FROM cart WHERE order_id = ? AND auto_repeat = ? AND pet_id = ? GROUP BY sku_id");

                            $sk = $db->prepare("SELECT s.id, s.stripe_product, s.product_code, s.title, p.title AS product, s.weight, s.quantity AS qty, s.unit FROM skus s LEFT JOIN products p ON s.product_id = p.id WHERE s.id = ?");


                            // Update skus with the Stripe Product ID
                            $su = $db->prepare("UPDATE skus SET stripe_product = ? WHERE id = ?");

                            // Update the subscription ID
                            $sq = $db->prepare("UPDATE auto_repeat SET subscription_id = ? WHERE id = ?");

                            while($f = $sf->fetchObject()){
                                if($f->auto_repeat == "bi-monthly"){
                                    $num = 2;
                                }else{
                                    $num = 1;
                                }
                                // Array
                                //$parts = array();

                                $sw->execute(array($order->customer_id, !empty($f->pet_id) ? $f->pet_id : "", $f->auto_repeat, date("Y-m-d"), date('Y-m-d', strtotime('+'.$num.' months')), "Active", $order->id, ($f->auto_value-$f->auto_discount)));
                                $auto_id = $db->lastInsertId();

                                $sg->execute(array($order->id, $f->auto_repeat, $f->pet_id));
                                while($g = $sg->fetchObject()){

                                    $sk->execute(array($g->sku_id));
                                    $k = $sk->fetchObject();

                                    if(empty($k->stripe_product)){
                                        // Create a product
                                        $product = $stripe->products->create([
                                            'name' => "{$k->product}: {$k->qty} x {$k->weight}{$k->unit}"       
                                        ]);

                                        $prod = $product->id;
                                        $su->execute(array($prod, $k->id));

                                    }


                                    $sl->execute(array($auto_id, $g->sku_id, ($g->price-$g->discount), $g->qty));
                                }


                            }

                            // Set up the subscription

                            $sa = $db->prepare("SELECT * FROM auto_repeat WHERE order_id = ?");
                            $sa->execute(array($order->id));

                            $sj = $db->prepare("SELECT a.id, s.stripe_product, a.sku_id, s.product_code, a.price, a.quantity, s.title, p.title AS product, s.weight, s.quantity AS qty, s.unit, a.next_due
                            FROM auto_repeat_items a 
                            LEFT JOIN skus s ON a.sku_id = s.id L
                            EFT JOIN products p ON s.product_id = p.id WHERE auto_id = ?");


                            while($a = $sa->fetchObject()){
                                if($a->auto_repeat == "bi-monthly"){
                                    $num = 2;
                                }else{
                                    $num = 1;
                                }

                                // Get our component parts
                                $parts = array();

                                $sj->execute(array($a->id));
                                while($j = $sj->fetchObject()){
                                    $parts[] = array('metadata' => array('sku' => $j->sku_id),
                                                      'price_data' => array(
                                                          'currency' => 'GBP',
                                                          'product' => $j->stripe_product,
                                                          'recurring' => array(
                                                              'interval' => 'month',
                                                              'interval_count' => $num
                                                          ),
                                                          'unit_amount' => round(($j->price*100))
                                                      ),
                                                      'quantity' => $j->quantity);
                                }
                                
                                $due = strtotime($a->next_due);
                                
                                $subs = $stripe->subscriptions->create([
                                  'customer' => $order->stripe_id,
                                  'items' => $parts,
                                  'off_session' => true,
                                  'collection_method' => 'charge_automatically',
                                  'billing_cycle_anchor' => $due,
                                  'trial_end' => $due

                                ]);

                                $su = $db->prepare("UPDATE auto_repeat SET subscription_id = ? WHERE id = ?");
                                $su->execute(array($subs->id, $a->id));

                                unset($parts);
                            }

                        }


                        unset($_SESSION['code']);
                        unset($_SESSION['account_password']);
                        unset($_SESSION['dog']);
                    
                    
                break;
            }
                    
        }
    break;
        
    default:
        // Stripe
        switch(STATE){
            case "DEV":
                $endpoint_secret = "whsec_8TStrTt77haftyy3SP4oc6bcRsbBbd1G";
            break;

            default:
                $endpoint_secret = "whsec_HbQyZPGXlpCzKdWaNYl9yFpBQpsc0pSs";  
            break;
        }



        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit();
        }

        $paymentIntent = $event->data->object; // contains a StripePaymentIntent

        // Handle the event
        switch ($event->type) {
            case 'payment_intent.processing':
                $su = $db->prepare("UPDATE orders SET ipn_received = ? WHERE intent_id = ?");
                $su->execute(array(date('Y-m-d H:i:s', $event->data->object->created), $event->data->object->id));
            break;

            case 'payment_intent.succeeded':
                $item = $paymentIntent->id;
                $status = $paymentIntent->charges->data[0]->paid;
                $order_id = $paymentIntent->charges->data[0]->metadata->order_id;
                //mail("david.oldfield@brainstormdesign.co.uk", "Stripe", $item." - ".$status." - ".$order_id);
                if($status == 1){
                    // Enure we haven't fired one off already

                    $st = $db->prepare("SELECT o.id, c.name, c.surname, c.email, c.tmp, o.customer_id, o.unique_id, o.referrer, c.unique_id AS customer_code, o.total, o.discount_total, o.confirmation, c.stripe_id, o.shipping, c.tmp_password
                                        FROM orders o 
                                        LEFT JOIN customers c ON o.customer_id = c.id
                                        WHERE o.intent_id = ? AND o.unique_id = ?");
                    $st->execute(array($item, $order_id));
                    if($st->rowCount() > 0){
                        $order = $st->fetchObject();
                        if($order->confirmation == "0000-00-00 00:00:00"){
                            try {
                                $stripe->customers->update(
                                  $order->stripe_id,
                                  ['invoice_settings' => array('default_payment_method' => $paymentIntent->charges->data[0]->payment_method)]
                                );


                            }catch (Exception $e) {
                                //mail("david.oldfield@brainstormdesign.co.uk", "Error", $e);
                            }
                            // Successful payment
                                $su = $db->prepare("UPDATE orders SET status = ?, ipn_received = ?, stripe_charge = ? WHERE intent_id = ? AND unique_id = ?");
                                $su->execute(array("Paid", date('Y-m-d H:i:s', $paymentIntent->charges->data[0]->created), $paymentIntent->charges->data[0]->id, $item, $order_id)); 


                            // Handle the transaction
                                $st = $db->prepare("SELECT *
                                                    FROM deliveries
                                                    WHERE order_id = ?");
                                $st->execute(array($order->id));
                                $delivery = $st->fetchObject();

                            // The delivery address
                                $delivery_address = $delivery->d_address.", ";
                                if(!empty($delivery->d_address2)){
                                    $delivery_address .= $delivery->d_address2.", ";
                                }
                                $delivery_address .= $delivery->d_city.", ".$delivery->d_county." ".strtoupper($delivery->d_postcode);

                                if($delivery->d_country != "United Kingdom"){
                                    $delivery_address .= " ".$delivery->d_country;
                                }

                                include "classes/class.phpmailer.php";


                                // Select the pet
                                $sp = $db->prepare("SELECT name FROM cart s LEFT JOIN pets p ON s.pet_id = p.unique_id WHERE s.order_id = ? AND p.name != ?");
                                $sp->execute(array($order->id, ""));
                                $pet = $sp->fetchObject();

                                include "includes/mail-headers.php";



                                // Are they a new customer?
                                if(!empty($order->tmp_password)){
                                    // Create a password and send them a welcome email
                                    $secure = new SecureHash();

                                    // Password
                                    $date = new DateTime();
                                    $hash =  $date->getTimestamp();
                                    $password = $order->tmp_password;

                                    // Salt
                                    $salt = uniqid(mt_rand(), true);

                                    // Encrypted password
                                    $encrypted = $secure->create_hash($password, $salt);


                                    $url = 'https://api-ssl.bitly.com/v4/shorten';
                                    $ch = curl_init($url);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['long_url' => MAIN_SITE."/refer/".$order->customer_code]));
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, [
                                        "Authorization: Bearer ".$bitly,
                                        "Content-Type: application/json"
                                    ]);

                                    $arr_result = json_decode(curl_exec($ch));



                                    // Unset the tmp flag and set the password
                                    $su = $db->prepare("UPDATE customers SET salt = ?, encryption = ?, tmp = ?, referral_code = ?, tmp_password = ? WHERE id = ?");
                                    $su->execute(array($salt, $encrypted, 0, !empty($arr_result->link) ? $arr_result->link : "", "", $order->customer_id));


                                    // The email

                                    $mailer = $mail_header;

                                    $mailer .= "<tr>
                                                <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: Arial, Gill Sans, Geneva, Museo Sans,  museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Welcome to Ooddles</td>
                                                </tr>
                                                <tr>
                                                <td style='padding:20px; background-color: #fff;'>
                                                <p style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;; font-size:12px; line-height:150%;'>Hello {$order->name}";
                                    if(!empty($pet->name)){
                                        $mailer .= " and {$pet->name}";
                                    }
                                    $mailer .= ",<br /><br />
                                                Welcome to Ooddles. <br /><br />

                                                To manage your account, please log into our O Hub, <a href='".MAIN_SITE."/o-hub' title='O Hub' style='color:#000; text-decoration:none; font-weight:bold;'>here</a> via the Ooddles website. Please use the email and password you set up.<br /><br />
                                                You can view your orders, track, update your details, add a dog and lots more. <br /><br />
                                                We hope you enjoy our refer a dog rewards programme, when you are logged in you will see your unique referral code, your friends will receive a discount and you can rack up your promo codes to use on future orders.<br /><br />
                                                We look forward to you sharing your dog Ooddling with us at <a href='{$company->facebook}' title='Facebook' style='color:#000; text-decoration:none; font-weight:bold;'>Facebook</a> or <a href='{$company->instagram}' title='Instagram' style='color:#000; text-decoration:none; font-weight:bold;'>Instagram</a> and do keep up to date with Oode and his antics.<br /><br />
                                                If you need any help, please contact customer services <a href='mailto:{$company->email}' title='Email' style='color:#000; text-decoration:none; font-weight:bold;'>{$company->email}</a>, or on live chat.<br /><br />

                                                Happy Ooddling.


                                                </p>
                                                </td>
                                                </tr>";

                                    $mailer .= $mail_footer;


                                    $email = new PHPMailer();
                                    $email->From = $company->email;
                                    $email->FromName = $company->name;

                                    $email->Subject = "Welcome to Ooddles";
                                    $email->Body = $mailer;
                                    $email->IsHTML(true);

                                    $email->AddAddress($order->email);
                                    //$email->AddAddress("david.oldfield@brainstormdesign.co.uk");
                                    $email->Send();

                                    // If there is a referral - make the refferer a discount code.
                                    if(!empty($order->referrer)){
                                        // Make them a code.
                                        $sh = $db->prepare("SELECT id FROM customers WHERE unique_id = ?");
                                        $sh->execute(array($order->referrer));
                                        if($sh->rowCount() > 0){
                                            $h = $sh->fetchObject();
                                            $discount_code = $hash.$h->id;

                                            $sq = $db->prepare("INSERT INTO discount_codes (discount_code, customer_id, percentage) VALUES (?,?,?)");
                                            $sq->execute(array($discount_code, $h->id, $company->referral_percent));

                                        }
                                    }



                                } // End of welcome email

                                // Order email
                                $mailer = $mail_header;
                                if(!empty($pet->name)){
                                    $tag = $pet->name;
                                }else{
                                    $tag = "your dog";
                                }
                                $mailer .= "<tr>
                                            <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: Arial, Gill Sans, Geneva, Museo Sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Your Ooddles order</td>
                                            </tr>
                                            <tr>
                                            <td style='padding:20px; background-color: #fff;'>
                                            <p style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; text-align:center;'>Hello ".ucwords(strtolower($order->name))."<br />
                                            Thank you for your order, please let {$tag} know it will be arriving shortly.<br />
                                            Your Order Number is {$order->unique_id}.</p>
                                            <table cellpadding='0' cellspacing='0' width='100%'>
                                            <tr>
                                            <th width='25%' style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>PRODUCT</th>
                                            <th width='50%' style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>DESCRIPTION</th>
                                            <th style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>QTY</th>
                                            <th style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>PRICE</th>
                                            </tr>";
                                $sd = $db->prepare("SELECT s.title, c.price, SUM(c.price) AS subtotal, COUNT(c.id) AS qty, p.title AS product_title, i.category, p.image, c.auto_repeat, s.quantity, s.weight, s.unit, c.discount, SUM(c.discount) AS overall_discount
                                                    FROM cart c
                                                    LEFT JOIN skus s ON c.sku_id = s.id
                                                    LEFT JOIN products p ON s.product_id = p.id
                                                    LEFT JOIN categories i ON p.category_id = i.id
                                                    LEFT JOIN orders o ON c.order_id = o.id
                                                    WHERE c.order_id = ?
                                                    GROUP BY c.sku_id, c.auto_repeat");            
                                $sd->execute(array($order->id));


                                while($d = $sd->fetchObject()){
                                    $mailer .= "<tr>
                                          <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'>";
                                    if(!empty($d->image)){
                                        $mailer .= "<img src='".MAIN_SITE."/thumb.php?src=/images/products/{$d->image}&w=80&h=80&zc=2' alt='{$d->title}' />";
                                    }
                                    $mailer .= "</td>
                                                <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'><strong>{$d->product_title}</strong><br />
                                                {$d->quantity} x {$d->weight}{$d->unit} {$d->title}";
                                    if(!empty($d->auto_repeat)){
                                        // Set the auto repeat flag
                                        $rerun = 1;
                                        $mailer .= "<br /><span style='font-style:italic;'>Auto Repeat {$d->auto_repeat}</span>";
                                    }
                                    $mailer.=  "</td>
                                                <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'>{$d->qty}</td>
                                                <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format(($d->subtotal-$d->overall_discount),2)."</td>
                                                </tr>";
                                }

                                $mailer .= "<tr>
                                            <td  colspan='2'></td>
                                            <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Subtotal</td>
                                            <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format($order->total,2)."</td>
                                            </tr>
                                            <tr>
                                            <td  colspan='2'></td>
                                            <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Discount</td>
                                            <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format($order->discount_total,2)."</td>
                                            </tr>
                                            <tr>
                                            <td  colspan='2'></td>
                                            <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Delivery</td>
                                            <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format($order->shipping,2)."</td>
                                            </tr>
                                            <tr>
                                            <td  colspan='2'></td>
                                            <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Total</td>
                                            <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format(($order->total-$order->discount_total+$order->shipping),2)."</td>
                                            </tr>
                                            </table>

                                            <p style='padding:10px 0px;  text-align:left;'><strong style='text-transform:uppercase;'>Delivery Details</strong><br />
                                            {$delivery_address}</p>
                                            <p style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; text-align:left;'>Please do log into your O Hub account, using your accounts details you created. <br />
                                            Any problems, please contact customer services at <a href='mailto:{$company->email}' title='Email {$company->name}' style='color:#000; text-decoration:none; font-weight:bold;'>{$company->email}</a> or jump onto live chat.<br /><br />
                                            Happy Ooddling<br />
                                            Ooddles Team</p>


                                            </td>
                                            </tr>";

                                $mailer .= $mail_footer;



                                $email = new PHPMailer();
                                $email->From = $company->email;
                                $email->FromName = $company->name;

                                $email->Subject = "Your Ooddling";
                                $email->Body = $mailer;
                                $email->IsHTML(true);

                                $email->AddAddress($order->email);

                                //$email->AddAddress("david.oldfield@brainstormdesign.co.uk");

                            // The invoice		
                                $html = file_get_contents(MAIN_SITE."/invoice.php?id=".$order->unique_id);

                                if(!empty($html)){
                                    // instantiate and use the dompdf class
                                    $options = new Options();
                                    $options->setIsRemoteEnabled(true);
                                    $options->setDefaultFont('Helvetica');

                                    $dompdf = new Dompdf($options);


                                    $dompdf->loadHtml($html);

                                // (Optional) Setup the paper size and orientation
                                    $dompdf->setPaper('A4', 'portrait');

                                // Render the HTML as PDF
                                    $dompdf->render();

                                    $link = $order->unique_id."-invoice.pdf";

                                    $output = $dompdf->output();

                                    file_put_contents($link, $output);


                                    // move the file
                                    if(file_exists(ROOT.'/'.$link)){
                                        rename(ROOT.'/'.$link, INVOICES.'/'.$link);    
                                    }

                                    $invoice = INVOICES.'/'.$link;
                                }
                                if(file_exists($invoice)){
                                    $email->AddAttachment($invoice);
                                }

                                $email->Send();

                                // Update the order with confirmation sent.
                                $su = $db->prepare("UPDATE orders SET confirmation = ? WHERE unique_id = ?");
                                $su->execute(array(date("Y-m-d H:i:s"), $order->unique_id));

                                if($rerun == 1){
                                    $sf = $db->prepare("SELECT SUM(price) AS auto_value, SUM(discount) AS auto_discount, pet_id, auto_repeat FROM cart WHERE auto_repeat != ? AND order_id = ? GROUP BY auto_repeat, pet_id");
                                    $sf->execute(array("", $order->id));

                                    // Prepare
                                    $sw = $db->prepare("INSERT INTO auto_repeat (customer_id, pet_id, frequency, start_date, next_due, status, order_id, total) VALUES (?,?,?,?,?,?,?,?)");

                                    $sl = $db->prepare("INSERT INTO auto_repeat_items (auto_id, sku_id, price, quantity) VALUES (?,?,?,?)");

                                    $sg = $db->prepare("SELECT COUNT(id) AS qty, sku_id, price, discount FROM cart WHERE order_id = ? AND auto_repeat = ? AND pet_id = ? GROUP BY sku_id");

                                    $sk = $db->prepare("SELECT s.id, s.stripe_product, s.product_code, s.title, p.title AS product, s.weight, s.quantity AS qty, s.unit FROM skus s LEFT JOIN products p ON s.product_id = p.id WHERE s.id = ?");


                                    // Update skus with the Stripe Product ID
                                    $su = $db->prepare("UPDATE skus SET stripe_product = ? WHERE id = ?");

                                    // Update the subscription ID
                                    $sq = $db->prepare("UPDATE auto_repeat SET subscription_id = ? WHERE id = ?");

                                    while($f = $sf->fetchObject()){
                                        if($f->auto_repeat == "bi-monthly"){
                                            $num = 2;
                                        }else{
                                            $num = 1;
                                        }
                                        // Array
                                        //$parts = array();

                                        $sw->execute(array($order->customer_id, !empty($f->pet_id) ? $f->pet_id : "", $f->auto_repeat, date("Y-m-d"), date('Y-m-d', strtotime('+'.$num.' months')), "Active", $order->id, ($f->auto_value-$f->auto_discount)));
                                        $auto_id = $db->lastInsertId();

                                        $sg->execute(array($order->id, $f->auto_repeat, $f->pet_id));
                                        while($g = $sg->fetchObject()){

                                            $sk->execute(array($g->sku_id));
                                            $k = $sk->fetchObject();

                                            if(empty($k->stripe_product)){
                                                // Create a product
                                                $product = $stripe->products->create([
                                                    'name' => "{$k->product}: {$k->qty} x {$k->weight}{$k->unit}"       
                                                ]);

                                                $prod = $product->id;
                                                $su->execute(array($prod, $k->id));

                                            }


                                            $sl->execute(array($auto_id, $g->sku_id, ($g->price-$g->discount), $g->qty));
                                        }


                                    }

                                    // Set up the subscription

                                    $sa = $db->prepare("SELECT * FROM auto_repeat WHERE order_id = ?");
                                    $sa->execute(array($order->id));

                                    $sj = $db->prepare("SELECT a.id, s.stripe_product, a.sku_id, s.product_code, a.price, a.quantity, s.title, p.title AS product, s.weight, s.quantity AS qty, s.unit FROM auto_repeat_items a LEFT JOIN skus s ON a.sku_id = s.id LEFT JOIN products p ON s.product_id = p.id WHERE auto_id = ?");


                                    while($a = $sa->fetchObject()){
                                        if($a->auto_repeat == "bi-monthly"){
                                            $num = 2;
                                        }else{
                                            $num = 1;
                                        }

                                        // Get our component parts
                                        $parts = array();

                                        $sj->execute(array($a->id));
                                        while($j = $sj->fetchObject()){
                                            $parts[] = array('metadata' => array('sku' => $j->sku_id),
                                                              'price_data' => array(
                                                                  'currency' => 'GBP',
                                                                  'product' => $j->stripe_product,
                                                                  'recurring' => array(
                                                                      'interval' => 'month',
                                                                      'interval_count' => $num
                                                                  ),
                                                                  'unit_amount' => round(($j->price*100))
                                                              ),
                                                              'quantity' => $j->quantity);
                                        }

                                        $subs = $stripe->subscriptions->create([
                                          'customer' => $order->stripe_id,
                                          'items' => $parts,
                                          'off_session' => true,
                                          'collection_method' => 'charge_automatically'    

                                        ]);

                                        $su = $db->prepare("UPDATE auto_repeat SET subscription_id = ? WHERE id = ?");
                                        $su->execute(array($subs->id, $a->id));

                                        unset($parts);
                                    }

                                }


                                unset($_SESSION['code']);
                                unset($_SESSION['account_password']);
                                unset($_SESSION['dog']);




                        }
                    }


                }

            break;

            case 'payment_intent.payment_failed':
                $su = $db->prepare("UPDATE orders SET status = ?, ipn_received = ? WHERE intent_id = ?");
                $su->execute(array("Failed", date('Y-m-d H:i:s', $event->data->object->created), $event->data->object->id));   
            break;

        }

        http_response_code(200);
        
    break;
}

?>