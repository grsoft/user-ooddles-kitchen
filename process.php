<? include "header.php"; ?>
    <div id="container">
        
        <img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?>" class="logo" />
        <section class="process">
            <div class="percentage">
                <span><? echo round($percent); ?>%</span>
                <div class="bar"><span></span></div>
                
            </div>
            
            <div id="opts">
                <p>Email: <? echo $_SESSION['email']; ?><br />
                    Mailing: <? echo $_SESSION['mailing']; ?><br />
                    Name: <? echo $_SESSION['name']; ?><br />
                    Breed: <? echo $_SESSION['breed']; ?><br />
                    Type: <? echo $_SESSION['type']; ?><br />
                    Size: <? echo $_SESSION['size']; ?><br />
                    Weight: <? echo $_SESSION['weight']; ?><br />
                    Condition: <? echo $_SESSION['condition']; ?><br />
                    Age Years: <? echo $_SESSION['years']; ?><br />
                    Age Months: <? echo $_SESSION['months']; ?><br />
                    Birth Day: <? echo $_SESSION['birth_day']; ?><br />
                    Birth Month: <? echo $_SESSION['birth_month']; ?><br />
                    Gender: <? echo $_SESSION['gender']; ?><br />
                    Spayed: <? echo $_SESSION['spayed']; ?><br />
                    Pregnant: <? echo $_SESSION['pregnant']; ?><br />
                    Neutered: <? echo $_SESSION['neutered']; ?><br />
                    Health: <? echo $_SESSION['health']; ?><br />
                    Dislike Ingredients: <? echo $_SESSION['ingredients']; ?><br />
                    Ingredients: <? echo $_SESSION['ingredients_list']; ?><br />
                    Food: <? echo $_SESSION['food']; ?><br />
                    
                    </p>
                
            </div><!--close opts-->
            
            
            <form name="ooddling" method="post" action="/actions.php?action=process&stage=<? echo $stage; ?>">
                <input name="url" type="hidden" value="<? echo $_SERVER['REQUEST_URI']; ?>" />
                <input name="mailing" type="hidden" value="<? echo $_SESSION['mailing']; ?>" />
                <input name="stage" type="hidden" value="<? echo $_GET['stage']; ?>" />
               
                
                <?
                switch($_GET['stage']){
                    
                    case 2:
                        ?>
                        <h1>Tell us about your dog</h1>
                        <input name="name" type="text" placeholder="Name" value="<? echo $_SESSION['name']; ?>" />
                        <input name="type" type="hidden" value="<? echo $_SESSION['type']; ?>" />
                        <input name="breed" type="hidden" value="<? echo $_SESSION['breed']; ?>" />
                
                        <div class="flex justify">
                            <div class="c_100"><a href="#" title="Pedigree" data-id="Pedigree" data-target="type" class="btn trigger <? if($_SESSION['type'] == "Pedigree"){ echo "active"; } ?>">Pedigree</a>
                            <input name="breeds" type="text" placeholder="Begin typing and select from the list" id="autocomplete" class="breeds" value="<? echo $_SESSION['breed']; ?>" />
                            </div>
                            <div class="c_100"><a href="#" title="Crossbreed" data-id="Crossbreed" data-target="type" class="btn trigger <? if($_SESSION['type'] == "Crossbreed"){ echo "active"; } ?>">Crossbreed</a>
                            <input name="crossbreed" type="text" placeholder="Begin typing your breed" id="autocomplete" class="crossbreeds" value="<? echo $_SESSION['breed']; ?>" />
                            </div>
                            <div class="c_100"><a href="#" title="Not Sure" data-id="Not Sure" data-target="type" class="btn trigger <? if($_SESSION['type'] == "Not Sure"){ echo "active"; } ?>">Not Sure</a></div>
                         </div><!--close justify-->
                        
                        <? include "includes/buttons.php";
                        
                    break;
                        
                    case 3:
                        ?>
                        <h1>How big is <? echo $_SESSION['name']; ?></h1>
                        <input name="size" type="hidden" value="<? echo $_SESSION['size']; ?>" />
                
                        <div class="flex justify sizes">
                            <?
                                $ss = $db->prepare("SELECT * FROM breed_sizes");
                                $ss->execute();
                                while($s = $ss->fetchObject()){
                                    ?>
                                    <div class="c_33"><p><img src="/images/<? echo $s->image; ?>" alt="<? echo $s->title; ?>" /></p><a href="#" title="<? echo $s->title; ?>" data-id="<? echo $s->title; ?>" data-target="size" class="btn trigger <? if($_SESSION['size'] == $s->title){ echo "active"; } ?>"><? echo $s->title; ?> <span>- <? echo $s->description; ?></span></a></div>
                                    <?
                                }
                            ?>
                        </div><!--close justify-->
                        <? include "includes/buttons.php";
                        
                    break;
                        
                    case 4:
                        ?>
                        <h1>How much does <? echo $_SESSION['name']; ?> weigh?</h1>
                <p>If <? echo $_SESSION['name']; ?> is a puppy, please put their expected adult weight.</p>
                        <input name="weight" type="number" placeholder="Kg" value="<? echo $_SESSION['weight']; ?>" step=".01" onkeypress="return is_number(event)" />
                <p>or</p>
                <input name="stones" type="number" placeholder="Stones" value="<? echo $_SESSION['stones']; ?>" step="1" onkeypress="return is_number(event)" /><input name="lbs" type="number" placeholder="lbs" value="<? echo $_SESSION['lbs']; ?>" step="1" max="14" onkeypress="return is_number(event)" />
                <p><span class="bold">Not Sure?</span><br />
                    <strong>Top Tip.</strong>  Jump on your scales at home without your dog, then back on holding your dog, and the difference is the weight of your dog. Always keep <? echo $_SESSION['name']; ?>'s size and weight updated at your O Hub.</p>
                        <? include "includes/buttons.php";       
                    break;
                        
                    case 5:
                        ?>
                        <h1>Describe <? echo $_SESSION['name']; ?>'s body condition?</h1>
                        <input name="condition" type="hidden" value="<? echo $_SESSION['condition']; ?>" />
                        <div class="flex justify sizes">
                            <?
                                $ss = $db->prepare("SELECT * FROM body_sizes");
                                $ss->execute();
                                while($s = $ss->fetchObject()){
                                    ?>
                                    <div class="c_33"><p><img src="/images/<? echo $s->image; ?>" alt="<? echo $s->title; ?>" /></p><a href="#" title="<? echo $s->title; ?>" data-id="<? echo $s->title; ?>" data-target="condition" class="btn trigger <? if($_SESSION['condition'] == $s->title){ echo "active"; } ?>"><? echo $s->title; ?></a><p><? echo $s->description; ?></p></div>
                                    <?
                                }
                            ?>
                        </div><!--close justify-->
                        <? include "includes/buttons.php";
                    break;
                        
                    case 6:
                        ?>
                        <input name="years" type="hidden" value="<? echo $_SESSION['years']; ?>" />
                        <input name="months" type="hidden" value="<? echo $_SESSION['months']; ?>" />
                        <input name="birth_day" type="hidden" value="<? echo $_SESSION['birth_day']; ?>" />
                        <input name="birth_month" type="hidden" value="<? echo $_SESSION['birth_month']; ?>" />
                        <h1>How old is <? echo $_SESSION['name']; ?>?</h1>
                        
                        <select name="years_range" data-id="years">
                            <option value="">Years</option>
                            <?
                            $years = range(0,25);
                            foreach($years as $y){
                                echo "<option value='{$y}'";
                                if($_SESSION['years'] == $y){ echo "selected"; }
                                echo ">{$y}</option>";
                            }
                            ?>
                        </select>
                        <select name="month_range" data-id="months">
                            <option value="">Months</option>
                            <?
                            $months = range(0,11);
                            foreach($months as $y){
                                echo "<option value='{$y}'";
                                if($_SESSION['months'] == $y){ echo "selected"; }
                                echo ">{$y}</option>";
                            }
                            ?>
                        </select><br /><br />
                        <h2>When is <? echo $_SESSION['name']; ?>'s birthday?</h2>
                <p>Tails Up – If you add <? echo $_SESSION['name']; ?>'s Birthday, we shall send <? echo $_SESSION['name']; ?> a birthday pressie.</p>
                        <select name="birth_day" data-id="birth_day">
                            <option value="">Day</option>
                            <?
                            foreach($birth_days as $y){
                                echo "<option value='{$y}'";
                                if($_SESSION['birth_day'] == $y){ echo "selected"; }
                                echo ">{$y}</option>";
                            }
                            ?>
                        </select>
                
                        <select name="birth_month" data-id="birth_month">
                            <option value="">Month</option>
                            <?
                            foreach($birth_months as $y){
                                echo "<option value='{$y}'";
                                if($_SESSION['birth_month'] == $y){ echo "selected"; }
                                echo ">{$y}</option>";
                            }
                            ?>
                        </select>
                        <? include "includes/buttons.php";
                    break;
                        
                    case 7:
                        ?>
                         <h1>Tell us a bit more about <? echo $_SESSION['name']; ?></h1>
                        <input name="gender" type="hidden" value="<? echo $_SESSION['gender']; ?>" />
                        <input name="spayed" type="hidden" value="<? echo $_SESSION['spayed']; ?>" />
                        <input name="pregnant" type="hidden" value="<? echo $_SESSION['pregnant']; ?>" />
                        <input name="neutered" type="hidden" value="<? echo $_SESSION['neutered']; ?>" />
                        <label>Is <? echo $_SESSION['name']; ?> Male or Female?</label>
                        <div class="flex justify">
                            <?
                                
                                foreach($genders as $g){
                                    ?>
                                    <div class="c_20"><a href="#" title="<? echo $g; ?>" data-id="<? echo $g; ?>" data-target="gender" class="btn trigger <? if($_SESSION['gender'] == $g){ echo "active"; } ?>"><? echo $g; ?></a></div>
                                    <?
                                }
                            ?>
                        </div><!--close justify-->
                        
                        <div class="inner_opt female">
                            <label>and is she spayed?</label>
                            
                            <div class="flex justify">
                            <?
                                foreach($answers as $g){
                                    ?>
                                    <div class="c_20"><a href="#" title="<? echo $g; ?>" data-id="<? echo $g; ?>" data-target="spayed" class="btn trigger <? if($_SESSION['spayed'] == $g){ echo "active"; } ?>"><? echo $g; ?></a></div>
                                    <?
                                }
                            ?>
                        </div><!--close justify-->
                            <label>Is <? echo $_SESSION['name']; ?> pregnant/nursing?</label>
                            
                            <div class="flex justify">
                            <?
                                foreach($answers as $g){
                                    ?>
                                    <div class="c_20"><a href="#" title="<? echo $g; ?>" data-id="<? echo $g; ?>" data-target="pregnant" class="btn trigger <? if($_SESSION['pregnant'] == $g){ echo "active"; } ?>"><? echo $g; ?></a></div>
                                    <?
                                }
                            ?>
                        </div><!--close justify-->
                            
                        </div><!--close female-->
                        <div class="inner_opt male">
                            <label>and has he been neutered?</label>
                            
                            <div class="flex justify">
                            <?
                                foreach($answers as $g){
                                    ?>
                                    <div class="c_20"><a href="#" title="<? echo $g; ?>" data-id="<? echo $g; ?>" data-target="neutered" class="btn trigger <? if($_SESSION['neutered'] == $g){ echo "active"; } ?>"><? echo $g; ?></a></div>
                                    <?
                                }
                            ?>
                        </div><!--close justify-->
                            
                        </div><!--close male-->
                        <div id="note">
                                <p>Thanks for letting us know <? echo $_SESSION['name']; ?> is <span class="opt_select"></span>.<br />
                                 You may need to keep an eye on <? echo $_SESSION['name']; ?>. If <? echo $_SESSION['name']; ?> is gaining weight, it is common after <span class="opt_select_past"></span>. If you feel <? echo $_SESSION['name']; ?> is putting on weight just reduce your daily food slightly or increase activity levels.  Remember the perfect weight for your dog is described as good condition, some fat cover but ribs easy to feel.</p>
                        </div><!--close note-->
                    
                
                        <? include "includes/buttons.php";
                    break;
                        
                    case 8:
                        ?>
                        <h1>Does <? echo $_SESSION['name']; ?> have any health issues or niggles?</h1>
                        <p>Select all which apply to <? echo $_SESSION['name']; ?>. If none apply, click next.</p>
                        <input name="health" type="hidden" value="<? echo $_SESSION['health']; ?>" />
                <?
                        if(!empty($_SESSION['health'])){
                            $health_bits = explode(",", $_SESSION['health']);
                        }
                        ?>
                         <div class="flex justify">
                             <?
                                $sf = $db->prepare("SELECT * FROM health");
                                $sf->execute();
                                while($f = $sf->fetchObject()){
                                    ?>
                                 <div class="c_33"><a href="#" title="<? echo $f->title; ?>" data-id="<? echo $f->title; ?>" data-target="health" data-tag="list"  data-filter="true" class="btn trigger <? if(in_array($f->title, $health_bits)){ echo "active"; } ?>"><? echo $f->title; ?></a><p><? echo $f->description; ?></p></div>
                                 <?
                                }
                                $sn = $db->prepare("SELECT * FROM niggles");
                                $sn->execute();
                                while($n = $sn->fetchObject()){
                                    ?>
                                 <div class="c_25"><a href="#" title="<? echo $f->niggle; ?>" data-id="<? echo $n->niggle; ?>" data-target="health" data-tag="list"  data-filter="true" class="btn trigger <? if(in_array($n->niggle, $health_bits)){ echo "active"; } ?>"><? echo $n->niggle; ?></a></div>
                                 <?
                                }
                                ?>
                            
                             
                        </div><!--close justify-->
                
                        <? include "includes/buttons.php";
                    break;
                        
                    case 9:
                        ?>
                        <h1>Ingredients <? echo $_SESSION['name']; ?> doesn't like?</h1>
                        <input name="ingredients" type="hidden" value="<? echo $_SESSION['ingredients']; ?>" />
                        <input name="ingredients_list" type="hidden" />
                
                         <div class="flex justify">
                                    <?
                                        foreach($answers as $g){
                                            ?>
                                            <div class="c_20"><a href="#" title="<? echo $g; ?>" data-id="<? echo $g; ?>" data-target="ingredients" data-tag="list" class="btn trigger <? if($_SESSION['ingredients'] == $g){ echo "active"; } ?>"><? echo $g; ?></a></div>
                                            <?
                                        }
                                    ?>
                                </div><!--close justify-->
                        <p>It's great news, all our recipes are Grain-Free and Hypoallergenic.</p>

                        <div class="inner_opt list">
                            <p>Select all which apply to <? echo $_SESSION['name']; ?>...</p>
                            <div class="flex justify">
                                    <?
                                        $ss = $db->prepare("SELECT * FROM ingredients");
                                        $ss->execute();
                                        while($s = $ss->fetchObject()){
                                            ?>
                                            <div class="c_25"><a href="#" title="<? echo $s->ingredient; ?>" data-id="<? echo $s->ingredient; ?>" data-target="ingredients_list" data-filter="true" class="btn trigger <? if($_SESSION['ingredients_list'] == $s->ingredient){ echo "active"; } ?>"><? echo $s->ingredient; ?></a></div>
                                            <?
                                        }
                                    ?>
                                </div><!--close justify-->

                        </div>
                
                
                        <? include "includes/buttons.php";
                    break;
                        
                    case 10:
                        ?>
                        <h1>How does <? echo $_SESSION['name']; ?> like <? if($_SESSION['gender'] == "Female"){ echo "her"; }else{ echo "his"; } ?> food?</h1>
                        <p>Select all which apply...</p>
                        <input name="food" type="hidden" value="<? echo $_SESSION['food']; ?>" />
                        <div class="flex justify food">
                                    <?
                                        $sc = $db->prepare("SELECT * FROM categories WHERE status = ? AND (id = ? OR id = ? OR id = ?)");
                                        $sc->execute(array("Published",1,2,5));
                                        $cats = explode(",", $_SESSION['food']);
                        
                                        while($c = $sc->fetchObject()){
                                        //foreach($food_types as $t => $k){
                                        
                                            ?>
                                            <div class="c_33">
                                                <!--<p><img src="/images/<? //echo $k[1]; ?>" alt="<? //echo $k[0]; ?>" /></p>--><a href="#" title="<? echo $c->category; ?>" data-id="<? echo $c->id; ?>" data-target="food" data-filter="true" class="btn trigger <? if(in_array($c->id,$cats)){ echo "active"; } ?>"><? echo $c->category; ?></a></div>
                                            <?
                                        }
                                    ?>
                                            
                                            <div class="c_33">
                                                <!--<p><img src="/images/<? //echo $k[1]; ?>" alt="<? //echo $k[0]; ?>" /></p>--><a href="#" title="Wet and Dry Mixed" data-id="mixed" data-target="food" data-filter="true" class="btn trigger <? if($_SESSION['food'] == "mixed"){ echo "active"; } ?>">Wet and Dry Mixed</a></div>
                                            <div class="c_33">
                                                <!--<p><img src="/images/<? //echo $k[1]; ?>" alt="<? //echo $k[0]; ?>" /></p>--><a href="#" title="Not Botherered" data-id="all" data-target="food" data-filter="true" class="btn trigger <? if($_SESSION['food'] == "all"){ echo "active"; } ?>">Not Bothered</a></div>
                                </div><!--close justify-->
                    <? include "includes/buttons.php";
                    break;
                        
                    //include "includes/stage-11.php";
                        
                    case 11:
                        ?>
                        <h1>Finally, how active is <? echo $_SESSION['name']; ?>?</h1>
                       <input name="activity" type="hidden" value="<? echo $_SESSION['activity']; ?>" />
                        
                        <div class="flex justify food">
                                    <?
                                        
                                        $ss = $db->prepare("SELECT * FROM activities");
                                        $ss->execute();
                                        while($s = $ss->fetchObject()){
                                            ?>
                                            <div class="c_25"><a href="#" title="<? echo $s->title; ?>" data-id="<? echo $s->id; ?>" data-target="activity" data-filter="false" class="btn trigger <? if($_SESSION['activity'] == $s->id){ echo "active"; } ?>"><? echo $s->title; ?></a><p><? echo $s->description; ?></p></div>
                                            <?
                                        }
                                    ?>
                                            
                                </div><!--close justify-->
                    <? include "includes/buttons.php";
                    break;
                        
                    //include "includes/stage-13.php";
                        
                    default:
                        ?>
                         <h1>Let's Start Ooddling</h1>
            
                         <input name="first" type="text" value="<? echo $_SESSION['first']; ?>" placeholder="First Name" /> <input name="surname" type="text" value="<? echo $_SESSION['surname']; ?>" placeholder="Surname" />
                        <input name="email" type="text" value="<? echo $_SESSION['email']; ?>" placeholder="Your email address" />

                         <p>Your email address is all we need to get your account set-up and save your sign-up process.</p>

                         <label>Do you love offers, free samples and new product updates?</label>
                         <div class="flex justify">
                            <div class="c_20"><a href="#" title="Yes" data-id="Yes" data-target="mailing" class="btn trigger <? if($_SESSION['mailing'] == "Yes"){ echo "active"; } ?>">Yes</a></div>
                            <div class="c_20"><a href="#" title="No" data-id="No" data-target="mailing" class="btn trigger <? if($_SESSION['mailing'] == "No"){ echo "active"; } ?>">No</a></div>
                         </div><!--close justify-->

                         <p>Find more details on how we store your data in our <a href="/privacy-policy" title="Privacy Policy">Privacy Policy</a></p>
                         <input name="submit" type="submit" value="Start Ooddling" />
                        <?
                    break;
                }
                ?>
           
            
            
            </form>
            
            
        </section>
        
    </div><!--close container-->
<? include "footer.php"; ?>