<? include "header.php"; 


    ?>
    <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Your Ooddles Order</h1>
       
        
    </div><!--close headline-->
    
</div><!--close banner-->
<section>

    <?
    
    $st = $db->prepare("SELECT * FROM orders WHERE unique_id = ?");
    $st->execute(array($_GET['id']));
    
    
    if($st->rowCount() == 0){
        ?>
        <div class="flex empty">
        <div class="c_33">
        </div>
        <div class="c_66">
            <div class="inner">
            <h3>What's going on?</h3>
            <p>We can't seem to locate that order. Check your email for confirmation.<br />
            Alternatively, you can email us on <a href="mailto:<? echo $company->email; ?>" title="Email <? echo $company->name; ?>"><? echo $company->email; ?></a> and we'll be happy to assist you.</p>
            </div>
            </div>
        </div>
        <?
    }else{
        $xx = $st->fetchObject();
        
        include "includes/order.php";
        ?>
   
            
   
    <?
    }
    ?>
    </section>
<?

include "includes/company.php";
include "footer.php"; ?>