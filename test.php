<?
include "configuration.php";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$sa = $db->prepare("SELECT * FROM customers WHERE (referral_code = ? OR referral_code = ?) AND salt != ? LIMIT 100");
$sa->execute(array("https://bit.ly/3lrkhgi", "", ""));

$su = $db->prepare("UPDATE customers SET unique_id = ?, referral_code = ? WHERE id = ?");

while($a = $sa->fetchObject()){
    
    if(empty($a->unique_id)){
        $date = new DateTime();
        $hash =  $date->getTimestamp();
        $unique_id = $hash.$a->id;
    }else{
        $unique_id = $a->unique_id;
    }
    
    $url = 'https://api-ssl.bitly.com/v4/shorten';
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['long_url' => MAIN_SITE."/refer/".$unique_id]));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        "Authorization: Bearer ".$bitly,
        "Content-Type: application/json"
    ]);

    $arr_result = json_decode(curl_exec($ch));
    
    $su->execute(array($unique_id, $arr_result->link, $a->id));
    
}


?>