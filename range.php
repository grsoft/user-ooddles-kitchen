<? include "header.php"; 


    ?>
    <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1><? echo $xx->title; ?></h1>
        <div class="c_75"><p><? echo $xx->introduction; ?></p></div>
        
    </div><!--close headline-->
    
</div><!--close banner-->
<section>
    <div id="breadcrumbs">
        <ul class="flex">
            <li><a href="/" title="<? echo $company->name; ?>">Home</a></li>
            <li>&rang;</li>
            <li><a href="/shop" title="Shop">Shop</a></li>
            <li>&rang;</li>
            <li><a href="/shop/<? echo $_GET['id']; ?>" title="<? echo $xx->title; ?>"><? echo $xx->title; ?></a></li>
            
            
        </ul>
        </div>
                
    </section>
    
    <section class="products shop">
    <div class="flex negative">
    <?
  

        $sc = $db->prepare("SELECT p.seo, r.seo AS range_seo, p.introduction, p.title, p.image
                            FROM products p 
                            LEFT JOIN categories c ON FIND_IN_SET(c.id, p.category_id)
                            LEFT JOIN product_ranges r ON p.range_id = r.id
                            WHERE p.status = 'Published' AND c.status ='Published' AND r.status ='Published'  AND FIND_IN_SET ('".$xx->id."',p.range_id)
                            GROUP BY p.title 
                            ORDER BY p.running_order=0, p.running_order ASC");
        $sc->execute(array("Published", "Published", "Published", $xx->id));
        
        while($c = $sc->fetchObject()){
            echo "<div class='c_25'>
                  <div class='inner item'>
                  <div class='product_img'>
                  <a href='/shop/{$c->range_seo}/{$c->seo}' title='{$c->title}'><img src='/thumb.php?src=/images/products/{$c->image}&w=300&h=300&zc=2' alt='{$c->title}' /></a>";
                if(!empty($c->introduction)){
                    echo "<div class='product_info'><p>{$c->introduction}</p></div>";
                }
            echo "</div>
                  <h3> <a href='/shop/{$c->range_seo}/{$c->seo}' title='{$c->title}'>{$c->title}</a></h3>
                  
                  <p><a href='/shop/{$c->range_seo}/{$c->seo}' title='{$c->title}' class='btn'>Buy</a></p>
                  </div>
                  </div>";
        }
    
    ?>
        </div>
    </section>
    <?

include "includes/company.php";
include "footer.php"; ?>