<? include "check-login.php"; 

?>

<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]><html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PZXXS8B');</script>
<!-- End Google Tag Manager -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-RK7KG53D29"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-RK7KG53D29');
</script>
<!--- Live Chat code --->
<script>var continuallySettings = { appID: "p87rwexqqrq9" };</script>

<script src="https://cdn-app.continual.ly/js/embed/continually-embed.latest.min.js"></script>

<!--- Live Chat code end --->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-WZG4K6HNT7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-WZG4K6HNT7');
</script>


<script>

dataLayer.push({
 gtag('event', purchase, {
  'send_to': 'G-RK7KG53D29',
  'items': '{{DLV - items}}',
  'transaction_id': '{{ecommerce.purchase.transaction_id}}',
  // ...
}); 
});
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KKJD59H');</script>
<!-- End Google Tag Manager -->
<!--<script>

dataLayer.push({
  gtag('event', view_item_list, {
  'send_to': 'G-RK7KG53D29',
  'items': '{{DLV - ecommerce.items}}',
 
});
});
</script>-->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width" />
    <meta name="google-site-verification" content="qdccvbdpr9OS-IQ8MZoxveZEIyMfxCP0Jr31N284RZU" />
    <meta name="facebook-domain-verification" content="1l2ebe7gyim4thwxq2fn7zqaq5qlkq" />
<link rel="stylesheet" type="text/css" href="/css/screen.css?v=<? echo rand(); ?>" />
<link rel="stylesheet" href="https://use.typekit.net/fgs1ctj.css">
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.jpg" />
<? include "includes/core.php"; ?>
    <script>var continuallySettings = { appID: "p87rwexqqrq9" };</script>
    <script src="https://cdn-app.continual.ly/js/embed/continually-embed.latest.min.js"></script>
    
    
    <script>(function(n,t,i,r){var u,f;n[i]=n[i]||{},n[i].initial={accountCode:"OODDL11112",host:"OODDL11112.pcapredict.com"},n[i].on=n[i].on||function(){(n[i].onq=n[i].onq||[]).push(arguments)},u=t.createElement("script"),u.async=!0,u.src=r,f=t.getElementsByTagName("script")[0],f.parentNode.insertBefore(u,f)})(window,document,"pca","//OODDL11112.pcapredict.com/js/sensor.js")</script>
<?
    if(URL == "oodles.brainstormdevelopment.co.uk"){
        ?>
        <meta name="robots" content="noindex,follow" />
        <?
    }
    if(PAGE == "/index.php"){
    ?>
    <!-- BEGIN PRIVY WIDGET CODE -->
<script type='text/javascript'> var _d_site = _d_site || 'F156C4A5A05A8F0AA57E5380'; </script>
<script src='https://widget.privy.com/assets/widget.js'></script>
<!-- END PRIVY WIDGET CODE -->
<?  } ?>    

    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '2172975929605429');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=2172975929605429&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/7aab2a332a39467ef5db24e00/6c3bce3751253dfb79d4f86c5.js");</script>
    <!-- Taboola Pixel Code -->
<script type='text/javascript'>
  window._tfa = window._tfa || [];
  window._tfa.push({notify: 'event', name: 'page_view', id: 1347642});
  !function (t, f, a, x) {
         if (!document.getElementById(x)) {
            t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
         }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/unip/1347642/tfa.js',
  'tb_tfa_script');
</script>
<noscript>
  <img src='https://trc.taboola.com/1347642/log/3/unip?en=page_view'
      width='0' height='0' style='display:none' />
</noscript>
<!-- End of Taboola Pixel Code -->
 <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
   <!--  <link rel="stylesheet" href="/css/demo.css">
    <link rel="stylesheet" href="/css/vanilla-zoom.css">
 <script src="/js/vanilla-zoom.js"></script> -->
 <link rel="stylesheet" href="/css/thumb_img.css">
 
 <!----------------------search image cdn----------------->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" />
 
 
 
 
 
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PZXXS8B"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KKJD59H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <?
    if(PAGE == "/index.php"){
        ?>




    <div id="popper">
        <p>For all current Ooddles customers, you need to make a new account at this new website, your old log in will not work.</p>
         <p>Any queries please let us know at <a href="mailto:info@ooddleskitchen.co.uk" title="Email <? echo $company->name; ?>">info@ooddleskitchen.co.uk</a></p>
        <img src="/images/icon-close.png" width="25" />
    </div><!--close popper-->
    <? } ?>
    <div id="mobile_menu">
        <div class="menu_inner">
        <img src="/images/logo-horizontal.png" alt="Logo" />
            <img src="/images/icon-close.png" alt="Close" class="close" />
            <? include "includes/subnav.php"; ?>
            <div class="quick_contact">
                <h3>Help or assistance?</h3>
                <p>Email: <a href="mailto:<? echo $company->email; ?>" title="Email <? echo $company->name; ?>"><? echo $company->email; ?></a></p>
            </div><!--close quick_contact-->
        </div><!--close menu_inner-->
        
    </div><!--close mobile_menu-->
    <?
    //echo PAGE;
        if(!empty($_SESSION['status']) && empty($_SESSION['popup'])){
	       echo "<div id='status'>".$_SESSION['status']."</div>";
        }
    if(!empty($_SESSION['popup'])){
        echo "<div id='upsell'>
              <div class='inner'>
              <img src='/images/icon-paw-brown.png' alt='Paw' class='icon' />
              <h3>Great choice!</h3>
              <p>We've updated your basket.<br />
              Remember, {$pet->name} also needs some wet food to compliment his dry food.<br />
              We've got a lot of tasty goodies available and plenty of treats and chews - take a look for yourself.</p>";
        ?>
        <div class="flex negative justify">
        <?
            // Our wheres...
            if(!empty($pet->size)){
                $where .= " AND (p.suitable_for LIKE '%{$pet->size}%' OR p.suitable_for = '')";
            }
            if(!empty($pet->dislikes)){
                if(stripos($pet->dislikes, ",") !== false){
                    $bits = explode(",", $pet->dislikes);
                    foreach($bits as $b){
                        $where .= " AND p.main_ingredient NOT LIKE '%{$b}%'";        
                    }
                }else{
                    $where .= " AND p.main_ingredient NOT LIKE '%{$pet->dislikes}%'";    
                }

            }
            $sp = $db->prepare("SELECT p.title, p.seo, p.image, p.food_type, r.seo AS range_seo, c.seo AS category_seo, c.category 
                                FROM skus s 
                                LEFT JOIN products p ON s.product_id = p.id 
                                LEFT JOIN product_ranges r ON p.range_id = r.id
                                LEFT JOIN categories c ON p.category_id = c.id
                                WHERE p.status = ? AND s.status = ? AND s.stock > ? AND (p.category_id = ? OR p.category_id = ?) {$where}
                                GROUP BY p.category_id");
            $sp->execute(array("Published", "Published", 0, 2, 6));
        
            while($p = $sp->fetchObject()){
                if($_GET['page'] == "/detail.php"){
                    $url = "/o-hub/ranges/".$p->category_seo;
                }else{
                    $url = "/get-ooddling/".$pet->unique_id."/".$p->category_seo;
                }
        ?>
          <div class="c_50">
              <div class="inner item">
                  <div class="product_img"><a href="<? echo $url; ?>" title="<? echo $p->category; ?>"><? if(!empty($p->image)){ echo "<img src='/thumb.php?src=/images/products/{$p->image}&w=400&h=400&zc=2' alt='{$p->category}' />";  } ?></a></div>
                  <h2><a href="<? echo $url; ?>" title="<? echo $p->category; ?>"><? echo $p->category; ?></a></h2>
                  <p><a href="<? echo $url; ?>" title="<? echo $p->category; ?>" class="btn white">Buy Now</a></p>
              </div><!--close item-->
          </div><!--close c_25-->
    <?  } ?>              
    </div><!--close flex-->
    <?
              
        echo "<p><span class='close'>[ CLOSE ]</span></p></div>
              </div>";
    }
    if($show_404 == 1){
        include "404-body.php";
        die();
    }
    
        ?>