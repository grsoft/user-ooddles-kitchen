<?php return array (
  'codeToName' => 
  array (
    32 => 'space',
    33 => 'exclam',
    34 => 'quotedbl',
    35 => 'numbersign',
    36 => 'dollar',
    37 => 'percent',
    38 => 'ampersand',
    39 => 'quotesingle',
    40 => 'parenleft',
    41 => 'parenright',
    42 => 'asterisk',
    43 => 'plus',
    44 => 'comma',
    45 => 'hyphen',
    46 => 'period',
    47 => 'slash',
    48 => 'zero',
    49 => 'one',
    50 => 'two',
    51 => 'three',
    52 => 'four',
    53 => 'five',
    54 => 'six',
    55 => 'seven',
    56 => 'eight',
    57 => 'nine',
    58 => 'colon',
    59 => 'semicolon',
    60 => 'less',
    61 => 'equal',
    62 => 'greater',
    63 => 'question',
    64 => 'at',
    65 => 'A',
    66 => 'B',
    67 => 'C',
    68 => 'D',
    69 => 'E',
    70 => 'F',
    71 => 'G',
    72 => 'H',
    73 => 'I',
    74 => 'J',
    75 => 'K',
    76 => 'L',
    77 => 'M',
    78 => 'N',
    79 => 'O',
    80 => 'P',
    81 => 'Q',
    82 => 'R',
    83 => 'S',
    84 => 'T',
    85 => 'U',
    86 => 'V',
    87 => 'W',
    88 => 'X',
    89 => 'Y',
    90 => 'Z',
    91 => 'bracketleft',
    92 => 'backslash',
    93 => 'bracketright',
    94 => 'asciicircum',
    95 => 'underscore',
    96 => 'grave',
    97 => 'a',
    98 => 'b',
    99 => 'c',
    100 => 'd',
    101 => 'e',
    102 => 'f',
    103 => 'g',
    104 => 'h',
    105 => 'i',
    106 => 'j',
    107 => 'k',
    108 => 'l',
    109 => 'm',
    110 => 'n',
    111 => 'o',
    112 => 'p',
    113 => 'q',
    114 => 'r',
    115 => 's',
    116 => 't',
    117 => 'u',
    118 => 'v',
    119 => 'w',
    120 => 'x',
    121 => 'y',
    122 => 'z',
    123 => 'braceleft',
    124 => 'bar',
    125 => 'braceright',
    126 => 'asciitilde',
    161 => 'exclamdown',
    162 => 'cent',
    163 => 'sterling',
    165 => 'yen',
    166 => 'brokenbar',
    167 => 'section',
    168 => 'dieresis',
    169 => 'copyright',
    170 => 'ordfeminine',
    171 => 'guillemotleft',
    174 => 'registered',
    175 => 'macron',
    176 => 'degree',
    177 => 'plusminus',
    178 => 'twosuperior',
    179 => 'threesuperior',
    180 => 'acute',
    182 => 'paragraph',
    183 => 'periodcentered',
    184 => 'cedilla',
    185 => 'onesuperior',
    186 => 'ordmasculine',
    187 => 'guillemotright',
    188 => 'onequarter',
    189 => 'onehalf',
    190 => 'threequarters',
    191 => 'questiondown',
    192 => 'Agrave',
    193 => 'Aacute',
    194 => 'Acircumflex',
    195 => 'Atilde',
    196 => 'Adieresis',
    197 => 'Aring',
    198 => 'AE',
    199 => 'Ccedilla',
    200 => 'Egrave',
    201 => 'Eacute',
    202 => 'Ecircumflex',
    203 => 'Edieresis',
    204 => 'Igrave',
    205 => 'Iacute',
    206 => 'Icircumflex',
    207 => 'Idieresis',
    208 => 'Eth',
    209 => 'Ntilde',
    210 => 'Ograve',
    211 => 'Oacute',
    212 => 'Ocircumflex',
    213 => 'Otilde',
    214 => 'Odieresis',
    215 => 'multiply',
    216 => 'Oslash',
    217 => 'Ugrave',
    218 => 'Uacute',
    219 => 'Ucircumflex',
    220 => 'Udieresis',
    221 => 'Yacute',
    222 => 'Thorn',
    223 => 'germandbls',
    224 => 'agrave',
    225 => 'aacute',
    226 => 'acircumflex',
    227 => 'atilde',
    228 => 'adieresis',
    229 => 'aring',
    230 => 'ae',
    231 => 'ccedilla',
    232 => 'egrave',
    233 => 'eacute',
    234 => 'ecircumflex',
    235 => 'edieresis',
    236 => 'igrave',
    237 => 'iacute',
    238 => 'icircumflex',
    239 => 'idieresis',
    240 => 'eth',
    241 => 'ntilde',
    242 => 'ograve',
    243 => 'oacute',
    244 => 'ocircumflex',
    245 => 'otilde',
    246 => 'odieresis',
    247 => 'divide',
    248 => 'oslash',
    249 => 'ugrave',
    250 => 'uacute',
    251 => 'ucircumflex',
    252 => 'udieresis',
    253 => 'yacute',
    254 => 'thorn',
    255 => 'ydieresis',
    256 => 'Amacron',
    257 => 'amacron',
    258 => 'Abreve',
    259 => 'abreve',
    260 => 'Aogonek',
    261 => 'aogonek',
    262 => 'Cacute',
    263 => 'cacute',
    264 => 'Ccircumflex',
    265 => 'ccircumflex',
    266 => 'Cdotaccent',
    267 => 'cdotaccent',
    268 => 'Ccaron',
    269 => 'ccaron',
    270 => 'Dcaron',
    271 => 'dcaron',
    272 => 'Dcroat',
    273 => 'dmacron',
    274 => 'Emacron',
    275 => 'emacron',
    276 => 'Ebreve',
    277 => 'ebreve',
    278 => 'Edotaccent',
    279 => 'edotaccent',
    280 => 'Eogonek',
    281 => 'eogonek',
    282 => 'Ecaron',
    283 => 'ecaron',
    284 => 'Gcircumflex',
    285 => 'gcircumflex',
    286 => 'Gbreve',
    287 => 'gbreve',
    288 => 'Gdotaccent',
    289 => 'gdotaccent',
    290 => 'Gcommaaccent',
    291 => 'gcommaaccent',
    292 => 'Hcircumflex',
    293 => 'hcircumflex',
    294 => 'Hbar',
    295 => 'hbar',
    296 => 'Itilde',
    297 => 'itilde',
    298 => 'Imacron',
    299 => 'imacron',
    300 => 'Ibreve',
    301 => 'ibreve',
    302 => 'Iogonek',
    303 => 'iogonek',
    304 => 'Idot',
    305 => 'dotlessi',
    306 => 'IJ',
    307 => 'ij',
    308 => 'Jcircumflex',
    309 => 'jcircumflex',
    310 => 'Kcommaaccent',
    311 => 'kcommaaccent',
    313 => 'Lacute',
    314 => 'lacute',
    315 => 'Lcommaaccent',
    316 => 'lcommaaccent',
    317 => 'Lcaron',
    318 => 'lcaron',
    319 => 'Ldot',
    320 => 'ldot',
    321 => 'Lslash',
    322 => 'lslash',
    323 => 'Nacute',
    324 => 'nacute',
    325 => 'Ncommaaccent',
    326 => 'ncommaaccent',
    327 => 'Ncaron',
    328 => 'ncaron',
    329 => 'napostrophe',
    330 => 'Eng',
    331 => 'eng',
    332 => 'Omacron',
    333 => 'omacron',
    334 => 'Obreve',
    335 => 'obreve',
    336 => 'Ohungarumlaut',
    337 => 'ohungarumlaut',
    338 => 'OE',
    339 => 'oe',
    340 => 'Racute',
    341 => 'racute',
    342 => 'Rcommaaccent',
    343 => 'rcommaaccent',
    344 => 'Rcaron',
    345 => 'rcaron',
    346 => 'Sacute',
    347 => 'sacute',
    348 => 'Scircumflex',
    349 => 'scircumflex',
    350 => 'Scedilla',
    351 => 'scedilla',
    352 => 'Scaron',
    353 => 'scaron',
    354 => 'Tcommaaccent',
    355 => 'tcommaaccent',
    356 => 'Tcaron',
    357 => 'tcaron',
    358 => 'Tbar',
    359 => 'tbar',
    360 => 'Utilde',
    361 => 'utilde',
    362 => 'Umacron',
    363 => 'umacron',
    364 => 'Ubreve',
    365 => 'ubreve',
    366 => 'Uring',
    367 => 'uring',
    368 => 'Uhungarumlaut',
    369 => 'uhungarumlaut',
    370 => 'Uogonek',
    371 => 'uogonek',
    372 => 'Wcircumflex',
    373 => 'wcircumflex',
    374 => 'Ycircumflex',
    375 => 'ycircumflex',
    376 => 'Ydieresis',
    377 => 'Zacute',
    378 => 'zacute',
    379 => 'Zdotaccent',
    380 => 'zdotaccent',
    381 => 'Zcaron',
    382 => 'zcaron',
    402 => 'florin',
    506 => 'Aringacute',
    507 => 'aringacute',
    508 => 'AEacute',
    509 => 'aeacute',
    510 => 'Oslashacute',
    511 => 'oslashacute',
    536 => 'Scommaaccent',
    537 => 'scommaaccent',
    710 => 'circumflex',
    711 => 'caron',
    728 => 'breve',
    729 => 'dotaccent',
    730 => 'ring',
    731 => 'ogonek',
    732 => 'tilde',
    733 => 'hungarumlaut',
    7808 => 'Wgrave',
    7809 => 'wgrave',
    7810 => 'Wacute',
    7811 => 'wacute',
    7812 => 'Wdieresis',
    7813 => 'wdieresis',
    7922 => 'Ygrave',
    7923 => 'ygrave',
    8211 => 'endash',
    8212 => 'emdash',
    8216 => 'quoteleft',
    8217 => 'quoteright',
    8218 => 'quotesinglbase',
    8220 => 'quotedblleft',
    8221 => 'quotedblright',
    8222 => 'quotedblbase',
    8224 => 'dagger',
    8225 => 'daggerdbl',
    8226 => 'bullet',
    8230 => 'ellipsis',
    8240 => 'perthousand',
    8249 => 'guilsinglleft',
    8250 => 'guilsinglright',
    8260 => 'fraction',
    8364 => 'Euro',
    8482 => 'trademark',
    8531 => 'onethird',
    8532 => 'twothirds',
    8539 => 'oneeighth',
    8540 => 'threeeighths',
    8541 => 'fiveeighths',
    8542 => 'seveneighths',
    8722 => 'minus',
    8776 => 'approxequal',
    8800 => 'notequal',
    8804 => 'lessequal',
    8805 => 'greaterequal',
    64256 => 'f_f',
    64257 => 'f_i',
    64258 => 'f_l',
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'false',
  'FullName' => 'Graphik-Medium',
  'Version' => 'Version 1.001;PS 001.001;hotconv 1.0.57;makeotf.lib2.0.21895',
  'PostScriptName' => 'false',
  'Weight' => 'Bold',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '50',
  'UnderlinePosition' => '-125',
  'FontHeightOffset' => '100',
  'Ascender' => '821',
  'Descender' => '-179',
  'FontBBox' => 
  array (
    0 => '-235',
    1 => '-225',
    2 => '1134',
    3 => '1123',
  ),
  'StartCharMetrics' => '544',
  'C' => 
  array (
    32 => 238,
    33 => 305,
    34 => 409,
    35 => 532,
    36 => 635,
    37 => 801,
    38 => 716,
    39 => 229,
    40 => 335,
    41 => 335,
    42 => 429,
    43 => 539,
    44 => 292,
    45 => 353,
    46 => 289,
    47 => 415,
    48 => 701,
    49 => 419,
    50 => 578,
    51 => 623,
    52 => 641,
    53 => 604,
    54 => 641,
    55 => 545,
    56 => 627,
    57 => 642,
    58 => 297,
    59 => 303,
    60 => 507,
    61 => 513,
    62 => 507,
    63 => 516,
    64 => 917,
    65 => 703,
    66 => 645,
    67 => 741,
    68 => 714,
    69 => 584,
    70 => 559,
    71 => 775,
    72 => 751,
    73 => 307,
    74 => 443,
    75 => 659,
    76 => 551,
    77 => 890,
    78 => 745,
    79 => 814,
    80 => 609,
    81 => 814,
    82 => 655,
    83 => 635,
    84 => 583,
    85 => 723,
    86 => 683,
    87 => 955,
    88 => 695,
    89 => 629,
    90 => 608,
    91 => 377,
    92 => 415,
    93 => 377,
    94 => 516,
    95 => 410,
    96 => 400,
    97 => 555,
    98 => 633,
    99 => 575,
    100 => 633,
    101 => 580,
    102 => 362,
    103 => 633,
    104 => 608,
    105 => 271,
    106 => 271,
    107 => 540,
    108 => 271,
    109 => 918,
    110 => 608,
    111 => 611,
    112 => 633,
    113 => 633,
    114 => 397,
    115 => 494,
    116 => 369,
    117 => 605,
    118 => 557,
    119 => 814,
    120 => 556,
    121 => 553,
    122 => 493,
    123 => 396,
    124 => 259,
    125 => 396,
    126 => 474,
    160 => 271,
    161 => 306,
    162 => 577,
    163 => 620,
    165 => 604,
    166 => 264,
    167 => 565,
    168 => 400,
    169 => 843,
    170 => 444,
    171 => 492,
    174 => 515,
    175 => 400,
    176 => 437,
    177 => 539,
    178 => 313,
    179 => 336,
    180 => 400,
    182 => 573,
    183 => 289,
    184 => 400,
    185 => 253,
    186 => 504,
    187 => 492,
    188 => 724,
    189 => 728,
    190 => 738,
    191 => 516,
    192 => 703,
    193 => 703,
    194 => 703,
    195 => 703,
    196 => 703,
    197 => 703,
    198 => 971,
    199 => 741,
    200 => 584,
    201 => 584,
    202 => 584,
    203 => 584,
    204 => 307,
    205 => 307,
    206 => 307,
    207 => 307,
    208 => 739,
    209 => 745,
    210 => 814,
    211 => 814,
    212 => 814,
    213 => 814,
    214 => 814,
    215 => 509,
    216 => 814,
    217 => 723,
    218 => 723,
    219 => 723,
    220 => 723,
    221 => 629,
    222 => 609,
    223 => 608,
    224 => 555,
    225 => 555,
    226 => 555,
    227 => 555,
    228 => 555,
    229 => 555,
    230 => 891,
    231 => 575,
    232 => 580,
    233 => 580,
    234 => 580,
    235 => 580,
    236 => 271,
    237 => 271,
    238 => 271,
    239 => 271,
    240 => 611,
    241 => 608,
    242 => 611,
    243 => 611,
    244 => 611,
    245 => 611,
    246 => 611,
    247 => 540,
    248 => 611,
    249 => 605,
    250 => 605,
    251 => 605,
    252 => 605,
    253 => 553,
    254 => 633,
    255 => 553,
    256 => 703,
    257 => 555,
    258 => 703,
    259 => 555,
    260 => 703,
    261 => 555,
    262 => 741,
    263 => 575,
    264 => 741,
    265 => 575,
    266 => 741,
    267 => 575,
    268 => 741,
    269 => 575,
    270 => 714,
    271 => 633,
    272 => 739,
    273 => 633,
    274 => 584,
    275 => 580,
    276 => 584,
    277 => 580,
    278 => 584,
    279 => 580,
    280 => 584,
    281 => 580,
    282 => 584,
    283 => 580,
    284 => 775,
    285 => 633,
    286 => 775,
    287 => 633,
    288 => 775,
    289 => 633,
    290 => 775,
    291 => 633,
    292 => 751,
    293 => 608,
    294 => 751,
    295 => 608,
    296 => 307,
    297 => 271,
    298 => 307,
    299 => 271,
    300 => 307,
    301 => 271,
    302 => 307,
    303 => 271,
    304 => 307,
    305 => 271,
    306 => 715,
    307 => 542,
    308 => 443,
    309 => 271,
    310 => 659,
    311 => 540,
    313 => 551,
    314 => 271,
    315 => 551,
    316 => 271,
    317 => 551,
    318 => 271,
    319 => 561,
    320 => 378,
    321 => 570,
    322 => 299,
    323 => 745,
    324 => 608,
    325 => 745,
    326 => 608,
    327 => 745,
    328 => 608,
    329 => 655,
    330 => 745,
    331 => 608,
    332 => 814,
    333 => 611,
    334 => 814,
    335 => 611,
    336 => 814,
    337 => 611,
    338 => 964,
    339 => 991,
    340 => 655,
    341 => 397,
    342 => 655,
    343 => 397,
    344 => 655,
    345 => 397,
    346 => 635,
    347 => 494,
    348 => 635,
    349 => 494,
    350 => 635,
    351 => 494,
    352 => 635,
    353 => 494,
    354 => 584,
    355 => 369,
    356 => 583,
    357 => 369,
    358 => 583,
    359 => 369,
    360 => 723,
    361 => 605,
    362 => 723,
    363 => 605,
    364 => 723,
    365 => 605,
    366 => 723,
    367 => 605,
    368 => 723,
    369 => 605,
    370 => 723,
    371 => 605,
    372 => 955,
    373 => 814,
    374 => 629,
    375 => 553,
    376 => 629,
    377 => 608,
    378 => 493,
    379 => 608,
    380 => 493,
    381 => 608,
    382 => 493,
    402 => 417,
    506 => 703,
    507 => 555,
    508 => 971,
    509 => 891,
    510 => 814,
    511 => 611,
    536 => 636,
    537 => 494,
    567 => 271,
    710 => 400,
    711 => 400,
    728 => 400,
    729 => 400,
    730 => 400,
    731 => 400,
    732 => 400,
    733 => 400,
    7808 => 955,
    7809 => 814,
    7810 => 955,
    7811 => 814,
    7812 => 955,
    7813 => 814,
    7922 => 629,
    7923 => 553,
    8201 => 101,
    8211 => 532,
    8212 => 909,
    8216 => 277,
    8217 => 277,
    8218 => 276,
    8220 => 473,
    8221 => 473,
    8222 => 473,
    8224 => 386,
    8225 => 386,
    8226 => 348,
    8230 => 840,
    8240 => 1164,
    8242 => 227,
    8243 => 409,
    8249 => 316,
    8250 => 316,
    8260 => 135,
    8364 => 692,
    8482 => 733,
    8531 => 758,
    8532 => 800,
    8539 => 751,
    8540 => 785,
    8541 => 775,
    8542 => 746,
    8722 => 540,
    8776 => 530,
    8800 => 513,
    8804 => 507,
    8805 => 507,
    9413 => 843,
    64256 => 667,
    64257 => 628,
    64258 => 628,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt22WwUFUYBdD9GYgJJggqKootBiqKKBYYiN2FYiIIIojd3d3d3d3d3d3d3fV0+KEzzBjIe+KsNXPnxD1nvn3j70lG0OgZI2OmWcZK84ydcTJuxsv4mSAt0jITZqJMnEkyaSZLq7TO5GmTtpkiU2aqtMvUmSbTpn2my/TpkBkyY2bKzJkls2a2zJ6OmSNzZq7MnU6ZJ/NmvnTO/FkgXbJgumahLJxuWSSLZrEsniXSPT2yZJbK0lkmPbNsemW5LJ8VsmJWyspZJatmtayeNbJm1sraWSfrZr30zvoj+vANNkifbJiNGnobZ5Nsms3SN5un32/3+meLDMjAbJlB2aphPDhDsnWGZptsm+2yfXbIjtkpO2eX7Jrdsnv2yJ7ZK3tnn+yb/bJ/DsiBOSgH55AcmsNyeI7IkTkqR+eYHJvjcnxOyIk5KSfnlJya03J6zsiZOStn55ycm/Nyfi7IhbkoF+eSXJrLcnmuyJW5Klfnmlyb63J9bsiNuSk355bcmttye+7Inbkrd+ee3Jv7cn8eyIN5KA/nkTyax/J4nsiTeSpP55k8m+fyfF7Ii3kpL+eVvJrX8nreyJt5K2/nnbyb9/J+PsiH+Sgf55N8ms/yeb7Ilw1v4at8nW/ybb7L9/khP+an/FypqtFq9BqjxqxmNVY1r7FrnBq3xqvxa4JqUS1rwpqoJq5JatKarFpV65q82lTbmqKmrKmqXU1d09S01b6mq+mrQ81QM9ZMNXPNUrPWbDV7daw5as6aq+auTjVPzVvzVeeavxaoLn/tE9eC/8J/8h9SXWuhWri61SK16N/eu1gtPgKVl/jne///qnv1+NM1S9ZStXQtUz1r2cbIBAAAAAAAAAAAAAAAAABNoXrVcrV8rVAr1kpNnWVkqZVrlabOwKiiVh3O3Gq1+rDeGrVmrdXQrl3r1LoN7XrVu4ado68NhrOzT8O1YW30u5mNa5PhrNt0xJP/l9RmTZ2g8VTfkV5h8+r3h3H/2qIG1MCRXZdf1ZaNWGvQ31y/1bB2cA3599MANIbauqkTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIwKamhtU9s2dQoAABrLL8ycnM0=',
  '_version_' => 6,
);