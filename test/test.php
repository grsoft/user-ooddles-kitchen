<?php
include "../configuration.php";
use setasign\Fpdi\Fpdi;
//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
require_once('fpdf/fpdf.php');
require_once('fpdi/src/autoload.php');

include "../vendor/autoload.php";
use Dompdf\Dompdf;
use Dompdf\Options;



// instantiate and use the dompdf class
    $options = new Options();
    $options->setIsRemoteEnabled(true);
    $options->setDefaultFont('Helvetica');




class ConcatPdf extends Fpdi
{
    public $files = array();

    public function setFiles($files)
    {
        $this->files = $files;
    }

    public function concat()
    {
        foreach($this->files AS $file) {
            $pageCount = $this->setSourceFile($file);
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                $pageId = $this->ImportPage($pageNo);
                $s = $this->getTemplatesize($pageId);
                $this->AddPage($s['orientation'], $s);
                $this->useImportedPage($pageId);
            }
        }
    }
}

$nums = array("1611277273140", "1611319637166");
$files = array();
foreach($nums as $n){
    $dompdf = new Dompdf($options); 
    
    $url = MAIN_SITE."/invoice.php?id=".$n;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $file = curl_exec($ch);
    curl_close($ch);

    
    
    $dompdf->loadHtml($file);

// (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
    $dompdf->render();

    $link = $n."-invoice.pdf";

    $output = $dompdf->output();
    
    file_put_contents($link, $output);
     
    $files[] = $link;
}




$pdf = new ConcatPdf();
$pdf->setFiles($files);
$pdf->concat();

$pdf->Output('D', 'ooddles-orders.pdf');


?>