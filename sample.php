<? 

include "header.php"; 
require_once('stripe/init.php');

?>

<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery Multislider Demos</title>
  <!--<link rel="stylesheet" href="css/screen.css"> ---->
  <link rel="stylesheet" href="/css/sample.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 
  <style>
    h2 {
      margin: 30px auto;
    }

    .container {
      margin: 150px auto;
    }

    
    /* --------------------------------------------------------------------------- */
  </style>
</head>

<body>
 
  
 

      
      <div itemscope itemtype="https://schema.org/Product">
    <div id="banner" class="half">
     <a href="/" title="<?php echo $company->name; ?> Logo"><img src="images/logo-horizontal.png" alt="<?php echo $company->name; ?> Logo" class="logo" /></a>
    <?php include "includes/nav.php"; ?>
    
    <div class="headline centre">
       
        
   
      <h1>Grab Your Sample Offer Bundle <br> Start Ooddling for £9.99</h1>

    </div></div>
    </div>
    
                                  <?php
                                        $ss = $db->prepare("SELECT * FROM skus WHERE product_id = ? AND status = ?");
                                        $ss->execute(array(333, "Published"));
                                        $s = $ss->fetchObject();
                                  ?>

                                  <div id="mySidepanel" class="sidepanel" style="z-index: 100 !important;">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
                                    <meta itemprop="url" content="<? echo MAIN_SITE.$_SERVER['REQUEST_URI']; ?>" />
                                    <form name="add" method="post" action="/actions.php?action=add-to-basket">
                                      <input name="sku" type="hidden" value="<? echo $s->id; ?>" />
                                    <input name="url" type="hidden" value="<? echo $_SERVER['REQUEST_URI']; ?>" />
                                    <div id="cartreload">
                                    <div class="side-font cat-4">
                                       <h6>Dry Food</h6>
                                      <h5 class="border_underline">
                                       
                                        <?php
                                      // print_r($xx);
                                          if(isset($_SESSION['cart_data']) && isset($_SESSION['cart_data'][4])){
                                            echo $_SESSION['cart_data'][4]['product'];
                                          }
                                        ?>
                                      </h6>
                                        <a href="#" data-proid="<?php echo $_SESSION['cart_data'][4]['productid'];?>" data-catid="5" class="delete">
                                     <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                  </div>
                                    <span class="border_bottam"></span>
                                   
                                  <div class="side-font cat-5">
                                    <h6>Wet Food</h6>
                                    <h5 class="border_underline">
                                        <?php
                                        
                                          if(isset($_SESSION['cart_data']) && isset($_SESSION['cart_data'][6])){
                                            echo $_SESSION['cart_data'][6]['product'];
                                          }
                                        ?>
                                    </h6>
                                    <span><a href="#" data-proid="<?php echo $_SESSION['cart_data'][6]['productid'];?>" data-catid="6" class="delete">
                                      <i class="fa fa-trash-o" aria-hidden="true"></i></span>
                                    </a></div>
                                     <div class="side-font cat-6">
                                    <h6 >Bare Ooddles (Raw)</h6>
                                    <h5 class="border_underline">
                                      <?php
                                        
                                          if(isset($_SESSION['cart_data']) && isset($_SESSION['cart_data'][5])){
                                            echo $_SESSION['cart_data'][5]['product'];
                                          }
                                        ?>
                                    </h6>
                                    <span><a href="#" data-proid="<?php echo $_SESSION['cart_data'][5]['productid'];?>" data-catid="5"  class="delete">
                                      <i class="fa fa-trash-o" aria-hidden="true"></i></span>
                                    </a>
                                  </div>
                                
                                  
                                    <div class="sidebar_addtocart">
                                    <p class='sidebar_text' style="padding: 2px;">Total Price £9.99</p>
                                    <?php 
                                    if($_SESSION['cart_data'][4]['product']!='' && $_SESSION['cart_data'][6]['product']!='' && $_SESSION['cart_data'][5]['product']!=''){
                                    ?>
                                    <p><input name="submit" type="submit" value="Add To Basket"  class='sidebar_2text' style="padding: 7px 40px 8px 40px; width: 100%; background-color: #653a2b;border-radius: 50px; font-weight: 600;color: white;text-align: center;" />
                                       <!-- <a href='#'  class='sidebar_2text' style="padding: 2px;">Add to Basket</a> --> </p>
                                       <?php }?>
                                  </div>
                                  </div>
                                  <div class="Bottom_text">
                                    <?php
                                     if($cart->qty > 0){
                                    ?>
                                       <a href="/basket">View Basket </a>
                                       <?php }?>
                                       <a href="/shop">Continue Shopping </a>
                                    </div>
                                  </form>
                                  </div>
                              
                                  <button class="openbtn"  onclick="openNav()"><img src="images/paw.png" style="width:50px" alt=""> </button>
                                  <!-- sidebar starting end -->
                                   <script>
                                    function openNav() {
                                      document.getElementById("mySidepanel").style.width = "350px";
                                      document.getElementById("mySidepanel").style.height = "400px";
                                    }
                  
                                    function closeNav() {
                                      document.getElementById("mySidepanel").style.width = "0";
                                    }
                                  </script>



  <!--close banner-->

  <div id="status" style="display: none;"><h4>Success</h4><p>You've got Ooddles in the basket.</p></div>
  <section>
    <div id="breadcrumbs">
      <ul class="flex">
        <li><a href="/" title="Ooddles">Home</a></li>
        <li>&rang;</li>
        <li><a href="/shop" title="Shop">Shop</a></li>
        <li>&rang;</li>
        <li><a href="" title="Shop">Sample Food</a></li>


      </ul>
    </div>
  </section>

<section>
<div class="headline centre">
<h3>If you wish to see all the ingredients and more info on any of the products below, please go to SHOP at the top of this page.</h3>
</div>
</section>

  <section>
    <div id="mixedSlider"  class="Slider_one" style="background-color:  #7fc2e5;">
      <h2 style="padding-top: 20px">Dry Food</h2>
      <div class="MS-content">
            

                <?php 
                
                $st = $db->prepare("SELECT * FROM sample_product WHERE category_id = 4");
                $st->execute(array());
                $num = $st->rowCount();

                for($x=1;$x<=$num;$x++)	
                {
                    $r = $st->fetchObject();
                    echo "<div class='item'>
					<div class='imgTitle'>
					<img class='sample-img' src = 'images/products/{$r->image}' />
					<h3 class='sample-head'><a href='' title='{$r->title}'>{$r->title}</a></h3>
           
           
					<p><a href='javascript:void(0)' title='Flea & Wormer Treatments' class='btn white butsave' data-sameproid='".$r->id."' data-sameproname='".$r->title."' data-samecatid='".$r->category_id."'>Add To Cart</a></p>
					</div></div>
         ";
                }
				echo "</div> <div class='MS-controls'>
        <button class='MS-left sample'><i class='fa fa-angle-left' aria-hidden='true'></i></button>
        <button class='MS-right sample'><i class='fa fa-angle-right' aria-hidden='true'></i></button>
      </div>";
                
                ?>
              
    
    </div>

  </section>

  

    <!--------------------- starting 2 slider  --------------------------->
 <section>
    <div id="mixedSlider3"  class="Slider_one" style="background-color:  #7fc2e5;">
      <h2 style="padding-top: 20px" >Wet Food</h2>
      <div class="MS-content">
        
         
          

                <?php 
                
                $st = $db->prepare("SELECT * FROM sample_product WHERE category_id = 6");
                $st->execute(array());
                $num = $st->rowCount();

                for($x=1;$x<=$num;$x++)	
                {
                    $r = $st->fetchObject();
                    echo "<div class='item'>
					<div class='imgTitle'>
					<img class='sample-img' src = 'images/products/{$r->image}' />
					<h3 class='sample-head'><a href='' title='{$r->title}'>{$r->title}</a></h3>
					<p><a href='javascript:void(0)' title='Flea & Wormer Treatments' class='btn white butsave' data-sameproid='".$r->id."' data-sameproname='".$r->title."'data-samecatid='".$r->category_id."'>Add To Cart</a></p>
					</div></div>";
                }?>
				 </div><div class='MS-controls'>
        <button class='MS-left sample'><i class='fa fa-angle-left' aria-hidden='true'></i></button>
        <button class='MS-right sample'><i class='fa fa-angle-right' aria-hidden='true'></i></button>
      </div>
                
              
   
    </div>

  </section>

    <!--------------------------------------------------- starting 3 slider end -------------------------------------->

     <section>
    <div id="mixedSlider2"  class="Slider_one" style="background-color:  #7fc2e5;">
      <h2 style="padding-top: 20px">Bare Ooddles (Raw)</h2>
      <div class="MS-content">
        
         
          

                <?php 
                
                $st = $db->prepare("SELECT * FROM sample_product WHERE category_id = 5");
                $st->execute(array());
                $num = $st->rowCount();

                for($x=1;$x<=$num;$x++)	
                {
                    $r = $st->fetchObject();
                    echo "<div class='item'>
					<div class='imgTitle'>
					<img class='sample-img' src = 'images/products/{$r->image}' />
					<h3 class='sample-head'><a href='' title='{$r->title}'>{$r->title}</a></h3>
					<p><a href='javascript:void(0)' title='Flea & Wormer Treatments' class='btn white butsave' data-sameproid='".$r->id."' data-sameproname='".$r->title."'data-samecatid='".$r->category_id."'>Add To Cart</a></p>
					</div></div>";
                } ?>
				</div> <div class='MS-controls'>
        <button class='MS-left sample'><i class='fa fa-angle-left' aria-hidden='true'></i></button>
        <button class='MS-right sample'><i class='fa fa-angle-right' aria-hidden='true'></i></button>
      </div>
                
                
    
    </div>

  </section>
    <!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script> -->
    <script src="/js/multislider.js"></script>
  
    <script>

      $('#mixedSlider').multislider({
        duration: 750,
        interval: 30000
      });

    
      $('#mixedSlider2').multislider({
        duration: 750,
        interval: 30000
      });

      $('#mixedSlider3').multislider({
        duration: 750,
        interval: 30000
      });

    </script>

<script>
$(document).ready(function() {
  $('.butsave').on('click', function(e) {
    e.preventDefault();
    
    var sam_product_id = $(this).data('sameproid');
    var sam_cat_id = $(this).data('samecatid');
    var sam_proname = $(this).data('sameproname');
    //alert(sam_product_id);
    if(sam_product_id!="" && sam_cat_id!="" ){
      $.ajax({
        url: "ajaxaddtocart.php",
        type: "POST",
        data: {
          sam_product_id: sam_product_id,
          sam_proname: sam_proname,
          sam_cat_id: sam_cat_id        
        },
        cache: false,
        success: function(dataResult){         
            $("#butsave").removeAttr("disabled");
            $('#fupForm').find('input:text').val('');
            $("#status").show(); 
             setTimeout(function() { 
                    $('#status').fadeOut('fast'); 
                }, 2000);  
            $("#cartreload").html(dataResult);               
          
          
        }
      });
    }
    else{
      alert('Please select product !');
    }
  });

  //delete ajax

  $(document).on('click','.delete',function(){
    var catid = $(this).data('catid');
    var proid = $(this).data('proid');
   
 
   var confirmalert = confirm("Are you sure?");
   if (confirmalert == true) {
     
      $.ajax({
        url: 'cartload.php',
        type: 'POST',
        data: { 
          catid: catid,
          proid: proid },
        success: function(response){

           $("#cartreload").html(response);

       /*   if(response == 1){
      $(el).closest('tr').css('background','tomato');
      $(el).closest('tr').fadeOut(800,function(){
         $(this).remove();
      });
          }else{
      alert('Invalid ID.');
          }*/

        }
      });
   }

}); 


});
</script>

</body>

</html>

<?php
include "includes/company.php";
include "footer.php"; ?>