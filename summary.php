<? include "header.php"; 


    ?>
    <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Your Ooddles Order</h1>
       
        
    </div><!--close headline-->
    
</div><!--close banner-->
<section>

    <?
    
    if($cart->qty == 0){
        ?>
        <div class="flex empty">
        <div class="c_33">
        </div>
        <div class="c_66">
            <div class="inner">
            <h3>What's going on?</h3>
            <p>You've got an empty basket. We've got a ton of tasty treats for your beloved pooch.<br />
                Use the menu at the top to browse our wide range of food.<br />
            Alternatively, you can email us on <a href="mailto:<? echo $company->email; ?>" title="Email <? echo $company->name; ?>"><? echo $company->email; ?></a> and we'll be happy to assist you.</p>
            </div>
            </div>
        </div>
        <?
    }else{
        ?>
    <div class="flex negative">
        <div class="c_50"><div class="inner">
            <p class="total">Your Order: <span>&pound;<? echo number_format(($cart->total-$cart->discount-$discount+$delivery_price),2); ?></span></p>
            
            
            <div class="flex negative summary">
                <div class="c_50"><div class="inner">
                    <h3>Billing Details</h3>
                     <p><strong><? echo $customer->name." ".$customer->surname; ?></strong><br />
                         <? echo $address; ?></p>
                    <p><strong>Telephone:</strong> <? echo $customer->telephone; ?><br />
                                    <strong>Email:</strong> <? echo $customer->email; ?></p>
                </div></div>
                
            <div class="c_50"><div class="inner">
            <h3>Shipping Details</h3>
            <p><strong><? echo $delivery->d_name." ".$delivery->d_surname; ?></strong><br />
                <? echo $delivery_address; ?></p>
            <p><strong>Telephone:</strong> <? echo $delivery->d_telephone; ?><br />
                            <strong>Email:</strong> <? echo $delivery->d_email; ?></p>
            </div></div>
                </div>
            
                <input name="ooddler" type="hidden" value="<? echo $_SESSION['ooddler']; ?>" />
            <?
        if(!empty($_SESSION['account_password'])){
            echo "<h3>Ooddles Account</h3><p>An account will be created with your chosen password.</p>";
        }
          
        
        if(($cart->total-$cart->discount-$discount+$delivery_price) == 0){
            ?>
             <p><a href="/actions.php?action=complete" title="Complete your order" class="btn brown check">Complete your order</a></p>
            <?
        }else{
        ?>
            <!--<p><input name="accept" type="checkbox" value="1" /> Please confirm you have read and accept our <a href="/privacy-policy" title="Terms and Conditions / Privacy Policy">Terms and Conditions</a>.</p>-->
                <div id="payment_container">
                  <div id="loading"><div class="inner"></div></div>
                    
                    
            <form action="/charge" method="post" id="payment-form">
            <div class="form-row">
                    <img src="/images/button-stripe.png" alt="Stripe" />
                  <label for="card-element">
                    Pay Securely with Stripe
                  </label>
                  <div id="card-element">
                   
                  </div>

                 
                  <div id="card-errors" role="alert"></div>
                </div>

                <button data-secret="<?= $payment->client_secret; ?>">Submit Payment</button>
              </form> 
                    <a href="https://stripe.com/en-gb" title="Stripe" target="_blank">What is Stripe?</a>
                  </div><!--close payment_container-->
                
            <p>Alternatively, you can pay with Paypal:</p>
            <p><a href="/actions.php?action=process-payment&option=paypal&id=<? echo $cart_hash; ?>" title="Pay with Paypal"><img src="/images/button-paypal.png" alt="Paypal" class="paypal" /></a></p> 
            
     <? } ?>
           
            
            </div></div>
        <div class="c_50"><div class="inner">
             <table class="table-responsive-full order_table">
      <thead>
        <tr>
          <th>Product</th>
          <th>Description</th>
          <th>Unit Price</th>
            <th>Quantity</th>
            <th>Subtotal</th>
        </tr>
      </thead>
      <tbody>
          <? 
         
                $sd = $db->prepare("SELECT s.title, c.basket_data,c.price, SUM(c.price) AS subtotal, COUNT(c.id) AS qty, p.title AS product_title, i.category, p.image, c.auto_repeat, s.quantity, s.weight, s.unit, c.discount, SUM(c.discount) AS overall_discount
                                    FROM cart c
                                    LEFT JOIN skus s ON c.sku_id = s.id
                                    LEFT JOIN products p ON s.product_id = p.id
                                    LEFT JOIN categories i ON p.category_id = i.id
                                    LEFT JOIN orders o ON c.order_id = o.id
                                    WHERE o.session_id = ? AND o.status = ?
                                    GROUP BY c.sku_id, c.auto_repeat");
                $sd->execute(array(session_id(), "Unpaid"));
                while($d = $sd->fetchObject()){
                    echo "<tr>
                          <td>";
                    if(!empty($d->image)){
                        echo "<img src='/thumb.php?src=/images/products/{$d->image}&w=80&h=80&zc=2' alt='{$d->title}' />";
                    }
                    echo "</td>";
                   
                    echo "<td>";
                    //echo $d->basket_data;exit;
                     if($d->basket_data){
                      echo "<strong>Sample Offer Bundle</strong><br />";
                    $restdata=unserialize($d->basket_data);

                    foreach ($restdata as $key => $value) {
                      //print_r($value);  exit;                    

                      echo $value['product'];
                      
                    }

                  }
                  else{
                       echo "<strong>{$d->product_title}</strong><br />
                          {$d->quantity} x {$d->weight}".strtolower($d->unit)." {$d->title} ";
                      }
                   echo "</td>";
                     echo"<td>&pound;".number_format($d->price,2)."</td>
                          <td>{$d->qty}</td>
                          <td>";
                    if($d->overall_discount > 0){
                       echo "&pound;".number_format(($d->subtotal-$d->overall_discount),2)." <span class='linethrough'>&pound;".number_format($d->subtotal,2)."</span>"; 
                    }else{
                        echo "&pound;".number_format($d->subtotal,2);    
                    }
                    if(!empty($d->auto_repeat)){
                        echo "<br /><span class='repeat'>Auto Repeat {$d->auto_repeat}</span>";
                    }
                    echo "</td>
                          </tr>";
                }
            ?>
      </tbody>
  </table>
            <div class="totals">
                <div class="flex negative">
                    <div class="c_50"><div class="inner">
                <p><strong>Subtotal:</strong> &pound;<? echo number_format(($cart->total-$cart->discount),2); ?><br />
                    <strong>Discount:</strong> &pound;<? echo number_format($discount,2); ?><br />
                    <strong>Delivery:</strong> &pound;<? echo number_format($delivery_price,2); ?><br />
                        <strong>Total:</strong> &pound;<? echo number_format(($cart->total-$cart->discount-$discount+$delivery_price),2); ?></p>
                        </div></div>
                    <div class="c_50"><div class="inner"></div></div>
                </div>
            </div><!--close totals-->
            <div class="help">
                <p>If you need any help or assistance with your order, please email us on <a href="mailto:<? echo $company->email; ?>" title="Email <? echo $company->name; ?>"><? echo $company->email; ?></a>.</p>
                
            </div><!--close help-->
             <img src="/images/payment-logos.jpg" alt="Payment Logos" class="payments" />
            </div></div>
    </div>
            
   
    <?
    }
    ?>
    </section>
<?

include "includes/company.php";
include "footer.php"; ?>