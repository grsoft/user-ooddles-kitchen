<? include "header.php"; 


    ?>
    <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1><? echo $xx->category; ?></h1>
        <div class="c_75"><p><? echo $xx->strapline; ?></p></div>
        
    </div><!--close headline-->
    
</div><!--close banner-->
<section>
    <div id="breadcrumbs">
        <ul class="flex" itemscope itemtype='http://schema.org/BreadcrumbList'>
            <li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem'>
                <meta itemprop="position" content="1" />
                <a href="/" title="<? echo $company->name; ?>" itemtype='http://schema.org/Thing' itemprop='item' id="<? echo MAIN_SITE; ?>"><span itemprop='name'>Home</span></a>
            <meta itemprop="url" content="<? echo MAIN_SITE; ?>" /></li>
            <li>&rang;</li>
            <li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem'>
                <meta itemprop="position" content="2" />
                <a href="/our-foods" title="Our Foods" itemtype='http://schema.org/Thing' itemprop='item' id="<? echo MAIN_SITE; ?>/our-foods">Our Foods</a>
            <meta itemprop="name" content="Dog Food" />
            <meta itemprop="url" content="<? echo MAIN_SITE; ?>/our-foods" /></li>
            <li>&rang;</li>
            <li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem'>
                <meta itemprop="position" content="3" />
                <a href="/our-foods/<? echo $_GET['id']; ?>" title="<? echo $xx->category; ?>" itemtype='http://schema.org/Thing' itemprop='item' id="<? echo MAIN_SITE; ?>/our-foods/<? echo $_GET['id']; ?>"><span itemprop='name'><? echo $xx->category; ?></span></a>
            <meta itemprop="url" content="<? echo MAIN_SITE; ?>/our-foods/<? echo $_GET['id']; ?>" />
            </li>
            
            
        </ul>
        </div>
                
    </section>
<? if(!empty($xx->image)){ ?><img src="/images/products/<? echo $xx->image; ?>" alt="<? echo $xx->category; ?>" class="placeholder" /><? } ?>





  <section class="centre">
      
      <? 
      $st = $db->prepare("SELECT * FROM videos WHERE category_id = ?");
      $st->execute(array($xx->id));
      if($st->rowCount() > 0){
          $r = $st->fetchObject();
          echo "<a href='#' title='Watch Video' data-id='{$r->id}' class='video_link'><img src='/images/{$r->image}' alt='Video' /><span></span></a></section><div id='video'></div><section class='centre'>";
      }
?>
      
      <? echo str_replace(array("<p>&nbsp;</p>", "<p></p>"), "", $xx->description); ?>
      <? 
        if($xx->id > 5){
            ?>
            <p class="start centre"><a href="/shop" title="Go Shopping" class="btn brown">Go Shopping</a></p>
            <?
        }else{
            ?>
          <p class="start centre"><a href="/shop" title="Go Shopping" class="btn brown">Go Shopping</a></p>
            <p class="start centre"><br /><a href="/get-ooddling" title="Start Ooddling" class="btn brown">Not sure which food or need a cost per day? Start Ooddling HERE</a><br /><br /></p>
            <?
        }
        ?>
                            
    </section>
<?
            $st = $db->prepare("SELECT p.title, p.image, r.title AS range_title, p.image, p.introduction, p.description, p.composition, p.id, p.nutrition, p.constituents, p.food_type, p.seo, r.seo AS range_seo, c.seo AS category_seo
                                FROM products p
                                LEFT JOIN product_ranges r ON p.range_id = r.id 
                                LEFT JOIN categories c ON p.category_id = c.id 
                                WHERE c.seo = ? AND p.status = ? ORDER BY RAND()");
            $st->execute(array($_GET['id'], "Published"));
            $num = $st->rowCount();
            if($num > 0 && $xx->id == 99999999){
                ?>
                    
                        <div class="product_list light_blue shop products">
                  <h2>Ooddles of Flavours</h2>
                 
                            <div class="flex negative">
						<? 
                            for($x=1; $x <= $num; $x++){
                                $r = $st->fetchObject();
                                echo "<div class='c_25'>
                                      <div class='inner item'>
                                      <div class='product_img'><a href='/shop/{$r->category_seo}/{$r->seo}' title='Buy Now'><img src='/thumb.php?src=/images/products/{$r->image}&w=500&h=500&zc=2' alt='{$r->title}' /></a></div>
                                      <h3><a href='/shop/{$r->category_seo}/{$r->seo}' title='Buy Now'>{$r->title}</a></h3>
                                      <p>{$r->introduction}</p>
                                      <p><a href='/shop/{$r->category_seo}/{$r->seo}' title='Buy Now' class='btn'>Buy</a></p>
                                      </div>
                                      </div>";
                                
                            }
                        ?>
                        </div>
                        </div>
                            
    <?    } ?>

 

<section class="centre">
    
    <div class="flex negative actions">
        <div class="c_33"><div class="inner"><a href="/breeders-and-stockists" title="Breeders and Stockists"><img src="/images/tab-breeders.jpg?v=<? echo rand(); ?>" alt="Breeders and Stockists" /></a></div></div>
        <div class="c_33"><div class="inner"><a href="/get-social" title="Get Social"><img src="/images/tab-social.jpg?v=<? echo rand(); ?>" alt="Get Social" /></a></div></div>
        <div class="c_33"><div class="inner"><a href="/blog" title="Ooddles Blog"><img src="/images/tab-blog.jpg?v=<? echo rand(); ?>" alt="Ooddles Blog" /></a></div></div>
        <div class="c_33"><div class="inner"><a href="/shop" title="Treats &amp; Chews"><img src="/images/tab-treats.jpg?v=<? echo rand(); ?>" alt="Treats &amp; Chews" /></a></div></div>
        <div class="c_33"><div class="inner"><a href="/rewards" title="Refer A Dog"><img src="/images/tab-refer.jpg?v=<? echo rand(); ?>" alt="Refer A Dog" /></a></div></div>
        <div class="c_33"><div class="inner"><a href="/how-it-works" title="How It Works"><img src="/images/tab-works.jpg?v=<? echo rand(); ?>" alt="How It Works" /></a></div></div>
        
    </div>
    
</section>
<?
include "includes/company.php";
include "footer.php"; ?>