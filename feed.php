<?
include "configuration.php";
$fp = fopen('merchant-feed.xml', 'w');
fwrite($fp, '<?xml version="1.0"?>
			 <rss version="2.0" 
			  xmlns:g="http://base.google.com/ns/1.0">
			  <channel>
			  <title>Ooddles Kitchen Product Feed</title>
			  <link>https://www.ooddleskitchen.co.uk</link>
			  <description>Ooddles Kitchen, premium quality dog food made with natural ingredients. Dry food, wet food or raw food, personalised for your dog.</description>');
//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
$st = $db->prepare("SELECT s.base_price, p.seo, s.title, r.title AS range_title, s.quantity, s.weight, s.unit, p.description, r.seo AS range_seo, s.product_code, c.category, c.merchant_category, s.stock, p.image, s.id
                    FROM skus s
					LEFT JOIN products p ON s.product_id = p.id
					LEFT JOIN product_ranges r ON p.range_id = r.id
                    LEFT JOIN categories c ON p.category_id = c.id
					WHERE s.status = ? AND p.status = ? AND r.status = ?");
$st->execute(array("Published","Published","Published"));


while($x = $st->fetchObject()){
	
	if(empty($x->description)){
		$description = "<![CDATA[".str_replace(array("&mdash;", "&rsquo;", "Ê"), "", strip_tags($r->description))."]]>";
	}else{
		$description = "<![CDATA[".str_replace(array("&mdash;", "&rsquo;", "Ê"), "", strip_tags($x->description))."]]>";
	}
	
	if($x->price > $min_spend){
		$shipping = 0;
	}else{
		$shipping = $ship_rate;
	}
	if($x->stock > 0){
        $stock = "in stock";
    }else{
        $stock = "out of stock";
    }
	
	$title = str_replace('&', 'and', $x->title)." (".$x->quantity." x ".$x->weight.strtolower($x->unit).") ".str_replace(array("&", ">"), array("&amp;", "&gt;"),$x->category);
	fwrite($fp, '<item>
				<title>'.$title.'</title>
				<link>https://www.ooddleskitchen.co.uk/shop/'.$x->range_seo.'/'.$x->seo.'</link>
				<g:id>'.$x->id.'</g:id>
				<description>'.$description.'</description>
				<g:image_link>https://www.ooddleskitchen.co.uk/images/products/'.$x->image.'</g:image_link> 
				<g:price>'.$x->base_price.' GBP</g:price> 
				<g:condition>new</g:condition>
				<g:mpn>'.str_replace("&", "&amp;", $x->product_code).'</g:mpn>
				<g:brand>Ooddles Kitchen</g:brand>
				<g:google_product_category>'.str_replace(array("&", ">"), array("&amp;", "&gt;"),$x->merchant_category).'</g:google_product_category>
				<g:product_type>'.str_replace(array("&", ">"), array("&amp;", "&gt;"),$x->merchant_category).' &gt; '.str_replace(array("&", ">"), array("&amp;", "&gt;"),$x->category).'</g:product_type>
				<g:availability>'.$stock.'</g:availability>
				<g:adwords_grouping>'.str_replace(array("&", ">"), array("&amp;", "&gt;"),$x->category).'</g:adwords_grouping>
				<g:shipping>
				<g:service>Courier</g:service>
				<g:price>'.number_format($shipping,2).' GBP</g:price>
				</g:shipping>
				</item>');
}

fwrite($fp, '</channel></rss>');
fclose($fp);
?>