<?
session_start();
include "check-login.php";
require_once('stripe/init.php');
$stripe = new \Stripe\StripeClient($stripe_secret_key);
print '<script async src="https://www.googletagmanager.com/gtag/js?id=G-WZG4K6HNT7"></script>';
//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
include "includes/mail-headers.php";    

include "classes/class.phpmailer.php";
switch($_GET['action']){
        
    case "verify":
        $st = $db->prepare("UPDATE mailing SET status = ? WHERE unique_id = ?");
        $st->execute(array("Verified", $_GET['id']));
        if($st->rowCount() > 0){
            $_SESSION['status'] = "<h4>Success</h4>
                                   <p>Thank you for joining Our Woof News</p>";    
        }else{
            $_SESSION['status'] = "<h4>Error</h4>
                                   <p>Sorry, we could not add you to our Woof News. Please try again.</p>";  
        }
        header("Location: /");
        
    break;
    case "password":
        // Does the email address exist?
            $st = $db->prepare("SELECT * FROM customers WHERE LOWER(email) = ? AND tmp = ?");
            $st->execute(array(str_replace(" ","", strtolower($_POST['email'])),0));
            if($st->rowCount() == 0){
                $_SESSION['status'] = "<h4>Error</h4>
                                       <p>There is not an account with that email address.</p>"; 
                $_SESSION['reset'] = 1;    
            }else{
                $xx = $st->fetchObject();
                // Create a hash
                $date = new DateTime();
                $hash =  $date->getTimestamp();
                
                $su = $db->prepare("UPDATE customers SET reset_id = ? WHERE id = ?");
                $su->execute(array($hash, $xx->id));
                
                $mailer = $mail_header;

				$mailer .= "<tr>
                            <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: Arial, museo sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Password Reset</td>
                            </tr>
                            <tr>
                            <td style='padding:20px; background-color: #fff;'>
							<p style='font-family:Arial, museo sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;; font-size:12px; line-height:150%;'>{$xx->name},<br />A request has been made to reset your password. <br />If this was you, please follow the link below to reset.</p>
                            <p style='font-family:Arial, museo sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;; font-size:12px; line-height:150%;'><a href='".MAIN_SITE."/reset/{$hash}' title='Reset Your Password' style='display:block; padding:8px 10px; background-color: #6c3727; color:#fff; text-transform:uppercase; text-decoration:none; font-weight:bold; text-align:center; font-family:Arial;'>Reset your password</a></p>
                            <p style='font-family:Arial, museo sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;; font-size:12px; line-height:150%;'>If this wasn't requested by you, please ignore this email - nothing has changed. If you require any further assistance please contact us on <a href='mailto:{$company->email}' title='Email {$company->name}'>{$company->email}</a>.</p>
                            </td>
                            </tr>";

                $mailer .= $mail_footer;

                $email = new PHPMailer();
                $email->From = $company->email;
                $email->FromName = $company->name;

                $email->Subject = $company->name." Password Reset";
                $email->Body = $mailer;
                $email->IsHTML(true);

                $email->AddAddress($_POST['email']);
                //$email->AddAddress("david.oldfield@brainstormdesign.co.uk");
                $email->Send();
                $_SESSION['status'] = "<h4>Success</h4>
                                       <p>An email has been sent to your account to reset your password.</p>";
                
                
            }
            header("Location: ".$_POST['url']);
    break;
        
    case "refer":
        if(!$_POST['name'] | !$_POST['email']){
            $_SESSION['status'] = "<h4>Error</h4>
                                   <p>Please add both the name and email address.</p>";
        }else{
            $st = $db->prepare("SELECT discount_code FROM discount_codes WHERE type_id = ?");
            $st->execute(array("First Order"));
            $x = $st->fetchObject();
            
            $mailer = $mail_header;

            $mailer .= "<tr>
                        <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: Arial, museo sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Join us and start Ooddling!</td>
                        </tr>
                        <tr>
                        <td style='padding:20px; background-color: #fff;'>
                        <p style='font-family:Arial, museo sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;; font-size:12px; line-height:150%;'>Hello ".$_POST['name']."<br />
                        I am referring you so you can enjoy 50% off your first order for Ooddles.  Please enter my referral code <strong>{$x->discount_code}</strong> at checkout  at the Ooddles website. You can only use it once.<br /><br />
                        <a href='https://www.ooddleskitchen.co.uk/refer/{$customer->unique_id}' title='Ooddles' style='color: #6c3727; font-weight: bold; text-decoration: none;'>www.ooddleskitchen.co.uk</a><br /><br />
                        When you order, you too will be allocated your own referral code and you can share with your doggie friends and family.<br /><br />
                        Happy Ooddling.</p>
                        </td>
                        </tr>";

            $mailer .= $mail_footer;

            $email = new PHPMailer();
            $email->From = $company->email;
            $email->FromName = $company->name;

            $email->Subject = "You have been referred - 50% off Promo Code for Ooddles";
            $email->Body = $mailer;
            $email->IsHTML(true);

            $email->AddAddress($_POST['email']);
            //$email->AddAddress("david.oldfield@brainstormdesign.co.uk");
            $email->Send();
            $_SESSION['status'] = "<h4>Success</h4>
                                   <p>Your email has been sent.</p>";
        }
        
        header("Location: ".$_POST['url']);
        
    break;
        
    case "remove-pet":
        $_SESSION['why'] = $_POST['why'];
        $_SESSION['other'] = $_POST['other'];
        
        if(empty($_POST['why']) && empty($_POST['email'])){
            $_SESSION['status'] = "<h4>Error</h4>
                                   <p>Sorry, you have not entered a required field. Please try again.</p>";
            header("Location: ".$_POST['url']);
        }else{
            // Remove.
            
            // Email Ooddles.
            $st = $db->prepare("SELECT c.name AS first, c.surname, p.name, p.unique_id, c.email FROM pets p LEFT JOIN customers c ON p.customer_id = c.id WHERE p.unique_id = ? AND customer_id = ?");
            $st->execute(array($_POST['id'],$customer->id));
            $xx = $st->fetchObject();
            
            $mailer = $mail_header;

            $mailer .= "<tr>
                        <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: museo sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Pet Removed</td>
                        </tr>
                        <tr>
                        <td style='padding:20px; background-color: #fff; text-align:center;'>
                        <p style='font-family:museo sans, museo-sans-rounded, Helvetica Neue, Helvetica, Arial, sans-serif;; font-size:13px; line-height:150%;'><strong>{$xx->first} {$xx->surname}</strong> has removed {$xx->name}.
                        <br /><strong>Reason:</strong> ";
            if($_POST['other']){
                $mailer .= $_POST['other'];
            }else{
                $mailer .= $_POST['why'];
            }
            $mailer .= "</p>
                        </td>
                        </tr>";

            $mailer .= $mail_footer;
           

            
            $email = new PHPMailer();
            $email->From = $xx->email;
            $email->FromName = $xx->name." ".$xx->surname;

            $email->Subject = $company->name.": Pet Removed";
            $email->Body = $mailer;
            $email->IsHTML(true);

            //$email->AddAddress($company->name);
            //$email->AddAddress("david.oldfield@brainstormdesign.co.uk");
            $email->Send();
            
            $su = $db->prepare("UPDATE pets SET inactive = ? WHERE unique_id = ?");
            $su->execute(array(1,$_POST['id']));
            
            // Cancel any auto repeat subscriptions
            
            $sd = $db->prepare("SELECT * FROM auto_repeat WHERE pet_id = ?");
            $sd->execute(array($_POST['id']));
            while($d = $sd->fetchObject()){
                try {
                    $stripe->subscriptions->update(
                      $d->subscription_id,
                      ['pause_collection' => array('behavior' => 'void')]
                    );
                }catch (Exception $e) {
                    mail("david.oldfield@brainstormdesign.co.uk", "Error", $e);
                }
                // Cancel at the end of the billing cycle.

                $su = $db->prepare("UPDATE auto_repeat SET status = ? WHERE id = ?");
                $su->execute(array("Inactive", $d->id));
            }
            $_SESSION['status'] = "<h4>Profile Removed</h4>
                                   <p>Thank you for trying Ooddles. We're sorry to see you go.</p>";
            header("Location: /o-hub");
            
        }
    break;
        
    case "mailing":
        $_SESSION['your_name'] = $_POST['name'];
        $_SESSION['your_email'] = $_POST['email'];
        $_SESSION['dog'] = $_POST['dog'];
        $_SESSION['dob'] = $_POST['dob'];
            
        if(!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['dog']) && !empty($_POST['dob'])){
            // Create a hash
                $date = new DateTime();
                $hash =  $date->getTimestamp();
            
            $st = $db->prepare("INSERT INTO mailing (email, name, dog, dob, unique_id) VALUES (?,?,?,?,?)");
            $st->execute(array($_POST['email'], $_POST['name'], $_POST['dog'], $_POST['dob'], $hash));
            
            $mailer = $mail_header;

            $mailer .= "<tr>
                        <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: museo sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Woof News Verification</td>
                        </tr>
                        <tr>
                        <td style='padding:20px; background-color: #fff;'>
                        <p style='font-family:museo sans, museo-sans-rounded, Helvetica Neue, Helvetica, Arial, sans-serif;; font-size:13px; line-height:150%;'>{$_POST['name']},<br />Please verify your email by clicking on the link below.</p>
                        <p><a href='".MAIN_SITE."/verify/{$hash}' title='Reset Your Password' style='display:block; padding:8px 10px; background-color: #6c3727; color:#fff; text-transform:uppercase; text-decoration:none; font-weight:bold; text-align:center;'>Verify your email</a></p>
                        <p style='font-family:museo sans, museo-sans-rounded, Helvetica Neue, Helvetica, Arial, sans-serif;; font-size:13px; line-height:150%;'>If this wasn't requested by you, please ignore this email. If you require any further assistance please contact us on <a href='mailto:{$company->email}' title='Email {$company->name}'>{$company->email}</a>.</p>
                        </td>
                        </tr>";

            $mailer .= $mail_footer;
           

            $admiMmail="info@ooddleskitchen.co.uk";
            //echo $admiMmail;
            $email = new PHPMailer();
            $email->From = $company->email;
            $email->FromName = $company->name;

            $email->Subject = $company->name." Email Verification";
            $email->Body = $mailer;
            $email->IsHTML(true);

            $email->AddAddress($_POST['email']);
            $email->AddAddress($admiMmail);
           // print_r($email);exit;
            $email->Send();
            
            $_SESSION['status'] = "<h4>Success</h4>
                                   <p>Please check your email for verification.</p>";  
            
            unset($_SESSION['your_name']);
            unset($_SESSION['your_email']);
            unset($_SESSION['dog']);
            unset($_SESSION['dob']);
        }else{
            $_SESSION['status'] = "<h4>Error</h4>
                                   <p>Sorry, you have not entered a required field. Please try again.</p>";    
        }
            
        
        header("Location: ".$_POST['url']);
    break;
        
    case "enquiry":
        if($_POST['submit']){
            // Set the sessions

            $_SESSION['name'] = $_POST['name'];
            $_SESSION['company'] = $_POST['company'];
            $_SESSION['email'] = $_POST['email'];
            $_SESSION['telephone'] = $_POST['telephone'];
            $_SESSION['website'] = $_POST['website'];
            $_SESSION['message'] = $_POST['message'];


            if(!$_POST['name'] | !$_POST['email'] | !$_POST['message']){
                $_SESSION['status'] = "<h4>Error</h4><p>You have not entered a required field.</p>";

            }else{
                if($_POST['terms'] != 1){
                    $_SESSION['status'] = "<h4>Error</h4><p>Please read and accept our Privacy Policy.</p>";

                }else{
                    if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                        $_SESSION['status'] = "<h4>Error</h4><p>You have not entered a valid email address.</p>";
                    }else{
                        ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL); 
                        try {
                            $url = 'https://www.google.com/recaptcha/api/siteverify';
                            $data = ['secret'   => $secret_key,
                                     'response' => $_POST['g-recaptcha-response'],
                                     'remoteip' => $_SERVER['REMOTE_ADDR']];

                            $options = [
                                'http' => [
                                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                                    'method'  => 'POST',
                                    'content' => http_build_query($data) 
                                ]
                            ];

                            $context  = stream_context_create($options);
                            $result = file_get_contents($url, false, $context);


                            $res = json_decode($result)->success;
                        }
                        catch (Exception $e) {
                            $res = null;
                        }
                        if($res != 1){
                            $_SESSION['status'] = "<h4>Error</h4><p>Your have not passed the recaptcha verification. Please try again.</p>";
                        }else{
                            $mail_header = "<body bgcolor='#efefef'>
                                            <table cellpadding='0' cellspacing='0' width='650' align='center' style='background-color:#fff; font-family:Helvetica, Georgia, Arial, Helvetica, sans-serif; font-size:12px; padding:0px;'>
                                            <tr>
                                            <td style='padding:0px; text-align:center; background-color: #000; color: #ffffff; border-bottom:2px solid #fff;'><img src='".MAIN_SITE."/images/mail-header-enquiry.jpg?v=".rand()."' alt='Logo' width='100%' /></td>
                                            </tr>
                                            <tr>
                                            <td style='background-color:#fff; color:#6c3727; padding:30px 30px;'>";
                            $mail_footer = "</td>
                                            </tr>
                                            </table>
                                            <table cellpadding='0' cellspacing='0' width='650' align='center' style='background-color:#6c3727; font-family:Helvetica, Georgia, Arial, Helvetica, sans-serif; font-size:12px; padding:0px 0px; margin-top:0px;'>

                                            <tr>
                                            <td style='padding:30px 10px; text-align:center;border-top:2px solid #fff;'>

                                            <p style='font-family:Helvetica, Georgia, Hevetica, Arial, sans-serif; font-size:11px;padding:0px; margin:0px; color:#fff;'><strong>Sent through ".$company->name." Website</strong></p></td>
                                            </tr>
                                            </table>
                                            </body>";

                            // We're good
                            $mail = $mail_header;
                            $mail .= "<h1 style='color:#6c3727; font-size:20px; padding:0px; margin:0px; font-weight:normal; text-transform:uppercase; font-weight:bold;'>{$company->name} Website Enquiry</h1>
                                      <p style='font-family:Helvetica, Georgia, Hevetica, Arial, sans-serif; font-size:13px; line-height:150%;'><strong>Name:</strong> ".$_POST['name']."<br />
                                    <strong>Company:</strong> ".$_POST['company']."<br />
                                    <strong>Email:</strong> <a href='mailto:".$_POST['email']."' style='color:#6c3727'>".$_POST['email']."</a><br />
                                    <strong>Telephone:</strong> ".$_POST['telephone']."<br />

                                    <strong>Website:</strong> <a href='".$_POST['website']."' style='color:#6c3727'>".$_POST['website']."</a><br />
                                    <strong>Message:</strong> ".strip_tags($_POST['message'])."</p>";
                            $mail .= $mail_footer;
                            
                            
                            //include "classes/class.phpmailer.php";
                            $email = new PHPMailer();
							 $email->Host = 'smtp.office365.com';  
                             $email->SMTPAuth = true;                               
                             $email->Username = 'info@ooddleskitchen.co.uk';               
                             $email->Password = 'Marinefish123@1';                           
                             $email->SMTPSecure = 'STARTTLS';                          
                             $email->Port = 587;   
                            $email->From = 'info@ooddleskitchen.co.uk';
							$email->FromName = 'Ooddles';
                            $email->Subject = 'Website Enquiry';
                            $email->Body = $mail;
                            $email->IsHTML(true);
                            //
							
							
							//$email->AddAddress($company->email);
                            $email->AddAddress("rudra31896@gmail.com","rudra");


                            $email->Send();



                            $_SESSION['status'] = "<h4>Success</h4><p>Thank you for your enquiry. We will respond shortly.</p>";

                            unset($_SESSION['name']);
                            unset($_SESSION['company']);
                            unset($_SESSION['email']);
                            unset($_SESSION['telephone']);
                            unset($_SESSION['website']);
                            unset($_SESSION['message']);
                        }

                    }
                }
            }
            header("Location: ".$_POST['url']);

        }
    break;
    
    case "login":
        
        // Usual checks
			if(!$_POST['email'] | !$_POST['password']){
				$_SESSION['status'] = "<h4>Error</h4>
                                    <p>Sorry, you have not entered all required fields. Please try again.</p>";
                $_SESSION['login'] = 1;
				
			}else{
				$auth = do_login(strtolower($_POST['email']), $_POST['password'], $db);
                
                
				if($auth["logged_in"] == 2){
					$_SESSION['unique_id'] = $auth["id"];
					$_SESSION['a_email'] = $auth["email"];
					$_SESSION['password'] = $auth["password"];
					
					// Get the profile details and the delivery details
					$st = $db->prepare("SELECT *
										FROM customers
										WHERE unique_id = :session");
					$st->execute(array("session" => $auth["id"]));
					
					$customer = $st->fetchObject();	
                    
                    // Update the orders with any session items
                    $sf = $db->prepare("UPDATE orders SET customer_id = ? WHERE session_id = ? AND status = ?");
                    $sf->execute(array($customer->id, session_id(), "Unpaid"));
					
                   
					$_SESSION['status'] = "<h4>Success</h4>
                                           <p>Welcome back {$customer->name}. You are now logged in.</p>";
                    
                    
				}else{
					$_SESSION['status'] = "<h4>Error</h4>
                                           <p>Sorry, the details you have entered are incorrect. Please try again.</p>";
                    $_SESSION['login'] = 1;
					
				}
			} 
            if(!empty($_POST['url'])){
                header("Location: ".$_POST['url']);
            }else{
               
            if($_POST['referral'] == "/addresses"){
                header("Location: /addresses");
            }else{
                header("Location: /o-hub");   
            }
                
            }
        
       
    break;
        
    case "add-to-basket":
        
        if($_POST['submit']){
            if(!$_POST['sku']){
                $_SESSION['status'] = "<h4>Error</h4><p>There was a problem adding the item to the basket - please try again.</p>";
            }else{
                //echo session_id();exit;
                // Does an order exist?
                $permitted_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $unque= substr(str_shuffle($permitted_chars), 0, 1);
                $sd = $db->prepare("SELECT * FROM orders WHERE session_id = ? AND status = ?");
                $sd->execute(array(session_id(), "Unpaid"));
                if($sd->rowCount() == 0){
                    $date = new DateTime();
                    $hash =  $date->getTimestamp();
                    
                    $hash = substr((rand()*rand()),0,6).$unque.substr($customer->name,0,1).substr($customer->surname,0,1);
                    
                    
                    
                    $sf = $db->prepare("INSERT INTO orders (created_on, unique_id, session_id, status, customer_id, referrer) VALUES (?,?,?,?,?,?)");
                    $sf->execute(array(date("Y-m-d H:i:s"), $hash, session_id(), "Unpaid", !empty($customer->id) ? $customer->id : "", !empty($_SESSION['referrer']) ? $_SESSION['referrer'] : ""));
                    $basket = $db->lastInsertId();
                    
                   
                }else{
                    $d = $sd->fetchObject();
                    $basket = $d->id;
                }
                
                // Get the sku info
                $sk = $db->prepare("SELECT s.base_price,s.product_id, p.category_id FROM skus s LEFT JOIN products p ON s.product_id = p.id WHERE s.id = ?");
                $sk->execute(array($_POST['sku']));
                $k = $sk->fetchObject();

                if($_POST['qty']){
                    $qty = $_POST['qty'];
                }else{
                    $qty = 1;
                }
                
                // Add to the cart

               // print_r($_SESSION['cart_data']);
                if($k->product_id==333){

                if(isset($_SESSION['cart_data']) && isset($_SESSION['cart_data'][4]))
                {
                    $Firstdata= $_SESSION['cart_data'][4]['product'];
                    }
                if(isset($_SESSION['cart_data']) && isset($_SESSION['cart_data'][5]))
                {
                    $Seconddata= $_SESSION['cart_data'][5]['product'];
                    }
                if(isset($_SESSION['cart_data']) && isset($_SESSION['cart_data'][6]))
                {
                    $Thirddata= $_SESSION['cart_data'][6]['product'];
                    }    

                $basket_data = [
                    [
                        
                        'product'=>$Firstdata,
                    ],
                    [
                        
                        'product'=>$Seconddata,
                    ],
                    [
                        
                        'product'=>$Thirddata,
                    ]
                ];

                  /*  echo "INSERT INTO cart basket_data='".serialize($basket_data)."' ";
                    echo "<br>";
                
                   echo "chk1";exit;*/
            }

            //print_r(serialize($basket_data));exit;

                $sc = $db->prepare("INSERT INTO cart (session_id, sku_id, price, created_on, order_id, auto_repeat, discount, pet_id,basket_data) VALUES (?,?,?,?,?,?,?,?,?)");
                for($x=1; $x <= $qty; $x++){
                   $test= $sc->execute(array(session_id(), 
                    $_POST['sku'], 
                    $k->base_price,
                    date("Y-m-d H:i:s"),
                    $basket, 
                    !empty($_POST['auto']) ? $_POST['auto'] : "", 
                    !empty($_POST['auto']) ? (($k->base_price/100)*$company->auto_repeat_saving) : "", 
                    !empty($_POST['pet_id']) ? $_POST['pet_id'] : "",
                    !empty(($basket_data)) ? serialize($basket_data) : NULL));
                }

               // print_r($test);exit;
                if(!empty($_POST['pet_id']) && $k->category_id == 1){
                    // See if the pet has Wet food.
                    $sp = $db->prepare("SELECT food_types FROM pets WHERE unique_id = ? AND food_types LIKE ?");
                    $sp->execute(array($_POST['pet_id'], "%2%"));
                    if($sp->rowCount() > 0){
                        $_SESSION['popup'] = "wet";
                    }
                }
                $_SESSION['status'] = "<h4>Success</h4><p>You've got Ooddles in the basket.</p>";

            }
            header("Location: ".$_POST['url']);
        }

    break;
        
    case "add-one":
        // How many do we have?
        $sf = $db->prepare("SELECT COUNT(c.id) AS qty, c.sku_id, c.order_id FROM cart c LEFT JOIN orders o ON c.order_id = o.id WHERE o.status = ? AND c.id = ? AND (o.customer_id = ? OR (o.session_id = ? AND o.customer_id = ?))");
        $sf->execute(array("Unpaid", $_GET['id'], $customer->id, session_id(), ""));
        if($sf->rowCount() > 0){
            $f = $sf->fetchObject();
            
            // How many do we have?
            $sw = $db->prepare("SELECT COUNT(c.id) AS qty FROM cart c LEFT JOIN orders o ON c.order_id = o.id WHERE o.id = ? AND c.sku_id = ?");
            $sw->execute(array($f->order_id, $f->sku_id));
            $w = $sw->fetchObject();
            

            // How much stock is there.
            $ss = $db->prepare("SELECT * FROM skus WHERE id = ?");
            $ss->execute(array($f->sku_id));
            $s = $ss->fetchObject();
            
            if(($w->qty+1) > $s->stock){
                $_SESSION['status'] = "<h4>Error</h4><p>Sorry, we do not have enough stock.</p>";
            }else{
                print '<script>
                dataLayer.push({
                    "event": "add_to_cart",
                    "ecommerce": {
                      "items": [{
                        "item_name": "Donut Friday Scented T-Shirt", // Name or ID is required.
                        "item_id": "67890",
                        "price": "33.75",
                        "item_brand": "Google",
                        "item_category": "Apparel",
                        "item_category_2": "Mens",
                        "item_category_3": "Shirts",
                        "item_category_4": "Tshirts",
                        "item_variant": "Black",
                        "item_list_name": "Search Results",
                        "item_list_id": "SR123",
                        "index": 1,
                        "quantity": "2"
                      }]
                    }
                  });
                </script>';
                $sg = $db->prepare("SELECT * FROM cart WHERE id = ?");
                $sg->execute(array($_GET['id']));
                $g = $sg->fetchObject();
                
                $sc = $db->prepare("INSERT INTO cart (session_id, sku_id, price, created_on, order_id, auto_repeat, pet_id) VALUES (?,?,?,?,?,?,?)");
                $sc->execute(array($g->session_id, $g->sku_id, $g->price, date("Y-m-d H:i:s"), $g->order_id, !empty($g->auto_repeat) ? $g->auto_repeat : "", !empty($g->pet_id) ? $g->pet_id : ""));
                $_SESSION['status'] = "<h4>Success</h4><p>You've got Ooddles in the basket.</p>";
            }
        }
        header("Location: /basket");
    break;
        
    case "remove-one":
        // How many do we have?
        $sf = $db->prepare("SELECT COUNT(c.id) AS qty, c.sku_id FROM cart c LEFT JOIN orders o ON c.order_id = o.id WHERE o.status = ? AND c.id = ? AND ((o.session_id = ? AND o.customer_id = ?) OR (o.session_id = ? AND o.customer_id = ?))");
        $sf->execute(array("Unpaid", $_GET['id'], "", $customer->id, session_id(), ""));
        if($sf->rowCount() > 0){
            print '<script>
                dataLayer.push({
                    "event": "remove_from_cart",
                    "ecommerce": {
                    "items": [{
                        "item_name": "Donut Friday Scented T-Shirt", // Name or ID is required.
                        "item_id": "67890",
                        "price": "33.75",
                        "item_brand": "Google",
                        "item_category": "Apparel",
                        "item_variant": "Black",
                        "item_list_name": "Search Results",  // If associated with a list selection.
                        "item_list_id": "SR123",  // If associated with a list selection.
                        "index": 1,  // If associated with a list selection.
                        "quantity": "1"
                    }]
                    }
                });
                </script>';
            $sd = $db->prepare("DELETE FROM cart WHERE id = ?");
            $sd->execute(array($_GET['id']));
            $_SESSION['status'] = "<h4>Success</h4><p>Your item has been removed from the basket.</p>";
        }else{
            $_SESSION['status'] = "<h4>Error</h4><p>Sorry, there was an issue removing the item.</p>";
        }
        header("Location: /basket");
    break;
        
    case "remove-all":
        
        $sf = $db->prepare("DELETE FROM cart WHERE sku_id = ? AND order_id = ?");
        $sf->execute(array($_GET['id'], $cart_id));
        
        
        if($sf->rowCount() > 0){
            print '<script>
                dataLayer.push({
                    "event": "remove_from_cart",
                    "ecommerce": {
                    "items": [{
                        "item_name": "Donut Friday Scented T-Shirt", // Name or ID is required.
                        "item_id": "67890",
                        "price": "33.75",
                        "item_brand": "Google",
                        "item_category": "Apparel",
                        "item_variant": "Black",
                        "item_list_name": "Search Results",  // If associated with a list selection.
                        "item_list_id": "SR123",  // If associated with a list selection.
                        "index": 1,  // If associated with a list selection.
                        "quantity": "1"
                    }]
                    }
                });
                </script>';
            $_SESSION['status'] = "<h4>Success</h4><p>Your items have been removed from the basket.</p>";
        }else{
            $_SESSION['status'] = "<h4>Error</h4><p>Sorry, there was an issue removing the items.</p>";
        }
        header("Location: /basket");
    break;
        
    case "apply-discount":
		
			unset($_SESSION['code']);
			unset($_SESSION['code_id']);
			if(!$_POST['code']){
				$_SESSION['status'] = "<h4>Sorry</h4><p>Sorry, you have not entered a discount code. Please try again.</p>";
				unset($_SESSION['code']);
				unset($_SESSION['code_id']);
				unset($_SESSION['code_percent']);
			}else{
				// The code was entered, does it exist?	
					$st = $db->prepare("SELECT *
					                    FROM discount_codes
										WHERE UPPER(discount_code) = ?");
					$st->execute(array(strtoupper(str_replace(" ", "", $_POST['code']))));
					if($st->rowCount() == 0){
                        if($company->order_code == strtoupper(str_replace(" ", "", $_POST['code']))){
                             $_SESSION['code'] = strtoupper(str_replace(" ", "", $_POST['code']));
                             $_SESSION['status'] = "<h4>Success</h4><p>The discount code is active and has been applied.</p>";
                        }else{
                            $_SESSION['status'] = "<h4>Sorry</h4><p>Sorry, the discount code you have entered does not exist. Please try again.</p>";
                            unset($_SESSION['code']);
                            unset($_SESSION['code_id']);
                            unset($_SESSION['code_percent']);
                        }
					}else{
						// The code exists, is it active?
							$x = $st->fetchObject();
							if($x->inactive == 1 || $x->redeemed == 1 || ($x->expiry_date < date("Y-m-d") && $x->expiry_date != "0000-00-00")){
								$_SESSION['status'] = "<h4>Sorry</h4><p>Sorry, the discount code you have entered has expired. Please try again.</p>";
								unset($_SESSION['code']);
								unset($_SESSION['code_id']);
								unset($_SESSION['code_percent']);
							}else{
								
                                // Make sure we haven't used this code previously.
            
                                $so = $db->prepare("SELECT * FROM orders WHERE customer_id = ? AND status = ? AND discount_code = ?");
                                $so->execute(array($customer->id, "Paid", $x->discount_code));
                                
                                if($so->rowCount() > 0){
                                    $_SESSION['status'] = "<h4>Sorry</h4><p>Sorry, the discount code you have entered has been used previously. Please try again.</p>";
                                    unset($_SESSION['code']);
                                    unset($_SESSION['code_id']);
                                    unset($_SESSION['code_percent']);
                                }else{
            
                                    if(!empty($x->customer_id)){
                                        if($x->customer_id == $customer->id){
                                            $_SESSION['status'] = "<h4>Success</h4><p>The discount code is active and has been applied.</p>";
                                            $_SESSION['code'] = $x->discount_code;
                                            $_SESSION['code_id'] = $x->id;
                                        }else{
                                            $_SESSION['status'] = "<h4>Sorry</h4><p>Sorry, the discount code you have entered is not allocated to you. Please try again.</p>";
                                            unset($_SESSION['code']);
                                            unset($_SESSION['code_id']);
                                            unset($_SESSION['code_percent']);
                                        }
                                    }else{

                                        $_SESSION['status'] = "<h4>Success</h4><p>The discount code is active and has been applied.</p>";
                                        $_SESSION['code'] = $x->discount_code;
                                        $_SESSION['code_id'] = $x->id;
                                    }
                                }
								
                                
							}
					}
				
			}
			header("Location: /basket");
		
	break;
        
    case "remove-code":
        unset($_SESSION['code']);
        unset($_SESSION['code_id']);
        unset($_SESSION['code_percent']);
        header("Location: /basket");
    break;
        
    case "process":
        if($_POST['submit']){
          switch($_POST['stage']){

              case 2:
                  $_SESSION['name'] = $_POST['name'];
                  $_SESSION['type'] = $_POST['type'];
                  if($_POST['breed']){
                      $_SESSION['breed'] = $_POST['breed'];
                      
                  }elseif($_POST['crossbreed']){
                    $_SESSION['breed'] = $_POST['crossbreed'];
                      
                  }
                  
                  if(!$_POST['name'] | !$_POST['type'] || ($_POST['type'] == "Pedigree" && empty($_POST['breed']))){
                      $_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>"; 
                      header("Location: ".$_POST['url']);
                  }else{
                      $parts = array("stage" => 3);
                      header("Location: /get-ooddling?".http_build_query($parts));
                  }
              break;

              case 3:
                  $_SESSION['size'] = $_POST['size'];

                  if(!$_POST['size']){
                      $_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>"; 
                      header("Location: ".$_POST['url']);
                  }else{
                      $parts = array("stage" => 4);
                      header("Location: /get-ooddling?".http_build_query($parts));
                  }
              break;

              case 4:
                  if(!empty($_POST['stones'])){
                      
                      $pounds = (($_POST['stones']*14)+$_POST['lbs']);
                      $_SESSION['stones'] = $_POST['stones'];
                      $_SESSION['lbs'] = $_POST['lbs'];
                      $_SESSION['weight'] = $pounds;
                  }else{
                      $_SESSION['weight'] = $_POST['weight'];    
                  }
                  
                  

                  if(!$_SESSION['weight']){
                      $_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>"; 
                      header("Location: ".$_POST['url']);
                  }else{
                      $parts = array("stage" => 5);
                      header("Location: /get-ooddling?".http_build_query($parts));
                  }
              break;

              case 5:
                  $_SESSION['condition'] = $_POST['condition'];

                  if(!$_POST['condition']){
                      $_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>"; 
                      header("Location: ".$_POST['url']);
                  }else{
                      if($_SESSION['mode'] == "edit"){
                          $parts = array("stage" => 6);
                      }else{
                          $parts = array("stage" => 6);
                      }
                      
                      header("Location: /get-ooddling?".http_build_query($parts));
                  }
              break;

              case 6:
                  $_SESSION['years'] = $_POST['years'];
                  $_SESSION['months'] = $_POST['months'];
                  $_SESSION['birth_day'] = $_POST['birth_day'];
                  $_SESSION['birth_month'] = $_POST['birth_month'];

                  if(!$_POST['birth_day'] | !$_POST['birth_month']){
                      $_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>"; 
                      header("Location: ".$_POST['url']);
                  }else{
                      $parts = array("stage" => 7);
                      header("Location: /get-ooddling?".http_build_query($parts));
                  }
              break;

              case 7:
                  $_SESSION['gender'] = $_POST['gender'];
                  $_SESSION['spayed'] = $_POST['spayed'];
                  $_SESSION['pregnant'] = $_POST['pregnant'];
                  $_SESSION['neutered'] = $_POST['neutered'];

                  if(!$_POST['gender']){
                      $_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>"; 
                      header("Location: ".$_POST['url']);
                  }else{
                      if(($_POST['gender'] == "Male" && empty($_POST['neutered'])) || ($_POST['gender'] == "Female" && (empty($_POST['spayed']) || empty($_POST['pregnant'])))){
                          $_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>"; 
                          header("Location: ".$_POST['url']);
                      }else{
                          $parts = array("stage" => 8);
                          header("Location: /get-ooddling?".http_build_query($parts));
                      }
                  }
              break;

              case 8:
                  $_SESSION['health'] = $_POST['health'];
                  if($_SESSION['mode'] == "edit"){
                      $parts = array("stage" => 10);
                  }else{
                      $parts = array("stage" => 9);
                  }
                  header("Location: /get-ooddling?".http_build_query($parts));
                  
              break;

              case 9:
                  $_SESSION['ingredients'] = $_POST['ingredients'];
                  $_SESSION['ingredients_list'] = $_POST['ingredients_list'];

                  if(!$_POST['ingredients'] || ($_POST['ingredients'] == "Yes" && empty($_POST['ingredients_list']))){
                      $_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>"; 
                      header("Location: ".$_POST['url']);
                  }else{
                      $parts = array("stage" => 10);
                      header("Location: /get-ooddling?".http_build_query($parts));
                  }
              break;
                  
              case 10:
                $_SESSION['food'] = $_POST['food'];

                  if(!$_POST['food']){
                      $_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>"; 
                      header("Location: ".$_POST['url']);
                  }else{  
                      $parts = array("stage" => 11);
                      header("Location: /get-ooddling?".http_build_query($parts));
                  }
              break;

              /*case 11:
                  $_SESSION['conditions'] = $_POST['conditions'];

                  //if(!$_POST['conditions']){
                      //$_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>"; 
                      //header("Location: ".$_POST['url']);
                  //}else{
                      $parts = array("stage" => 12);
                      header("Location: /get-ooddling?".http_build_query($parts));
                      
                  //}
              break;*/
                  
              case 11:
                  
                  $_SESSION['activity'] = $_POST['activity'];

                  if(!$_POST['activity']){
                      $_SESSION['status'] = "<h4>Error</h4><p>Please agree to the disclaimer.</p>"; 
                      header("Location: ".$_POST['url']);
                  }else{
                      
                      if($_SESSION['mode'] != "edit"){
                          if(empty($customer->id)){
                              // Create our profile
                              $sk = $db->prepare("SELECT * FROM customers WHERE session_id = ?");
                              $sk->execute(array(session_id()));
                              if($sk->rowCount() == 0){
                                  $date = new DateTime();
                                  $hash =  $date->getTimestamp();

                                  // Insert the customer
                                      $sc = $db->prepare("INSERT INTO customers (name, surname, email, mailing_list, session_id, created_on, unique_id, account_type, tmp) VALUES (?,?,?,?,?,?,?,?,?)");
                                      $sc->execute(array($_SESSION['first'],$_SESSION['surname'],$_SESSION['email'],$_SESSION['mailing'],session_id(),date("Y-m-d H:i:s"),$hash, "Consumer",1));
                                      $customer = $db->lastInsertId();
                              }else{
                                      $k = $sk->fetchObject();
                                  // Update
                                      $su = $db->prepare("UPDATE customers SET email = ?, mailing_list = ? WHERE session_id = ?");
                                      $su->execute(array($_SESSION['email'],$_SESSION['mailing'],session_id()));
                                      $customer = $k->id;
                              }
                          }else{
                              $customer = $customer->id;
                              $date = new DateTime();
                              $hash =  $date->getTimestamp();
                          }
                      }
                      
                      if($_SESSION['food'] == "all"){
                          $sf = $db->prepare("SELECT * FROM categories WHERE id < ?");
                          $sf->execute(array(6));
                          $cat = array();
                          while($f = $sf->fetchObject()){
                              $cat[] = $f->id;
                          }
                          $food = implode(",", $cat);
                      }else{
                          $food = explode(",", $_SESSION['food']);
                          
                          if(in_array("mixed", $food)){
                              $food[] = 1;
                              $food[] = 2;
                          }
                          if (($key = array_search('mixed', $food)) !== false) {
                            unset($food[$key]);
                          }
                          $food = array_unique($food);
                          $food = implode(",", $food);
                      }
                      
                      
                      $months = (($_SESSION['years']*12)+$_SESSION['months']);
                      $birth_year = date("Y", strtotime("-".$months." months"));
                      
                      
                      
                      // Create our pet profile

                      $sl = $db->prepare("SELECT * FROM pets WHERE session_id = ?");
                      $sl->execute(array(session_id()));
                      if($sl->rowCount() == 0 && empty($_SESSION['mode'])){
                          // Insert

                          $unique_id = $hash.$customer;

                          $sg = $db->prepare("INSERT INTO pets (customer_id, name, breed, type_id, size, weight, physical_condition, age, birthday, gender, spayed, pregnant, neutered, health_issues, conditions, dislikes, food_types, activity_id, unique_id, created_on, session_id, birth_year, birth_month) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                          $sg->execute(array($customer, !empty($_SESSION['name']) ? $_SESSION['name'] : "", !empty($_SESSION['breed']) ? $_SESSION['breed'] : "", !empty($_SESSION['type']) ? $_SESSION['type'] : "", !empty($_SESSION['size']) ? $_SESSION['size'] : "", !empty($_SESSION['weight']) ? $_SESSION['weight'] : "", !empty($_SESSION['condition']) ? $_SESSION['condition'] : "", !empty($_SESSION['years']) ? $_SESSION['years'].":".$_SESSION['months'] : "", !empty($_SESSION['birth_day']) ? $_SESSION['birth_day'].":".date("m",strtotime($_SESSION['birth_month'])) : "", !empty($_SESSION['gender']) ? $_SESSION['gender'] : "", !empty($_SESSION['spayed']) ? $_SESSION['spayed'] : "", !empty($_SESSION['pregnant']) ? $_SESSION['pregnant'] : "", !empty($_SESSION['neutered']) ? $_SESSION['neutered'] : "", !empty($_SESSION['health']) ? $_SESSION['health'] : "", !empty($conditions) ? $conditions : "", !empty($_SESSION['ingredients_list']) ? $_SESSION['ingredients_list'] : "", !empty($food) ? $food : "", !empty($_SESSION['activity']) ? $_SESSION['activity'] : "", $unique_id, date("Y-m-d H:i:s"), session_id(), !empty($birth_year) ? $birth_year : "", !empty(date("m",strtotime($_SESSION['birth_month']))) ? date("m",strtotime($_SESSION['birth_month'])) : ""));
                      }else{
                          // Update the pet
                          $l = $sl->fetchObject();
                          $unique_id = $l->unique_id;

                          $sp = $db->prepare("UPDATE pets SET name = ?, breed = ?, type_id = ?, size = ?, weight = ?, physical_condition = ?, age = ?, birthday = ?, gender = ?, spayed = ?, pregnant = ?, neutered = ?, health_issues = ?, conditions = ?, dislikes = ?, food_types = ?, activity_id = ?, birth_year = ?, birth_month = ? WHERE session_id = ?");
                          $sp->execute(array(!empty($_SESSION['name']) ? $_SESSION['name'] : "", !empty($_SESSION['breed']) ? $_SESSION['breed'] : "", !empty($_SESSION['type']) ? $_SESSION['type'] : "", !empty($_SESSION['size']) ? $_SESSION['size'] : "", !empty($_SESSION['weight']) ? $_SESSION['weight'] : "", !empty($_SESSION['condition']) ? $_SESSION['condition'] : "", !empty($_SESSION['years']) ? $_SESSION['years'].":".$_SESSION['months'] : "", !empty($_SESSION['birth_day']) ? $_SESSION['birth_day'].":".date("m",strtotime($_SESSION['birth_month'])) : "", !empty($_SESSION['gender']) ? $_SESSION['gender'] : "", !empty($_SESSION['spayed']) ? $_SESSION['spayed'] : "", !empty($_SESSION['pregnant']) ? $_SESSION['pregnant'] : "", !empty($_SESSION['neutered']) ? $_SESSION['neutered'] : "", !empty($_SESSION['health']) ? $_SESSION['health'] : "", !empty($_SESSION['conditions']) ? $_SESSION['conditions'] : "", !empty($_SESSION['ingredients_list']) ? $_SESSION['ingredients_list'] : "", !empty($food) ? $food : "", !empty($_SESSION['activity']) ? $_SESSION['activity'] : "", !empty($birth_year) ? $birth_year : "", !empty(date("m",strtotime($_SESSION['birth_month']))) ? date("m",strtotime($_SESSION['birth_month'])) : "", session_id()));
                      }
                      
                      if($_SESSION['mode'] == "edit"){
                          header("Location: /o-hub/pets/".$l->unique_id);   
                      }else{
                          $_SESSION['ooddler'] = 1;
                          header("Location: /get-ooddling/".$unique_id);    
                      }
                      
                  }
                  
              break;
                  
              /*case 13:
                  
                  $_SESSION['disclaimer'] = $_POST['disclaimer'];

                  if(!$_POST['disclaimer']){
                      $_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>"; 
                      header("Location: ".$_POST['url']);
                  }else{
                      
                  }
                  
                  
              break;*/

              default:
                  $_SESSION['first'] = $_POST['first'];
                  $_SESSION['surname'] = $_POST['surname'];
                  $_SESSION['email'] = $_POST['email'];
                  $_SESSION['mailing'] = $_POST['mailing'];
                  if(!$_POST['email'] | !$_POST['mailing']){
                      $_SESSION['status'] = "<h4>Error</h4><p>Please complete all fields.</p>";
                      header("Location: ".$_POST['url']);
                  }else{
                      // Check to see a customer exists
                      //$sf = $db->prepare("SELECT * FROM customers WHERE email = ? AND inactive = ? AND tmp != ?");
                     $sf = $db->prepare("SELECT * FROM customers WHERE email = '".$_POST['email']."' ");
                      //$sf->execute(array(str_replace(" ", "", strtolower($_POST['email'])),0,1));
                     $sf->execute();
                      if($sf->rowCount() == 0){
                          // Stage 2
                          $parts = array("stage" => 2);
                          header("Location: /get-ooddling?".http_build_query($parts));   
                      }else{
                          $_SESSION['status'] = "<h4>Error</h4><p>There is already an account associated with that email address. Please login.</p>";
                          header("Location: ".$_POST['url']);
                      }

                  }
              break;
          }
        }    
    break;
        
    case "update-details":
        if($_POST['submit'] && $logged_in == 2){

            // $m= $_POST['country'];
            // echo $m; exit;
            
            $_SESSION['name'] = $_POST['name'];
            $_SESSION['surname'] = $_POST['surname'];
            $_SESSION['address'] = $_POST['address'];
            $_SESSION['town'] = $_POST['town'];
            $_SESSION['city'] = $_POST['city'];
            $_SESSION['county'] = $_POST['county'];
            $_SESSION['postcode'] = $_POST['postcode'];
            $_SESSION['email'] = $_POST['email'];
            $_SESSION['telephone'] = $_POST['telephone'];
            $_SESSION['country'] = $_POST['country'];
            
            if(!$_POST['name'] | !$_POST['surname'] | !$_POST['address'] | !$_POST['town'] | !$_POST['city'] | !$_POST['postcode'] | !$_POST['email'] | !$_POST['telephone'] | !$_POST['county'] | !$_POST['country']){
                $_SESSION['status'] = "<h4>Error</h4>
                                       <p>You have not entered a required field.</p>"; 
                header("Location: /o-hub/edit-details");
            }else{
                // Check the email format
                if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                    $_SESSION['status'] = "<h4>Error</h4>
                                           <p>The email format you have entered is not valid.</p>";  
                    header("Location: /o-hub/edit-details");
                }else{ 
                    
                    
                   
                    $sp = $db->prepare("UPDATE customers SET name = ?, surname = ?, email = ?, address = ?, town = ?, city = ?, postcode = ?, county = ?, country = ?, telephone = ?, mailing_list = ? WHERE id = ?");
                    $sp->execute(array(
                        stripslashes(ucwords(strtolower($_POST['name']))),
                        stripslashes(ucwords(strtolower($_POST['surname']))),
                        stripslashes(str_replace(" ", "", $_POST['email'])),
                        stripslashes(ucwords(strtolower($_POST['address']))),
                        stripslashes(ucwords(strtolower($_POST['town']))),
                        stripslashes(ucwords(strtolower($_POST['city']))),
                        strtoupper($_POST['postcode']),
                        stripslashes(ucwords(strtolower($_POST['county']))),
                        $_POST['country'],
                        $_POST['telephone'],
                        !empty($_POST['mailing']) ? $_POST['mailing'] : "",
                        $customer->id
                    ));
                    
                    unset($_SESSION['name']);
                    unset($_SESSION['surname']);
                    unset($_SESSION['address']);
                    unset($_SESSION['town']);
                    unset($_SESSION['city']);
                    unset($_SESSION['county']);
                    unset($_SESSION['postcode']);
                    unset($_SESSION['email']);
                    unset($_SESSION['telephone']);
                    unset($_SESSION['country']);
                    
                    $_SESSION['status'] = "<h4>Success</h4>
                                           <p>Your details have been updated.</p>"; 
                    header("Location: /o-hub");
                }
            }
            
        }
        
    break;
        
    case "update-password":
        if($_POST['submit']){
            // Get the account
            $secure = new SecureHash();

            // salt
            $salt = uniqid(mt_rand(), true);

            // encrypted password
            $encrypted = $secure->create_hash($_POST['new_password'], $salt);
            
            
            // Update the account
            $su = $db->prepare("UPDATE customers SET salt = ?, encryption = ? WHERE unique_id = ?");
            $su->execute(array($salt, $encrypted, $_SESSION['unique_id']));
            
            $_SESSION['password'] = $_POST['new_password'];
            
            $_SESSION['status'] = "<h4>Success</h4>
                                   <p>Your password has been updated.</p>";
            header("Location: /o-hub");
            
            
        }    
    break;
        
    case "process-order":
        // We're committed
        if($_POST['accept'] == 1){
            $st = $db->prepare("SELECT * FROM orders WHERE session_id = ? AND status = ?");
            $st->execute(array(session_id(), "Unpaid"));

            if($st->rowCount() > 0){
                $xx = $st->fetchObject();

                // Update the delivery
                $su = $db->prepare("UPDATE deliveries SET order_id = ?, session_id = ? WHERE session_id = ?");
                $su->execute(array($xx->id, "", session_id()));

                // Set the total
                $sd = $db->prepare("SELECT SUM(price) AS total, SUM(discount) AS discount FROM cart WHERE order_id = ?");
                $sd->execute(array($xx->id));
                $d = $sd->fetchObject();

                if($cart->discount > 0){
                    //$discount = ($discount+$cart->discount);
                }

                $su = $db->prepare("UPDATE orders SET total = ?, discount_code = ?, discount_total = ?, customer_id = ? WHERE id = ?");
                $su->execute(array(($d->total-$d->discount), !empty($_SESSION['code']) ? $_SESSION['code'] : "", !empty($discount) ? $discount : "", $customer->id, $xx->id));

                header("Location: /simulate.php?id=".$xx->unique_id);



            }
        }else{
            $_SESSION['status'] = "<h4>Error</h4>
                                   <p>Sorry, you must accept our Terms and Condtions.</p>";
            header("Location: /summary");    
        }
        
    break;
        
    case "activate-auto-repeat":
        $st = $db->prepare("SELECT * FROM auto_repeat WHERE id = ? AND customer_id = ?");
        $st->execute(array($_GET['id'], $customer->id));
        if($st->rowCount() > 0){
            $r = $st->fetchObject();
            
            $su = $db->prepare("UPDATE auto_repeat SET status = ?, next_due = ? WHERE id = ?");
            $su->execute(array("Active", date('Y-m-d', strtotime('+1 week')), $_GET['id']));
            $_SESSION['status'] = "<h4>Success</h4>
                                   <p>Your auto repeat order has been updated.</p>";
            //echo strtotime("+7 day");
            $stripe->subscriptions->update(
              $r->subscription_id,
              ['pause_collection' => '',
               'trial_end' => strtotime("+7 day"),
                'proration_behavior' => 'none']
            );
            
        }else{
            $_SESSION['status'] = "<h4>Error</h4>
                                   <p>There was a problem updating your auto repeat order - please contact us.</p>";   
        }
        header("Location: /o-hub/auto-repeat/".$_GET['id']);
    break;
        
    case "cancel-auto-repeat":
        $st = $db->prepare("SELECT * FROM auto_repeat WHERE id = ? AND customer_id = ?");
        $st->execute(array($_GET['id'], $customer->id));
        if($st->rowCount() > 0){
            $r = $st->fetchObject();
            try {
                $stripe->subscriptions->update(
                  $r->subscription_id,
                  ['pause_collection' => array('behavior' => 'void')]
                );
            }catch (Exception $e) {
                mail("david.oldfield@brainstormdesign.co.uk", "Error", $e);
            }
            // Cancel at the end of the billing cycle.
            
            $su = $db->prepare("UPDATE auto_repeat SET status = ? WHERE id = ?");
            $su->execute(array("Inactive", $_GET['id']));
            $_SESSION['status'] = "<h4>Success</h4>
                                   <p>Your auto repeat order has been updated.</p>";
            
        }else{
            $_SESSION['status'] = "<h4>Error</h4>
                                   <p>There was a problem updating your auto repeat order - please contact us.</p>";   
        }
        header("Location: /o-hub/auto-repeat/".$_GET['id']);
    break;
        
    case "reorder":
        $st = $db->prepare("SELECT * FROM orders WHERE unique_id = ?");
        $st->execute(array($_GET['id']));
        $r = $st->fetchObject();
        
        // Does an order exist?
        $sd = $db->prepare("SELECT * FROM orders WHERE session_id = ? AND status = ?");
        $sd->execute(array(session_id(), "Unpaid"));
        if($sd->rowCount() == 0){
            $date = new DateTime();
            $hash =  $date->getTimestamp();

            $sf = $db->prepare("INSERT INTO orders (created_on, unique_id, session_id, status, customer_id) VALUES (?,?,?,?,?)");
            $sf->execute(array(date("Y-m-d H:i:s"), $hash, session_id(), "Unpaid", !empty($r->customer_id) ? $r->customer_id : ""));
            $basket = $db->lastInsertId();


        }else{
            $d = $sd->fetchObject();
            $basket = $d->id;
        }
        
        
        
        // Get all the items from the cart
        $sd = $db->prepare("SELECT * FROM cart WHERE order_id = ?");
        $sd->execute(array($r->id));
        $qty = $sd->rowCount();
        
        
        $sc = $db->prepare("INSERT INTO cart (session_id, sku_id, price, created_on, order_id, pet_id) VALUES (?,?,?,?,?,?)");
        for($x=1; $x <= $qty; $x++){
            $d = $sd->fetchObject();
            $sc->execute(array(session_id(), $d->sku_id, $d->price, date("Y-m-d H:i:s"), $basket, !empty($d->pet_id) ? $d->pet_id : ""));
        }
        header("Location: /basket");
        
        
    break;

    case "add-pet": 
        
        // Variables
        unset($_SESSION['name']);
        unset($_SESSION['breed']);
        unset($_SESSION['type']);
        unset($_SESSION['size']);
        unset($_SESSION['weight']);
        unset($_SESSION['condition']);
        
        unset($_SESSION['years']);
        unset($_SESSION['months']);
        
        unset($_SESSION['birth_day']);
        unset($_SESSION['birth_month']);
        unset($_SESSION['gender']);
        unset($_SESSION['spayed']);
        unset($_SESSION['pregnant']);
        unset($_SESSION['neutered']);
        
        unset($_SESSION['health']);
        unset($_SESSION['food']);
        
        unset($_SESSION['activity']);
        
        header("Location: /get-ooddling?stage=2");
    break;
        
    case "edit-pet":
        $st = $db->prepare("SELECT * FROM pets WHERE unique_id = ?");
        $st->execute(array($_GET['id']));
        $x = $st->fetchObject();
        
        // Set the session for the pet
        $su = $db->prepare("UPDATE pets SET session_id = ? WHERE unique_id = ?");
        $su->execute(array(session_id(), $_GET['id']));
        
        $_SESSION['mode'] = "edit";
        
        // Variables
        $_SESSION['name'] = $x->name;
        $_SESSION['breed'] = $x->breed;
        $_SESSION['type'] = $x->type_id;
        $_SESSION['size'] = $x->size;
        $_SESSION['weight'] = $x->weight;
        $_SESSION['condition'] = $x->physical_condition;
        
        $bits = explode(":", $x->age);
        $_SESSION['years'] = $bits[0];
        $_SESSION['months'] = $bits[1];
        
        $bobs = explode(":", $x->birthday);
        $_SESSION['birth_day'] = $bobs[0];
        $_SESSION['birth_month'] = $bobs[1];
        
        $_SESSION['gender'] = $x->gender;
        $_SESSION['spayed'] = $x->spayed;
        $_SESSION['pregnant'] = $x->pregnant;
        $_SESSION['neutered'] = $x->neutered;
        
        $_SESSION['health'] = $x->health_issues;
        $_SESSION['food'] = $x->food_types;
        
        $_SESSION['activity'] = $x->activity_id;
        
        
        header("Location: /get-ooddling?stage=3");
    break;
        
    case "complete":
        if(($cart->total-$cart->discount-$discount+$delivery_price) == 0){
            // It's a freebie
            $st = $db->prepare("SELECT o.id, c.name, c.surname, c.email, c.tmp, o.customer_id, o.unique_id, o.referrer, c.unique_id AS customer_code, o.total, o.discount_total, o.confirmation, c.stripe_id, o.shipping, c.tmp_password, o.discount_code
                                FROM orders o 
                                LEFT JOIN customers c ON o.customer_id = c.id
                                WHERE o.id = ?");
            $st->execute(array($cart_id));
            
            if($st->rowCount() > 0){
                $order = $st->fetchObject();
                if($order->confirmation == "0000-00-00 00:00:00"){
                    
                
                    
                    $su = $db->prepare("UPDATE orders SET status = ?, ipn_received = ? WHERE id = ?");
                    $su->execute(array("Paid", date('Y-m-d H:i:s'), $cart_id)); 
                    
                    $sf = $db->prepare("SELECT * FROM discount_codes WHERE discount_code = ?");
                    $sf->execute(array($order->discount_code));
                    if($sf->rowCount() > 0){
                        $f = $sf->fetchObject();
                        
                        if($f->one_use == 1){
                            $sk = $db->prepare("UPDATE discount_codes SET redeemed = ? WHERE id = ?");
                            $sk->execute(array(1,$f->id));
                        }
                    }
                    
                        send_order_email($db, $order->unique_id);
                        unset($_SESSION['code']);
                    
                        unset($_SESSION['first']);
                        unset($_SESSION['surname']);
                        unset($_SESSION['email']);
                        unset($_SESSION['b_telephone']);
                        unset($_SESSION['b_address1']);
                        unset($_SESSION['b_address2']);
                        unset($_SESSION['b_city']);
                        unset($_SESSION['b_postcode']);
                        unset($_SESSION['b_county']);
                        unset($_SESSION['b_country']);

                        unset($_SESSION['d_first']);
                        unset($_SESSION['d_surname']);
                        unset($_SESSION['d_email']);
                        unset($_SESSION['d_telephone']);
                        unset($_SESSION['d_address1']);
                        unset($_SESSION['d_address2']);
                        unset($_SESSION['d_city']);
                        unset($_SESSION['d_postcode']);
                        unset($_SESSION['d_county']);	
                        unset($_SESSION['d_country']);
                        unset($_SESSION['name']);
                    
                        header("Location: /order/".$order->unique_id);
                    
                }
                
            }
        }
    break;
        
    case "process-payment":
        switch($_GET['option']){
            case "paypal":
                // Get the items within the cart
                
                $st = $db->prepare("SELECT * FROM orders WHERE unique_id = ?");
                $st->execute(array($_GET['id']));
                $xx = $st->fetchObject();
               
                
                $ss = $db->prepare("SELECT s.title, p.title AS product_title, c.price FROM cart c LEFT JOIN skus s ON c.sku_id = s.id LEFT JOIN products p ON s.product_id = p.id WHERE c.order_id = ?");
                $ss->execute(array($xx->id));
                $count = $ss->rowCount();
                for($i=1; $i <= $count; $i++){
                    $s = $ss->fetchObject();
                    // Only need this for Paypal  
                    $items .= "<input name='item_name_".$i."' type='hidden' value='".str_replace("'", "&#39;", $s->title." - ".$s->product_title)."' />
                               <input name='amount_".$i."' type='hidden' value='".round($s->price,2)."' />";
                    
                    if($i == 1){
                        $items .= "<input name='shipping_".$i."' type='hidden' value='".round($xx->shipping,2)."' />";
                    }
                }
                ?>
                <script language="JavaScript"> setTimeout('document.gopay.submit()',100); </script>
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="gopay">
                  <input type="hidden" name="cmd" value="_cart">
                  <input type="hidden" name="upload" value="1">
                  <input type="hidden" name="business" value="<? echo $company->paypal; ?>" />
                  <input type="hidden" name="custom" value="<? echo $xx->unique_id; ?>" />
                  <input type="hidden" name="notify_url" value="<? echo MAIN_SITE."/ipn.php?option=paypal"; ?>" />
                  <input type="hidden" name="return" value="<? echo MAIN_SITE."/order/{$xx->unique_id}"; ?>" />
                  <input type="hidden" name="cancel_return" value="<? echo MAIN_SITE."/order/{$xx->unique_id}"; ?>" />
                  <input type="hidden" name="no_shipping" value="1" />
                  <input type="hidden" name="first_name" value="<? echo $customer->first; ?>" />
                  <input type="hidden" name="last_name" value="<? echo $customer->surname; ?>" />
                  <input type="hidden" name="address1" value="<? echo $customer->address; ?>" />
                  <input type="hidden" name="address2" value="<? echo $customer->town; ?>" />
                  <input type="hidden" name="city" value="<? echo $customer->city; ?>" />
                  <input type="hidden" name="state" value="<? echo $customer->county; ?>" />
                  <input type="hidden" name="zip" value="<? echo $customer->postcode; ?>" />
                  <input type="hidden" name="country" value="<? echo $iso; ?>" />
                  <input type="hidden" name="email" value="<? echo $customer->email; ?>" />
                  <input type="hidden" name="telephone" value="<? echo $customer->telephone; ?>" />
                  <input type='hidden' name="currency_code" value="GBP" />
                  <!--<input type="hidden" name="tax_cart" value="<? //echo round($vat,2); ?>">-->
                  <input type="hidden" name="discount_amount_cart" value="<? echo round($xx->discount_total,2); ?>">
                  <?
                        echo $items;
                      ?>
                  <input type="hidden" name="bn" value="PP-BuyNowBF" />
                  <!--<input name="submit" type="submit" value="Order now" />-->
                </form>
                <?
            break;
        }
    break;
}



?>