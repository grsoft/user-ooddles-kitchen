<div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Oops, that's an error.</h1>
       
        
    </div><!--close headline-->
    
</div><!--close banner-->
<section>

    <div class="flex error">
        <div class="c_33">
        </div>
        <div class="c_66">
            <div class="inner">
            <h3>404, Page not found.</h3>
            <p>You're probably looking for something tasty - don't worry, we've got plenty, it is just that the page you are looking for will have been moved or deleted.</p>
            <p>Please use the menu at the top of the page to browse our range.<br />
            Alternatively, you can email us on <a href="mailto:<? echo $company->email; ?>" title="Email <? echo $company->name; ?>"><? echo $company->email; ?></a> and we'll be happy to assist you.</p>
            </div>
            </div>
        </div>
   
    </section>
<?

include "includes/company.php";
include "footer.php"; ?>