<? include "header.php"; ?>

<?

       
        if(!empty($_SESSION['status'])){
	       echo "<div id='status'>".$_SESSION['status']."</div>";
        }
        
        $st = $db->prepare("SELECT * FROM pets WHERE unique_id = ?");
        $st->execute(array($_GET['id']));
        $xx = $st->fetchObject();
        
        $cats = explode(",", $xx->food_types);

        $sf = $db->prepare("SELECT * FROM categories WHERE id > ?");
        $sf->execute(array(5));
        while($f = $sf->fetchObject()){
            $cats[] = $f->id;
        }
    
        $sc = $db->prepare("SELECT category FROM categories WHERE id = ?");
        $types = array();
        foreach($cats as $k){
            $sc->execute(array($k));
            $c = $sc->fetchObject();
            $types[] = $c->category;
        }
if(empty($_GET['cat']) && empty($_SESSION['seen'])){
        ?>
            <div id="loader">
                <div>
                <h1>Thanks for giving us the <br />tails up on <? echo $xx->name; ?></h1>
                <p><img src="/images/loading.svg" alt="Loading" width="200" /></p>
                <h2>Just a minute while we Ooddle</h2>
                <p>We are working out which quality recipes will work for <? echo $xx->name; ?>, we shall show you all of them to select from.</p>
                <p>All our food comes with the feeding guide on the packaging for you to follow easily and personalised with <? echo $xx->name; ?>'s name on it.</p>
                    </div><!--close div-->
            </div><!--close loader-->
<? } ?>
            
            
            
        <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Recommended for <? echo $xx->name; ?></h1>
       
        
    </div><!--close headline-->
    
</div><!--close banner-->
    <div>
       
        
        
        
     
        <?
if(!empty($xx->food_types)){
    $cats = explode(",", $xx->food_types);
    ?>
<section class="products recommended">
    
    
    <?
    list($years,$months) = explode(":", $xx->age);
    // Get our size
    $ss = $db->prepare("SELECT id FROM size_age WHERE age_from <= ? AND age_to >= ?");
    $ss->execute(array(($years*12)+$months,($years*12)+$months));
    $s = $ss->fetchObject();
    
    // Our wheres...
    if(!empty($xx->size)){
        $where .= " AND p.suitable_for LIKE '%{$xx->size}%'";
    }
    if(!empty($s->id)){
        $where .= " AND p.life_stages LIKE '%{$s->id}%'";
    }
    if(!empty($xx->dislikes)){
        if(stripos($xx->dislikes, ",") !== false){
            $bits = explode(",", $xx->dislikes);
            foreach($bits as $b){
                $where .= " AND p.main_ingredient NOT LIKE '%{$b}%'";        
            }
        }else{
            $where .= " AND p.main_ingredient NOT LIKE '%{$xx->dislikes}%'";    
        }
        
    }
    // We are specific.
    if(!empty($_GET['cat'])){
        $and = " AND c.seo = '".$_GET['cat']."'";
    }
    
    if(!empty($xx->conditions)){
        $cs = explode(",", $xx->conditions);
        foreach($cs as $c){
            //$and .= " AND p.exclusions NOT LIKE '%{$c}%'";
        }
    }
    //echo $where;
    $sp = $db->prepare("SELECT p.title, p.seo, p.image, p.food_type, r.seo AS range_seo, c.seo AS category_seo, c.category, p.me , p.category_id, s.weight, s.unit, s.base_price, s.quantity
                        FROM skus s 
                        LEFT JOIN products p ON s.product_id = p.id 
                        LEFT JOIN product_ranges r ON p.range_id = r.id
                        LEFT JOIN categories c ON p.category_id = c.id
                        WHERE p.status = ? AND s.status = ? AND s.stock > ? AND p.category_id = ? {$where} {$and}
                        GROUP BY s.product_id
                        ORDER BY s.base_price ASC");
    
    // Activity level
    $sg = $db->prepare("SELECT me FROM activities WHERE id = ?");
    $sg->execute(array($xx->activity_id));
    $g = $sg->fetchObject();
    
    
    $sc = $db->prepare("SELECT category FROM categories WHERE id = ?");
    
    foreach($cats as $t){
       $sc->execute(array($t));
       $c = $sc->fetchObject();
        
        $sp->execute(array("Published", "Published", 0, strtolower($t)));
       
        if($sp->rowCount() > 0){
            
            
        ?>
    
    <div class="flex negative recommendations">
        <div class="c_100"><h3><? echo $c->category; ?></h3></div>
        <?
           while($p = $sp->fetchObject()){
               
               switch($p->category_id){
                case 1: // Dry
                    $low = $g->me;
                    $weight = $xx->weight;
                    $dog_cals = round(pow($weight, 0.75),2);
                    $der = round(($low * $dog_cals));

                    $calorie_per_kg = ($p->me*10);
                    
                    if($p->unit == "KG"){
                        $per_day = round((($der * 1000)/$calorie_per_kg));     
                    }else{
                        $per_day = round(($der/$calorie_per_kg));  
                    } 
                    $cost = number_format(($p->base_price/round(floor((($p->weight*1000)/$per_day)))),2);   
                       
                       
                break;

                case 2: // Wet
                    
                    $low = $g->me;
                    $weight = $xx->weight;
                    $dog_cals = round(pow($weight, 0.75),2);
                    $der = round(($low * $dog_cals));

                    $calorie_per_kg = ($p->me*10);
                       
                    if($p->unit == "KG"){
                        $per_day = round((($der * 1000)/$calorie_per_kg));  
                        $cost = number_format(($p->base_price/round(floor((($p->weight*1000)/$per_day)))),2);    
                    }else{
                        
                        $per_day = round(($der/$calorie_per_kg)); 
                        $cost = number_format(($p->base_price/round(floor((($p->weight*$p->quantity)/$per_day)))),2);    
                    }  
                       
                     
                break;
            }
        ?>
          <div class="c_20">
              <div class="inner item">
                  <div class="product_img"><a href="/get-ooddling/<? echo $_GET['id']; ?>/<? echo $p->category_seo; ?>/<? echo $p->seo; ?>" title="<? echo $p->title; ?>"><? if(!empty($p->image)){ echo "<img data-src='{$sirv}/images/products/{$p->image}?w=400&h=400&canvas.width=400&canvas.height=400' alt='{$p->title}' class='Sirv' />"; } ?></a></div>
                  <h2><a href="/get-ooddling/<? echo $_GET['id']; ?>/<? echo $p->category_seo; ?>/<? echo $p->seo; ?>" title="<? echo $p->title; ?>"><? echo $p->title; ?></a><!--<br />
                  <span class="from">From &pound;<? //echo $cost; ?> per day</span>--></h2>
                  
                  <p><a href="/get-ooddling/<? echo $_GET['id']; ?>/<? echo $p->category_seo; ?>/<? echo $p->seo; ?>" title="<? echo $p->title; ?>" class="btn white">View More</a></p>
              </div><!--close item-->
          </div><!--close c_25-->
    <?  } ?>              
    </div><!--close flex-->
    <? }
    } ?>
    
</section>
<? } ?>
    </div><!--close container-->
<? include "footer.php"; ?>