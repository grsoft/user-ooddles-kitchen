<?
// Initialize the session.
	session_start();

// Unset all of the session variables.
	unset($_SESSION['a_name']);
    unset($_SESSION['a_email']);
    unset($_SESSION['password']);
    unset($_SESSION['unique_id']);
    unset($_SESSION['customer_id']);

// If it's desired to kill the session, also delete the session cookie.
// Note: This will destroy the session, and not just the session data!
	if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time()-42000, '/');
	}

// Finally, destroy the session.
	session_destroy();
	
//redirect
	header("Location: /"); 
	
?>