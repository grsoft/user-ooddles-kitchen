<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'main' );

/** MySQL database username */
define( 'DB_USER', 'ooddles' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Drqu@ngo99_' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9km1U$6L5X7(|LFYa+33PJC:}N4iU6Pq-@7$.=Kmt;[7Tk3v (.W7=19<_z n tK');
define('SECURE_AUTH_KEY',  '~52X_6|y#=fbWA7!a>&Yl~]^g0+/|L`o~Y=S#z+@?1;7dyk7O6*Tu.>;Feb;-y;t');
define('LOGGED_IN_KEY',    '<6s(Fr1Yf$H+LmH~=Vu5}qCQ7YethkRvHG]LP4Y[|~-71:tRkP;vQ|q5P;h:v+D+');
define('NONCE_KEY',        'wcFs4i:c ;Q%da)6T*&Sr>u4`4Sqhr4K8KAXm#+zq:L_PI9?{&8wp^5:Uco1Mgp1');
define('AUTH_SALT',        'yp} H|(z$tW+1ZlPoQI?s6Qqvwyy)4g!?.qTH`5?JoAGoj8@~QY%mJ<z&8F!n;m|');
define('SECURE_AUTH_SALT', 'W}^1OgEjVADSY7]6gG^!Qi)6AOBt.$c8@hc:Nf^LHT[p+hO*@4)(g0,OYq+I4sPk');
define('LOGGED_IN_SALT',   'JrLX<`gYU@K]*z(w:h^>JDHnOP?/|Rs;P;m0n=mv+C[n u)/`UkgK{)99v$h</-S');
define('NONCE_SALT',       'b[v.WNxc8 $Oy+x9MeQ=A: k}&^A|QH@6-&n0E`I;{x6XR/rd}~&(u!R|CPw=nHj');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
