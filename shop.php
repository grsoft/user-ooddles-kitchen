<? include "header.php"; 


    ?>
    <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Ooddles Shop</h1>
        <div class="c_75"><p>You can shop with ease, without going through our <strong>Start Ooddling</strong> wizard.<br />
 
For dry food, you do need to click on <strong>Start Ooddling</strong>, so we can recommend the right recipes for your dog.</p><br />
        <p class="start centre"><a href="/get-ooddling" title="Start Ooddling" class="btn brown">Start Ooddling</a></p></div>
        
    </div><!--close headline-->
    
</div><!--close banner-->
<section>
    <div id="breadcrumbs">
        <ul class="flex">
            <li><a href="/" title="<? echo $company->name; ?>">Home</a></li>
            <li>&rang;</li>
            <li><a href="/shop" title="Shop">Shop</a></li>
            
            
        </ul>
        </div>
                
    </section>
    
    <section class="products shop">
    <div class="flex negative">
    <?
        $sc = $db->prepare("SELECT r.id, r.title, r.seo, r.introduction, r.image
                            FROM products p 
                            LEFT JOIN categories c ON p.category_id = c.id
                            LEFT JOIN product_ranges r ON p.range_id = r.id
                            GROUP BY r.title 
                            ORDER BY r.id=35 desc,r.id=30 desc,r.id=31 desc, r.running_order=0, r.running_order ASC");
        $sc->execute();
        $sp = $db->prepare("SELECT * FROM products WHERE range_id = ? AND status = ?");
       
        
        while($c = $sc->fetchObject()){
            $sp->execute(array($c->id, "Published"));
            if($sp->rowCount() > 0){
                if(empty($c->image)){
                    $p = $sp->fetchObject();
                    $image = $sirv."/images/products/".$p->image;
                }else{
                    $image = $sirv."/images/ranges/".$c->image;
                }
                
                echo "<div class='c_25'>
                      <div class='inner item'>
                      <div class='product_img'><a href='/shop/{$c->seo}' title='{$c->title}'><img src='{$image}?canvas.width=500&canvas.height=500&w=500&h=500' alt='{$c->title}' class='Sirv' /></a></div>
                      <h3><a href='/shop/{$c->seo}' title='{$c->title}'>{$c->title}</a></h3>
                      <p class='intro_text'>{$c->introduction}</p>
                      <p><a href='/shop/{$c->seo}' title='{$c->title}' class='btn '>View Range</a></p>
                      </div>
                      </div>";
            }
        }
    
    ?>
        </div>
        
    </section>

<section>
        <div class="centre">
        <p class="start centre"><a href="/get-ooddling" title="Start Ooddling" class="btn brown">Not sure which food or need a cost per day? Start Ooddling HERE</a></p>
            </div>
</section>
    <?

include "includes/company.php";
include "footer.php"; ?>