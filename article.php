<? include "header.php"; ?>
<div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Care &amp; Advice</span></h1>
        
    </div><!--close headline-->
    
</div><!--close banner-->
<?
if($logged_in == 2){
    ?>
<section>
    <div class="flex negative article">
    <?
        echo "<div class='c_30'>
                <div class='inner'>";
        if(!empty($xx->image)){
            echo "<img src='/images/news/{$xx->image}' alt='{$xx->title}' />";
        }
        echo "<p class='date'>".date("F j, Y", strtotime($xx->publish_on))."</p>
              <a href='/care-and-advice' title='Back to Articles' class='btn'>Back to Articles</a>
              </div>
              </div>
              <div class='c_70'>
              <div class='inner'>
              <h2>{$xx->title}</h2>
              <p class='intro'>{$xx->introduction}</p>
              {$xx->description}
              </div>
              </div>";
    
    ?>
        <script type="application/ld+json">
{
   "@context": "https://schema.org",
   "@type": "NewsArticle",
   "url": "<? echo MAIN_SITE."/".$_SERVER['REQUEST_URI']; ?>",
   "publisher":{
      "@type":"Organization",
      "name":"<? echo $company->name; ?> Kitchen",
      "logo":"<? echo MAIN_SITE."/images/logo-horizontal.png"; ?>"
   },
   "headline": "<? echo $xx->title; ?>",
   "mainEntityOfPage": "<? echo MAIN_SITE."/".$_SERVER['REQUEST_URI']; ?>",
   "articleBody": "<? echo $xx->description; ?>",
   "image":[
      "<? echo MAIN_SITE."/images/news/{$xx->image}"; ?>"
   ],
   "datePublished":"<? echo str_replace('+00:00', 'Z', gmdate('c', strtotime($xx->publish_on))); ?>"
}
</script>
        
        </div><!--close flex-->
    </section>
<? }else{
    include "includes/login.php";
}
include "includes/company.php";
include "footer.php"; ?>