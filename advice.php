<? include "header.php"; ?>
<div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Care &amp; Advice</span></h1>
        
    </div><!--close headline-->
    
</div><!--close banner-->
<?
if($logged_in == 2){
    ?>
<section>
    <div class="flex negative">
    <?
        $st = $db->prepare("SELECT * FROM advice WHERE publish_on <= ?");
        $st->execute(array(date("Y-m-d")));
        $num = $st->rowCount();
    
        for($x=1; $x <= $num; $x++){
            $r = $st->fetchObject();
            echo "<div class='c_25'>
                  <div class='news_item'>
                  <p class='date'>".date("F j, Y", strtotime($r->publish_on))."</p>";
            if(!empty($r->image)){
                echo "<div class='news_img'><a href='/care-and-advice/{$r->seo}' title='{$r->title}'><img src='/thumb.php?src=".THUMBS.$r->image."&w=300&h=300' alt='{$r->title}' /></a></div>";
            }else{
                echo "<div class='news_img'><a href='/care-and-advice/{$r->seo}' title='{$r->title}'><img src='/thumb.php?src=/images/thumbs/what-is-ooddlying-3.jpg&w=300&h=300' alt='{$r->title}' /></a></div>";
            }
                  
            echo "<div class='desc'>
                  <h3><a href='/care-and-advice/{$r->seo}' title='{$r->title}'>{$r->title}</a></h3>
                  <p>{$r->introduction}<br />
                  <a href='/care-and-advice/{$r->seo}' title='{$r->title}'>Read more</a></p>
                  </div>
                  </div>
                  </div>";
        }
    
    ?>
        </div><!--close flex-->
    </section>

<? }else{
    include "includes/login.php";
}

include "includes/company.php";
include "footer.php"; ?>