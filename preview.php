<?
include "configuration.php";
?>
<!doctype html>
<html>
<head>
    <title>Preview</title>
<meta charset="UTF-8">
<style type="text/css">
	body, html {
		height:100%;
		padding:0px;
		margin:0px;
		display:block;
		overflow:hidden;
	}
</style>
</head>

<body>
<?
$video = '<iframe src="https://player.vimeo.com/video/474633086?transparent=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>';
$iframe = preg_replace('/height="(.*?)"/i', 'height="100%"', $video);
$iframe = preg_replace('/width="(.*?)"/i', 'width="100%"', $iframe);
echo $iframe;

?>
</body>