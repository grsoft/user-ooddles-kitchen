<? include "header.php";

?>
<div itemscope itemtype="https://schema.org/Product">
    <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img itemprop="image" src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1 itemprop="name"><? echo $xx->title; ?></h1>
        <meta itemprop="brand" content="<? echo $company->name; ?> Kitchen" />
        
    </div><!--close headline-->
    
</div><!--close banner-->
<section>
    <div id="breadcrumbs">
        <ul class="flex" itemscope itemtype='https://schema.org/BreadcrumbList'>
            <li itemprop='itemListElement' itemscope itemtype='https://schema.org/ListItem'><a href="/" title="<? echo $company->name; ?>" itemprop='item'>
                <meta itemprop='name' content='<? echo $company->name; ?> Home' />
                      <meta itemprop='position' content='1' />
                <? echo $company->name; ?> Home</a></li>
            <li>&rang;</li>
            <li itemprop='itemListElement' itemscope itemtype='https://schema.org/ListItem'><a href="/shop" title="Shop" itemprop='item'>
                <meta itemprop='name' content='Shop' />
                      <meta itemprop='position' content='2' />
                Shop</a></li>
            <li>&rang;</li>
            <li itemprop='itemListElement' itemscope itemtype='https://schema.org/ListItem'><a href="/shop/<? echo $_GET['r']; ?>" title="<? echo $xx->range_title; ?>" itemprop='item'><meta itemprop='name' content='<? echo $xx->range_title; ?>' />
                      <meta itemprop='position' content='3' />
                <? echo $xx->range_title; ?></a></li>
            <li>&rang;</li>
            <li itemprop='itemListElement' itemscope itemtype='https://schema.org/ListItem'><a href="/shop/<? echo $_GET['r']; ?>/<? echo $_GET['id']; ?>" title="<? echo $xx->title; ?>" itemprop='item'>
                <meta itemprop='name' content='<? echo $xx->title; ?>' />
                      <meta itemprop='position' content='4' />
                <? echo $xx->title; ?></a></li>
            
            
        </ul>
        </div>
                
    </section>


    
     <section class="blue_bg product light_blue" >
        <div class="flex">
            <div class="c_50 single-image">
			<div class="container-fluid">
     
                            <div class='image_thumb' id='image_thumb'>
                            <div class='show' width="100%" height="100%">

                            <?php
                            echo  "<img itemprop='image' src='".$sirv."/images/products/{$xx->image}' alt='{$xx->title}' id='show-img'>"; ?>
                           
                           
                         </div>
                         <div class='small-img'>
                          
                           <div class='small-container'>
                             <div id='small-img-roll'>
                           
                             <?php
                             echo "<img itemprop='image' src='".$sirv."/images/products/{$xx->image}' alt='{$xx->title}' class='show-small-img'>";

                             $galleryimage=$db->prepare("select * from product_thumbs where product_id= ?");
                             $galleryimage->execute(array($xx->id));
                             while($result=$galleryimage->fetch()){
                             ?>
                             <img  src="<?=$sirv;?>/images/products/<?php echo $result['image'] ; ?>" class="show-small-img" alt="">
                          <?php }?>
                                               

					
                    </div>
                    </div>
                    
                  </div>


                  </div>
                    
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
      <script src="/js/zoom-image.js"></script> 
      <script src="/js/main.js  "></script>
    <script type="text/javascript">
    
    
    
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    
    </script>
  
   </div>  
 <?  
                    $sb = $db->prepare("SELECT b.title, b.image FROM product_benefit p LEFT JOIN benefits b ON p.benefit_id = b.id WHERE product_id = ?"); 
                    $sb->execute(array($xx->id));
                    if($sb->rowCount() > 0){
                ?>   
				
                    <br /><br /><h5>Key Benefits</h5>
                    <div class="flex benefits">
                        <?
                            while($b = $sb->fetchObject()){
                                echo "<div class='c_15'><span data-tooltip='{$b->title}'><img src='/images/{$b->image}' title='{$b->title}'/></span></div>";
                            }
                        ?>
                    </div>
                <?  } if($xx->category_id == 1 || $xx->category_id == 2 || $xx->category_id == 5){
                ?>
                
                <a href="/get-ooddling" title="Start Ooddling"><img itemprop="image" data-src="<? echo $sirv; ?>/images/ooddles-the-wizard.jpg?w=850" alt="Blog" class="Sirv wizard" /></a>
                <? } ?>
                
            </div>
            
            <div class="c_50 single-basket">
                <div class="details">
                    <h3><? echo $xx->introduction; ?></h3>
                   
                    
                    
                    
                    <div class="purchase smaller">
                        <div itemprop="offers" itemscope itemtype="https://schema.org/Offer">
                        
                            <?
                                $ss = $db->prepare("SELECT * FROM skus WHERE product_id = ? AND status = ?");
                                $ss->execute(array($xx->id, "Published"));
                                if($ss->rowCount() > 0){
                                    ?>
                                    <div class="flex">
                                      <div class="c_40"><div class="saver"><h5>Item</h5></div></div>
                                      <div class="c_15"><h5 class="nub">Price</h5></div>   
                                      <div class="c_25"><div class="qty"><h5>Quantity</h5></div></div>
                                        
                                    </div>
                            <?  }
                                
                                while($s = $ss->fetchObject()){
                                    ?>
                                <meta itemprop="url" content="<? echo MAIN_SITE.$_SERVER['REQUEST_URI']; ?>" />
                                <form name="add" method="post" action="/actions.php?action=add-to-basket">
                                    <meta itemprop="priceCurrency" content="GBP" />
                                    <input name="sku" type="hidden" value="<? echo $s->id; ?>" />
                                    <input name="url" type="hidden" value="<? echo $_SERVER['REQUEST_URI']; ?>" />
                                    <div class="flex">
                                        <div class="c_40"><div class="saver"><p class="bold"><? echo $s->title; if($xx->category_id != 11){  echo " ({$s->quantity} x {$s->weight}".strtolower($s->unit).")"; }  ?></p></div></div>
                                    <div class="c_15">
                                        <span id="product"><? echo $s->id; ?></span>
                                        
                                        <!--<p class="bubble"><? //echo $s->weight.strtolower($s->unit); ?></p>-->
                                        <p class="bold">&pound;<span itemprop="price"><? echo number_format($s->base_price,2); ?></span></p>
                                        
                                    </div>
                                    
                                    <div class="c_25">
                                        <div class="qty">
                                        
                                        <?
                                            if($s->stock > 0){
                                                ?>
                                            <link itemprop="availability" href="https://schema.org/InStock" />
                                            
                                        <div class="flex">
                                            
                                            <div><span class="nug" data-id="minus">-</span></div>
                                            <div>&nbsp;&nbsp;<span id="qty">0</span>&nbsp;&nbsp;<input name="qty" type="hidden" value="" /></div>
                                            <div><span class="nug" data-id="plus">+</span></div>
                                            <span id="stock"><? echo $s->stock; ?></span>
                                        </div>
                                    <? }else{
                                            echo "Out of stock";
                                                ?>
                                            <link itemprop="availability" href="https://schema.org/OutOfStock" />
                                            <?
                                        }
                                    ?>
                                    </div><!--close qty-->
                                        </div>
                                    
                                    <div class="c_20">
                                        
                                        <p><input name="submit" type="submit" value="Add To Basket" /></p></div>
                                    <div class="c_100 breaker"></div>
                                     </div>
                                    </form>
                                    
                                    <?
                                }
                                
                            ?>
                            
                          
                            </div><!--close offers-->
                            
                       
                       <div itemprop='description'><?  echo $xx->description; ?></div> 
                    </div><!--close purchase-->
                    
                    
                    
                </div><!--close details-->
                
                
            </div>
            
        </div><!--close flex-->
        
    </section>

    <? $nutrition = $xx->nutrition;
        $constituents = $xx->constituents;
    ?>
 <section id="more">
      
        <ul class="flex tabs">
            <li class="active"><a href="#" title="<? echo $xx->title; ?> Key Benefits" data-id="description">Key Benefits</a></li>
           <!-- <li><a href="#" title="<?// echo $xx->title; ?> Composition" data-id="composition">Composition</a></li>-->
           <? if (!empty($nutrition ) || !empty($constituents)) { ?>
            <li><a href="#" title="<? echo $xx->title; ?> Analytical Constituents" data-id="nutrition">Composition/Analytical Constituents</a></li>
            <? }?>
            <li><a href="#" title="<? echo $xx->title; ?> Feeding Guide" data-id="guide">Feeding Guide</a></li>
        </ul>
        <div id="info">
            <div class="sector" id="description">
                <? echo $xx->description; ?>
            </div><!--close sector-->
            
            <div class="sector" id="composition">
                <div class="flex">
                    <div class="c_100">
                        <h3>The Superfoods</h3>
                    <?
                        // Are we linked to food?
                        $sf = $db->prepare("SELECT f.title, f.description FROM product_food p LEFT JOIN foods f ON p.food_id = f.id WHERE p.product_id = ? ORDER BY f.title ASC");
                        $sf->execute(array($xx->id));
                        if($sf->rowCount() > 0){
                            echo "<div class='flex ingredients'>";
                            while($f = $sf->fetchObject()){
                                echo "<div class='c_80'><h4>{$f->title}</h4>{$f->description}</div>";
                            }
                            echo "</div>";
                        }
                    ?></div>
                    <div class="c_100"><? echo $xx->composition; ?></div>
                </div>
            </div><!--close sector-->
            
           
            
            <div class="sector" id="nutrition">
                <div class="flex">
                    <div class="c_100"><? echo $xx->constituents; ?></div>
                    <div class="c_100"><? echo $xx->nutrition; ?></div>
                </div>
            </div><!--close sector-->
            
            <div class="sector" id="guide">
                <div class="flex">
                    <div class="c_100">
                        <?
                        if(!empty($xx->feeding)){
                            echo str_replace("oodles.brainstormdevelopment", "www.ooddleskitchen", $xx->feeding);
                        }
                        ?>
                
                    </div>
                    <div class="c_100">
                        <h3>Additional Information</h3>
                        <p>All dogs are different and the guidelines should be adapted to take into account breed, age, temperament and activity level of the individual dog. When changing foods please introduce gradually over a period of two weeks. Always ensure fresh, clean water is available.</p>
                    </div>
                </div><!--close flex-->
            </div><!--close sector-->
            
            
            
            
        </div><!--close info-->
        
        
        
    </section>
</div>
    <?
            $st = $db->prepare("SELECT p.title, p.image, r.title AS range_title, p.image, p.introduction, p.description, p.composition, p.id, p.nutrition, p.constituents, p.food_type, p.seo, r.seo AS range_seo, c.seo AS category_seo
                                FROM products p
                                LEFT JOIN product_ranges r ON p.range_id = r.id 
                                LEFT JOIN categories c ON p.category_id = c.id 
                                WHERE p.id != ? AND p.status = ? AND r.seo = ? ORDER BY RAND() LIMIT 6");
            $st->execute(array($xx->id, "Published", $_GET['r']));
            $num = $st->rowCount();
            if($num > 0){
                ?>
                    
                        <div class="related light_blue">
                  <h3>Why not try these</h3>
                 <div class="glide">

				<div class="glide__arrows">
					<button class="glide__arrow prev" data-glide-dir="<">prev</button>
					<button class="glide__arrow next" data-glide-dir=">">next</button>
				</div>

				<div class="glide__wrapper">
					<ul class="glide__track">
                        <li class="glide__slide">
                            <div class="flex justify">
						<? 
                            for($x=1; $x <= $num; $x++){
                                $r = $st->fetchObject();
                                echo "<div class='c_25'>
                                      <div class='inner'>
                                      <div class='product_img'><a href='/shop/{$r->range_seo}/{$r->seo}' title='{$r->title}'><img itemprop='image' src='/thumb.php?src=/images/products/{$r->image}&w=500&h=500&zc=2' alt='{$r->title}' /></a></div>
                                      <h3><a href='/shop/{$r->range_seo}/{$r->seo}' title='{$r->title}'>{$r->range_title}<br />{$r->title}</a></h3>
                                      <p><a href='/shop/{$r->range_seo}/{$r->seo}' title='{$r->title}' class='btn white'>Buy Now</a></p>
                                      </div>
                                      </div>";
                                if($x % 3 == 0 && $x != $num){
                                    echo "</div></li><li class='glide__slide'><div class='flex justify'>";
                                }
                            }
                        ?>
                        </div>
                        </li>
						
						
					</ul>
				</div>

				<div class="glide__bullets"></div>

			</div>
                            </div>
                            
    <? } 

include "includes/company.php";
include "footer.php"; ?>

