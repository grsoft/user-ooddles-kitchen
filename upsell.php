<? include "header.php"; ?>

<div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Great Choice</h1>
        <div class="c_75"><? echo "<p>We've updated your basket.<br />
              Remember, {$pet->name} also needs some wet food to compliment his dry food.<br />
              We've got a lot of tasty goodies available and plenty of treats and chews - take a look for yourself.</p>"; ?></div>
        
    </div><!--close headline-->
    
</div><!--close banner-->

    
      <section class="blue_bg">
           <div class="flex negative range_wrap justify">
        <?
            // Our wheres...
            if(!empty($pet->size)){
                $where .= " AND (p.suitable_for LIKE '%{$pet->size}%' OR p.suitable_for = '')";
            }
            if(!empty($pet->dislikes)){
                if(stripos($pet->dislikes, ",") !== false){
                    $bits = explode(",", $pet->dislikes);
                    foreach($bits as $b){
                        $where .= " AND p.main_ingredient NOT LIKE '%{$b}%'";        
                    }
                }else{
                    $where .= " AND p.main_ingredient NOT LIKE '%{$pet->dislikes}%'";    
                }

            }
            $sp = $db->prepare("SELECT p.title, p.seo, p.image, p.food_type, r.seo AS range_seo, c.seo AS category_seo, c.category 
                                FROM skus s 
                                LEFT JOIN products p ON s.product_id = p.id 
                                LEFT JOIN product_ranges r ON p.range_id = r.id
                                LEFT JOIN categories c ON p.category_id = c.id
                                WHERE p.status = ? AND s.status = ? AND s.stock > ? AND (p.category_id = ? OR p.category_id = ?) {$where}
                                GROUP BY p.category_id");
            $sp->execute(array("Published", "Published", 0, 2, 6));
        
            while($p = $sp->fetchObject()){
                if(PAGE == "/detail.php" || PAGE == "/upsell.php"){
                    $url = "/o-hub/ranges/".$p->category_seo;
                }else{
                    $url = "/get-ooddling/".$pet->unique_id."/".$p->category_seo;
                }
        ?>
          <div class="c_25">
              <div class="inner">
                  <div class="product_img"><a href="<? echo $url; ?>" title="<? echo $p->category; ?>"><? if(!empty($p->image)){ echo "<img src='/thumb.php?src=/images/products/{$p->image}&w=400&h=400&zc=2' alt='{$p->category}' />";  } ?></a></div>
                  <h2><a href="<? echo $url; ?>" title="<? echo $p->category; ?>"><? echo $p->category; ?></a></h2>
                  <p><a href="<? echo $url; ?>" title="<? echo $p->category; ?>" class="btn brown">Buy Now</a></p>
              </div><!--close item-->
          </div><!--close c_25-->
    <?  } ?>              
    </div><!--close flex-->
          
    </section>

<?
include "includes/company.php";
include "footer.php"; ?>