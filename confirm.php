<? include "header.php"; ?>
<div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    <div class="headline centre">
                <h1><? echo ucwords(str_replace("-", " ", $_GET['area'])); ?></h1>
        
            </div><!--close headline-->
 
    
</div><!--close banner-->
            <?
            
            if($logged_in == 2){
                switch($_GET['area']){
                    case "activate-auto-repeat":
                        $st = $db->prepare("SELECT * FROM auto_repeat WHERE id = ? AND customer_id = ?");
                        $st->execute(array($_GET['id'], $customer->id));
                        if($st->rowCount() > 0){
                            $xx = $st->fetchObject();
                            
                            echo "<section class='centre'>
                                  <h2>Activate Confirmation</h2>
                                  <p>Great choice. Please confirm you wish to activate the Auto Repeat {$xx->frequency} order for &pound;".number_format($xx->total,2).".<br />
                                  Remember, you can update the order at any time.</p>
                                  <p><a href='/actions.php?action=activate-auto-repeat&id={$xx->id}' title='Activate Auto Repeat' class='btn brown check cancel'>Activate Auto Repeat</a></p>
                                  </section>";
                        }
                    break;
                    case "cancel-auto-repeat":
                        $st = $db->prepare("SELECT * FROM auto_repeat WHERE id = ? AND customer_id = ?");
                        $st->execute(array($_GET['id'], $customer->id));
                        if($st->rowCount() > 0){
                            $xx = $st->fetchObject();
                            
                            echo "<section class='centre'>
                                  <h2>Cancellation Confirmation</h2>
                                  <p>Please confirm you wish to cancel the Auto Repeat {$xx->frequency} order for &pound;".number_format($xx->total,2).".<br />
                                  Remember, you can re-activate the order at any time.</p>
                                  <p><a href='/actions.php?action=cancel-auto-repeat&id={$xx->id}' title='Cancel Auto Repeat' class='btn brown check cancel'>Cancel Auto Repeat</a></p>
                                  </section>";
                        }
                    break;
                        
                    case "remove-pet":
                        $st = $db->prepare("SELECT * FROM pets WHERE unique_id = ?");
                        $st->execute(array($_GET['id']));
                        $xx = $st->fetchObject();
                        
                        echo "<section class='centre'>
                              <h2>Before we see {$xx->name} go</h2>
                              <p>Are you sure you want to remove {$xx->name}'s' profile? Tell us why below:</p>
                              <form name='remove' method='post' action='/actions.php?action=remove-pet'>
                              <input name='url' type='hidden' value='".$_SERVER['REQUEST_URI']."' />
                              <input name='id' type='hidden' value='".$_GET['id']."' />";
                        ?>
                              <p><input name="why" type="radio" value="Re-Homed" <? if($_SESSION['why'] == "Re-Homed"){ echo "checked='checked'"; } ?> /> <? echo $xx->name; ?> has been re-homed.<br />
                                  <input name="why" type="radio" value="Passed Away" <? if($_SESSION['why'] == "Passed Away"){ echo "checked='checked'"; } ?> /> <? echo $xx->name; ?> has passed away</p>

                                <p>I do not wish to be part of Ooddles anymore because:</p>
                                <p><input name="why" type="radio" value="Don't like the food" <? if($_SESSION['why'] == "Don't like the food"){ echo "checked='checked'"; } ?> /> Don't like the food<br />
                                  <input name="why" type="radio" value="Poor service" <? if($_SESSION['why'] == "Poor service"){ echo "checked='checked'"; } ?> /> Poor service<br />
                                <input name="other" type="text" placeholder="Other, please specfiy" value="<? echo $_SESSION['other']; ?>" /></p>
                                <p><input name="submit" type="submit" value="Remove Profile" /></p>
                        <?      
                        echo "</form></section>";
                        
                    break;
                        
                    
                }
                
            }else{
                include "includes/login.php";
            }
            

          if($logged_in != 2){ 
              include "includes/ranges.php";
              ?>
              <h3 class="subtitle">Personalised Premium Dog Food - Delivered Directly To Your Door</h3>
              <?
              include "includes/hub.php"; 
          } ?>
    
<? 
include "includes/company.php";
include "footer.php"; ?>