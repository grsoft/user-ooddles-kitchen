<?
include "check-login.php";
error_reporting(E_ALL); ini_set('display_errors', TRUE); ini_set('display_startup_errors', TRUE);
// Send an email
$st = $db->prepare("SELECT * FROM customers WHERE reset_id = ?");
$st->execute(array($_GET['id']));

$xx = $st->fetchObject();

include "includes/mail-headers.php";   

// Get the account
$secure = new SecureHash();
                    
// Password
$password = substr(rand()*rand(),0,10);

// salt
$salt = uniqid(mt_rand(), true);

// encrypted password
$encrypted = $secure->create_hash($password, $salt);




// Update the account
$su = $db->prepare("UPDATE customers SET salt = ?, encryption = ?, reset_id = ?, tmp = ? WHERE reset_id = ?");
$su->execute(array($salt, $encrypted, "", 0, $_GET['id']));

$imgs = array("header-lucy.jpg", "header-charlie.jpg");
shuffle($imgs);

$banners = array(array("image-treats.jpg", "shop"));
shuffle($banners);

// Mail header and footer
$mailer = $mail_header;



$mailer .= "<tr>
            <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: museo sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Your new password</td>
            </tr>
            <tr>
            <td style='padding:20px; background-color: #fff;'>
            <p style='font-family:Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;; font-size:13px; line-height:150%;'>{$xx->name},<br />Your password has been successfully changed.<br /><br />Your new password: <strong>{$password}</strong><br /><br />
            If you have any problems signing in, please contact us on <a href='mailto:{$company->email}' title='Email {$company->name}' style='font-family:Museo Sans, Helvetica, Georgia, Hevetica, Arial, sans-serif; font-size:12px;padding:0px; margin:0px; color:#333; text-decoration:none;'>{$client->email}</a> and we'll be happy to help.</p>
            </td>
            </tr>";

$mailer .= $mail_footer;

include "classes/class.phpmailer.php";
$email = new PHPMailer();
$email->From = $company->email;
$email->FromName = $company->company;

$email->Subject = "Your password has been reset";
$email->Body = $mailer;
$email->IsHTML(true);

$email->AddAddress($xx->email);
$email->Send();


$_SESSION['status'] = "<h4>Success</h4>
                       <p>Please check your email for your new password.</p>"; 




// Log them in.
$_SESSION['name'] = $xx->name;
$_SESSION['email'] = $xx->email;
$_SESSION['password'] = $password;
$_SESSION['unique_id'] = $xx->unique_id;

header("Location: /o-hub");


?>