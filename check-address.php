<?
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
include "check-login.php";


// Check to see if we have the billing info / delivery info and set the session variables
	if($_POST['submit']){
		
		// Set variables
			$_SESSION['first'] = $_POST['b_first'];
			$_SESSION['surname'] = $_POST['b_surname'];
			$_SESSION['email'] = $_POST['b_email'];
			$_SESSION['b_telephone'] = $_POST['b_telephone'];
			$_SESSION['b_address1'] = $_POST['b_address1'];
			$_SESSION['b_address2'] = $_POST['b_address2'];
			$_SESSION['b_city'] = $_POST['b_city'];
			$_SESSION['b_postcode'] = $_POST['b_postcode'];
			$_SESSION['b_county'] = $_POST['b_county'];	
			$_SESSION['b_country'] = $_POST['b_country'];	
			
			$_SESSION['d_first'] = $_POST['d_first'];
			$_SESSION['d_surname'] = $_POST['d_surname'];
			$_SESSION['d_email'] = $_POST['d_email'];
			$_SESSION['d_telephone'] = $_POST['d_telephone'];
			$_SESSION['d_address1'] = $_POST['d_address1'];
			$_SESSION['d_address2'] = $_POST['d_address2'];
			$_SESSION['d_city'] = $_POST['d_city'];
			$_SESSION['d_postcode'] = $_POST['d_postcode'];
			$_SESSION['d_county'] = $_POST['d_county'];	
			$_SESSION['d_country'] = $_POST['d_country'];
            $_SESSION['name'] = $_POST['dog'];
        
            $_SESSION['account_password'] = str_replace(" ", "",$_POST['password']);
			
			if(empty($_POST['same'])){
				$_SESSION['same'] = "-";
			}else{
				$_SESSION['same'] = $_POST['same'];
			}
			$_SESSION['mailing'] = $_POST['mailing'];
			$_SESSION['special'] = $_POST['special'];
			$_SESSION['saturday'] = $_POST['saturday'];
		
		      unset($_SESSION['insurance']);
			
			// Set the session on the account
			$_SESSION['account'] = $_POST['account'];
        
        
            // Insert deliveries
            $sdi = $db->prepare("INSERT INTO deliveries (session_id, d_name, d_surname, d_email, d_telephone, d_address, d_address2, d_city, d_postcode, d_county, d_country) VALUES (:session, :first, :surname, :email, :telephone, :address1, :address2, :city, :postcode, :county, :country)");
            // Update deliveries
            $sdu = $db->prepare("UPDATE deliveries SET d_name = :first, d_surname = :surname, d_email = :email, d_telephone = :telephone, d_address = :address1, d_address2 = :address2, d_city = :city, d_county = :county, d_postcode = :postcode, d_country = :country WHERE session_id = :session");
            // Insert customers
            $sci = $db->prepare("INSERT INTO customers (session_id, name, surname, email, telephone, address, town, city, postcode, county, country, created_on, mailing_list, tmp, tmp_password, unique_id) VALUES (:session, :first, :surname, :email, :telephone, :address1, :address2, :city, :postcode, :county, :country, :date, :mailing, :tmp, :password, :unique)");
            // Update customers
            $scu = $db->prepare("UPDATE customers SET name = :first, surname = :surname, email = :email, telephone = :telephone, address = :address1, town = :address2, city = :city, county = :county, country = :country, postcode = :postcode, mailing_list = :mailing, tmp = :tmp, tmp_password = :password WHERE session_id = :session");
        
            $sq = $db->prepare("SELECT * FROM pets WHERE session_id = ?");
            
            $sqi = $db->prepare("INSERT INTO pets (name, unique_id, session_id, customer_id) VALUES (?,?,?,?)");
            $squ = $db->prepare("UPDATE pets SET name = ? WHERE session_id = ?");
        
        
            if($_POST['password'] && strlen($_POST['password']) < 6){
                $_SESSION['status'] = "<h4>Error</h4><p>Sorry, your account password is not 6 characters or more. Please try again.</p>";
				header("Location: /addresses");	
                die();
            }
			
        
            if($logged_in != 2){
			// Check to see if all the mandatory fields have been completed
				if(!$_POST['b_first'] | !$_POST['b_surname'] | !$_POST['b_email'] | !$_POST['b_telephone'] | !$_POST['b_address1'] | !$_POST['b_city'] | !$_POST['b_postcode'] | !$_POST['b_county'] | !$_POST['b_country'] | !$_POST['dog']){
					$_SESSION['status'] = "<h4>Error</h4><p>Sorry, you have not entered a required field. Please try again.</p>";
					header("Location: /addresses");	
				}else{
					
					// Is the billing email valid?
						if(!filter_var($_POST['b_email'], FILTER_VALIDATE_EMAIL)){
							$_SESSION['status'] = "<h4>Error</h4><p>Sorry, the email address you have entered is incorrect. Please try again.</p>";
							header("Location: /addresses");
						}else{
							// Do they exist as a registered customer
                            
                                $st = $db->prepare("SELECT * FROM customers WHERE email = ? AND salt != ? AND inactive = ?");
                                $st->execute(array(str_replace(" ", "", $_POST['b_email']), "", 0));
                                
                                if($st->rowCount() > 0){
                                    $_SESSION['status'] = "<h4>Error</h4><p>Sorry, your email is attached to an account - please login.</p>";
					                header("Location: /addresses");	    
                                }
								
								  // Are they a customer?
									  $st = $db->prepare("SELECT *
														  FROM customers
														  WHERE session_id = :session");
									  $st->execute(array("session" => session_id()));
									  $count2 = $st->rowCount();
												  
								  // All good for the billing.. are we the same for delivery?
									  $st = $db->prepare("SELECT *
														  FROM deliveries
														  WHERE session_id = :session");
									  $st->execute(array("session" => session_id()));
									  $count = $st->rowCount();
									  
									  // Prepare our update and insert statements to re-use
									  
                                
									  
									  
									  if($_POST['same'] == "1" || $_POST['exclude_delivery'] == 1){
										  
									  // Same - see if it already exists in the deliveries table
										  if($count == 0){
											  // No match - insert
												  $sdi->execute(array("session" => session_id(), "first" => $_POST['b_first'], "surname" => $_POST['b_surname'], "email" => str_replace(" ", "", $_POST['b_email']), "telephone" => $_POST['b_telephone'], "address1" => $_POST['b_address1'], "address2" => $_POST['b_address2'], "city" => $_POST['b_city'], "postcode" => $_POST['b_postcode'], "county" => $_POST['b_county'], "country" => $_POST['b_country']));
                                              
                                                
											  
										  }else{
											  // Match - update
												  $sdu->execute(array("session" => session_id(), "first" => $_POST['b_first'], "surname" => $_POST['b_surname'], "email" => str_replace(" ", "", $_POST['b_email']), "telephone" => $_POST['b_telephone'], "address1" => $_POST['b_address1'], "address2" => $_POST['b_address2'], "city" => $_POST['b_city'], "postcode" => $_POST['b_postcode'], "county" => $_POST['b_county'], "country" => $_POST['b_country']));
											  
										  }
										  
												  if($count2 == 0){
                                                      $date = new DateTime();
                                                      $hash =  $date->getTimestamp();
                                                      
													  // Insert
														  $sci->execute(array("session" => session_id(), "first" => $_POST['b_first'], "surname" => $_POST['b_surname'], "email" => str_replace(" ", "", $_POST['b_email']), "telephone" => $_POST['b_telephone'], "address1" => $_POST['b_address1'], "address2" => $_POST['b_address2'], "city" => $_POST['b_city'], "postcode" => $_POST['b_postcode'], "county" => $_POST['b_county'], "country" => $_POST['b_country'], "date" => date("Y-m-d"), "mailing" => 1, "tmp" => 1, "password" => !empty($_POST['password']) ? str_replace(" ", "",$_POST['password']) : "", "unique" => $hash));
                                                          $id = $db->lastInsertId();
                                                      
                                                      // Send to stripe
                                                      
                                                      // Update the orders with any session items
                                                          $sf = $db->prepare("UPDATE orders SET customer_id = ? WHERE session_id = ? AND status = ?");
                                                          $sf->execute(array($id, session_id(), "Unpaid"));
                                                      
                                                      // The dog
                                                          $sq->execute(array($_POST['dog']));
                                                          if($sq->rowCount() == 0){
                                                              $date = new DateTime();
                                                              $hash =  $date->getTimestamp();

                                                              $sqi->execute(array($_POST['dog'], $hash, session_id(), $id));
                                                          }else{
                                                              $squ->execute(array($_POST['dog'], session_id()));
                                                              $q = $sq->fetchObject();
                                                              $hash = $q->unique_id;
                                                          }
                                                      
                                                      // Update the cart with the dog's unique id
                                                      
                                                      $sb = $db->prepare("UPDATE cart SET pet_id = ? WHERE order_id = ?");
                                                      $sb->execute(array($hash, $cart_id));
                                                      
                                                      // Append the initials
                                                            $sf = $db->prepare("UPDATE orders SET unique_id = ? WHERE unique_id = ?");
                                                            $sf->execute(array($cart_hash.substr($_POST['b_first'],0,1).substr($_POST['b_surname'],0,1),$cart_hash));
												  }else{
													  // Update
														  $scu->execute(array("session" => session_id(), "first" => $_POST['b_first'], "surname" => $_POST['b_surname'], "email" => str_replace(" ", "", $_POST['b_email']), "telephone" => $_POST['b_telephone'], "address1" => $_POST['b_address1'], "address2" => $_POST['b_address2'], "city" => $_POST['b_city'], "postcode" => $_POST['b_postcode'], "county" => $_POST['b_county'], "country" => $_POST['b_country'], "mailing" => 1, "tmp" => 1, "password" => !empty($_POST['password']) ? str_replace(" ", "",$_POST['password']) : ""));
													  
												  }
										  header("Location: /summary");
										  
									  }else{
										  if(!$_POST['d_first'] | !$_POST['d_surname'] | !$_POST['d_telephone'] | !$_POST['d_address1'] | !$_POST['d_city'] | !$_POST['d_postcode'] | !$_POST['d_county'] | !$_POST['d_country']){
											  
											  $_SESSION['status'] = "<h4>Error</h4><p>Sorry, you have not entered a required field. Please try again.</p>";
											  header("Location: /addresses");	
										  }else{
											  if($count == 0){
												  // No match - insert
												  $sdi->execute(array("session" => session_id(), "first" => $_POST['d_first'], "surname" => $_POST['d_surname'], "email" => str_replace(" ", "", $_POST['d_email']), "telephone" => $_POST['d_telephone'], "address1" => $_POST['d_address1'], "address2" => $_POST['d_address2'], "city" => $_POST['d_city'], "postcode" => $_POST['d_postcode'], "county" => $_POST['d_county'], "country" => $_POST['d_country']));
											  }else{
												  // Match - update
													  $sdu->execute(array("session" => session_id(), "first" => $_POST['d_first'], "surname" => $_POST['d_surname'], "email" => str_replace(" ", "", $_POST['d_email']), "telephone" => $_POST['d_telephone'], "address1" => $_POST['d_address1'], "address2" => $_POST['d_address2'], "city" => $_POST['d_city'], "postcode" => $_POST['d_postcode'], "county" => $_POST['d_county'], "country" => $_POST['d_country']));
											  }
											  
												  if($count2 == 0){
                                                      $date = new DateTime();
                                                      $hash =  $date->getTimestamp();
													  // Insert
														  $sci->execute(array("session" => session_id(), "first" => $_POST['b_first'], "surname" => $_POST['b_surname'], "email" => str_replace(" ", "", $_POST['b_email']), "telephone" => $_POST['b_telephone'], "address1" => $_POST['b_address1'], "address2" => $_POST['b_address2'], "city" => $_POST['b_city'], "postcode" => $_POST['b_postcode'], "county" => $_POST['b_county'], "country" => $_POST['b_country'], "date" => date("Y-m-d"), "mailing" => 1, "tmp" => 1, "password" => !empty($_POST['password']) ? str_replace(" ", "",$_POST['password']) : "", "unique" => $hash));
                                                          $id = $db->lastInsertId();
                                                      // Update the orders with any session items
                                                          $sf = $db->prepare("UPDATE orders SET customer_id = ? WHERE session_id = ? AND status = ?");
                                                          $sf->execute(array($id, session_id(), "Unpaid"));
                                                      
                                                      // The dog
                                                          $sq->execute(array($_POST['dog']));
                                                          if($sq->rowCount() == 0){
                                                              $date = new DateTime();
                                                              $hash =  $date->getTimestamp();

                                                              $sqi->execute(array($_POST['dog'], $hash, session_id(), $id));
                                                          }else{
                                                              $squ->execute(array($_POST['dog'], session_id()));
                                                              $q = $sq->fetchObject();
                                                              $hash = $q->unique_id;
                                                          }
                                                      
                                                      // Update the cart with the dog's unique id
                                                      
                                                        $sb = $db->prepare("UPDATE cart SET pet_id = ? WHERE order_id = ?");
                                                        $sb->execute(array($hash, $cart_id));
                                                      
                                                      // Append the initials
                                                          $sf = $db->prepare("UPDATE orders SET unique_id = ? WHERE unique_id = ?");
                                                          $sf->execute(array($cart_hash.substr($_POST['b_first'],0,1).substr($_POST['b_surname'],0,1),$cart_hash));
                                                      
												  }else{
													  // Update
														  $scu->execute(array("session" => session_id(), "first" => $_POST['b_first'], "surname" => $_POST['b_surname'], "email" => str_replace(" ", "", $_POST['b_email']), "telephone" => $_POST['b_telephone'], "address1" => $_POST['b_address1'], "address2" => $_POST['b_address2'], "city" => $_POST['b_city'], "postcode" => $_POST['b_postcode'], "county" => $_POST['b_county'], "country" => $_POST['b_country'], "mailing" => 1, "tmp" => 1, "password" => !empty($_POST['password']) ? str_replace(" ", "",$_POST['password']) : ""));
													  
												  }
												  
											  header("Location: /summary");
										  }
									  }
	
						}
							
					}
            }else{
                // We're logged in.
                
                 // All good for the billing.. are we the same for delivery?
                  $st = $db->prepare("SELECT *
                                      FROM deliveries
                                      WHERE session_id = :session");
                  $st->execute(array("session" => session_id()));
                  $count = $st->rowCount();
                
                if(!$_POST['b_first'] | !$_POST['b_surname'] | !$_POST['b_email'] | !$_POST['b_telephone'] | !$_POST['b_address1'] | !$_POST['b_city'] | !$_POST['b_postcode'] | !$_POST['b_county'] | !$_POST['b_country']){
                    $_SESSION['status'] = "<h4>Error</h4><p>Sorry, you have not entered a required field. Please try again.</p>";
					header("Location: /addresses");	   
                }else{
                    $scu = $db->prepare("UPDATE customers SET name = :first, surname = :surname, email = :email, telephone = :telephone, address = :address1, town = :address2, city = :city, county = :county, country = :country, postcode = :postcode WHERE id = :session");
                    $scu->execute(array("session" => $customer->id, "first" => $_POST['b_first'], "surname" => $_POST['b_surname'], "email" => str_replace(" ", "", $_POST['b_email']), "telephone" => $_POST['b_telephone'], "address1" => $_POST['b_address1'], "address2" => $_POST['b_address2'], "city" => $_POST['b_city'], "postcode" => $_POST['b_postcode'], "county" => $_POST['b_county'], "country" => $_POST['b_country']));
                }
                
                if($_POST['same'] == "1" || $_POST['exclude_delivery'] == 1){
										  
                    // Same - see if it already exists in the deliveries table
                        if($count == 0){
                            // No match - insert
                                $sdi->execute(array("session" => session_id(), "first" => $_POST['b_first'], "surname" => $_POST['b_surname'], "email" => str_replace(" ", "", $_POST['b_email']), "telephone" => $_POST['b_telephone'], "address1" => $_POST['b_address1'], "address2" => $_POST['b_address2'], "city" => $_POST['b_city'], "postcode" => $_POST['b_postcode'], "county" => $_POST['b_county'], "country" => $_POST['b_country']));

                        }else{
                            // Match - update
                                $sdu->execute(array("session" => session_id(), "first" => $_POST['b_first'], "surname" => $_POST['b_surname'], "email" => str_replace(" ", "", $_POST['b_email']), "telephone" => $_POST['b_telephone'], "address1" => $_POST['b_address1'], "address2" => $_POST['b_address2'], "city" => $_POST['b_city'], "postcode" => $_POST['b_postcode'], "county" => $_POST['b_county'], "country" => $_POST['b_country']));

                        }

                                
                        header("Location: /summary");

                    }else{
                        if(!$_POST['d_first'] | !$_POST['d_surname'] | !$_POST['d_telephone'] | !$_POST['d_address1'] | !$_POST['d_city'] | !$_POST['d_postcode'] | !$_POST['d_county'] | !$_POST['d_country']){

                            $_SESSION['status'] = "<h4>Error</h4><p>Sorry, you have not entered a required field. Please try again.</p>";
                            header("Location: /addresses");	
                        }else{
                            if($count == 0){
                                // No match - insert
                                $sdi->execute(array("session" => session_id(), "first" => $_POST['d_first'], "surname" => $_POST['d_surname'], "email" => str_replace(" ", "", $_POST['d_email']), "telephone" => $_POST['d_telephone'], "address1" => $_POST['d_address1'], "address2" => $_POST['d_address2'], "city" => $_POST['d_city'], "postcode" => $_POST['d_postcode'], "county" => $_POST['d_county'], "country" => $_POST['d_country']));
                            }else{
                                // Match - update
                                    $sdu->execute(array("session" => session_id(), "first" => $_POST['d_first'], "surname" => $_POST['d_surname'], "email" => str_replace(" ", "", $_POST['d_email']), "telephone" => $_POST['d_telephone'], "address1" => $_POST['d_address1'], "address2" => $_POST['d_address2'], "city" => $_POST['d_city'], "postcode" => $_POST['d_postcode'], "county" => $_POST['d_county'], "country" => $_POST['d_country']));
                            }


                            header("Location: /summary");
                        }
                    }
                
                
            }
				
	}


?>