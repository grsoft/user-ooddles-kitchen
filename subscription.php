<?
include "configuration.php";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('stripe/init.php');
$stripe = new \Stripe\StripeClient($stripe_secret_key);

$st = $db->prepare("SELECT a.frequency, a.id, c.stripe_id FROM auto_repeat a LEFT JOIN customers c ON a.customer_id = c.id WHERE a.order_id = ?");
$st->execute(array(58));
$x = $st->fetchObject();

switch($x->frequency){
    case "monthly":
        $num = 1;
        $id = "prod_IFDQP7yodNePlD";
    break;
        
    case "bi-monthly":
        $num = 2;
    break;
}



$sl = $db->prepare("SELECT s.id, s.stripe_product, a.sku_id, s.product_code, a.price, a.quantity, s.title, p.title AS product, s.weight, s.quantity AS qty, s.unit FROM auto_repeat_items a LEFT JOIN skus s ON a.sku_id = s.id LEFT JOIN products p ON s.product_id = p.id WHERE auto_id = ?");
$sl->execute(array($x->id));
$parts = array();

$su = $db->prepare("UPDATE skus SET stripe_product = ? WHERE id = ?");
while($l = $sl->fetchObject()){
    
    if(empty($l->stripe_product)){
        // Create a product
        $product = $stripe->products->create([
            'name' => "{$l->product}: {$l->qty} x {$l->weight}{$l->unit}"       
        ]);
        
        $prod = $product->id;
        $su->execute(array($prod, $l->id));
        
    }else{
        $prod = $l->stripe_product;
    }
    $parts[] = array('metadata' => array('sku' => $l->sku_id),
                    'price_data' => array(
                        'currency' => 'GBP',
                        'product' => $prod,
                        'recurring' => array(
                            'interval' => 'month',
                            'interval_count' => $num
                        ),
                        'unit_amount' => round(($l->price*100))
                    ),
                    'quantity' => $l->quantity);
}



$subs = $stripe->subscriptions->create([
  'customer' => $x->stripe_id,
  'items' => $parts,
  'off_session' => true,
  'collection_method' => 'charge_automatically'    
    
]);

$su = $db->prepare("UPDATE auto_repeat SET subscription_id = ? WHERE id = ?");
$su->execute(array($subs->id, $x->id));

print_r($subs);

?>