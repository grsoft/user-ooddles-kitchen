<?
include "vendor/autoload.php";
// reference the Dompdf namespace
    use Dompdf\Dompdf;
    use Dompdf\Options;

function do_login($email, $password, $db){
	$st = $db->prepare("SELECT *
	                    FROM customers
						WHERE email = ? AND inactive != ? AND tmp != ?");
	$st->execute(array($email,1,1));
	
	$num = $st->rowCount();
	
    // check for existing account (this entails a query, example in Zend)
    if($num > 0) {
		$r = $st->fetchObject();
        
        if(empty($r->unique_id)){
            // Update the profile
            $date = new DateTime();
            $hash =  $date->getTimestamp();
            $su = $db->prepare("UPDATE customers SET unique_id = ? WHERE id = ?");
            $su->execute(array($hash, $r->id));
            
            // Re-execute the query.
            $st->execute(array($email,1,1));
            $r = $st->fetchObject();
        }
        
        // load the class
        $secure = new SecureHash();        

        // check for password match
        if ($secure->validate_hash($password, $r->encryption, $r->salt)) {
            // success, log the user in and redirect
			// Update the field with a login date
			$st = $db->prepare("UPDATE customers SET lastlogin = :date, tmp_password = :tmp WHERE unique_id = :id");
			$st->execute(array("date" => date("Y-m-d H:i:s"), "tmp" => "", "id" => $r->unique_id));
			
            // Update any cart items with the pet
            $sp = $db->prepare("SELECT unique_id FROM pets WHERE customer_id = ?");
            $sp->execute(array($r->id));
            $p = $sp->fetchObject();
            
            $su = $db->prepare("UPDATE cart SET pet_id = ? WHERE session_id = ?");
            $su->execute(array($p->unique_id, session_id()));
            
            
			// Return
            return array("name" => $r->first, "email" => $r->email, "id" => $r->unique_id, "password" => $password, "logged_in" => 2);
        }else{
            // error, password mismatch
            return array("logged_in" => 0);
        }
    }
}

// functions
function check_login($email, $password, $db){
	$st = $db->prepare("SELECT *
	                    FROM customers
						WHERE email = ? AND inactive != ? AND tmp != ?");
	$st->execute(array($email,1,1));
	$num = $st->rowCount();
	
    // check for existing account (this entails a query, example in Zend)
    if ($num > 0) {
		$r = $st->fetchObject();
        // load the class
        $secure = new SecureHash();        
		
		
        // check for password match
        if ($secure->validate_hash($password, $r->encryption, $r->salt)) {
            // success, log the user in and redirect
            return 1;
        } else {
            // error, password mismatch
            return 0;
        }
    }
}
function last_order_id($db){
    $st = $db->prepare("SELECT id FROM orders ORDER By id DESC LIMIT 1");
    $st->execute();
    $r = $st->fetchObject();
    
    return ($r->id + 1);
}
function get_orders($db, $id){
    if(!empty($id[1])){
        $limit = "LIMIT ".$id[1];
    }
    $st = $db->prepare("SELECT * FROM orders WHERE customer_id = ? AND status = ? ORDER BY created_on DESC {$limit}");
    $st->execute(array($id[0], "Paid"));
    if($st->rowCount() == 0){
        echo "<tr>
              <td colspan='4'>No orders</td>
              </tr>";
    }else{
        while($r = $st->fetchObject()){
            echo "<tr>";
            if(empty($id[1])){
                  echo "<td data-label='Order'>{$r->unique_id}</td>";
            }
            echo "<td data-label='Date'>".date("j.m.Y", strtotime($r->ipn_received))."</td>
                  <td data-label='Amount'>&pound;".number_format($r->total,2)."</td>
                  <td data-label='Status'>{$r->status}</td>";
            if(empty($id[1])){
                echo "<td data-label='Shipping'>{$r->shipping_status}</td>
                      <td data-label='Tracking'></td>";
            }
            echo "<td data-label='Action'><a href='/o-hub/orders/{$r->unique_id}' title='View Order' class='btn blue'>View</a>";
            if($r->status == "Paid" && empty($id[1])){
                echo " <a href='/actions.php?action=reorder&id={$r->unique_id}' title='Re-Order' class='btn blue'>Re-Order</a>";
            }
            echo "</td>
                  </tr>";
        }
    }
}
function get_auto_repeat($db,$customer){
    if(!empty($customer[1])){
        $and = " AND pet_id = '{$customer[1]}'";
    }
    $st = $db->prepare("SELECT a.id, a.start_date, p.name, a.frequency, a.status, a.next_due FROM auto_repeat a LEFT JOIN pets p ON a.pet_id = p.unique_id WHERE a.customer_id = ? {$and}");
    $st->execute(array($customer[0]));
    if($st->rowCount() == 0){
        echo "<tr>
              <td colspan='7'>No auto repeat orders</td>
              </tr>";
    }else{
        $ss = $db->prepare("SELECT * FROM auto_repeat_items WHERE auto_id = ?");
        while($r = $st->fetchObject()){
            $total = array();
            $ss->execute(array($r->id));
            while($s = $ss->fetchObject()){
                $total[] = ($s->quantity*$s->price);
            }
            echo "<tr>
                  <td>".date("jS F, Y", strtotime($r->start_date))."</td>
                  <td>{$r->name}</td>
                  <td>&pound;".number_format(array_sum($total),2)."</td>
                  <td>".ucwords($r->frequency)."</td>
                  <td>".date("jS F, Y", strtotime($r->next_due))."</td>
                  <td>{$r->status}</td>
                  
                  <td><a href='/o-hub/auto-repeat/{$r->id}' title='View Order' class='btn blue'>View</a>";
            switch($r->status){
                case "Active":
                    ?>
                    <a href="/o-hub/confirm/cancel-auto-repeat/<? echo $r->id; ?>" title="Cancel Auto Repeat" class="btn red">Cancel</a>
                    <?
                break;
                case "Inactive":
                    ?>
                    <a href="/o-hub/confirm/activate-auto-repeat/<? echo $r->id; ?>" title="Activate Auto Repeat" class="btn red">Activate</a>
                    <?
                break;
            }
            echo "</td>
                  </tr>";
        }
    }
}
function send_order_email($db, $id){
    global $company;
    
    include "includes/mail-headers.php";
    
    $st = $db->prepare("SELECT o.id, c.name, c.surname, c.email, c.tmp, o.customer_id, o.unique_id, o.referrer, c.unique_id AS customer_code, o.total, o.discount_total, o.confirmation, c.stripe_id, o.shipping, c.tmp_password
                        FROM orders o 
                        LEFT JOIN customers c ON o.customer_id = c.id
                        WHERE o.unique_id = ?");
    $st->execute(array($id));
    
    if($st->rowCount() > 0){
        $order = $st->fetchObject();
        
        // Select the pet
        $sp = $db->prepare("SELECT name FROM cart s LEFT JOIN pets p ON s.pet_id = p.unique_id WHERE s.order_id = ? AND p.name != ?");
        $sp->execute(array($order->id, ""));
        $pet = $sp->fetchObject();
        
        
        if(!empty($order->tmp_password)){
                            // Create a password and send them a welcome email
                            $secure = new SecureHash();

                            // Password
                            $date = new DateTime();
                            $hash =  $date->getTimestamp();
                            $password = $order->tmp_password;

                            // Salt
                            $salt = uniqid(mt_rand(), true);

                            // Encrypted password
                            $encrypted = $secure->create_hash($password, $salt);
                            
                            
                            $url = 'https://api-ssl.bitly.com/v4/shorten';
                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['long_url' => MAIN_SITE."/refer/".$order->customer_code]));
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                                "Authorization: Bearer ".$bitly,
                                "Content-Type: application/json"
                            ]);

                            $arr_result = json_decode(curl_exec($ch));
                
                            

                            // Unset the tmp flag and set the password
                            $su = $db->prepare("UPDATE customers SET salt = ?, encryption = ?, tmp = ?, referral_code = ?, tmp_password = ? WHERE id = ?");
                            $su->execute(array($salt, $encrypted, 0, !empty($arr_result->link) ? $arr_result->link : "", "", $order->customer_id));


                            // The email

                            $mailer = $mail_header;

                            $mailer .= "<tr>
                                        <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: Arial, Gill Sans, Geneva, Museo Sans,  museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Welcome to Ooddles</td>
                                        </tr>
                                        <tr>
                                        <td style='padding:20px; background-color: #fff;'>
                                        <p style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;; font-size:12px; line-height:150%;'>Hello {$order->name}";
                            if(!empty($pet->name)){
                                $mailer .= " and {$pet->name}";
                            }
                            $mailer .= ",<br /><br />
                                        Welcome to Ooddles. <br /><br />

To manage your account, please log into our O Hub, <a href='".MAIN_SITE."/o-hub' title='O Hub' style='color:#000; text-decoration:none; font-weight:bold;'>here</a> via the Ooddles website. Please use the email and password you set up.<br /><br />
You can view your orders, track, update your details, add a dog and lots more. <br /><br />
We hope you enjoy our refer a dog rewards programme, when you are logged in you will see your unique referral code, your friends will receive a discount and you can rack up your promo codes to use on future orders.<br /><br />
We look forward to you sharing your dog Ooddling with us at <a href='{$company->facebook}' title='Facebook' style='color:#000; text-decoration:none; font-weight:bold;'>Facebook</a> or <a href='{$company->instagram}' title='Instagram' style='color:#000; text-decoration:none; font-weight:bold;'>Instagram</a> and do keep up to date with Oode and his antics.<br /><br />
If you need any help, please contact customer services <a href='mailto:{$company->email}' title='Email' style='color:#000; text-decoration:none; font-weight:bold;'>{$company->email}</a>, or on live chat.<br /><br />

Happy Ooddling.


</p>
                                        </td>
                                        </tr>";

                            $mailer .= $mail_footer;
                            
                            

                            $email = new PHPMailer();
                            $email->From = $company->email;
                            $email->FromName = $company->name;

                            $email->Subject = "Welcome to Ooddles";
                            $email->Body = $mailer;
                            $email->IsHTML(true);

                            $email->AddAddress($order->email);
                            //$email->AddBCC("david.oldfield@brainstormdesign.co.uk");
                            $email->Send();

                            



                        } // End of welcome email
        
         // Handle the transaction
            $st = $db->prepare("SELECT *
                                FROM deliveries
                                WHERE order_id = ?");
            $st->execute(array($order->id));
            $delivery = $st->fetchObject();

        // The delivery address
            $delivery_address = $delivery->d_address.", ";
            if(!empty($delivery->d_address2)){
                $delivery_address .= $delivery->d_address2.", ";
            }
            $delivery_address .= $delivery->d_city.", ".$delivery->d_county." ".strtoupper($delivery->d_postcode)." ".$delivery->d_country;

           


            // Select the pet
            $sp = $db->prepare("SELECT name FROM cart s LEFT JOIN pets p ON s.pet_id = p.unique_id WHERE s.order_id = ? AND p.name != ?");
            $sp->execute(array($order->id, ""));
            $pet = $sp->fetchObject();
        
        
        // Order email
            $mailer = $mail_header;
            if(!empty($pet->name)){
                $tag = $pet->name;
            }else{
                $tag = "your dog";
            }
            $mailer .= "<tr>
                        <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: Arial, Gill Sans, Geneva, Museo Sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Your Ooddles order</td>
                        </tr>
                        <tr>
                        <td style='padding:20px; background-color: #fff;'>
                        <p style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; text-align:center;'>Hello {$order->name}<br />
                        Thank you for your order, please let {$tag} know it will be arriving shortly.<br />
                        Your Order Number is {$order->unique_id}.</p>
                        <table cellpadding='0' cellspacing='0' width='100%'>
                        <tr>
                        <th width='25%' style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>PRODUCT</th>
                        <th width='50%' style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>DESCRIPTION</th>
                        <th style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>QTY</th>
                        <th style='text-align:left; text-transform:uppercase; font-weight:bold; padding:10px 0px; border-top:1px solid #000; border-bottom:1px solid #000; font-size:12px;'>PRICE</th>
                        </tr>";
            $sd = $db->prepare("SELECT s.title, c.price, SUM(c.price) AS subtotal, COUNT(c.id) AS qty, p.title AS product_title, i.category, p.image, c.auto_repeat, s.quantity, s.weight, s.unit, c.discount, SUM(c.discount) AS overall_discount
                                FROM cart c
                                LEFT JOIN skus s ON c.sku_id = s.id
                                LEFT JOIN products p ON s.product_id = p.id
                                LEFT JOIN categories i ON p.category_id = i.id
                                LEFT JOIN orders o ON c.order_id = o.id
                                WHERE c.order_id = ?
                                GROUP BY c.sku_id, c.auto_repeat");            
            $sd->execute(array($order->id));


            while($d = $sd->fetchObject()){
                $mailer .= "<tr>
                      <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'>";
                if(!empty($d->image)){
                    $mailer .= "<img src='".MAIN_SITE."/thumb.php?src=/images/products/{$d->image}&w=80&h=80&zc=2' alt='{$d->title}' />";
                }
                $mailer .= "</td>
                            <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'><strong>{$d->product_title}</strong><br />
                            {$d->quantity} x {$d->weight}{$d->unit} {$d->title}";
                if(!empty($d->auto_repeat)){
                    // Set the auto repeat flag
                    $rerun = 1;
                    $mailer .= "<br /><span style='font-style:italic;'>Auto Repeat {$d->auto_repeat}</span>";
                }
                $mailer.=  "</td>
                            <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'>{$d->qty}</td>
                            <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:10px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format(($d->subtotal-$d->overall_discount),2)."</td>
                            </tr>";
            }

            $mailer .= "<tr>
                        <td  colspan='2'></td>
                        <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Subtotal</td>
                        <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format($order->total,2)."</td>
                        </tr>
                        <tr>
                        <td  colspan='2'></td>
                        <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Discount</td>
                        <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format($order->discount_total,2)."</td>
                        </tr>
                        <tr>
                        <td  colspan='2'></td>
                        <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Delivery</td>
                        <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format($order->shipping,2)."</td>
                        </tr>
                        <tr>
                        <td  colspan='2'></td>
                        <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;font-weight:bold;'>Total</td>
                        <td style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; padding:5px 0px; border-bottom:1px solid #ccc;'>&pound;".number_format(($order->total-$order->discount_total+$order->shipping),2)."</td>
                        </tr>
                        </table>
                        
                        <p style='padding:10px 0px;  text-align:left;'><strong style='text-transform:uppercase;'>Delivery Details</strong><br />
                        {$delivery_address}</p>
                        <p style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:12px; line-height:150%; text-align:left;'>Please do log into your O Hub account, using your accounts details you created. <br />
                        Any problems, please contact customer services at <a href='mailto:{$company->email}' title='Email {$company->name}' style='color:#000; text-decoration:none; font-weight:bold;'>{$company->email}</a> or jump onto live chat.<br /><br />
                        Happy Ooddling<br />
                        Ooddles Team</p>


                        </td>
                        </tr>";

            $mailer .= $mail_footer;
        
            
            include "classes/class.phpmailer.php";
            $email = new PHPMailer();
            $email->From = $company->email;
            $email->FromName = $company->name;

            $email->Subject = "Your Ooddling";
            $email->Body = $mailer;
            $email->IsHTML(true);
            //echo $company->email;
            $email->AddAddress($order->email);
            //$email->AddBCC("david.oldfield@brainstormdesign.co.uk");
            // The invoice		
            $html = file_get_contents(MAIN_SITE."/invoice.php?id=".$order->unique_id);

            if(!empty($html)){
                // instantiate and use the dompdf class
                $options = new Options();
                $options->setIsRemoteEnabled(true);
                $options->setDefaultFont('Helvetica');

                $dompdf = new Dompdf($options);


                $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation
                $dompdf->setPaper('A4', 'portrait');

            // Render the HTML as PDF
                $dompdf->render();

                $link = $order->unique_id."-invoice.pdf";

                $output = $dompdf->output();

                file_put_contents($link, $output);


                // move the file
                if(file_exists(ROOT.'/'.$link)){
                    rename(ROOT.'/'.$link, INVOICES.'/'.$link);    
                }

                $invoice = INVOICES.'/'.$link;
            }
            if(file_exists($invoice)){
                $email->AddAttachment($invoice);
            }
       
            $email->Send();

            // Update the order with confirmation sent.
            $su = $db->prepare("UPDATE orders SET confirmation = ? WHERE unique_id = ?");
            $su->execute(array(date("Y-m-d H:i:s"), $order->unique_id));
        
            unset($_SESSION['code']);
            unset($_SESSION['account_password']);
            unset($_SESSION['dog']);
        
        
    }
}
function create_auto_repeat($db, $id){
    // Create the auto repeat
    
}
function fetch_auto_repeat($db, $subscription_id){
    global $stripe_secret_key;
    // Fetch the auto_repeat items from stripe
    $st = $db->prepare("SELECT * FROM auto_repeat WHERE subscription_id = ?");
    $st->execute(array($subscription_id));
    $x = $st->fetchObject();
    
    require_once('stripe/init.php');
    $stripe = new \Stripe\StripeClient($stripe_secret_key);

    $parts = $stripe->subscriptions->retrieve($subscription_id);
    
    
    $ss = $db->prepare("SELECT id FROM skus WHERE stripe_product = ?");

    if(!empty($parts->items->data)){
        $sd = $db->prepare("DELETE FROM auto_repeat_items WHERE auto_id = ?");
        $sd->execute(array($x->id));

        $si = $db->prepare("INSERT INTO auto_repeat_items (auto_id, sku_id, price, quantity, stripe_id, stripe_product) VALUES (?,?,?,?,?,?)");

        $sums = array();

        foreach($parts->items->data as $d){

            if($d->price->product != "prod_IncCS1IgE5S1Ur"){
                $ss->execute(array($d->price->product));
                $s = $ss->fetchObject();

                $si->execute(array($x->id, $s->id, ($d->price->unit_amount/100), $d->quantity, $d->id, $d->price->product));

                $sums[] = (($d->price->unit_amount/100) * $d->quantity);
                $frequency = $d->price->recurring->interval_count;
            }else{
                $shipping = "4.95";
            }
        }
        if($frequency == 2){
            $freq = "bi-monthly";
        }else{
            $freq = "monthly";
        }

    // Update the auto repeat
        $sg = $db->prepare("UPDATE auto_repeat SET frequency = ?, next_due = ?, status = ?, total = ?, shipping = ?, last_updated = ? WHERE id = ?");
        $sg->execute(array($freq, date("Y-m-d", $parts->current_period_end), ucwords($parts->status), array_sum($sums), !empty($shipping) ? $shipping : 0, date("Y-m-d H:i:s"), $x->id));
    }
    
}



// Function to return the JavaScript representation of a TransactionData object.
function getTransactionJs(&$trans) {
  return <<<HTML
ga('ecommerce:addTransaction', {
  'id': '{$trans['id']}',
  'affiliation': '{$trans['affiliation']}',
  'revenue': '{$trans['revenue']}',
  'shipping': '{$trans['shipping']}',
  'tax': '{$trans['tax']}'
});
HTML;
}

// Function to return the JavaScript representation of an ItemData object.
function getItemJs(&$transId, &$item) {
  return <<<HTML
ga('ecommerce:addItem', {
  'id': '$transId',
  'name': '{$item['name']}',
  'sku': '{$item['sku']}',
  'category': '{$item['category']}',
  'price': '{$item['price']}',
  'quantity': '{$item['quantity']}'
});
HTML;
}

?>