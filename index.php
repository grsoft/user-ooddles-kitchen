<?php include "header.php"; ?>
    <div id="banner" class="geoff">
        <a href="/" title="<? echo $company->name; ?> Logo"><img src="<? echo $sirv; ?>/images/logo.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
        <? include "includes/nav.php"; ?>
        
        
        <?
        $st = $db->prepare("SELECT * FROM banners WHERE status = ?");
        $st->execute(array("Published"));
        if($st->rowCount() > 0){
            ?>
        
        <div class="glide">

				<div class="glide__arrows">
					<button class="glide__arrow prev" data-glide-dir="<">prev</button>
					<button class="glide__arrow next" data-glide-dir=">">next</button>
				</div>

				<div class="glide__wrapper">
					<ul class="glide__track">
                        <?
                while($r = $st->fetchObject()){
                    echo "<li class='glide__slide'><a href='/get-ooddling' title='Start Ooddling'><img data-src='{$sirv}/images/{$r->image}' alt='Banner' class='advert Sirv' /><img data-src='{$sirv}/images/{$r->mobile_image}' alt='Banner' class='advert mobile Sirv' /></a></li>";
                }
						?>
						
						
					</ul>
				</div>

				<div class="glide__bullets"></div>

			</div>
    <?   } ?>    
        
       
        
    </div><!--close banner-->
    <?
        if($company->order_discount > 0){
            echo "<div class='strapline'><a href='/get-ooddling' title='Get Ooddling'>Are you new to Ooddling? - First order {$company->order_discount}% off with {$company->order_code} code</a></div><!--close strapline-->";
        }
    ?>
    <section>
       
        <section>

        <?php
        $st = $db->prepare("SELECT * FROM company");
        $st->execute(array());
        ?>
       
        
            <div class="introduction introduction2">
            <?php while($r = $st->fetchObject()){
                echo "<p>{$r->introduction}</p>";
            }
                ?>
       
        
            <!-- <div class="introduction introduction2">
                <h1>Ooddles - Natural & Made With Fresh Quality Meat.</h1>
                    <h1>The Perfect Choice For Every Dog, Every Day.</h1>
    <p>When your dog eats quality daily nutrition, full of fresh meat, they look and feel amazing.</p>
       <p> They don't need special recipes; they just need fresh & natural.</p>
        <p>Ooddles foods are full of fresh meat, veggies, botanicals & slowly cooked.<p>
        <p>Dry, Wet or Raw you decide. Grain free and hypollergenic.</p>
       <p> Personalised and delivered.</p></div> -->
        </section>
		
		
        <section>
        <div class="flex negative options more_images MoreImage_Gallery" style="text-align: -webkit-center">
            <div class="c_33">
                <a href="/fussy-eaters">
                    <img src="<? echo $sirv; ?>/images/more1.png" alt="">
                </a>
            </div>

            <div class="c_33">
                <a href="/allergies">
                    <img src="<? echo $sirv; ?>/images/more2.png" alt=""></a>
            </div>

            <div class="c_33">
                <a href="/sensitive-tummies">
                    <img src="<? echo $sirv; ?>/images/more3.png" alt=""></a>

            </div>

        </div>
    </section>
        <!--<div class="introduction">
            <h1>Ooddles Natural Premium Dog Food</h1>
<p>Personalised and delivered to your door. Wet, dry or Bare (Raw) - You decide.</p>
            <a href="#" title="Read More" class="scroll btn yellow" data-id="more_about">Read More</a>
            
            
        
        <a href="/preview.php" title="Meet Oode" class="preview"><img data-src="<? echo $sirv; ?>/images/meet-oode.jpg?w=410" alt="Meet Oode" class="Sirv" /></a>
        </div><!--close introduction-->
        
    </section>
<?
   // $st = $db->prepare("SELECT * FROM videos");
    //$st->execute(array());
   // if($st->rowCount() > 0){
   //     echo "<section><div class='flex negative videos'>";
   //     while($r = $st->fetchObject()){
   //         echo "<div class='c_33'><div class='inner'><a href='#' title='Watch Video' data-id='{$r->id}'><img src='/images/{$r->image}' alt='Video' /><span></span></a></div></div>";
   //     }
     //   echo "</div></section><div id='video'></div>";
   //}
?>
<!--<section>
    <div class="flex negative">
        <div class="c_33"><div class="inner"><a href="/get-ooddling" title="Start Ooddling"><img data-src="<? echo $sirv; ?>/images/image-really-good.jpg?w=700" alt="Really Good Dog Food" class="Sirv" /></a></div></div>
        <div class="c_33"><div class="inner"><a href="/get-ooddling" title="Start Ooddling"><img data-src="<? echo $sirv; ?>/images/image-are.jpg?w=700" alt="Ooddles Foods Are" class="Sirv" /></a></div></div>
        <div class="c_33"><div class="inner"><a href="/get-ooddling" title="Start Ooddling"><img data-src="<? echo $sirv; ?>/images/image-change.jpg?w=700" alt="Make The Change" class="Sirv" /></a></div></div>
    </div>
</section>-->
 <? include "includes/ranges.php"; ?>
<!-- <section>
    <!--<a href="/shop" title="Shop"><img data-src="<? //echo $sirv; ?>/images/banner-amazing.jpg?w=1500" alt="Amazing" class="Sirv non_mobile" />
    <img data-src="<? //echo $sirv; ?>/images/banner-amazing-mobile.jpg?w=1500" alt="Amazing" class="Sirv mobile" /></a>
	 <!-- <a href="/sample.php" title="Shop"><img data-src="<? //echo $sirv; ?>/images/bundlepack.png?w=1500" alt="Amazing" class="Sirv non_mobile" />
    <img data-src="<? //echo $sirv; ?>/images/bundlepack.png?w=1500" alt="Amazing" class="Sirv mobile" /></a> 
</section> -->
 <section class="hub text-center">
    
    <div class="flex negative ">
        
        
        <div class="c_33">
            <div class="inner">
       <a href="/bundle-offer" title="Sample"><img src="<? echo $sirv; ?>/images/m1.png"  alt="Breeders" class="Sirv" /></a>
            </div><!--close inner-->
        </div><!--close c_33-->
        
    

        <div class="c_33">
            <div class="inner">
       <a href="/blog/ooddles-tv" title="Watch Video"><img src="<? echo $sirv; ?>/images/m2.png"  alt="Breeders" class="Sirv" /></a>
            </div><!--close inner --->
        </div>


        
        <div class="c_33">
            <div class="inner">
       <a href="/shop" title="Shop"><img src="<? echo $sirv; ?>/images/m3.png"  alt="Breeders" class="Sirv" /></a>
            </div><!--close inner-->
        </div>


        

        <!-- <div class="c_50">
            <div class="inner">
                <a href="/shop" title="Breeders"><img src="<? //echo $sirv; ?>/images/9.png" alt="Breeders" class="Sirv" /></a>
            </div>
        </div>close c_33 -->
    
        
  
    </div><!--close flex-->
   
    </section>
   



    <section class="hub image_collection text-center">

    <div class="flex negative">
        <div class="c_25 text-center res-image">
            <div class="inner-img ">
            <a href="/shop/accessories" title="Accessories & Toys"><img src="<? echo $sirv; ?>/images/p1.png" alt="" class="Sirv img-fluid"  /></a>
            </div>
        </div>
        <div class="c_25 text-center res-image">
            <div class="inner-img " style="text-align: center;">
               <a href="/ooddles-business.php" title="Ooddles-business"><img src="<? echo $sirv; ?>/images/middle.png" alt="" class="Sirv img-fluid"  /></a>
            </div>
        </div>
        <div class="c_25 text-center res-image">
            <div class="inner-img" style="text-align: center;">
            <a href="#" title="Woof Magzine"><img src="<? echo $sirv; ?>/images/p3.png" alt="" class="Sirv img-fluid"  /></a>
            </div>
        </div>
        <div class="c_25 text-center res-image">
            <div class="inner-img">
                <a href="/sign-up" title=""><img src="<? echo $sirv; ?>/images/p4.png" alt="" class="Sirv img-fluid"  /></a>
            </div>
        </div>
        <div class="c_50">
            <div class="inner-img">
                <a href="/come-dine-with-ooddles" title=""><img src="<? echo $sirv; ?>/images/L1.png" alt="" class="Sirv img-fluid"  /></a>
            </div>
        </div>
        <div class="c_50 ">
            <div class="inner-img">
                <a href="/o-hub" title="Start Ooddling"><img src="<? echo $sirv; ?>/images/L2.png" alt="" class="Sirv img-fluid"  /></a>
            </div>
        </div>
    </div>
</section>

	 <!-- <section class="hub image_collection text-center">
    
    <div class="flex negative">
        <div class="c_25 text-center">
            <div class="inner-img ">
                <a href="/dog-accessories" title="Breeders"><img src="<? //echo $sirv; ?>/images/10.png" alt="Breeders" class="Sirv img-fluid w-75 " /></a>
            </div>
        </div>
        <div class="c_25 text-center">
            <div class="inner-img " style="text-align: center;">
                <a href="#" title="Breeders">business panel<img src="#" alt="" class="Sirv   w-75"  /></a>
            </div>
        </div>
        <div class="c_25 text-center ">
            <div class="inner-img" style="text-align: center;">
                <a href="#" title="Breeders"> business panel<img src="#" alt="" class="Sirv img-fluid  w-75 "  /></a>
            </div>
        </div>
        <div class="c_25 text-center">
            <div class="inner-img">
                <a href="/sign-up" title="Breeders"><img src="<? //echo $sirv; ?>/images/11.png" alt="Breeders " class="Sirv  img-fluid w-75"  /></a>
            </div>
        </div>
    </div>
</section> -->
<!-- <section>
    <a href="/OoddlesKitchen" title="Shop"><img src="<?// echo $sirv; ?>/images/12.png" alt="Amazing" class="Sirv non_mobile" />
    <img data-src="" alt="Amazing" class="Sirv mobile" /></a>
	<a href="/get-ooddling" title="Shop"><img src="<? //echo $sirv; ?>/images/13.png" alt="Amazing" class="Sirv non_mobile" />
    <img data-src="" alt="Amazing" class="Sirv mobile" /></a>
</section> -->
<!-- 8 session end   -->
    <!--<h3 class="subtitle">Feeding the nations much loved dogs - Personalised premium dog food</h3>-->
    <? //include "includes/hub.php"; ?>
<? //include "includes/snippets.php"; ?>

<h1><p class="start centre">Don't take our word for it, see what others say</p></h1>
   
<section class="no_top_pad">
<div id="feefo_wrapper">
    <div id="feefo-service-review-carousel-widgetId" class="feefo-review-carousel-widget-service"></div>
</div><!--close wrapper-->
    </section>

<section class="hub image_collection HubImages_Slider text-center">
    <div class="flex negative">
        <!-- first slider start here  -->
        <div class="c_50">
            <div id='mixedSlider' class='home-slider-left' style='background-color: #7fc2e5;'>
                <div class=' flex negative top-content12 '>
                   <div class="c_70 Slide_content">
                        <h1>Get Social</h1>
                        <p style="font-size: 18px; margin-top: 10px;">We love to see your dog</br>
                        Ooddling</br>
                        Tag @ooddleskitchen for </br>
                       your chance to be featured</p>
                    </div>


                <div class="c_30 right_image">
                          <a href="/come-dine-with-ooddles" title="Come Dine With Ooddles">  <img src="/images/bottomlogo.png" alt="bottom logo"></a>
                  </div>
                  </div>

                <div class='MS-content'>
                    <div class='item '>
                        <div class='imgTitle'>
                            <img src='/images/Asset 1.png' alt='' />
                              </div>

                    </div>
                    <div class='item '>
                        <div class='imgTitle'>
                            <img src='/images/Asset 2.png' alt='' />
                        </div>

                    </div>

                    <div class='item '>
                        <div class='imgTitle'>
                            <img src='/images/Asset 3.png' alt='' />
                        </div>

                    </div>

                    <div class='item '>
                        <div class='imgTitle'>
                            <img src='/images/Asset 4.png' alt='' />
                        </div>

                    </div>
                   
                   
                  
                </div>
                <div class='MS-controls'>
                    <button class='MS-left'><i class='fa-2x fa fa-angle-left' aria-hidden='true'></i></button>
                    <button class='MS-right'><i class='fa-2x fa fa-angle-right' aria-hidden='true'></i></button>
                </div>
                <div class='flex text-center bottom_text'>
                    <a href="https://www.facebook.com/OoddlesKitchen/"><i class="fa-2x fa fa-facebook-official" aria-hidden="true"></i></a>

                    <span class='icon_text'>Follow us</span>
                    <a href="https://www.instagram.com/ooddleskitchen/">
                        <i class="fa-2x fa fa-instagram" aria-hidden="true"></i></a>
                </div>
                <div class="slider_button">
                <a href="https://www.ooddleskitchen.co.uk/come-dine-with-ooddles" title="come dine with ooddles" class="goButton  brown">Go</a></div>
            </div>
        </div>
<!-------------------------------------------------- 2nd slider start here  -------------------------------------------------->
        <div class="c_50 ">
        <div id='mixedSliderHome' class='home-slider-right' >
        <div class='top-content12'>
                    
                        <h1>Ooddles Blog</h1>
                        <p style="font-size: 18px; font-weight: bold; margin-bottom: 15px;">Plenty of Interesting articles, recipes and lots more</p>
                        
                </div>
                              
                              <div class='MS-content'>
<?php
                              $st = $db->prepare("SELECT * FROM advice WHERE publish_on <= ? ORDER BY publish_on DESC LIMIT 7");
                        $st->execute(array(date("Y-m-d")));
                        $num = $st->rowCount();
                        if($deviceType == "phone"){
                            $count = 1;
                        }else{
                            $count = 4;
                        }
                        for($x=1; $x <= $num; $x++){
                                 $r = $st->fetchObject();
                                
                              

                                 echo"<div class='item news_item'>
                        <div class='imgTitle'>
                        <a href='blog/{$r->seo}' title='{$r->title}'><img src='/images/thumbs/{$r->image}' alt='{$r->title}' /></a>
                        </div>
                        <div class='description'>
						<div class='title-height'>
                  <h3 class='home-slider-title' style='text-align: left;'><a href='blog/{$r->seo}' title='{$r->title}'>{$r->title}</a></h3>
				  </div>
                  <p class='date' style='text-align: left;'>".date("F j, Y", strtotime($r->publish_on))."</p>
				  
                  <p class='home-slider-readmore' style = 'text-align:left;'>
				  
                  <a  href='blog/{$r->seo}' title='{$r->title}'>Read more</a></p>
                  </div>

                    </div>";
                    }
?>
                </div>
                       <div class='MS-controls'>
                        <button class='MS-left' ><i class='fa-2x fa fa-angle-left' aria-hidden='true'></i></button>
                        <button class='MS-right' ><i class='fa-2x fa fa-angle-right' aria-hidden='true'></i></button>
                          </div>
                         <div class="slider_button">
                        <a href="/blog" title="Blog" class="goButton  brown">Go</a></div>
                          </div>
        </div>
        
    </div>
</section>

<?php include "includes/company.php"; ?>

<?php include "footer.php"; ?>

<script src="/js/multislider.js"></script>
<script>
    $('#mixedSlider').multislider({
        duration: 750,
        interval: 30000
      });
      
      $('#mixedSliderHome').multislider({
        duration: 750,
        interval: 30000
      });
</script>




