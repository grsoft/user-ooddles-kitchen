<? include "header.php"; 


    ?>
    <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Your Ooddles Basket</h1>
       
        
    </div><!--close headline-->
    
</div><!--close banner-->
<section>

    <?
    if($cart->qty == 0){
        ?>
        <div class="flex empty">
        <div class="c_33">
        </div>
        <div class="c_66">
            <div class="inner">
            <h3>What's going on?</h3>
            <p>You've got an empty basket. We've got a ton of tasty treats for your beloved pooch.<br />
                Use the menu at the top to browse our wide range of food.<br />
            Alternatively, you can email us on <a href="mailto:<? echo $company->email; ?>" title="Email <? echo $company->name; ?>"><? echo $company->email; ?></a> and we'll be happy to assist you.</p>
            </div>
            </div>
        </div>
        <?
    }else{
        ?>
    <div class="flex negative">
        <div class="c_33"><div class="inner">
            <p class="total">Your Order:<br />
            <span>&pound;<? echo number_format(($cart->total-$cart->discount-$discount+$delivery_price),2); ?></span></p>
            
            <p><a href="/addresses" title="Checkout" class="btn brown check">Proceed to Checkout</a></p>
            
            <?
        if($logged_in == 2){
            echo "<p><a href='/o-hub/ranges' title='Ranges'>Carry On Shopping</a></p>
                  <h3>Why not add some treats, chews, or order your flea and wormer.</h3>
                  <p><a href='/o-hub/ranges' title='Ranges' class='btn brown check small'>Shop Now</a></p>";
        }else{
            echo "<p><a href='/shop' title='Ranges'>Carry On Shopping</a></p>
                  <p>If you have an O-Hub account, please <a href='/o-hub' title='Login'>login</a> for a faster checkout.</p>
                  <h3>Why not add some treats, chews, or order your flea and wormer.</h3>
                  <p><a href='/shop' title='Ranges' class='btn brown check small'>Shop Now</a></p>";    
        }
            ?>
           
            
            </div></div>
        <div class="c_66"><div class="inner">
             <table class="table-responsive-full order_table">
      <thead>
        <tr>
          <th>Product</th>
          <th>Description</th>
          <th>Unit Price</th>
            <th>Quantity</th>
            <th>Subtotal</th>
            <th>Action</th>
        </tr>
      </thead>
      <tbody>
          <? 
                $sd = $db->prepare("SELECT c.id,c.basket_data, s.title, c.price, SUM(c.price) AS subtotal, COUNT(c.id) AS qty, p.title AS product_title, i.category, p.image, c.sku_id, c.auto_repeat, s.quantity, s.weight, s.unit, c.discount, SUM(c.discount) AS overall_discount
                                    FROM cart c
                                    LEFT JOIN skus s ON c.sku_id = s.id
                                    LEFT JOIN products p ON s.product_id = p.id
                                    LEFT JOIN categories i ON p.category_id = i.id
                                    LEFT JOIN orders o ON c.order_id = o.id
                                    WHERE o.session_id = ? AND o.status = ?
                                    GROUP BY c.sku_id, c.auto_repeat");
                $sd->execute(array(session_id(), "Unpaid"));
                while($d = $sd->fetchObject()){

                    echo "<tr>
                          <td>";
                    if(!empty($d->image)){
                        echo "<img src='/thumb.php?src=/images/products/{$d->image}&w=80&h=80&zc=2' alt='{$d->title}' />";
                    }


                    echo "</td>";
                   
                    echo "<td>";
                     if($d->basket_data){
                      echo "<strong>Sample Offer Bundle</strong><br />";
                    $restdata=unserialize($d->basket_data);

                    foreach ($restdata as $key => $value) {
                     // print_r($value);                      

                      echo $value['product'];
                      
                    }

                  }
                  else{
                       echo "<strong>{$d->product_title}</strong><br />
                          {$d->quantity} x {$d->weight}".strtolower($d->unit)." {$d->title} ";
                      }
                   echo "</td>";
                        
                    echo "<td>&pound;".number_format($d->price,2)."</td>
                          <td>{$d->qty}</td>
                          <td>";
                    if($d->overall_discount > 0){
                       echo "&pound;".number_format(($d->subtotal-$d->overall_discount),2)." <span class='linethrough'>&pound;".number_format($d->subtotal,2)."</span>"; 
                    }else{
                        echo "&pound;".number_format($d->subtotal,2);    
                    }
                    if(!empty($d->auto_repeat)){
                        echo "<br /><span class='repeat'>Auto Repeat {$d->auto_repeat}</span>";
                    }
                    echo "</td>
                          <td class='actions'><a href='/actions.php?action=add-one&id={$d->id}' title='Add One'><img src='/images/icon-add.png' alt='Add One' /></a><a href='/actions.php?action=remove-one&id={$d->id}' title='Remove One' class='nug plus'><img src='/images/icon-minus-one.png' alt='Remove One' /></a><a href='/actions.php?action=remove-all&id={$d->sku_id}' title='Remove All' class='nug plus'><img src='/images/icon-trash.png' alt='Trash' /></a></td>
                          </tr>";
                }
            ?>
      </tbody>
  </table>
            <?
        
        if(($cart->total-$cart->discount-$discount) < $min_spend){
            echo "<div class='note'><p>Spend another &pound;".number_format(($min_spend - ($cart->total-$cart->discount-$discount)),2)." and get <span>FREE DELIVERY</span></p></div>";
        }
        ?>
            <div class="totals">
                <div class="flex negative">
                    <div class="c_33"><div class="inner">
                <p><strong>Subtotal:</strong> &pound;<? echo number_format(($cart->total-$cart->discount),2); ?><br />
                    <strong>Discount:</strong> &pound;<? echo number_format($discount,2); ?><br />
                    <strong>Delivery:</strong> &pound;<? echo number_format($delivery_price,2); ?><br />
                        <strong>Total:</strong> &pound;<? echo number_format(($cart->total-$cart->discount-$discount+$delivery_price),2); ?></p>
                        </div></div>
                    <div class="c_66"><div class="inner">
                        
                        <form name="discount" method="post" action="/actions.php?action=apply-discount">
                            <h3>Discount Code?</h3>
                            <?
        if(!empty($_SESSION['code'])){
            echo "<p><strong>Code applied: </strong> ".$_SESSION['code'];
            if($minimum_spend > 0){
                echo " (Minimum Spend: &pound;".number_format($minimum_spend,2).")";
            }
            echo "</p> 
                  <p><a href='/actions.php?action=remove-code' title='Remove Code'>[ Remove Code ]</a></p>";
        }else{
            
        ?>
            <p>If you have a discount code, please enter it below:</p>
            <input name="code" type="text" value="<? echo $_SESSION['code']; ?>" /><input name="submit" type="submit" value="Apply" />
        <? } ?>                    
                        </form>
                        </div></div>
                </div>
            </div><!--close totals-->
            <div class="help">
                <p>If you need any help or assistance with your order, please email us on <a href="mailto:<? echo $company->email; ?>" title="Email <? echo $company->name; ?>"><? echo $company->email; ?></a>.</p>
                
            </div><!--close help-->
             <img src="/images/payment-logos.jpg" alt="Payment Logos" class="payments" />
            </div></div>
    </div>
            
   
    <?
    }
    ?>
    </section>
<?
            $st = $db->prepare("SELECT p.title, p.image, r.title AS range_title, p.image, p.introduction, p.description, p.composition, p.id, p.nutrition, p.constituents, p.food_type, p.seo, r.seo AS range_seo, c.seo AS category_seo
                                FROM products p
                                LEFT JOIN product_ranges r ON p.range_id = r.id 
                                LEFT JOIN categories c ON p.category_id = c.id 
                                WHERE c.id = ? AND p.status = ? ORDER BY RAND() LIMIT 8");
            $st->execute(array(6, "Published"));
            $num = $st->rowCount();
            if($num > 0){
                ?>
                    <section class="light_blue products shop">
                        <div class="related light_blue">
                  <h3>Why not try these tasty treats?</h3>
                 <div class="glide">

				<div class="glide__arrows">
					<button class="glide__arrow prev" data-glide-dir="<">prev</button>
					<button class="glide__arrow next" data-glide-dir=">">next</button>
				</div>

				<div class="glide__wrapper">
					<ul class="glide__track">
                        <li class="glide__slide">
                            <div class="flex negative justify">
						<? 
                            for($x=1; $x <= $num; $x++){
                                $r = $st->fetchObject();
                                echo "<div class='c_25'>
                                      <div class='inner item'>
                                      <div class='product_img'><a href='/shop/{$r->range_seo}/{$r->seo}' title='{$r->title}'><img src='/thumb.php?src=/images/products/{$r->image}&w=500&h=500&zc=2' alt='{$r->title}' /></a></div>
                                      <h3><a href='/shop/{$r->range_seo}/{$r->seo}' title='{$r->title}'>{$r->range_title}<br />{$r->title}</a></h3>
                                      <p><a href='/shop/{$r->range_seo}/{$r->seo}' title='{$r->title}' class='btn white'>Buy Now</a></p>
                                      </div>
                                      </div>";
                                if($x % 3 == 0 && $x != $num){
                                    echo "</div></li><li class='glide__slide'><div class='flex negative justify'>";
                                }
                            }
                        ?>
                        </div>
                        </li>
						
						
					</ul>
				</div>

				<div class="glide__bullets"></div>

			</div>
                            </div>
                            </section>
    <? } 

include "includes/company.php";
include "footer.php"; ?>