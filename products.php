<? include "header.php"; 


    ?>
    <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Our Foods</h1>
        <div class="c_75">
            <?
            $sf = $db->prepare("SELECT * FROM pages WHERE id = ?");
            $sf->execute(array(10));
            $f = $sf->fetchObject();
            echo "<p>{$f->introduction}</p>{$f->description}";
            ?>
         <br /><p class="start centre"><a href="/shop" title="Shop Now" class="btn brown">Shop Now</a></p>
        </div>
        
    </div><!--close headline-->
    
</div><!--close banner-->
<section>
    <div id="breadcrumbs">
        <ul class="flex">
            <li><a href="/" title="<? echo $company->name; ?>">Home</a></li>
            <li>&rang;</li>
            <li><a href="/our-foods" title="Our Foods">Our Foods</a></li>
            
            
        </ul>
        </div>
                
    </section>
    
    <section class="products">
    <div class="flex negative">
    <?
        
        // Our wheres...
        if(!empty($xx->size)){
            $where .= " AND p.suitable_for LIKE '%{$xx->size}%'";
        }
        if(!empty($xx->dislikes)){
            if(stripos($xx->dislikes, ",") !== false){
                $bits = explode(",", $xx->dislikes);
                foreach($bits as $b){
                    $where .= " AND p.main_ingredient NOT LIKE '%{$b}%'";        
                }
            }else{
                $where .= " AND p.main_ingredient NOT LIKE '%{$xx->dislikes}%'";    
            }

        }
        $sc = $db->prepare("SELECT c.id, c.category, c.seo FROM products p LEFT JOIN categories c ON p.category_id = c.id WHERE p.status = ? AND c.status = ? GROUP BY c.category ORDER BY c.id ASC");
        $sc->execute(array("Published", "Published"));
        $sp = $db->prepare("SELECT * FROM products WHERE category_id = ? AND status = ?");
        
        while($c = $sc->fetchObject()){
            $sp->execute(array($c->id, "Published"));
            if($sp->rowCount() > 0){
                $p = $sp->fetchObject();
                echo "<div class='c_25'>
                      <div class='inner item'>
                      <div class='product_img'><a href='/our-foods/{$c->seo}' title='{$c->category}'><img src='/thumb.php?src=/images/products/{$p->image}&w=300&h=300&zc=2' alt='{$c->category}' /></a></div>
                      <h3><a href='/our-foods/{$c->seo}' title='{$c->category}'>{$c->category}</a></h3>
                      <p><a href='/our-foods/{$c->seo}' title='{$c->category}' class='btn white'>View Range</a></p>
                      </div>
                      </div>";
            }
        }
    
    ?>
        </div>
    </section>
    <?

include "includes/company.php";
include "footer.php"; ?>