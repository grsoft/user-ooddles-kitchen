<? include "header.php"; 


    ?>
    <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Your Addresses</h1>
       
        
    </div><!--close headline-->
    
</div><!--close banner-->
<section>

    <?
    if($cart->qty == 0){
        ?>
        <div class="flex empty">
        <div class="c_33">
        </div>
        <div class="c_66">
            <div class="inner">
            <h2>What's going on?</h2>
            <p>You've got an empty basket. We've got a ton of tasty treats for your beloved pooch.<br />
                Use the menu at the top to browse our wide range of food.<br />
            Alternatively, you can email us on <a href="mailto:<? echo $company->email; ?>" title="Email <? echo $company->name; ?>"><? echo $company->email; ?></a> and we'll be happy to assist you.</p>
            </div>
            </div>
        </div>
        <?
    }else{
        if($logged_in == 2){
            ?>
            <div class="flex negative justify">
       
        <div class="c_66">
            <div class="inner guest_check">
                <form name="checkout" method="post" action="/check-address.php">
                    
               
                
                
                    
                    <div class="flex negative">
                        <div class="c_50"><div class="inner">
                            <h3>Your Details</h3>
                          <p><strong>Name:</strong> <? echo $customer->name; ?> <? echo $customer->surname; ?><br />
                              <strong>Email:</strong> <? echo $customer->email; ?><br />
                              <strong>Telephone:</strong> <? echo $customer->telephone; ?></p>
                            <input name="b_first" type="hidden" value="<? echo $customer->name; ?>" />
                            <input name="b_surname" type="hidden" value="<? echo $customer->surname; ?>" />
                            <input name="b_email" type="hidden" value="<? echo $customer->email; ?>" />
                            <input name="b_telephone" type="hidden" value="<? echo $customer->telephone; ?>"  />
                            
                            <?
            if(empty($customer->address)){
                ?>
                  <div class="flex new_address">
                            
                            
                            <div class="c_50">
                                <label>Address 1</label>
                                <input name="b_address1" type="text" value="<? echo $_SESSION['b_address1']; ?>" tabindex="5" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>Address 2</label>
                                <input name="b_address2" type="text" value="<? echo $_SESSION['b_address2']; ?>" tabindex="6"  /></div>
                            <div class="c_50">
                                <label>City</label>
                                <input name="b_city" type="text" value="<? echo $_SESSION['b_city']; ?>" tabindex="7" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>County</label>
                                <input name="b_county" type="text" value="<? echo $_SESSION['b_county']; ?>" tabindex="8" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>Postcode</label>
                                <input name="b_postcode" type="text" value="<? echo $_SESSION['b_postcode']; ?>" tabindex="9" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>Country</label>
                                <select name="b_country"><option value="">Required</option>
                    <? 
                        $sb = $db->prepare("SELECT * FROM countries ORDER BY country ASC"); 
                        $sb->execute();
                        while($b = $sb->fetchObject()){
                            if($_SESSION['b_country'] == $b->country || empty($_SESSION['b_country']) && $b->country == "United Kingdom"){
                                echo "<option value='{$b->country}' selected>{$b->country}</option>";
                            }else{
                                echo "<option value='{$b->country}'>{$b->country}</option>";
                            }
                            
                        }
                    ?>
                    </select></div>
                            </div><!--close flex-->          
                <?            
            }else{
            ?>
                            <p><? echo $address; ?></p>
                            <input name="b_address1" type="hidden" value="<? echo $customer->address; ?>" />
                            <input name="b_address2" type="hidden" value="<? echo $customer->town; ?>" />
                            <input name="b_city" type="hidden" value="<? echo $customer->city; ?>" />
                            <input name="b_county" type="hidden" value="<? echo $customer->county; ?>"  />
                            <input name="b_postcode" type="hidden" value="<? echo $customer->postcode; ?>"  />
                            <input name="b_country" type="hidden" value="<? echo $customer->country; ?>"  />
                            
            <? } ?>
                            </div>
                            </div>
                        <div class="c_50"><div class="inner">
                            <h3>Shipping Details</h3>
                            
                            <p><input type="checkbox" name="same" value="1" id="same" <? if($_SESSION['same'] == "1"){ echo "checked='checked'"; }elseif(empty($_SESSION['same'])){ echo "checked='checked'"; } ?> /> Ship to billing address</p>
                        <div id="delivery_opts">
                        <div class="flex">
                            <div class="c_50">
                                <label>First Name</label>
                                <input name="d_first" type="text" value="<? echo $_SESSION['d_first']; ?>" tabindex="1" id="b_first" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Surname</label>
                                <input name="d_surname" type="text" value="<? echo $_SESSION['d_surname']; ?>" tabindex="2" id="b_surname" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Telephone Number</label>
                                <input name="d_telephone" type="text" value="<? echo $_SESSION['d_telephone']; ?>" tabindex="3" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Email Address</label>
                                <input name="d_email" type="text" value="<? echo $_SESSION['d_email']; ?>" tabindex="4" id="b_email" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Address 1</label>
                                <input name="d_address1" type="text" value="<? echo $_SESSION['d_address1']; ?>" tabindex="5" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>Address 2</label>
                                <input name="d_address2" type="text" value="<? echo $_SESSION['d_address2']; ?>" tabindex="6"  /></div>
                            <div class="c_50">
                                <label>City</label>
                                <input name="d_city" type="text" value="<? echo $_SESSION['d_city']; ?>" tabindex="7" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>County</label>
                                <input name="d_county" type="text" value="<? echo $_SESSION['d_county']; ?>" tabindex="8" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>Postcode</label>
                                <input name="d_postcode" type="text" value="<? echo $_SESSION['d_postcode']; ?>" tabindex="9" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>Country</label>
                                <select name="d_country"><option value="">Required</option>
                    <? 
                        $sb = $db->prepare("SELECT * FROM countries ORDER BY country ASC"); 
                        $sb->execute();
                        while($b = $sb->fetchObject()){
                            if($_SESSION['d_country'] == $b->country || empty($_SESSION['d_country']) && $b->country == "United Kingdom"){
                                echo "<option value='{$b->country}' selected>{$b->country}</option>";
                            }else{
                                echo "<option value='{$b->country}'>{$b->country}</option>";
                            }
                            
                        }
                    ?>
                    </select></div>
                            </div><!--close flex-->
                            </div><!--close delivery-->
                            
                        </div>
                            </div>
                    </div><!--close flex-->
                
                
                
                
                
                
                
                
                
                
                
            <p><? if($cart->total > 0){
                        echo "<input name='submit' type='submit' value='Proceed' />";
                    }
                ?>&nbsp;&nbsp;&nbsp;&nbsp; <a href="javascript:history.back();" title="Back">Back</a></p>
                    </form>
            </div>
        </div>
    </div>
            <?
            
        }else{
        ?>
    <div class="flex negative">
        <div class="c_33"><div class="inner login_check">
            <h2>Already an Ooddler?</h2>
            
            <p>If you have an O-Hub account already, please login.</p>
            <div class="msg code_block">
                <h3>Please note</h3>
                <p>If you had an account on our previous website, those details will not work. Please create an account.</p>
            </div>
           <? include "includes/login.php"; ?>
            
            </div></div>
        <div class="c_66">
            <div class="inner guest_check">
                <form name="checkout" method="post" action="/check-address.php" autocomplete="off">
               <h2>NEW USER?</h2> 
                    <div class="account_box">
                    <h3>Create Your New User Account Here</h3>
                    <p>If you'd like to create an account to track your orders, please enter a password below; <br /><strong>Mininum of 6 characters</strong>. Your account will be created when your payment has been received.</p>
          
                <p class="c_50"><input name="password" type="text" autocomplete="off" value="<? echo $_SESSION['account_password']; ?>" /></p>
                </div>
                <p>Please enter your details below</p>
                
                <h3>Your Details</h3>
                <p></p>
                <div class="flex">
                            <div class="c_50">
                                <label>First Name</label>
                                <input name="b_first" type="text" value="<? echo $_SESSION['first']; ?>" tabindex="1" id="b_first" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Surname</label>
                                <input name="b_surname" type="text" value="<? echo $_SESSION['surname']; ?>" tabindex="2" id="b_surname" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Telephone Number</label>
                                <input name="b_telephone" type="text" value="<? echo $_SESSION['b_telephone']; ?>" tabindex="3" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Email Address</label>
                                <input name="b_email" type="text" value="<? echo $_SESSION['email']; ?>" tabindex="4" id="b_email" placeholder="Required" /></div>
                </div>
                    <h3>Your Dogs Name</h3>
                <p></p>
                <div class="flex">
                            <div class="c_50">
                                <label>Name</label>
                                <input name="dog" type="text" value="<? echo $_SESSION['name']; ?>" tabindex="5" id="dog" placeholder="Required" /></div>
                            
                           
                </div>
                
                
                <h3>Billing Details</h3>
                <p>Please complete all required fields.</p>
                        <div class="flex">
                            
                            
                            <div class="c_50">
                                <label>Postcode</label>
                                <input name="b_postcode" type="text" value="<? echo $_SESSION['b_postcode']; ?>" tabindex="9" placeholder="Required" /></div>
                            <div class="c_50">
                               <label>House Number/Name</label>
                                <input name="b_address1" type="text" value="<? echo $_SESSION['b_address1']; ?>" tabindex="5" placeholder="Required" />
                            </div>
                                    
                            <div class="c_50">
                                <label>Address 2</label>
                                <input name="b_address2" type="text" value="<? echo $_SESSION['b_address2']; ?>" tabindex="6"  /></div>
                            <div class="c_50">
                                <label>City</label>
                                <input name="b_city" type="text" value="<? echo $_SESSION['b_city']; ?>" tabindex="7" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>County</label>
                                <input name="b_county" type="text" value="<? echo $_SESSION['b_county']; ?>" tabindex="8" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Country</label>
                                <select name="b_country"><option value="">Required</option>
                    <? 
                        $sb = $db->prepare("SELECT * FROM countries ORDER BY country ASC"); 
                        $sb->execute();
                        while($b = $sb->fetchObject()){
                            if($_SESSION['b_country'] == $b->country || empty($_SESSION['b_country']) && $b->country == "United Kingdom"){
                                echo "<option value='{$b->country}' selected>{$b->country}</option>";
                            }else{
                                echo "<option value='{$b->country}'>{$b->country}</option>";
                            }
                            
                        }
                    ?>
                    </select></div>
                            </div><!--close flex-->
                
                
                <h3>Shipping Details</h3>
                
                <p><input type="checkbox" name="same" value="1" id="same" <? if($_SESSION['same'] == "1"){ echo "checked='checked'"; }elseif(empty($_SESSION['same'])){ echo "checked='checked'"; } ?> /> Ship to billing address</p>
                        <div id="delivery_opts">
                        <div class="flex">
                            <div class="c_50">
                                <label>First Name</label>
                                <input name="d_first" type="text" value="<? echo $_SESSION['d_first']; ?>" tabindex="1" id="b_first" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Surname</label>
                                <input name="d_surname" type="text" value="<? echo $_SESSION['d_surname']; ?>" tabindex="2" id="b_surname" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Telephone Number</label>
                                <input name="d_telephone" type="text" value="<? echo $_SESSION['d_telephone']; ?>" tabindex="3" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Email Address</label>
                                <input name="d_email" type="text" value="<? echo $_SESSION['d_email']; ?>" tabindex="4" id="b_email" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>Postcode</label>
                                <input name="d_postcode" type="text" value="<? echo $_SESSION['d_postcode']; ?>" tabindex="9" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>House Number/Name</label>
                                <input name="d_address1" type="text" value="<? echo $_SESSION['d_address1']; ?>" tabindex="5" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>Address 2</label>
                                <input name="d_address2" type="text" value="<? echo $_SESSION['d_address2']; ?>" tabindex="6"  /></div>
                            <div class="c_50">
                                <label>City</label>
                                <input name="d_city" type="text" value="<? echo $_SESSION['d_city']; ?>" tabindex="7" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>County</label>
                                <input name="d_county" type="text" value="<? echo $_SESSION['d_county']; ?>" tabindex="8" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Country</label>
                                <select name="d_country"><option value="">Required</option>
                    <? 
                        $sb = $db->prepare("SELECT * FROM countries ORDER BY country ASC"); 
                        $sb->execute();
                        while($b = $sb->fetchObject()){
                            if($_SESSION['d_country'] == $b->country || empty($_SESSION['d_country']) && $b->country == "United Kingdom"){
                                echo "<option value='{$b->country}' selected>{$b->country}</option>";
                            }else{
                                echo "<option value='{$b->country}'>{$b->country}</option>";
                            }
                            
                        }
                    ?>
                    </select></div>
                            </div><!--close flex-->
                            </div><!--close delivery-->
                
                
            <p><? if($cart->total > 0){
                        echo "<input name='submit' type='submit' value='Proceed' />";
                    }
                ?>&nbsp;&nbsp;&nbsp;&nbsp; <a href="javascript:history.back();" title="Back">Back</a></p>
                    </form>
                
            </div>
        </div>
    </div>
            
   
    <?
        }
    }
    ?>
    </section>
<?

include "includes/company.php";
include "footer.php"; ?>