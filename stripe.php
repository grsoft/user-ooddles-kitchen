<?
include "configuration.php";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('stripe/init.php');


$st = $db->prepare("SELECT * FROM customers WHERE id = ?");
$st->execute(array(34904));
$x = $st->fetchObject();
$stripe = new \Stripe\StripeClient($stripe_secret_key);

if(empty($x->stripe_id)){
    
    $customer = $stripe->customers->create([
        'name' => $x->name.' '.$x->surname,
        'email' => $x->email,
        'phone' => $x->telephone,
        'address' => array('line1' => $x->address, 'city' => $x->city, 'postal_code' => $x->postcode, 'country' => $x->country),
        'metadata' => array('type' => $x->account_type, 'unique_id' => $x->unique_id)
    ]);


    if(!empty($customer->id)){
        $su = $db->prepare("UPDATE customers SET stripe_id = ? WHERE unique_id = ?");
        $su->execute(array($customer->id, $x->unique_id));
    }
}else{
    // Update
    $stripe->customers->update(
      $x->stripe_id,
      [
          'name' => $x->name.' '.$x->surname,
          'email' => $x->email,
          'phone' => $x->telephone,
          'address' => array('line1' => $x->address, 'city' => $x->city, 'postal_code' => $x->postcode, 'country' => $x->country),
          'metadata' => array('type' => $x->account_type, 'unique_id' => $x->unique_id)
      ]
    );
}

// Get an order
$sd = $db->prepare("SELECT * FROM orders WHERE customer_id = ? ORDER BY id DESC LIMIT 1");
$sd->execute(array($x->id));
$order = $sd->fetchObject();



// Get the delivery address
$sl = $db->prepare("SELECT * FROM deliveries WHERE order_id = ?");
$sl->execute(array($order->id));
$delivery = $sl->fetchObject();

// Create a payment intent.

$payment = $stripe->paymentIntents->create([
    'amount' => 200,
    'currency' => 'gbp',
    'setup_future_usage' => 'off_session',
    'payment_method_types' => ['card'],
    'metadata' => ['integration_check' => 'accept_a_payment'],
    'customer' => $x->stripe_id,
    'description' => 'Ooddles Order',
    'receipt_email' => $x->email,
    'shipping' => array('name' => $delivery->d_name.' '.$delivery->d_surname,
                       'address' => array('line1' => $delivery->d_address,
                                         'city' => $delivery->d_city,
                                         'postal_code' => $delivery->d_postcode,
                                         'country' => $delivery->d_country),
                        'carrier' => 'DPD'
                       ),
    'metadata' => array('order_id' => $order->unique_id)
    
]);

//echo $payment;

$so = $db->prepare("UPDATE orders SET intent_id = ?, secret = ? WHERE id = ?");
$so->execute(array($payment->id, $payment->client_secret, $order->id));

?>
<script
			  src="https://code.jquery.com/jquery-3.5.1.min.js"
			  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			  crossorigin="anonymous"></script>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
    $(document).ready(function(){
       var stripe = Stripe('<?= $stripe_key; ?>');
       var elements = stripe.elements(); 
       var style = {
          base: {
            color: "#32325d",
          }
        };

        var card = elements.create("card", { style: style });
        card.mount("#card-element");
        
        card.on('change', ({error}) => {
          const displayError = document.getElementById('card-errors');
          if (error) {
            displayError.textContent = error.message;
          } else {
            displayError.textContent = '';
          }
        });
        
        
        var form = document.getElementById('payment-form');

        form.addEventListener('submit', function(ev) {
            $("#loading").show();
            ev.preventDefault();
            var clientSecret = $('#payment-form button').attr('data-secret');
            
          stripe.confirmCardPayment(clientSecret, {
            payment_method: {
              card: card,
              billing_details: {
                name: '<?= $x->name.' '.$x->surname; ?>'
              }
            }
          }).then(function(result) {
            if (result.error) {
              // Show error to your customer (e.g., insufficient funds)
              console.log(result.error.message);
                $("#loading").fadeOut();
            } else {
              // The payment has been processed!
              if (result.paymentIntent.status === 'succeeded') {
                window.location.replace("<?= MAIN_SITE."/order/".$x->unique_id; ?>");
              }
            }
          });
        });
    });
</script>
<style type="text/css">
    /**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
    #payment_container {
        background-color: #efefef;
        width: 35%;
        border-radius: 5px;
        padding:20px 30px;
        position: relative;
        z-index: 1;
    }
    #loading {
        position: absolute;
        top: 0px;
        left:0px;
        width: 100%;
        height:100%;
        z-index: 99999;
        background-color: #fff;
        opacity: 0.2;
        display: none;
        
    }
    #loading .inner {
        display: flex;
        align-content: center;
        align-items: center;
        justify-content: center;
        position: absolute;
        top: 0px;
        left:0px;
        width: 100%;
        height:100%;
        z-index: 99999;
        color: #fff;
    }
.StripeElement {
  box-sizing: border-box;

  height: 40px;

  padding: 10px 12px;

  border: 1px solid transparent;
  border-radius: 4px;
  background-color: white;

  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
    #payment_container button {
        background-color: #653a2b;
        color: #fff;
        display: block;
        padding:5px 20px;
        border-radius:50px;
        border:none;
        width: 100%;
        margin-top:10px;
    }
    
</style>

<div id="payment_container">
    <div id="loading"><div class="inner">Loading</div></div>
<form action="/charge" method="post" id="payment-form">
  <div class="form-row">
    <label for="card-element">
      Credit or debit card
    </label>
    <div id="card-element">
      <!-- A Stripe Element will be inserted here. -->
    </div>

    <!-- Used to display form errors. -->
    <div id="card-errors" role="alert"></div>
  </div>

  <button data-secret="<?= $payment->client_secret; ?>">Submit Payment</button>
</form>
    </div><!--close payment_container-->