<?php
include "check-login.php";
//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
require_once('stripe/init.php');
$stripe = new \Stripe\StripeClient($stripe_secret_key);



switch($_GET['type']){
    case "subscription":
        $endpoint_secret = "whsec_YMCrkcm37qsb5yOiIyYf2cdCrWbtju1w";  

        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            //mail("david.oldfield@brainstormdesign.co.uk", "Stripe", $e);   
            http_response_code(400);
            exit();
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit();
        } 
        
        // We'll update the auto repeat first
        fetch_auto_repeat($db, $event->data->object->subscription);
        
        $st = $db->prepare("SELECT * FROM orders WHERE invoice_id = ?");
        $st->execute(array($event->data->object->id));
        if($st->rowCount() > 0){
            $x = $st->fetchObject();
            
            if($x->status != "Paid" && $event->data->object->paid == "true"){
                // Fire off the order email.
                $su = $db->prepare("UPDATE orders SET status = ?, ipn_received = ? WHERE id = ?");
                $su->execute(array("Paid", date("Y-m-d H:i:s"), $x->id));
                send_order_email($db, $x->unique_id);
            }
        }else{
            // Create an order from the auto repeat
            
            
            $ss = $db->prepare("SELECT * FROM auto_repeat WHERE subscription_id = ?");
            $ss->execute(array($event->data->object->subscription));
            if($ss->rowCount() > 0){
                $s = $ss->fetchObject();
                
                // Create a delivery address
                $sc = $db->prepare("SELECT * FROM deliveries WHERE order_id = ?");
                $sc->execute(array($s->order_id));
                $c = $sc->fetchObject();
                
                
                $last_id = last_order_id($db);
                    
                $hash = $last_id.strtoupper(substr($c->d_name,0,1)).strtoupper(substr($c->d_surname,0,1));
                
                $status = "Paid";
                $ipn = date("Y-m-d H:i:s");
                
                $sf = $db->prepare("INSERT INTO orders (created_on, unique_id, session_id, status, customer_id, total, shipping, subscription, invoice_id, ipn_received) VALUES (?,?,?,?,?,?,?,?,?,?)");
                $sf->execute(array(date("Y-m-d H:i:s"), $hash, session_id(), "Paid", !empty($s->customer_id) ? $s->customer_id : "", $s->total, !empty($s->shipping) ? $s->shipping : "", 1, $event->data->object->id, date("Y-m-d H:i:s")));
                
                $order_id = $db->lastInsertId();
                
                
                
                // Insert the delivery address
                $sdi = $db->prepare("INSERT INTO deliveries (order_id, d_name, d_surname, d_email, d_telephone, d_address, d_address2, d_city, d_postcode, d_county, d_country) VALUES (:session, :first, :surname, :email, :telephone, :address1, :address2, :city, :postcode, :county, :country)");
                
                $sdi->execute(array("session" => $order_id, "first" => $c->d_name, "surname" => $c->d_surname, "email" => $c->d_email, "telephone" => $c->d_telephone, "address1" => !empty($c->d_address) ? $c->d_address : "", "address2" => !empty($c->d_address2) ? $c->d_address2 : "", "city" => $c->d_city, "postcode" => $c->d_postcode, "county" => !empty($c->d_county) ? $c->d_county : "", "country" => $c->d_country));
                
                // Get our items and add to the cart
                $sp = $db->prepare("SELECT * FROM auto_repeat_items WHERE auto_id = ?");
                
                $si = $db->prepare("INSERT INTO cart (session_id, sku_id, price, created_on, order_id, pet_id) VALUES (?,?,?,?,?,?)");
                $sp->execute(array($s->id));
                while($p = $sp->fetchObject()){
                    for($k=1; $k <= $p->quantity; $k++){
                        $si->execute(array(session_id(), $p->sku_id, $p->price, date("Y-m-d H:i:s"), $order_id, !empty($s->pet_id) ? $s->pet_id : ""));
                    }
                }
                send_order_email($db, $hash);
            }
            
            
            
        }
        //mail("david.oldfield@brainstormdesign.co.uk", "Stripe", $event->data->object->subscription);       
    break;
        
    case "auto_repeat_deleted":
        
        $endpoint_secret = "whsec_ZOYlj9pIyX46vIPAsyCh01FlSg9U79qh";  

        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            //mail("david.oldfield@brainstormdesign.co.uk", "Stripe", $e);   
            http_response_code(400);
            exit();
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit();
        } 
        
        fetch_auto_repeat($db, $event->data->object->id);
        
        
        
    break;
    
    case "auto_repeat_updated":
        $endpoint_secret = "whsec_aHGMBI78nejPhi50R3r41RGC2jrCVlxy";  

        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            //mail("david.oldfield@brainstormdesign.co.uk", "Stripe", $e);   
            http_response_code(400);
            exit();
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit();
        } 
        fetch_auto_repeat($db, $event->data->object->id);
        //mail("david.oldfield@brainstormdesign.co.uk", "Stripe", $event);
    break;
}
?>