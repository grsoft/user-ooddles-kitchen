<? include "check-login.php";
	
/* check they filled in what they were supposed to and authenticate */
	if(!$_POST['username'] | !$_POST['password']){
		
        $_SESSION['status'] = "<h4>Error</h4><p>You have not entered an email address and password.</p>";
		header("Location: /login");
	}else{
		
		$auth = do_login(str_replace(" ", "", $_POST['username']), str_replace(" ", "", $_POST['password']), $db);
		
		if($auth["logged_in"] == 2){
			$_SESSION['unique_id'] = $auth["id"];
			$_SESSION['a_email'] = $auth["email"];
			$_SESSION['password'] = $auth["password"];
            
			
		}else{
			// Ensure all the session details are dead
			unset($_SESSION['admin_id']);
			unset($_SESSION['a_email']);
			unset($_SESSION['name']);
			unset($_SESSION['password']);
			 $_SESSION['status'] = "<h4>Error</h4><p>Your login details are incorrect, please try again or contact us.</p>";
			session_regenerate_id();
			header("Location: /login");
		}

	}
?>