<?
include "../configuration.php";

if(empty($_SESSION['geo'])){
    // DPD API
    $ch = curl_init();

    $string = "ooddleskitchen:oodkit123";
    $user = base64_encode($string);

    curl_setopt($ch, CURLOPT_URL,"https://api.dpd.co.uk/user/?action=login");
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization: Basic '. $user,
        'GEOClient: account/106387',
        'Content-Length: 0'
    );
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    $parts = json_decode($server_output);
    curl_close ($ch);

    //print_r($parts);

    $geo = $parts->data->geoSession;
    $_SESSION['geo'] = $geo;
}

// Get some services
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://api.dpd.co.uk/shipping/network/?collectionDetails.address.locality=Leeds&collectionDetails.address.county=West%20Midlands&collectionDetails.address.postcode=LS285UJ&collectionDetails.address.countryCode=GB&deliveryDetails.address.locality=York&deliveryDetails.address.county=West%20Yorkshire&deliveryDetails.address.postcode=YO267PU&deliveryDetails.address.countryCode=GB&deliveryDirection=1&numberOfParcels=1&totalWeight=5&shipmentType=0");
    curl_setopt($ch, CURLOPT_POST, 0);


    $headers = array(
        'Content-Type: application/json',
        'Accept: application/json',
        'GEOClient: account/106387',
        'GEOSession: '.$_SESSION['geo']
    );
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    $parts = json_decode($server_output);
    curl_close ($ch);

    print_r($parts);


?>