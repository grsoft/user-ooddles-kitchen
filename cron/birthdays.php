<?php
include "../configuration.php";
include "../includes/mail-headers.php";
include "../classes/class.phpmailer.php";

/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

*/


$st = $db->prepare("SELECT p.name, p.birth_year, p.birthday, p.name AS pet_name,p.birth_month,o.total, c.name, c.surname, p.customer_id, o.created_on,c.address, c.town, c.city, c.county, c.postcode, c.country, c.email, c.telephone FROM pets p 
                            LEFT JOIN customers c ON p.customer_id = c.id 
                            LEFT JOIN   orders o ON o.customer_id = c.id 
                            WHERE c.tmp != ? AND c.inactive != ? AND p.inactive != ? AND o.created_on > DATE_SUB(now(), INTERVAL 2 MONTH) AND birth_month = ?  AND o.total>10
                            GROUP BY p.customer_id, p.unique_id ORDER BY o.created_on ASC");

$sd = $db->prepare("INSERT INTO discount_codes (type_id, discount_code, percentage, range_id, customer_id, limited, one_use) VALUES (?,?,?,?,?,?,?)");
$st->execute(array(1,1,1,date("m")));

while($r = $st->fetchObject()){
    $day = explode(":", $r->birthday);
    $age = (date("Y") - $r->birth_year);
    if($day[0] > 0 && $day[0] == date("j")){
        // Generate a discount code
        $code = "HB".$r->id.date("y");
        
        //$sd->execute(array("Birthday", $code, 100, 8, $r->customer_id, 1, 1));
        
        $mailer = "<body bgcolor='#efefef'>
                    <table cellpadding='0' cellspacing='0' width='650' align='center' style='background-color:#fff; font-family: Arial, Gill Sans, Geneva, Museo Sans, museo sans, museo-sans-rounded, Helvetica, Georgia, Arial, Helvetica, sans-serif; font-size:12px; padding:0px;'>
                    <tr>
                    <td style='padding:0px; text-align:center; background-color: #000; color: #ffffff; border-bottom:2px solid #fff;'><img src='".MAIN_SITE."/images/birthday-header.jpg?v=".rand()."' alt='Logo' width='100%' /></td>
                    </tr>
                    <tr>
                    <td style='padding:10px 20px; background-color: #653a2b; color: #fff; text-align:center; font-family: Arial, Gill Sans, Geneva, Museo Sans, museo-sans-rounded, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:24px;'>Happy Birthday - A Pressie From Me!</td>
                    </tr>
                    <tr>
                    <td style='padding:20px; background-color: #fff;'>
                    <p style='font-family: Arial, Gill Sans, Geneva, Museo Sans, Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:14px; line-height:150%; text-align:center;'>Hello ".ucwords(strtolower($r->name))." and {$r->pet_name}. <br /><br />
                    Happy Birthday to {$r->pet_name} from all the Ooddles Team and woof woof from Oode on your special day.<br /> 
                    A pressie is on the way for {$r->pet_name} to enjoy! 
                    <br /><br />
                    We would love you to share {$r->pet_name}'s special day at our Ooddles <a href='{$company->facebook}' title='Facebook' style='color: #653a2b; font-weight:bold;'>Facebook</a> or <a href='{$company->instagram}' title='Instagram' style='color: #653a2b; font-weight:bold;'>Instagram</a>.<br />

                    <strong>Ooddles Team</strong></p>
                    </td>
                    </tr>";
        $mailer .= $mail_footer;

        $email = new PHPMailer();
        $email->From = $company->email;
        $email->FromName = $company->name;

        $email->Subject = "Happy Birthday to ".$r->pet_name;
        $email->Body = $mailer;
        $email->IsHTML(true);

        $email->AddAddress($r->email);
        //$email->AddBCC("david.oldfield@brainstormdesign.co.uk");
        $email->Send();    
    }
}
            

?>