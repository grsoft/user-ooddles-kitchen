<?php
define("HOST", "localhost:3306");
define("USER", "ooddles");
define("PASS", "Drqu@ngo99_");
define("DATABASE", "main");
try {
    $db = new PDO("mysql:host=".HOST.";dbname=".DATABASE, USER, PASS);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} 
catch(PDOException $err){
    $error = $err->getMessage();
}
ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
require_once '/var/www/vhosts/ooddleskitchen.co.uk/admin.ooddleskitchen.co.uk/excel.php';
require_once '/var/www/vhosts/ooddleskitchen.co.uk/admin.ooddleskitchen.co.uk/PHPExcel/IOFactory.php';

    $st = $db->prepare("SELECT o.id, c.name, c.surname, c.email, o.ipn_received, o.unique_id 
                        FROM orders o 
                        LEFT JOIN customers c ON o.customer_id = c.id 
                        WHERE status = ? AND shipping_status = ? AND o.exported = ? AND total > ?");
    $st->execute(array("Paid", "Shipped",0,0));

    $su = $db->prepare("UPDATE orders SET exported = ? WHERE id = ?");
    
// Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

// Headers
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setTitle('Orders');

    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'name');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'email');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'date');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'description');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'merchant identifier');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'product search code');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'order ref');


    $number = $st->rowCount();

    $row = 1;

    for($x=1; $x <= $number; $x++){
        $r = $st->fetchObject();

            $col = 0;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, ($row+1), ucwords(strtolower($r->name))." ".ucwords(strtolower($r->surname)));
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, ($row+1), $r->email);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, ($row+1), date("d/m/Y", strtotime($r->ipn_received)));
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, ($row+1), "The Ooddles Kitchen Experience");
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, ($row+1), "ooddles-kitchen-co-uk");
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, ($row+1), "EXPERIENCE");
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, ($row+1), $r->unique_id);

        $row++;

        $su->execute(array(1,$r->id));
    }
    $filename = "orders";
    //header("Content-Type: application/vnd.ms-excel");
    //header("Content-Disposition: attachment; filename=\"".$filename.".csv\"");
    //header("Cache-Control: max-age=0");
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
    $objWriter->save($filename.'.csv');
?>