<?
include "../configuration.php";
$list = array();

// Categories
$st = $db->prepare("SELECT *
                    FROM breeds
                    WHERE type_id = ?");
$st->execute(array("Pedigree"));

while($r = $st->fetchObject()){
	$list[] = array($r->breed);
}



$lines = array();
foreach($list as $l){
	$lines[] = "{value:'".htmlentities($l[0], ENT_QUOTES)."'}";
}
$new = implode(",", $lines);
$fp = fopen('breeds.js', 'w');
fwrite($fp, 'var breeds = ['.$new.'];');
fclose($fp);

rename("/home/brainsto/oodles.brainstormdevelopment.co.uk/cron/breeds.js", "/home/brainsto/oodles.brainstormdevelopment.co.uk/js/breeds.js");
unset($list);

$list = array();

// Categories
$st = $db->prepare("SELECT *
                    FROM breeds
                    WHERE type_id = ?");
$st->execute(array("Crossbreed"));

while($r = $st->fetchObject()){
	$list[] = array($r->breed);
}



$lines = array();
foreach($list as $l){
	$lines[] = "{value:'".htmlentities($l[0], ENT_QUOTES)."'}";
}
$new = implode(",", $lines);
$fp = fopen('crossbreeds.js', 'w');
fwrite($fp, 'var crossbreeds = ['.$new.'];');
fclose($fp);

rename("/home/brainsto/oodles.brainstormdevelopment.co.uk/cron/crossbreeds.js", "/home/brainsto/oodles.brainstormdevelopment.co.uk/js/crossbreeds.js");

?>