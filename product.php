<? include "header.php"; 

if($logged_in == 2){
    ?>
    <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline">
        <h1><? if(!empty($pet->name)){ echo "Suitable foods for {$pet->name}"; }else{ echo $xx->title; } ?></h1>
        
    </div><!--close headline-->
    
</div><!--close banner-->
<section>
    <div id="breadcrumbs">
        <ul class="flex">
            <li><a href="/o-hub" title="O Hub">O Hub</a></li>
            <li>&rang;</li>
            <li><a href="/o-hub/ranges" title="Our Range">Our Range</a></li>
            <li>&rang;</li>
            <li><a href="/o-hub/ranges/<? echo $_GET['range']; ?>" title="<? echo $xx->category; ?>"><? echo $xx->category; ?></a></li>
            <li>&rang;</li>
            <li><a href="/o-hub/ranges/<? echo $_GET['range']; ?>/<? echo $_GET['id']; ?>" title="<? echo $xx->title; ?>"><? echo $xx->title; ?></a></li>
            
        </ul>
        </div>
                
    </section>
   
<?
    include "includes/product-detail.php";
    
            if(!empty($pet->name)){
                // Our wheres...
                if(!empty($pet->size)){
                    $where .= " AND p.suitable_for LIKE '%{$pet->size}%'";
                }
                if(!empty($pet->dislikes)){
                    $where .= " AND p.main_ingredient != '{$pet->dislikes}'";
                } 
                $st = $db->prepare("SELECT p.title, p.seo, p.image, p.food_type, r.seo AS range_seo, c.seo AS category_seo, c.category, r.image AS category_image 
                                    FROM skus s 
                                    LEFT JOIN products p ON s.product_id = p.id 
                                    LEFT JOIN product_ranges r ON p.range_id = r.id
                                    LEFT JOIN categories c ON p.category_id = c.id
                                    WHERE p.id != ? AND p.status = ? AND s.status = ? AND s.stock > ? AND LOWER(p.food_type) = ? {$where}
                                    GROUP BY s.product_id");
                $st->execute(array($xx->id, "Published", "Published", 0, $xx->food_type));
            }else{
                $st = $db->prepare("SELECT p.title, p.image, r.title AS range_title, p.image, p.introduction, p.description, p.composition, p.id, p.nutrition, p.constituents, p.food_type, p.seo, r.seo AS range_seo, c.seo AS category_seo, c.category, r.image AS category_image 
                                    FROM products p
                                    LEFT JOIN product_ranges r ON p.range_id = r.id
                                    LEFT JOIN categories c ON p.category_id = c.id
                                    WHERE p.id != ? AND food_type = ? AND p.status = ? ORDER BY RAND() LIMIT 6");
                $st->execute(array($xx->id, $xx->food_type, "Published"));    
            }
            
            $num = $st->rowCount();
            if($num > 0){
            echo "<div class='related light_blue'>";
                if(!empty($pet->name)){
                    $tag = "?pet=".$_GET['pet'];
                    echo "<h3>Other Food Suitable for {$pet->name}</h3>";
                }else{
                    echo "<h3>Why not try something different?</h3>";
                }
            ?>
        <div class="glide">

				<div class="glide__arrows">
					<button class="glide__arrow prev" data-glide-dir="<">prev</button>
					<button class="glide__arrow next" data-glide-dir=">">next</button>
				</div>

				<div class="glide__wrapper">
					<ul class="glide__track">
                        <li class="glide__slide">
                            <div class="flex justify">
						<? 
                            for($x=1; $x <= $num; $x++){
                                $r = $st->fetchObject();
                                if(!empty($r->category_image)){
                                    $image = $sirv."/images/ranges/".$r->category_image;
                                    
                                }else{
                                    $image = $sirv."/images/products/".$r->image;
                                }
                                
                                echo "<div class='c_25'>
                                      <div class='inner'>
                                      <div class='product_img'><a href='/o-hub/ranges/{$r->category_seo}/{$r->seo}{$tag}' title='{$r->title}'><img data-src='{$image}?canvas.width=500&canvas.height=500&w=500&h=500' alt='{$r->title}' class='Sirv' /></a></div>
                                      <h3><a href='/o-hub/ranges/{$r->category_seo}/{$r->seo}{$tag}' title='{$r->title}'>{$r->title}</a></h3>
                                      <p><a href='/o-hub/ranges/{$r->category_seo}/{$r->seo}{$tag}' title='{$r->title}' class='btn white'>Buy Now</a></p>
                                      </div>
                                      </div>";
                                if($x % 3 == 0 && $x != $num){
                                    echo "</div></li><li class='glide__slide'><div class='flex justify'>";
                                }
                            }
                        ?>
                        </div>
                        </li>
						
						
					</ul>
				</div>

				<div class="glide__bullets"></div>

			</div>
        
        <?
        echo "</div>";
    }
    ?>
    <?
}else{
    ?>
    <div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline">
        <h1>You cannot view this page</span></h1>
        <p>You need to be logged in to view this page.</p>
    </div><!--close headline-->
    
</div><!--close banner-->
    <?
    include "includes/login.php";
}
include "includes/company.php";
include "footer.php"; ?>