<section>
    <div id="breadcrumbs">
        <ul class="flex">
            <li><a href="/o-hub" title="O Hub">O Hub</a></li>
            <li>&rang;</li>
            <li><a href="<? echo $_REQUEST['URI']; ?>" title="<? echo ucwords($_GET['id']); ?>"><? echo ucwords($_GET['id']); ?></a></li>
            
            
        </ul>
        </div>
                
    </section>
<img src="/images/ooddles-range.jpg" alt="Placeholder" class="placeholder" />
<section class="products">
    <div class="flex negative justify">
    <?
        $sc = $db->prepare("SELECT c.id, c.category, c.seo, r.image AS category_image 
                            FROM products p 
                            LEFT JOIN product_ranges r ON p.range_id = r.id
                            LEFT JOIN categories c ON p.category_id = c.id 
                            WHERE p.status = ? AND c.status = ? GROUP BY c.category ORDER BY c.id ASC");
        $sc->execute(array("Published", "Published"));
        
        $sp = $db->prepare("SELECT * FROM products WHERE category_id = ? AND status = ?");
        
        $sk = $db->prepare("SELECT * FROM pets WHERE customer_id = ? AND inactive = ?");
        
        while($c = $sc->fetchObject()){
            $sp->execute(array($c->id, "Published"));
            if($sp->rowCount() > 0){
                $p = $sp->fetchObject();
                
                if(!empty($c->category_image)){
                    $image = $sirv."/images/ranges/".$c->category_image;
                }else{
                    $image = $sirv."/images/products/".$p->image;
                }
                
                if($pets > 1 && $c->id != 6){ // Create out 2 links
                   echo "<div class='c_25'>
                        <div class='inner item'>
                        <div class='product_img'><img data-src='{$image}?canvas.width=500&canvas.height=500&w=500&h=500' alt='{$c->category}' class='Sirv' /></div>
                        <h3>{$c->category}</h3>";
                  $sk->execute(array($customer->id,0));
                    while($k = $sk->fetchObject()){
                        echo "<p><a href='/o-hub/ranges/{$c->seo}?pet={$k->unique_id}' title='{$c->category}' class='btn white'>View Range for {$k->name}</a></p>";    
                    }
                        
                   echo "</div>
                        </div>"; 
                }else{
                    echo "<div class='c_25'>
                          <div class='inner item'>
                          <div class='product_img'><a href='/o-hub/ranges/{$c->seo}' title='{$c->category}'><img data-src='{$image}?canvas.width=500&canvas.height=500&w=500&h=500' alt='{$c->category}' class='Sirv' /></a></div>
                          <h3><a href='/o-hub/ranges/{$c->seo}' title='{$c->category}'>{$c->category}</a></h3>
                          <p><a href='/o-hub/ranges/{$c->seo}' title='{$c->category}' class='btn white'>View Range</a></p>
                          </div>
                          </div>";    
                }
                
            }
        }
    
    ?>
        </div>
    </section>