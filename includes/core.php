<?
switch(PAGE){
	
	case "/404.php":
		header("HTTP/1.0 404 Not Found");
		echo "<title>404 Page Not Found | {$company->name}</title>
		      <meta name='robots' content='noindex,nofollow' />";
        $show_404 = 1;
	break;
        
    case "/account.php":
        echo "<title>O-Hub | {$company->name}</title>
              <meta name='robots' content='noindex,nofollow' />";    
    break;
        
    case "/account-page.php":
        echo "<title>O-Hub | {$company->name}</title>
              <meta name='robots' content='noindex,nofollow' />";  
        
        // Are we a standard page?
        switch($_GET['id']){
                
            default:
                $st = $db->prepare("SELECT * FROM pages WHERE seo = ?");
                $st->execute(array($_GET['id']));
                if($st->rowCount() > 0){
                    $xx = $st->fetchObject();
                    echo "<title>{$xx->title} | O-Hub | {$company->name}</title>
                          <meta name='robots' content='noindex,nofollow' />";
                    $page = "content";
                    $title = $xx->title;
                }else{
                    header("HTTP/1.0 404 Not Found");
                    echo "<title>404 Page Not Found | {$company->name}</title>
                          <meta name='robots' content='noindex,nofollow' />";
                    
                }
            break;
        }
    break;
        
    case "/addresses.php":
        echo "<title>Addresses | {$company->name}</title>
              <meta name='robots' content='noindex,nofollow' />";    
    break;
        
    case "/article.php":
        $st = $db->prepare("SELECT * FROM advice WHERE publish_on <= ? AND seo = ?");
        $st->execute(array(date("Y-m-d"), $_GET['id']));
        if($st->rowCount() > 0){
            $xx = $st->fetchObject();
            
            echo "<title>{$xx->title} | Care and Advice</title>
                  <meta name='description' content='{$xx->introduction}' />
                  <meta property='og:url' content='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />
                  <meta propertyp='og:title' content='{$xx->title} | Care and Advice' />
                  <meta property='og:description' content='{$xx->introduction}' />
                  <meta property='og:site_name' content='{$company->name}'/>
                  <meta property='og:image' content='".MAIN_SITE.NEWS.$xx->image."' />
                  <meta property='og:type' content='article' />
                  <meta property='og:locale' content='en_GB' />";
            echo "<link rel='canonical' href='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />
                  <meta name='robots' content='noindex,nofollow' />";	
            
        }else{
            header("HTTP/1.0 404 Not Found");
            echo "<title>404 Page Not Found | {$company->name}</title>
                  <meta name='robots' content='noindex,nofollow' />";
            $show_404 = 1;
        }
    break;
        
    case "/blog-article.php":
        $st = $db->prepare("SELECT * FROM advice WHERE publish_on <= ? AND seo = ?");
        $st->execute(array(date("Y-m-d"), $_GET['id']));
        if($st->rowCount() > 0){
            $xx = $st->fetchObject();
            echo !empty($xx->meta_title) ? "<title>{$xx->meta_title}</title><meta property='og:title' content='{$xx->meta_title}' />" : 
                                           "<title>{$xx->title} for Dogs - {$company->name}</title><meta property='og:title' content='{$xx->title} for Dogs - {$company->name}' />";
            
            echo !empty($xx->meta_description) ? "<meta name='description' content='{$xx->meta_description}' /><meta property='og:description' content='{$xx->meta_description}' />" : 
                                                 "<meta name='description' content='{$xx->introduction}' /><meta property='og:description' content='{$xx->introduction}' />";
            echo "<meta property='og:url' content='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />
                  <meta property='og:site_name' content='{$company->name}'/>
                  <meta property='og:image' content='".MAIN_SITE.NEWS.$xx->image."' />
                  <meta property='og:type' content='article' />
                  <meta property='og:locale' content='en_GB' />";
            echo "<link rel='canonical' href='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />";	
            
        }else{
            header("HTTP/1.0 404 Not Found");
            echo "<title>404 Page Not Found | {$company->name}</title>
                  <meta name='robots' content='noindex,nofollow' />";
            $show_404 = 1;
        }
    break;
        
    case "/basket.php":
        echo "<title>Basket | {$company->name}</title>
              <meta name='robots' content='noindex,nofollow' />";    
    break;
        
    case "/category.php":
        $st = $db->prepare("SELECT * FROM categories WHERE seo = ? AND status = ?");
        $st->execute(array($_GET['id'], "Published"));
        if($st->rowCount() > 0){
            $xx = $st->fetchObject();
            
            echo "<title>{$xx->meta_title}</title>
                  <meta name='description' content='{$xx->meta_description}' />
                  <meta property='og:url' content='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />
                  <meta property='og:title' content='{$xx->meta_title}' />
                  <meta property='og:description' content='{$xx->meta_description}' />
                  <meta property='og:site_name' content='{$company->name}'/>
                  <meta property='og:image' content='".MAIN_SITE.IMAGES.$xx->image."' />
                  <meta property='og:type' content='website' />
                  <meta property='og:locale' content='en_GB' />";
            echo "<link rel='canonical' href='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />";	
            
        }else{
            header("HTTP/1.0 404 Not Found");
            echo "<title>404 Page Not Found | {$company->name}</title>
                  <meta name='robots' content='noindex,nofollow' />";
            $show_404 = 1;
        }
        
    break;
        
    case "/page.php":
        $st = $db->prepare("SELECT * FROM pages WHERE seo = ?");
        $st->execute(array($_GET['id']));
        if($st->rowCount() > 0){
            $type_id = "page";
            
            $xx = $st->fetchObject();
            echo !empty($xx->meta_title) ? "<title>{$xx->meta_title}</title><meta property='og:title' content='{$xx->meta_title}' />" : 
                                           "<title>{$xx->title} for Dogs - {$company->name}</title><meta property='og:title' content='{$xx->title} for Dogs - {$company->name}' />";
            
            echo !empty($xx->meta_description) ? "<meta name='description' content='{$xx->meta_description}' /><meta property='og:description' content='{$xx->meta_description}' />" : 
                                                 "<meta name='description' content='{$xx->introduction}' /><meta property='og:description' content='{$xx->introduction}' />";
            
            echo "<meta property='og:url' content='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />
                  <meta property='og:site_name' content='{$company->name}'/>
                  <meta property='og:image' content='".MAIN_SITE.IMAGES."logo.png' />
                  <meta property='og:type' content='website' />
                  <meta property='og:locale' content='en_GB' />";
            echo "<link rel='canonical' href='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />";	
            
        }else{
            // Are we a category?
            $st = $db->prepare("SELECT * FROM _categories WHERE status = ? AND seo = ?");
            $st->execute(array("Published", $_GET['id']));
            if($st->rowCount() > 0){
                $type_id = "category";
            
                $xx = $st->fetchObject();
                $breadcrumbs[] =  array("name" => $xx->alias, "link" => "/".$xx->seo);
                
                echo !empty($xx->meta_title) ? "<title>{$xx->meta_title}</title><meta property='og:title' content='{$xx->meta_title}' />" : 
                                               "<title>{$xx->title} for Dogs - {$company->name}</title><meta property='og:title' content='{$xx->title} for Dogs - {$company->name}' />";

                echo !empty($xx->meta_description) ? "<meta name='description' content='{$xx->meta_description}' /><meta property='og:description' content='{$xx->meta_description}' />" : 
                                                     "<meta name='description' content='{$xx->introduction}' /><meta property='og:description' content='{$xx->introduction}' />";

                echo "<meta property='og:url' content='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />
                      <meta property='og:site_name' content='{$company->name}'/>
                      <meta property='og:image' content='".MAIN_SITE.IMAGES.$xx->image."' />
                      <meta property='og:type' content='website' />
                      <meta property='og:locale' content='en_GB' />";
                echo "<link rel='canonical' href='".MAIN_SITE.$xx->canonical."' />";   
            }else{
            
                header("HTTP/1.0 404 Not Found");
                echo "<title>404 Page Not Found | {$company->name}</title>
                      <meta name='robots' content='noindex,nofollow' />";
                $show_404 = 1;
            }
        }
    break;
        
    case "/product.php":
        if($logged_in == 2){
            $st = $db->prepare("SELECT p.title, p.image, r.title AS range_title, p.image, p.introduction, p.description, p.composition, p.id, p.nutrition, p.constituents, p.food_type, p.feeding, p.category_id, c.category, c.seo AS category_seo, p.me
                                FROM products p
                                LEFT JOIN product_ranges r ON p.range_id = r.id
                                LEFT JOIN categories c ON p.category_id = c.id
                                WHERE c.seo = ? AND p.seo = ? AND p.status = ?");
            $st->execute(array($_GET['range'], $_GET['id'], "Published"));
            
            if($st->rowCount() > 0){
                $xx = $st->fetchObject();
                echo "<title>{$xx->title} | {$company->name}</title>";
                if($pets == 1 || !empty($_GET['pet'])){
                    
                    if(!empty($_GET['pet'])){
                        $sp = $db->prepare("SELECT * FROM pets WHERE unique_id = ? AND customer_id = ?");
                        $sp->execute(array($_GET['pet'], $customer->id));    
                    }else{
                        $sp = $db->prepare("SELECT * FROM pets WHERE customer_id = ?");
                        $sp->execute(array($customer->id));    
                    }
                    $pet = $sp->fetchObject();
                    
                    $sg = $db->prepare("SELECT raw_adjust, me FROM activities WHERE id = ?");
                    $sg->execute(array($pet->activity_id));
                    $g = $sg->fetchObject();
                    
                    // How many months old are we?
                    $parts = explode(":", $pet->age);
                    $months = (($parts[0]*12)+$parts[1]);
                    
                    switch($xx->category_id){
                        case 1: // Dry
                            
                            if($months > 12){
                                $low = $g->me;
                                $weight = $pet->weight;
                                $dog_cals = round(pow($weight, 0.75),2);
                                $der = round(($low * $dog_cals));

                                $carolie_per_kg = ($xx->me*10);

                                $per_day = round((($der * 1000)/$carolie_per_kg));    
                            }else{
                                
                                // We need the breed size and age
                                $sh = $db->prepare("SELECT percentage FROM puppy_supplement s LEFT JOIN breed_sizes b ON s.size_id = b.id WHERE from_months <= ? AND to_months >= ? AND b.title = ? AND s.category_id = ?");
                                $sh->execute(array($months, $months, $pet->size, $xx->category_id));
                                $h = $sh->fetchObject();
                                $per_day = $h->percentage;
                                
                            }
                        break;
                            
                        case 2: // Wet
                            $low = $g->me;
                            $weight = $pet->weight;
                            $dog_cals = round(pow($weight, 0.75),2);
                            $der = round(($low * $dog_cals));

                            $carolie_per_kg = ($xx->me*10);

                            $per_day = round((($der * 1000)/$carolie_per_kg));    
                        break;
                            
                        case 5:
                            
                            
                            if($months < 12){
                                // Get the supplement from the raw feeding guide
                                $sl = $db->prepare("SELECT percentage FROM puppy_supplement WHERE from_months <= ? AND to_months >= ? AND category_id = ?");
                                $sl->execute(array($months,$months,$xx->category_id));;
                                $l = $sl->fetchObject();
                            
                                $puppy_percentage = ($raw_percent+$l->percentage);
                            }else{
                                // Raw adjustment
                                $calc = ($pet->weight*$g->raw_adjust);
                                
                                // We're simply weight based.
                                $sl = $db->prepare("SELECT percentage FROM adult_supplement WHERE from_weight <= ? AND to_weight >= ? AND category_id = ?");
                                $sl->execute(array($pet->weight,$pet->weight,$xx->category_id));
                                $l = $sl->fetchObject();
                                $puppy_percentage = $l->percentage;
                                
                                // Do we have any activity adjustments
                                $sf = $db->prepare("SELECT size_adjust FROM body_sizes WHERE title = ?");
                                $sf->execute(array($pet->physical_condition));
                                $f = $sf->fetchObject();
                                
                                if($f->size_adjust < 0){
                                    $adjuster = $f->size_adjust;
                                    $direction = "minus";
                                }else{
                                    if($f->size_adjust > $g->raw_adjust){
                                        $adjuster = $f->size_adjust;
                                    }else{
                                        $adjuster = $g->raw_adjust;
                                    }
                                }
                            }
                            $per_day = ((($pet->weight*1000)/100)*$puppy_percentage); 
                            
                            if(!empty($adjuster)){
                                
                                $diff = (($per_day/100)*$adjuster);
                                $per_day = ($per_day+$diff);
                                
                            }
                            
                        break;
                    }
                    
                    
                }
            }else{
                echo "<title>404 Page Not Found | {$company->name}</title>";    
            }
        }else{
             echo "<title>Permission Denied | {$company->name}</title>";   
        }
        echo "<meta name='robots' content='noindex,nofollow' />";
    break;
        
    case "/option.php":
        $st = $db->prepare("SELECT * FROM categories WHERE seo = ? AND status = ?");
        $st->execute(array($_GET['id'], "Published"));
        if($st->rowCount() > 0){
            $xx = $st->fetchObject();
            echo !empty($xx->meta_title) ? "<title>{$xx->meta_title}</title><meta property='og:title' content='{$xx->meta_title}' />" : 
                                           "<title>{$xx->category} for Dogs - {$company->name}</title><meta property='og:title' content='{$xx->category} for Dogs - {$company->name}' />";
            
            echo !empty($xx->meta_description) ? "<meta name='description' content='{$xx->meta_description}' /><meta property='og:description' content='{$xx->meta_description}' />" : 
                                                 "<meta name='description' content='{$xx->introduction}' /><meta property='og:description' content='{$xx->introduction}' />";
            
            echo "<meta property='og:url' content='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />
                  <meta property='og:site_name' content='{$company->name}'/>
                  <meta property='og:image' content='".MAIN_SITE.PRODUCTS.$xx->image."' />
                  <meta property='og:type' content='website' />
                  <meta property='og:locale' content='en_GB' />";
            echo "<link rel='canonical' href='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />";
            
            if($pets == 1 || !empty($_GET['pet'])){
                    
                    if(!empty($_GET['pet'])){
                        $sp = $db->prepare("SELECT * FROM pets WHERE unique_id = ? AND customer_id = ?");
                        $sp->execute(array($_GET['pet'], $customer->id));    
                    }else{
                        $sp = $db->prepare("SELECT * FROM pets WHERE customer_id = ?");
                        $sp->execute(array($customer->id));    
                    }
                    $pet = $sp->fetchObject();
            }
            
        }else{
            // Are we a pet?
            $st = $db->prepare("SELECT * FROM pets WHERE customer_id = ? AND unique_id = ?");
            $st->execute(array($customer->id, $_GET['id']));
            if($st->rowCount() > 0){
                $xx = $st->fetchObject();
                
                echo "<title>{$xx->name} - {$company->name}</title>";
            
                echo "<link rel='canonical' href='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />";    
            }else{
                $st = $db->prepare("SELECT * FROM orders WHERE unique_id = ?");
                $st->execute(array($_GET['id']));
                if($st->rowCount() > 0){
                    $xx = $st->fetchObject();
                    
                    echo "<title>Order Summary</title>
                          <meta name='robots' content='noindex,nofollow' />";
                    
                }else{
                    if($_GET['area'] == "auto-repeat"){
                        $st = $db->prepare("SELECT a.id, a.order_id, a.total, p.name, a.frequency, a.next_due, a.status FROM auto_repeat a LEFT JOIN pets p ON a.pet_id = p.unique_id WHERE a.id = ? AND a.customer_id = ?");
                        $st->execute(array($_GET['id'], $customer->id));
                        if($st->rowCount() > 0){
                            $xx = $st->fetchObject();
                             $st = $db->prepare("SELECT * FROM deliveries WHERE order_id = ?");
                            $st->execute(array($xx->order_id));
                            $delivery = $st->fetchObject();

                        // The delivery address
                            $delivery_address = $delivery->d_address.", ";
                            if(!empty($delivery->d_address2)){
                                $delivery_address .= $delivery->d_address2.", ";
                            }
                            $delivery_address .= $delivery->d_city.", <br />".$delivery->d_county." ".strtoupper($delivery->d_postcode)."<br />".$delivery->d_country;
                            
                            echo "<title>Auto Repeat Summary</title><meta name='robots' content='noindex,nofollow' />";
                    
                        }else{
                            header("HTTP/1.0 404 Not Found");
                            echo "<title>404 Page Not Found | {$company->name}</title>
                                  <meta name='robots' content='noindex,nofollow' />";
                            $show_404 = 1;  
                        }
                        
                    }
                       
                }
                
                   
            }
            
            
        }
        
    break;
        
    case "/range.php":
        $st = $db->prepare("SELECT * FROM product_ranges WHERE seo = ? AND status = ?");
        $st->execute(array($_GET['id'], "Published"));
        if($st->rowCount() > 0){
            $xx = $st->fetchObject();
            echo !empty($xx->meta_title) ? "<title>{$xx->meta_title}</title><meta property='og:title' content='{$xx->meta_title}' />" : 
                                           "<title>{$xx->title} for Dogs - {$company->name}</title><meta property='og:title' content='{$xx->title} for Dogs - {$company->name}' />";
            
            echo !empty($xx->meta_description) ? "<meta name='description' content='{$xx->meta_description}' /><meta property='og:description' content='{$xx->meta_description}' />" : 
                                                 "<meta name='description' content='{$xx->introduction}' /><meta property='og:description' content='{$xx->introduction}' />";
            
            echo "<meta property='og:url' content='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />
                  <meta property='og:site_name' content='{$company->name}'/>
                  <meta property='og:image' content='".MAIN_SITE.PRODUCTS.$xx->image."' />
                  <meta property='og:type' content='website' />
                  <meta property='og:locale' content='en_GB' />";
            echo "<link rel='canonical' href='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />";
            
            if($pets == 1 || !empty($_GET['pet'])){
                    
                    if(!empty($_GET['pet'])){
                        $sp = $db->prepare("SELECT * FROM pets WHERE unique_id = ? AND customer_id = ?");
                        $sp->execute(array($_GET['pet'], $customer->id));    
                    }else{
                        $sp = $db->prepare("SELECT * FROM pets WHERE customer_id = ?");
                        $sp->execute(array($customer->id));    
                    }
                    $pet = $sp->fetchObject();
            }
            
        }else{
            header("HTTP/1.0 404 Not Found");
            echo "<title>404 Page Not Found | {$company->name}</title>
                  <meta name='robots' content='noindex,nofollow' />";
            $show_404 = 1;
        }
        
    break;
        
    case "/recommendation.php":
        
            $st = $db->prepare("SELECT p.title, p.image, r.title AS range_title, p.image, p.introduction, p.description, p.composition, p.id, p.nutrition, p.constituents, p.food_type, p.feeding, p.category_id, c.category, c.seo AS category_seo, p.me
                                FROM products p
                                LEFT JOIN product_ranges r ON p.range_id = r.id
                                LEFT JOIN categories c ON p.category_id = c.id
                                WHERE c.seo = ? AND p.seo = ? AND p.status = ?");
            $st->execute(array($_GET['range'], $_GET['id'], "Published"));
            
            if($st->rowCount() > 0){
                $xx = $st->fetchObject();
                echo "<title>{$xx->title} | {$company->name}</title>";
                if($pets == 1 || !empty($_GET['pet'])){
                    
                    if(!empty($_GET['pet'])){
                        $sp = $db->prepare("SELECT * FROM pets WHERE (unique_id = ? AND session_id = ?) OR (unique_id = ? AND customer_id = ? AND customer_id > ?)");
                        $sp->execute(array($_GET['pet'], session_id(), $_GET['pet'], $customer->id, 0));    
                    }
                    
                    $pet = $sp->fetchObject();
                    
                    // How many months old are we?
                    $parts = explode(":", $pet->age);
                    $months = (($parts[0]*12)+$parts[1]);
                   
                    
                    $sg = $db->prepare("SELECT raw_adjust, me FROM activities WHERE id = ?");
                    $sg->execute(array($pet->activity_id));
                    $g = $sg->fetchObject();
                    
                    switch($xx->category_id){
                        case 1: // Dry
                            

                            if($months > 12){
                                $low = $g->me;
                                $weight = $pet->weight;
                                $dog_cals = round(pow($weight, 0.75),2);
                                $der = round(($low * $dog_cals));

                                $carolie_per_kg = ($xx->me*10);

                                $per_day = round((($der * 1000)/$carolie_per_kg));    
                            }else{
                                
                                // We need the breed size and age
                                $sh = $db->prepare("SELECT percentage FROM puppy_supplement s LEFT JOIN breed_sizes b ON s.size_id = b.id WHERE from_months <= ? AND to_months >= ? AND b.title = ? AND s.category_id = ?");
                                $sh->execute(array($months, $months, $pet->size, $xx->category_id));
                                $h = $sh->fetchObject();
                                $per_day = $h->percentage;
                                
                            }   
                        break;
                            
                        case 2: // Wet
                            $low = $g->me;
                            $weight = $pet->weight;
                            $dog_cals = round(pow($weight, 0.75),2);
                            $der = round(($low * $dog_cals));

                            $carolie_per_kg = ($xx->me*10);

                            $per_day = round((($der * 1000)/$carolie_per_kg));    
                        break;
                            
                        case 5:
                            // How many months old are we?
                            $parts = explode(":", $pet->age);
                            $months = (($parts[0]*12)+$parts[1]);
                            
                            if($months < 12){
                                // Get the supplement from the raw feeding guide
                                $sl = $db->prepare("SELECT percentage FROM puppy_supplement WHERE from_months <= ? AND to_months >= ? AND category_id = ?");
                                $sl->execute(array($months,$months,$xx->category_id));;
                                $l = $sl->fetchObject();
                            
                                $puppy_percentage = ($raw_percent+$l->percentage);
                            }else{
                                // Raw adjustment
                                $calc = ($pet->weight*$g->raw_adjust);
                                
                                // We're simply weight based.
                                $sl = $db->prepare("SELECT percentage FROM adult_supplement WHERE from_weight <= ? AND to_weight >= ? AND category_id = ?");
                                $sl->execute(array($pet->weight,$pet->weight,$xx->category_id));
                                $l = $sl->fetchObject();
                                $puppy_percentage = $l->percentage;
                                
                                // Do we have any activity adjustments
                                $sf = $db->prepare("SELECT size_adjust FROM body_sizes WHERE title = ?");
                                $sf->execute(array($pet->physical_condition));
                                $f = $sf->fetchObject();
                                
                                if($f->size_adjust < 0){
                                    $adjuster = $f->size_adjust;
                                    $direction = "minus";
                                }else{
                                    if($f->size_adjust > $g->raw_adjust){
                                        $adjuster = $f->size_adjust;
                                    }else{
                                        $adjuster = $g->raw_adjust;
                                    }
                                }
                            }
                            $per_day = ((($pet->weight*1000)/100)*$puppy_percentage); 
                            
                            if(!empty($adjuster)){
                                
                                $diff = (($per_day/100)*$adjuster);
                                $per_day = ($per_day+$diff);
                                
                            }
                            
                        break;
                    }
                    
                }
            }else{
                echo "<title>404 Page Not Found | {$company->name}</title>";    
            }
       
        echo "<meta name='robots' content='noindex,nofollow' />";
    break;
        
    case "/detail.php":
        $st = $db->prepare("SELECT p.id, p.title, p.image, p.introduction, p.description, p.meta_title, p.meta_description, r.title AS range_title, r.seo AS range_seo, p.composition, p.category_id, p.nutrition, p.constituents, p.food_type, p.feeding
                            FROM products p
                            LEFT JOIN product_ranges r ON p.range_id = r.id
                            WHERE r.seo = ? AND p.seo = ? AND p.status = ? AND r.status = ?");
        $st->execute(array($_GET['r'], $_GET['id'], "Published", "Published"));
        if($st->rowCount() > 0){
            $xx = $st->fetchObject();
            echo !empty($xx->meta_title) ? "<title>{$xx->meta_title}</title><meta property='og:title' content='{$xx->meta_title}' />" : 
                                           "<title>{$xx->title} for Dogs - {$company->name}</title><meta property='og:title' content='{$xx->title} for Dogs - {$company->name}' />";
            
            echo !empty($xx->meta_description) ? "<meta name='description' content='{$xx->meta_description}' /><meta property='og:description' content='{$xx->meta_description}' />" : 
                                                 "<meta name='description' content='".strip_tags($xx->introduction)."' /><meta property='og:description' content='".strip_tags($xx->introduction)."' />";
            
            echo "<meta property='og:url' content='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />
                  <meta property='og:site_name' content='{$company->name}'/>
                  <meta property='og:image' content='".MAIN_SITE.PRODUCTS.$xx->image."' />
                  <meta property='og:type' content='website' />
                  <meta property='og:locale' content='en_GB' />";
            echo "<link rel='canonical' href='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />";	
            
        }else{
            header("HTTP/1.0 404 Not Found");
            echo "<title>404 Page Not Found | {$company->name}</title>
                  <meta name='robots' content='noindex,nofollow' />";
            $show_404 = 1;
        }
        
    break;
        
    case "/order.php":
        echo "<title>Order | {$company->name}</title>
              <meta name='robots' content='noindex,nofollow' />";    
    break;
        
    case "/process.php":
        $bits = (100/11);
        if(empty($_GET['stage'])){
            $percent = $bits;
            $stage = 1;
        }else{
            $percent = round(($_GET['stage']*$bits));
            $stage = $_GET['stage'];
        }
        unset($_SESSION['seen']);
        echo "<title>Start Ooddling with {$company->name}</title>
              <meta name='robots' content='noindex,nofollow' />";
    break;
        
        
    case "/summary.php":
        /*ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);*/
        
        echo "<title>Summary | {$company->name}</title>
              <meta name='robots' content='noindex,nofollow' />";  
        
        // Finalise the order
        $st = $db->prepare("SELECT * FROM orders WHERE session_id = ? AND status = ?");
        $st->execute(array(session_id(), "Unpaid"));
        
        if($st->rowCount() > 0){
              $xx = $st->fetchObject();

              // Update the delivery
              $su = $db->prepare("UPDATE deliveries SET order_id = ? WHERE session_id = ?");
              $su->execute(array($xx->id, session_id()));

              // Set the total
              $sd = $db->prepare("SELECT SUM(price) AS total, SUM(discount) AS discount FROM cart WHERE order_id = ?");
              $sd->execute(array($xx->id));
              $d = $sd->fetchObject();

              if($cart->discount > 0){
                  //$discount = ($discount+$cart->discount);
              }

              $su = $db->prepare("UPDATE orders SET total = ?, discount_code = ?, discount_total = ?, customer_id = ?, shipping = ? WHERE id = ?");
              $su->execute(array($d->total, !empty($_SESSION['code']) ? $_SESSION['code'] : "", !empty($discount) ? $discount : "", $customer->id, !empty($delivery_price) ? $delivery_price : "", $xx->id));
        
              $sq = $db->prepare("SELECT * FROM orders WHERE id = ?");
              $sq->execute(array($xx->id));
              $q = $sq->fetchObject();
        
        if(round(($q->total-$q->discount_total+$delivery_price)*100) > 0){
              require_once('stripe/init.php');
              $stripe = new \Stripe\StripeClient($stripe_secret_key);

            if(!empty($customer->unique_id)){
                // Create a unique ID
                
                
              // Create the customer object
              if(empty($customer->stripe_id)){
                  try {
                      $cust = $stripe->customers->create([
                          'name' => $customer->name.' '.$customer->surname,
                          'email' => $customer->email,
                          'phone' => $customer->telephone,
                          'address' => array('line1' => $customer->address, 'city' => $customer->city, 'postal_code' => $customer->postcode, 'country' => $customer->country),
                          'metadata' => array('type' => $customer->account_type, 'unique_id' => $customer->unique_id)
                      ]);

                      $customer_id = $cust->id;
                  }catch (Exception $e) {

                  }

                  if(!empty($cust->id)){
                      $su = $db->prepare("UPDATE customers SET stripe_id = ? WHERE unique_id = ?");
                      $su->execute(array($cust->id, $customer->unique_id));
                  }
              }else{
                  // Update
                  $stripe->customers->update(
                    $customer->stripe_id,
                    [
                        'name' => $customer->name.' '.$customer->surname,
                        'email' => $customer->email,
                        'phone' => $customer->telephone,
                        'address' => array('line1' => $customer->address, 'city' => $customer->city, 'postal_code' => $customer->postcode, 'country' => $customer->country),
                        'metadata' => array('type' => $customer->account_type, 'unique_id' => $customer->unique_id)
                    ]
                  );

                  $customer_id = $customer->stripe_id;
              }
            }



              $payment = $stripe->paymentIntents->create([
                  'amount' => round(($q->total-$q->discount_total+$delivery_price)*100),
                  'currency' => 'gbp',
                  'setup_future_usage' => 'off_session',
                  'payment_method_types' => ['card'],
                  'customer' => $customer_id,
                  'description' => 'Ooddles Order '.$xx->unique_id,
                  'shipping' => array('name' => $delivery->d_name.' '.$delivery->d_surname,
                                     'address' => array('line1' => $delivery->d_address,
                                                       'city' => $delivery->d_city,
                                                       'postal_code' => $delivery->d_postcode,
                                                       'country' => $iso),
                                      'carrier' => 'DPD'
                                     ),
                  'metadata' => array('order_id' => $xx->unique_id)

              ]);

              $so = $db->prepare("UPDATE orders SET intent_id = ?, secret = ? WHERE id = ?");
              $so->execute(array($payment->id, $payment->client_secret, $xx->id));
        }
       
        
        
        
        ?>
        <script
			  src="https://code.jquery.com/jquery-3.5.1.min.js"
			  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			  crossorigin="anonymous"></script>
        <script src="https://js.stripe.com/v3/"></script>
        <script type="text/javascript">
            $(document).ready(function(){
               var stripe = Stripe('<?= $stripe_key; ?>');
               var elements = stripe.elements(); 
               var style = {
                  base: {
                    color: "#32325d",
                  }
                };
                
                var card = elements.create("card", { style: style });
                card.mount("#card-element");

                card.on('change', ({error}) => {
                  const displayError = document.getElementById('card-errors');
                  if (error) {
                    displayError.textContent = error.message;
                  } else {
                    displayError.textContent = '';
                  }
                });


                var form = document.getElementById('payment-form');

                form.addEventListener('submit', function(ev) {
                    $("#loading").show();
                    ev.preventDefault();
                    var clientSecret = $('#payment-form button').attr('data-secret');

                  stripe.confirmCardPayment(clientSecret, {
                    payment_method: {
                      card: card,
                      billing_details: {
                        name: '<?= $customer->name.' '.$customer->surname; ?>',
                        address: {
                            line1: '<? echo $customer->address; ?>',
                            city: '<? echo $customer->city; ?>',
                            postal_code: '<? echo $customer->postcode; ?>',
                            country: '<? echo $iso; ?>'
                        }
                      }
                    }
                  }).then(function(result) {
                    if (result.error) {
                      // Show error to your customer (e.g., insufficient funds)
                        $("#card-errors").text(result.error.message);
                      console.log(result.error.message);
                        $("#loading").fadeOut();
                    } else {
                      // The payment has been processed!
                      if (result.paymentIntent.status === 'succeeded') {
                          setTimeout(function(){
                            window.location.replace("<?= MAIN_SITE."/order/".$xx->unique_id; ?>");   
                          },3000);
                        
                      }
                    }
                  });
                });
            });
        </script>

        <?
        }
        
    break;
        
    case "/upsell.php":
        $sp = $db->prepare("SELECT * FROM pets WHERE unique_id = ?");
        $sp->execute(array($_GET['pet'])); 
        $pet = $sp->fetchObject();
        
        echo "<title>{$company->meta_title}</title>
			  <meta name='description' content='{$company->meta_description}' />
			  <meta property='og:url' content='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />
			  <meta property='og:title' content='{$company->meta_title}' />
			  <meta property='og:description' content='{$company->meta_description}' />
			  <meta property='og:site_name' content='{$company->name}'/>
			  <meta property='og:image' content='".MAIN_SITE.IMAGES."oode.jpg' />";
		echo "<link rel='canonical' href='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />";	

        
    break;
   
	
	default:
		echo "<title>{$company->meta_title}</title>
			  <meta name='description' content='{$company->meta_description}' />
			  <meta property='og:url' content='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />
			  <meta property='og:title' content='{$company->meta_title}' />
			  <meta property='og:description' content='{$company->meta_description}' />
			  <meta property='og:site_name' content='{$company->name}'/>
			  <meta property='og:image' content='".MAIN_SITE.IMAGES."oode.jpg' />";
		echo "<link rel='canonical' href='".MAIN_SITE.$_SERVER['REQUEST_URI']."' />";	  
			  
	break;
}

?>