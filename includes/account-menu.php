<div id="menu">
                        <h3><span><img src="/images/logo-o.png" alt="O Hub" /> Hub</span></h3>
                        <ul>
                            <li><a href="/o-hub" title="Account">Account Home</a></li>
                            <li><a href="/o-hub/pets" title="Pet Profiles">Pet Profiles</a></li>
                            <li><a href="/o-hub/subscriptions" title="Subscriptions">Subscriptions</a></li>
                            <li><a href="/o-hub/orders" title="Order History">Order History</a></li>
                            <li><a href="/o-hub/my-details" title="My Details">My Details</a></li>
                            
                            <li class="split"><a href="#" title="Our Dog Food">Our Dog Food</a></li>
                            <?
                                $st = $db->prepare("SELECT * FROM pages WHERE visibility = ?");
                                $st->execute(array("Private"));
                                while($r = $st->fetchObject()){
                                    echo "<li><a href='/o-hub/{$r->seo}' title='{$r->title}'>{$r->title}</a></li>";
                                }
                            ?>
                            <li><a href="#" title="Feeding Guides">Feeding Guides</a></li>
                            
                        </ul>
                        <p>Need help or advice? <a href="#" title="Email <? echo $company->name; ?>">Drop us an email</a></p>
                        
                    </div><!--close menu-->