<section class="no_bottom_pad">
    
<div id="quick_blog">
    <h2>Ooddles Woof Blog</h2>
        <div class="glide">

				<div class="glide__arrows">
					<button class="glide__arrow prev" data-glide-dir="<">prev</button>
					<button class="glide__arrow next" data-glide-dir=">">next</button>
				</div>

				<div class="glide__wrapper">
					<ul class="glide__track">
                        <?
                        $st = $db->prepare("SELECT * FROM advice WHERE publish_on <= ? ORDER BY publish_on DESC LIMIT 12");
                        $st->execute(array(date("Y-m-d")));
                        $num = $st->rowCount();
                        if($deviceType == "phone"){
                            $count = 1;
                        }else{
                            $count = 4;
                        }
                        echo "<li class='glide__slide'><div class='flex negative'>";
                        for($x=1; $x <= $num; $x++){
                            $r = $st->fetchObject();
                            echo "<div class='c_25'>
                                  <div class='news_item'>";
                            if(!empty($r->image)){
                                echo "<div class='news_img'><a href='/blog/{$r->seo}' title='{$r->title}'><img src='/thumb.php?src=".THUMBS.$r->image."&w=300&h=250' alt='{$r->title}' /></a></div>";
                            }else{
                                echo "<div class='news_img'><a href='/care-and-advice/{$r->seo}' title='{$r->title}'><img src='/thumb.php?src=/images/thumbs/what-is-ooddlying-3.jpg&w=300&h=250' alt='{$r->title}' /></a></div>";
                            }

                            echo "<div class='desc'>
                                  <h3><a href='/blog/{$r->seo}' title='{$r->title}'>{$r->title}</a></h3>
                                  <p class='date'>".date("F j, Y", strtotime($r->publish_on))."</p>
                                  <a href='/blog/{$r->seo}' title='{$r->title}'>Read more</a></p>


                                  </div>
                                  </div>
                                  </div>";
                            if($x % $count == 0 && $x != $num){
                                echo "</div></li><li class='glide__slide'><div class='flex negative'>";    
                            }
                        }
                        echo "</div></li>";
                        
                
						?>
						
						
					</ul>
				</div>

				<div class="glide__bullets"></div>

			</div>
        </div><!--close quick_blog-->
    <p class="start centre"><a href="/blog" title="Ooddles Blog" class="btn brown">See all our articles in the blog</a></p>
    </section>
<section class="no_top_pad">
<div id="feefo_wrapper">
    <div id="feefo-service-review-carousel-widgetId" class="feefo-review-carousel-widget-service"></div>
</div><!--close wrapper-->
    </section>