<section>
    <div id="breadcrumbs">
        <ul class="flex">
            <li><a href="/o-hub" title="O Hub">O Hub</a></li>
            <li>&rang;</li>
            <li><a href="/o-hub/ranges" title="Our Range">Our Range</a></li>
            <li>&rang;</li>
            <li><a href="<? echo $_REQUEST['URI']; ?>" title="<? echo $xx->category; ?>"><? echo $xx->category; ?></a></li>
            
            
        </ul>
        </div>
                
    </section>

       <img src="/images/image-placeholder.jpg" alt="Placeholder" class="placeholder" />

<section class="products">
    <div class="flex negative justify">
        <?
            // Our wheres...
            if(!empty($pet->size)){
                $where .= " AND (p.suitable_for LIKE '%{$pet->size}%' OR p.suitable_for = '')";
            }
            if(!empty($pet->dislikes)){
                if(stripos($pet->dislikes, ",") !== false){
                    $bits = explode(",", $pet->dislikes);
                    foreach($bits as $b){
                        $where .= " AND p.main_ingredient NOT LIKE '%{$b}%'";        
                    }
                }else{
                    $where .= " AND p.main_ingredient NOT LIKE '%{$pet->dislikes}%'";    
                }

            }

/*echo "SELECT p.title, p.seo, p.image, p.food_type, r.seo AS range_seo, c.seo AS category_seo, c.category 
                                FROM skus s 
                                LEFT JOIN products p ON s.product_id = p.id 
                                LEFT JOIN product_ranges r ON p.range_id = r.id
                                LEFT JOIN categories c ON p.category_id = c.id
                                WHERE p.status = Published AND s.status = Published AND s.stock > 0 AND p.category_id = ".$xx->id." {$where}
                                GROUP BY s.product_id"; exit;
*/
            $sp = $db->prepare("SELECT p.title, p.seo, p.image, p.food_type, r.seo AS range_seo, c.seo AS category_seo, c.category 
                                FROM skus s 
                                LEFT JOIN products p ON s.product_id = p.id 
                                LEFT JOIN product_ranges r ON p.range_id = r.id
                                LEFT JOIN categories c ON p.category_id = c.id
                                WHERE p.status = ? AND s.status = ? AND s.stock > ? AND p.category_id = ? {$where}
                                GROUP BY s.product_id");
            $sp->execute(array("Published", "Published", 0, $xx->id));
        
            while($p = $sp->fetchObject()){
        ?>
          <div class="c_25">
              <div class="inner item">
                  <div class="product_img"><a href="/o-hub/ranges/<? echo $_GET['id']; ?>/<? echo $p->seo; if(!empty($_GET['pet'])){ echo "?pet=".$_GET['pet']; } ?>" title="<? echo $p->title; ?>"><? if(!empty($p->image)){ echo "<img src='/thumb.php?src=/images/products/{$p->image}&w=400&h=400&zc=2' alt='{$p->title}' />";  } ?></a></div>
                  <h2><a href="/o-hub/ranges/<? echo $_GET['id']; ?>/<? echo $p->seo; if(!empty($_GET['pet'])){ echo "?pet=".$_GET['pet']; } ?>" title="<? echo $p->title; ?>"><? echo $p->title; ?></a></h2>
                  <p><? echo $p->introduction; ?></p>
                  <p><a href="/o-hub/ranges/<? echo $_GET['id']; ?>/<? echo $p->seo; if(!empty($_GET['pet'])){ echo "?pet=".$_GET['pet']; } ?>" title="<? echo $p->title; ?>" class="btn white">Buy Now</a></p>
              </div><!--close item-->
          </div><!--close c_25-->
    <?  } ?>              
    </div><!--close flex-->
    
</section>
<?
            $st = $db->prepare("SELECT p.title, p.image, r.title AS range_title, p.image, p.introduction, p.description, p.composition, p.id, p.nutrition, p.constituents, p.food_type, p.seo, r.seo AS range_seo, c.seo AS category_seo
                                FROM skus s 
                                LEFT JOIN products p ON s.product_id = p.id 
                                LEFT JOIN product_ranges r ON p.range_id = r.id
                                LEFT JOIN categories c ON p.category_id = c.id
                                WHERE p.status = ? AND s.status = ? AND s.stock > ? AND p.category_id != ? {$where} ORDER BY RAND() LIMIT 6");
            $st->execute(array("Published", "Published", 0, $xx->id));
            $num = $st->rowCount();
            if($num > 0){
                ?>
                    
                        <section>
                            <div class="related light_blue">
                  <h3>Why not try...</h3>
                 <div class="glide">

				<div class="glide__arrows">
					<button class="glide__arrow prev" data-glide-dir="<">prev</button>
					<button class="glide__arrow next" data-glide-dir=">">next</button>
				</div>

				<div class="glide__wrapper">
					<ul class="glide__track">
                        <li class="glide__slide">
                            <div class="flex justify">
						<? 
                            for($x=1; $x <= $num; $x++){
                                $r = $st->fetchObject();
                                echo "<div class='c_25'>
                                      <div class='inner'>
                                      <div class='product_img'><a href='/o-hub/ranges/{$r->category_seo}/{$r->seo}' title='{$r->title}'><img src='/thumb.php?src=/images/products/{$r->image}&w=500&h=500&zc=2' alt='{$r->title}' /></a></div>
                                      <h3><a href='/o-hub/ranges/{$r->category_seo}/{$r->seo}' title='{$r->title}'>{$r->range_title}<br />{$r->title}</a></h3>
                                      <p><a href='/o-hub/ranges/{$r->category_seo}/{$r->seo}' title='{$r->title}' class='btn white'>Buy Now</a></p>
                                      </div>
                                      </div>";
                                if($x % 3 == 0 && $x != $num){
                                    echo "</div></li><li class='glide__slide'><div class='flex justify'>";
                                }
                            }
                        ?>
                        </div>
                        </li>
						
						
					</ul>
				</div>

				<div class="glide__bullets"></div>

			</div>
                            </div>
                            </section>
                            
    <? } ?>