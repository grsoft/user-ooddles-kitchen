<section>
    <div id="breadcrumbs">
        <ul class="flex">
            <li><a href="/o-hub" title="O Hub">O Hub</a></li>
            <li>&rang;</li>
            <li><a href="/o-hub/pets" title="Pets">Pets</a></li>
            <li>&rang;</li>
            <li><a href="<? echo $_REQUEST['URI']; ?>" title="<? echo $xx->name; ?>"><? echo $xx->name; ?></a></li>
            
            
        </ul>
        
        </div>
   
    <div class="flex negative">
        <div class="c_40">
            <div class="inner">
                 <div class="profile">
                     <h2><? echo $xx->name; ?>'s Profile</h2>
             <div class="flex questions negative">
            <div class="c_50">
                <div class="inner">
                    <h4>Breed</h4>
                    <p><? echo $xx->breed; ?></p>
                </div>
            </div>
            <div class="c_50">
                <div class="inner">
                    <h4>Type</h4>
                    <p><? echo $xx->type_id; ?></p>
                </div>
            </div>
            <div class="c_50">
                <div class="inner">
                    <h4>Size</h4>
                    <p><? echo $xx->size; ?></p>
                </div><!--close inner-->
            </div>
            
            <div class="c_50">
                <div class="inner">
                    <h4>Weight</h4>
                    <p><? echo $xx->weight; ?>kg</p>
                </div><!--close inner-->
            </div>
             <div class="c_50">
                <div class="inner">
                    <h4>Physical Condition</h4>
                    <p><? echo $xx->physical_condition; ?></p>
                </div><!--close inner-->
            </div>
             <div class="c_50">
                <div class="inner">
                    <h4>Age</h4>
                    <p><? list($years,$months) = explode(":", $xx->age); echo $years." years, ".$months." months"; ?></p>
                </div><!--close inner-->
            </div>
             <div class="c_50">
                <div class="inner">
                    <h4>Birthday</h4>
                    <p><? 
                        if(!empty($xx->birthday)){
                        $bits = explode(":", $xx->birthday);
                            
                        $monthNum  = $bits[1];
                        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                        $monthName = $dateObj->format('F'); 
                        echo $bits[0]." ".$monthName;
                        }
                        
                        ?></p>
                </div><!--close inner-->
            </div>
             <div class="c_50">
                <div class="inner">
                    <h4>Gender</h4>
                    <p><? echo $xx->gender; ?></p>
                </div><!--close inner-->
            </div>
            
            <?
                if(!empty($xx->spayed)){
                    ?>
             <div class="c_50">
                <div class="inner">
                    <h4>Spayed</h4>
                    <p><? echo $xx->spayed; ?></p>
                </div><!--close inner-->
            </div>
            <? } ?>
            
            <?
            if(!empty($xx->pregnant)){
                ?>
            
             <div class="c_50">
                <div class="inner">
                    <h4>Pregnant</h4>
                    <p><? echo $xx->pregnant; ?></p>
                </div><!--close inner-->
            </div>
        <? } ?>    
            
            <?
            if(!empty($xx->neutered)){
                ?>
            
             <div class="c_50">
                <div class="inner">
                    <h4>Neutered</h4>
                    <p><? echo $xx->neutered; ?></p>
                </div><!--close inner-->
            </div>
        <? } ?>    
            
             <div class="c_50">
                <div class="inner">
                    <h4>Health Issues</h4>
                    <p><? echo str_replace(",", ", ", $xx->health_issues); ?></p>
                </div><!--close inner-->
            </div>
            <!--<div class="c_25">
                <div class="inner">
                    <h4>Conditions</h4>
                    <p><? //echo $xx->conditions; ?></p>
                </div><!--close inner
            </div>-->
            <?
            if(!empty($xx->dislikes)){
                ?>
            
            <div class="c_50">
                <div class="inner">
                    <h4>Dislikes</h4>
                    <p><? echo $xx->dislikes; ?></p>
                </div><!--close inner-->
            </div>
            <? } ?>
            <div class="c_50">
                <div class="inner">
                    <h4>Food Types</h4>
                    <p><? 
                        
                        if(stripos($xx->food_types, ",") !== false){
                            $cats = explode(",", $xx->food_types);
                        }else{
                            $cats = array($xx->food_types);
                        }
                        $sc = $db->prepare("SELECT category FROM categories WHERE id = ?");
                        $types = array();
                        foreach($cats as $k){
                            $sc->execute(array($k));
                            $c = $sc->fetchObject();
                            $types[] = $c->category;
                        }
                        echo implode(", ", $types);
                        ?></p>
                </div><!--close inner-->
            </div>
            <div class="c_50"><div class="inner"><a href="/actions.php?action=edit-pet&id=<? echo $xx->unique_id; ?>" class="btn white small">Edit Profile</a><p><a href="/confirm.php?area=remove-pet&id=<? echo $xx->unique_id; ?>" class="btn white small">Remove Profile</a></p></div></div>
        </div><!--close flex-->
                </div><!--close profile-->
            </div><!--close inner-->
            
            
        </div><!--close c_40-->
        <div class="c_60">
            <div class="inner">
                <div class="orders auto_repeat">
                <h2>Auto Repeat Orders for <? echo $xx->name; ?></h2>
    
    <table class="table-responsive-full">
      <thead>
        <tr>
          <th>Start</th>
          <th>Pet</th>
          <th>Total</th>
            <th>Frequency</th>
            <th>Next</th>
            <th>Status</th>
             <th class="actions">Action</th>
        </tr>
      </thead>
      <tbody>
          <?
            get_auto_repeat($db, array($customer->id, $xx->unique_id));
          ?>


      </tbody>
  </table>
                </div><!--close orders-->
            </div>
            
        </div>
        
    
</div><!--close flex-->
        
    </section>

<?
if(!empty($xx->food_types)){
    ?>
<section class="products recommended">
    
    
    <?
    // Get our size
    $ss = $db->prepare("SELECT id FROM size_age WHERE age_from <= ? AND age_to >= ?");
    $ss->execute(array(($years*12)+$months,($years*12)+$months));
    $s = $ss->fetchObject();
    
    // Our wheres...
    if(!empty($xx->size)){
        $where .= " AND p.suitable_for LIKE '%{$xx->size}%'";
    }
    if(!empty($s->id)){
        $where .= " AND p.life_stages LIKE '%{$s->id}%'";
    }
    if(!empty($xx->dislikes)){
        if(stripos($xx->dislikes, ",") !== false){
            $bits = explode(",", $xx->dislikes);
            foreach($bits as $b){
                $where .= " AND p.main_ingredient NOT LIKE '%{$b}%'";        
            }
        }else{
            $where .= " AND p.main_ingredient NOT LIKE '%{$xx->dislikes}%'";    
        }
        
    }
    $sp = $db->prepare("SELECT p.title, p.seo, p.image, p.food_type, r.seo AS range_seo, c.seo AS category_seo, c.category 
                        FROM skus s 
                        LEFT JOIN products p ON s.product_id = p.id 
                        LEFT JOIN product_ranges r ON p.range_id = r.id
                        LEFT JOIN categories c ON p.category_id = c.id
                        WHERE p.status = ? AND s.status = ? AND s.stock > ? AND p.category_id = ? {$where}
                        GROUP BY s.product_id");
    
    foreach($cats as $t){
       $sc->execute(array($t));
       $c = $sc->fetchObject();
        
        $sp->execute(array("Published", "Published", 0, strtolower($t)));
        if($sp->rowCount() > 0){
        ?>
    <h3>Recommended <? echo $c->category; ?> for <? echo $xx->name; ?></h3>
    <div class="flex negative">
        <?
           while($p = $sp->fetchObject()){
        ?>
          <div class="c_25">
              <div class="inner item">
                  <div class="product_img"><a href="/o-hub/ranges/<? echo $p->category_seo; ?>/<? echo $p->seo; ?>?pet=<? echo $_GET['id']; ?>" title="<? echo $p->title; ?>"><? if(!empty($p->image)){ echo "<img src='/thumb.php?src=/images/products/{$p->image}&w=400&h=400' alt='{$p->title}' />"; } ?></a></div>
                  <h2><a href="/o-hub/ranges/<? echo $p->category_seo; ?>/<? echo $p->seo; ?>?pet=<? echo $_GET['id']; ?>" title="<? echo $p->title; ?>"><? echo $p->title; ?></a></h2>
                  
                  <p><a href="/o-hub/ranges/<? echo $p->category_seo; ?>/<? echo $p->seo; ?>?pet=<? echo $_GET['id']; ?>" title="<? echo $p->title; ?>" class="btn white">Buy Now</a></p>
              </div><!--close item-->
          </div><!--close c_25-->
    <?  } ?>              
    </div><!--close flex-->
    <? }
    } ?>
    
</section>
<? } ?>