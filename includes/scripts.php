<script
src="https://code.jquery.com/jquery-3.5.1.min.js"
integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
crossorigin="anonymous"></script>
<script src="https://scripts.sirv.com/sirv.js"></script>
<script src="/js/glide.js"></script>
<script type="text/javascript" src="/js/heights.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="/js/colorbox.js"></script>


<?
if($deviceType == "phone"){
    ?>
    <script type="text/javascript">
        $(window).bind("load", function(){
            var b = $("#banner").outerHeight();
            $("#banner").css("height", b+"px").css("min-height", "10px");
        })    
    </script>
<?
}
?>
<script type="text/javascript">
    <?
		if(!empty($_SESSION['status'])){
			?>
			$(document).ready(function(){
				$("#status").delay(3000).fadeOut();
                $("#status").click(function(){
                    $(this).fadeOut();
                });
			});
			<?
		}
		?>
    $(document).ready(function(){
        <?
        if($deviceType != "phone"){
            ?>
        $("a.dropper").hover(function(){
            $(this).addClass("active");
            $(".dropdown").show();
        });
    <?  } ?>    

            $("a.dropper").hover(function(){
                
                $(this).addClass("active");
                        $(".dropdown").show();
            });
            $(".subnav a:not(.dropper), .headline, .nav, #banner img").hover(function(){
                        $("a.dropper").removeClass("active");
                        $(".dropdown").hide();
                    });
                    $("#dropdown a").hover(function(){
                        $("a.dropper").addClass("active");
                        $(".dropdown").show();
                    });

        /*$(".subnav a:not(.dropper), .headline, .nav, #banner img").hover(function(){
            $("a.dropper").removeClass("active");
            $(".dropdown").hide();
        });
        $("#dropdown a").hover(function(){
            $("a.dropper").addClass("active");
            $(".dropdown").show();
        });*/
		//------------------------------------------------new dropdown-----------------------------------
		 
        $("a.dropper2").hover(function(){
            
            $(this).addClass("active");
                    $(".dropdown2").show();
        });
        $(".subnav a:not(.dropper2), .headline, .nav, #banner img").hover(function(){
                    $("a.dropper2").removeClass("active");
                    $(".dropdown2").hide();
                });
                $("#dropdown2 a").hover(function(){
                    $("a.dropper2").addClass("active");
                    $(".dropdown2").show();
                });


             
            $("a.dropper3").hover(function(){
                
                $(this).addClass("active");
                        $(".dropdown3").show();
            });
            $(".subnav a:not(.dropper3), .headline, .nav, #banner img").hover(function(){
                        $("a.dropper3").removeClass("active");
                        $(".dropdown3").hide();
                    });
                    $("#dropdown3 a").hover(function(){
                        $("a.dropper3").addClass("active");
                        $(".dropdown3").show();
                    });


// dropper 4

            $("a.dropper4").hover(function(){
                
                $(this).addClass("active");
                        $(".dropdown4").show();
            });
            $(".subnav a:not(.dropper4), .headline, .nav, #banner img").hover(function(){
                        $("a.dropper4").removeClass("active");
                        $(".dropdown4").hide();
                    });
                    $("#dropdown4 a").hover(function(){
                        $("a.dropper4").addClass("active");
                        $(".dropdown4").show();
                    });

	
		
		
		
        $("a.forgot").click(function(e){
            e.preventDefault();
            $("form[name=password]").fadeToggle();
        });
        $(".nav button, img.close").click(function(e){
            e.preventDefault();
            $("#mobile_menu").toggleClass("active");
        });
        
        $(".close").click(function(){
            $("#upsell").fadeOut();
        });
        
        $("a.scroll").click(function(e){
              e.preventDefault();
                var y = $(this).attr("data-id");
              var target_offset = $("#"+y).offset();
              var target_top = target_offset.top;
              setTimeout(function(){
                  $('html, body').animate({scrollTop:(target_top-120)}, 800);
              },500);   
          })
    });
</script>
<?
switch(PAGE){
    case "/account.php":
        ?>
        <script>
            $(document).ready(function(){
                //equalheight('.c_33 .inner'); 
                
                var carousel = $('.glide').glide({
                    type: 'slider',
                    autoplay: 4000,
                    animationDuration: 600,
                    hoverpause: false
                });
            });
            $(window).resize(function(){
                //equalheight('.c_33 .inner');    
            });
            $(window).bind("load", function(){
                //equalheight('.c_33 .inner');    
            })
        </script>
        <?
    break;
        
    case "/addresses.php":
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
               
                
                $("input[name=type]").change(function(){
                    if($(this).val() == "Delivery"){
                        $("#delivery_wrap").show();
                    } else {
                        $("#delivery_wrap").hide();
                    }
                });
                $("input#same").change(function(){
                    $("#delivery_opts").fadeToggle();
                });
                <? if($_SESSION['same'] == "-"){ ?>
                    $("#delivery_opts").show();
                <? } ?>
                <? if($_SESSION['type'] == "Delivery"){ ?>
                    $("#delivery_wrap").show();
                <? } ?>
                
            });
            
        </script>
        <?
    break;
        
    case "/basket.php":
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
               
                
                var carousel = $('.glide').glide({
                    type: 'carousel',
                    autoplay: 6000,
                    animationDuration: 600,
                    hoverpause: false
                });
                
            })    
        </script>
        <?
    break;
        
    case "/category.php":
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                equalheight('.products h3');
                
                var carousel = $('.glide').glide({
                    type: 'carousel',
                    autoplay: 6000,
                    animationDuration: 600,
                    hoverpause: false
                });
                
                $("a.video_link").click(function(e){
                    e.preventDefault();
                    var t = $(this).attr("data-id");
                    
                    $.post("/load.php?type=video&video="+t, function(data) {
                        
                        $("#video").html("<video playsinline loop autoplay controls id='bgvid'><source src='/videos/"+data.webm+"' type='video/webm'><source src='/videos/"+data.mp4+"' type='video/mp4'></video>");   
                    }, "json");
                    
                        $("#video").fadeIn();
                   
                })
                
            })
            $(window).resize(function(){
               equalheight('.products h3');
            });
            $(window).bind("load", function(){
                equalheight('.products h3');
            })
        </script>
        <?
    break;
        
    case "/shop.php":
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                equalheight('.c_25 .intro_text');
                
            })
            $(window).resize(function(){
               equalheight('.c_25 .intro_text');
            });
            $(window).bind("load", function(){
                equalheight('.c_25 .intro_text');
            })
        </script>
        <?
    break;
        
    case "/blog.php":
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                equalheight('.c_25 h3');
                
            })
            $(window).resize(function(){
               equalheight('.c_25 h3');
            });
            $(window).bind("load", function(){
                equalheight('.c_25 h3');
            })
        </script>
        <?
    break;
        
    case "/blog-article.php":
        ?>
        <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5fb669e2efdc7a0012dc0054&product=inline-share-buttons" async="async"></script>
        <?
    break;
        
    case "/detail.php":
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $("ul.tabs li").click(function(e){
                    e.preventDefault();
                    var t = $(this).find("a").attr("data-id");
                    $(".sector").hide();
                    $("#"+t).show();
                    $("ul.tabs li").removeClass("active");
                    $(this).addClass("active");
                });
                
                $(".nug").click(function(e){
                    e.preventDefault();
                    // Quantity
                    var q = $(this).parent().parent().find("#qty");
                    var qty = $(this).parent().parent().find("input[name=qty]");
                    
                    var s = $(this).parent().parent().find("#stock");
                    var p = parseInt(q.html());
                    // Direction
                    var t = $(this).attr("data-id");
                    // Stock
                    var s = parseInt(s.html());
                    
                    
                    if(t == "minus"){
                        if(p == 0){
                            var h = 0;
                            $(q).html(0);
                        } else {
                            var h = (p-1);
                            $(q).html();
                        }
                    }
                    if(t == "plus"){
                        if(p == s){
                             var h = s;
                            
                        } else {
                            var h = (p+1);
                        }
                    }
                    
                    $(q).html(h);
                    $(qty).val(h);
                    
                    
                });
                
                var carousel = $('.glide').glide({
                    type: 'carousel',
                    autoplay: 6000,
                    animationDuration: 600,
                    hoverpause: false
                });
                
            })    
        </script>
        <?
    break;
        
    case "/index.php":
        ?>
        <script>
            $(document).ready(function(){
                var carousel = $('#banner .glide').glide({
                    type: 'carousel',
                    autoplay: 4000,
                    animationDuration: 600,
                    hoverpause: false
                });
                
                 var news = $('#quick_blog .glide').glide({
                    type: 'carousel',
                    autoplay: 6000,
                    animationDuration: 600,
                    hoverpause: true
                });
                equalheight('#quick_blog h3');
                
                $("a.preview").colorbox({iframe:true, width:"80%", height:"75%"});
                
                setTimeout(function(){
                    $("#popper").fadeOut();
                },10000);
                
                $("#popper").click(function(){
                    $(this).fadeOut();
                });
                
                $(".videos a").click(function(e){
                    e.preventDefault();
                    var t = $(this).attr("data-id");
                    
                    $.post("/load.php?type=video&video="+t, function(data) {
                        
                        $("#video").html("<video playsinline loop autoplay controls id='bgvid'><source src='/videos/"+data.webm+"' type='video/webm'><source src='/videos/"+data.mp4+"' type='video/mp4'></video>");   
                    }, "json");
                    
                        $("#video").fadeIn();
                   
                })
                
                
            });
            
            $(window).resize(function(){
                equalheight('#quick_blog h3');
            });
            $(window).bind("load", function(){
                equalheight('#quick_blog h3');
            })
			

		</script>
        <?
    break;
        
    case "/faq.php":
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".faq_wrap h4").click(function(e){
                    e.preventDefault();
                    $(this).toggleClass("active");
                    $(this).next(".answer").fadeToggle();
                })    
            })   
            
        </script>
        <?
    break;
    case "/process.php":
        ?>
        <script type="text/javascript" src="/js/jquery.autocomplete.js"></script>
        <script type="text/javascript" src="/js/breeds.js?v=<? echo rand(); ?>"></script>
        <script type="text/javascript" src="/js/crossbreeds.js?v=<? echo rand(); ?>"></script>
        <script type="text/javascript">
            function is_number(evt) {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }
            $(window).bind("load", function(){
                
                setTimeout(function(){
                    $(".bar span").css("width", "<? echo $percent; ?>%");
                },1000);
               
                $("a.trigger").click(function(e){
                    e.preventDefault();
                    
                    var r = $(this).attr("data-id");
                    var t = $(this).attr("data-target");
                    var q = $(this).attr("data-tag");
                    
                    if($(this).attr("data-filter") != "true"){
                        // Remove the active state and populate the val.
                        $(this).parent().parent().find("a.trigger").removeClass("active");  
                        $("input[name="+t+"]").val(r);
                        $(this).toggleClass("active");
                    } else {
                        // We need to create an arry of the ingredients
                        $(this).toggleClass("active");
                        var input = [];
                        $(".justify .trigger").each(function() {
                            if($(this).attr("data-filter") == "true" && $(this).hasClass("active")){
                                input.push($(this).attr("data-id")); 
                            }
                        });
                        $("input[name="+t+"]").val(input);
                    }
                    
                    if(r == "Pedigree"){
                        $(".process input[name=breeds]").show();
                        $(".process input[name=crossbreed]").hide();
                    }
                    if(r == "Crossbreed"){
                        
                        $(".process input[name=crossbreed]").show();
                        $(".process input[name=breeds]").hide();
                    }
                    
                   
                    
                    if(r == "Male"){
                        $(".inner_opt").hide();
                        $(".male").fadeIn();
                        $("#note").fadeOut();
                    }
                    if(r == "Female"){
                        $(".inner_opt").hide();
                        $(".female").fadeIn();
                        $("#note").fadeOut();
                    }
                    
                    if($(this).attr("data-target") == "spayed" && r == "Yes"){
                        $(".opt_select").html($(this).attr("data-target"));
                        $(".opt_select_past").html($(this).attr("data-target").replace("ed", "ing"));
                        $("#note").fadeIn();
                    } else if($(this).attr("data-target") == "spayed" && r == "No"){
                        $("#note").fadeOut();
                    }
                    
                    if($(this).attr("data-target") == "neutered" && r == "Yes"){
                        $(".opt_select").html($(this).attr("data-target"));
                        $(".opt_select_past").html($(this).attr("data-target").replace("ed", "ing"));
                        $("#note").fadeIn();
                    } else if($(this).attr("data-target") == "neutered" && r == "No"){
                        $("#note").fadeOut();
                    }
                    
                    // If we need a dropdown
                    if(q == "list"){
                        if(r == "Yes"){
                            $("."+q).fadeIn();    
                        } else {
                            $(".inner_opt").hide();
                        }
                        
                    } 
                });
                
                $("select").change(function(){
                    var r = $(this).val();
                    var t = $(this).attr("data-id");
                    $("input[name="+t+"]").val(r);
                });
                
                $('.breeds').autocomplete({
                    lookup: breeds,
                    showNoSuggestionNotice: false,
                    minChars: 3,
                    noSuggestionNotice: "This isn't in our list - but keeping typing and we'll add it",
                    modifySuggestions: function(suggestions, query){ return suggestions; },
                    lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                        var words = $.trim(queryLowerCase).split(/\s/g);
                        var is_match = false;
                        var l = words.length; 
                        var sum = 0
                        for(i in words){
                            if(suggestion.value.toLowerCase().indexOf(words[i]) !== -1){
                                is_match = true;
                                sum += 1;
                            }
                        }
                        if(sum == l){
                            return is_match;
                        }


                    },
                    onSelect: function (suggestion) {
                        $("input[name=breed]").val(suggestion.value);
                       
                    }

                });
                $('.crossbreeds').autocomplete({
                    lookup: crossbreeds,
                    showNoSuggestionNotice: false,
                    minChars: 3,
                    noSuggestionNotice: "This isn't in our list - but keeping typing and we'll add it",
                    modifySuggestions: function(suggestions, query){ return suggestions; },
                    lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                        var words = $.trim(queryLowerCase).split(/\s/g);
                        var is_match = false;
                        var l = words.length; 
                        var sum = 0
                        for(i in words){
                            if(suggestion.value.toLowerCase().indexOf(words[i]) !== -1){
                                is_match = true;
                                sum += 1;
                            }
                        }
                        if(sum == l){
                            return is_match;
                        }


                    },
                    onSelect: function (suggestion) {
                        $("input[name=breed]").val(suggestion.value);
                       
                    }

                });
                <?
                    if($_GET['stage'] == 7 && !empty($_SESSION['gender'])){
                        if($_SESSION['gender'] == "Male"){
                            ?>
                            $(".male").show();
                            <?
                        }else{
                            ?>
                            $(".female").show();
                            <?
                        }      
                        
                    }
                    if($_GET['stage'] == 9 && $_SESSION['ingredients'] == "Yes"){ ?>
                        $(".inner_opt").show();
                <?  } ?>    
                    
               
            });    
        </script>
    <?
    break;
        
    case "/option.php":
        ?>
        <script type="text/javascript">
            $(document).ready(function(){
                
                
                var carousel = $('.glide').glide({
                    type: 'carousel',
                    autoplay: 6000,
                    animationDuration: 600,
                    hoverpause: false
                });
                
            })    
        </script>
        <?
    break;
        
    
        
    case "/product.php":
        ?>
        <script src="https://unpkg.com/@popperjs/core@2"></script>
        <script src="https://unpkg.com/tippy.js@6"></script>
        <script type="text/javascript">
            function get_data(){
                
                var h = parseInt($("#qty").html());
                <?
                if($sku_count > 1){
                    ?>
                    var id = $("select[name=product]").children(":selected").val();
                    $("#product").html(id);
                    var price = $("select[name=product]").children(":selected").attr('data-id');
                    $("#base").html(price);
                    var weight = $("select[name=product]").children(":selected").attr('data-weight');
                    $("#weight").html(weight);
                    var sample = $("select[name=product]").children(":selected").attr('data-sample');
                    if(sample == 1){
                        $('#costs').hide();
                    } else {
                        $('#costs').show();
                    }
            <?  } ?>
                
                var id = $("#product").html();
                var price = (parseFloat($("#base").html()).toFixed(2)*h);
                var weight = (parseFloat($("#weight").html()).toFixed(2)*h);
                
                
                var a = $("input[name=auto_percent]").val();
                if(a == 0){
                    $("#saver").html("");
                } else {
                    $("#saver").html(a+"%");
                }
                
                var discount = ((price/100)*a);
                var price = (price-discount);
                
                $(".price span").html(parseFloat((price)).toFixed(2));
                //$("#weight").html(weight);
                $("#total_weight").html(parseFloat((weight/1000)).toFixed(2)+"kg");
               
                $("input[name=sku]").val(id);
                
                var per = parseInt($("#grams").html());
                
                
                var days = parseInt(Math.floor((weight/per)));
                
                if(days == 0){
                    var days = 1;
                }
                
                $("#per_day").html(per);
                $("#min_day_calc, #min_days").html(days);
                $("#min_cost_calc, #min_cost").html(parseFloat((price/days)).toFixed(2));
                
            }
            
            $(document).ready(function(){
                $("ul.tabs li").click(function(e){
                    e.preventDefault();
                    var t = $(this).find("a").attr("data-id");
                    $(".sector").hide();
                    $("#"+t).show();
                    $("ul.tabs li").removeClass("active");
                    $(this).addClass("active");
                });
                
                tippy('.benefits img', {
                  content(reference) {
                    const title = reference.getAttribute('title');
                    reference.removeAttribute('title');
                    return title;
                  },
                });
                
                var carousel = $('.glide').glide({
                    type: 'carousel',
                    autoplay: 6000,
                    animationDuration: 600,
                    hoverpause: false
                });
                
                 <?
                if($sku_count > 1){
                    ?>
                 get_data();
                 $("select[name=product]").change(function(){
                     get_data();
                      var price = parseFloat($("#base").html()).toFixed(2);
                      var h = parseInt($("#qty").html());
                      $(".price span").html(parseFloat((price*h)).toFixed(2));

                 });
            <? } ?>    
                
                get_data();        
                
                $(".nug").click(function(e){
                    e.preventDefault();
                    // Quantity
                    var p = parseInt($("#qty").html());
                    // Direction
                    var t = $(this).attr("data-id");
                    
                    
                    // Min Days
                    var min_day = $("#min_days").html();
                    // Min Cost
                    var min_cost = $("#min_cost").html();
                    
                    
                    if(t == "minus"){
                        if(p == 0){
                            var h = 0;
                            $("#qty").html(0);
                        } else {
                            var h = (p-1);
                            $("#qty").html();
                        }
                    }
                    if(t == "plus"){
                        if(p == 5){
                             var h = 5;
                            // Do the alert to get a trade account
                        } else {
                            var h = (p+1);
                        }
                    }
                    $("#qty").html(h);
                    $("input[name=qty]").val(h);
                    
                    
                    get_data();
                });
                
                $(".repeat a").click(function(e){
                    e.preventDefault();
                    $(".repeat a").removeClass("active");
                    $(this).addClass("active");
                    $(".nub").html($(this).attr("data-id"));
                    $("input[name=auto]").val($(this).attr("data-tag"));
                    $("input[name=auto_percent]").val($(this).attr("data-percent"));
                    get_data()
                })
                $("a.more").click(function(e){
                    e.preventDefault();
                    var target_offset = $("#more").offset();
                    var target_top = target_offset.top;
                    setTimeout(function(){
                        $('html, body').animate({scrollTop:(target_top-120)}, 800);
                    },500);   
                })
            })    
        </script>
        <?
    break;
        
    case "/recommendation.php":
        ?>
        <script src="https://unpkg.com/@popperjs/core@2"></script>
        <script src="https://unpkg.com/tippy.js@6"></script>
        <script type="text/javascript">
            function get_data(){
                
                var h = parseInt($("#qty").html());
                <?
                if($sku_count > 1){
                    ?>
                    var id = $("select[name=product]").children(":selected").val();
                    $("#product").html(id);
                    var price = $("select[name=product]").children(":selected").attr('data-id');
                    $("#base").html(price);
                    var weight = $("select[name=product]").children(":selected").attr('data-weight');
                    $("#weight").html(weight);
                    var sample = $("select[name=product]").children(":selected").attr('data-sample');
                    if(sample == 1){
                        $('#costs').hide();
                    } else {
                        $('#costs').show();
                    }
            <?  } ?>
                
                var id = $("#product").html();
                var price = (parseFloat($("#base").html()).toFixed(2)*h);
                var weight = (parseFloat($("#weight").html()).toFixed(2)*h);
                
                
                var a = $("input[name=auto_percent]").val();
                if(a == 0){
                    $("#saver").html("");
                } else {
                    $("#saver").html(a+"%");
                }
                
                var discount = ((price/100)*a);
                var price = (price-discount);
                
                $(".price span").html(parseFloat((price)).toFixed(2));
                //$("#weight").html(weight);
                $("#total_weight").html(parseFloat((weight/1000)).toFixed(2)+"kg");
               
                 $("input[name=sku]").val(id);
                
                
                var per = parseInt($("#grams").html());
                
                
                var days = parseInt(Math.floor((weight/per)));
                if(days == 0){
                    var days = 1;
                }
                
                $("#per_day").html(per);
                $("#min_day_calc, #min_days").html(days);
                $("#min_cost_calc, #min_cost").html(parseFloat((price/days)).toFixed(2));
                
            }
            
            $(document).ready(function(){
                $("ul.tabs li").click(function(e){
                    e.preventDefault();
                    var t = $(this).find("a").attr("data-id");
                    $(".sector").hide();
                    $("#"+t).show();
                    $("ul.tabs li").removeClass("active");
                    $(this).addClass("active");
                });
                
                tippy('.benefits img', {
                  content(reference) {
                    const title = reference.getAttribute('title');
                    reference.removeAttribute('title');
                    return title;
                  },
                });
                
                var carousel = $('.glide').glide({
                    type: 'carousel',
                    autoplay: 6000,
                    animationDuration: 600,
                    hoverpause: false
                });
                
                 <?
                if($sku_count > 1){
                    ?>
                 get_data();
                 $("select[name=product]").change(function(){
                     get_data();
                      var price = parseFloat($("#base").html()).toFixed(2);
                      var h = parseInt($("#qty").html());
                      $(".price span").html(parseFloat((price*h)).toFixed(2));

                 });
            <? } ?>    
                
                get_data();        
                
                $(".nug").click(function(e){
                    e.preventDefault();
                    // Quantity
                    var p = parseInt($("#qty").html());
                    // Direction
                    var t = $(this).attr("data-id");
                    
                    
                    // Min Days
                    var min_day = $("#min_days").html();
                    // Min Cost
                    var min_cost = $("#min_cost").html();
                    
                    
                    if(t == "minus"){
                        if(p == 0){
                            var h = 0;
                            $("#qty").html(0);
                        } else {
                            var h = (p-1);
                            $("#qty").html();
                        }
                    }
                    if(t == "plus"){
                        if(p == 5){
                             var h = 5;
                            // Do the alert to get a trade account
                        } else {
                            var h = (p+1);
                        }
                    }
                    $("#qty").html(h);
                    $("input[name=qty]").val(h);
                    
                    
                    
                    /*if(h > 0){
                       
                        // Now show the calc
                        $.post("/load.php?sku="+q+"&qty="+h, function(data) {
                            
                            if(data.price > 0){
                                var g = parseFloat(data.price*h).toFixed(2);
                                $(".price span").html(parseFloat(g).toFixed(2));
                            } else {
                                var g = parseFloat((b*h)).toFixed(2);
                                $(".price span").html(g);
                            }
                            
                            // Weight of order
                            $("#total_weight").html(parseFloat((w*h)).toFixed(2)+"kg");
                            
                            var per = parseInt($("#per_day").html());
                            var days = parseInt(Math.floor((((w*h)*1000)/per)));
                            
                            // Min Days
                            $("#min_day_calc").html(parseInt(days));
                            // Min Cost
                            $("#min_cost_calc").html(parseFloat((g/(min_day*h))).toFixed(2));
                            
                            
                            // Do we have a saving.
                            var c = parseFloat((b*h).toFixed(2));
                            
                            if(c > g){
                                //$("p.save").html("SAVE £"+parseFloat((c-g)).toFixed(2));
                            }else{
                                //$("p.save").html("");
                            }
                        }, "json");
                    }*/
                    get_data();
                });
                
                $(".repeat a").click(function(e){
                    e.preventDefault();
                    $(".repeat a").removeClass("active");
                    $(this).addClass("active");
                    $(".nub").html($(this).attr("data-id"));
                    $("input[name=auto]").val($(this).attr("data-tag"));
                    $("input[name=auto_percent]").val($(this).attr("data-percent"));
                    get_data()
                })
                $("a.more").click(function(e){
                    e.preventDefault();
                    var target_offset = $("#more").offset();
                    var target_top = target_offset.top;
                    setTimeout(function(){
                        $('html, body').animate({scrollTop:(target_top-120)}, 800);
                    },500);   
                })
                
            })    
        </script>
        <?
    break;
        
    case "/results.php":
        ?>
        <script type="text/javascript">
            $(window).bind("load", function(){
                setTimeout(function(){
                    $("#loader").fadeOut();
                },4500);
            })    
        </script>

        <?        
    break;
        
    case "/summary.php":
        ?>
        <script type="text/javascript">
            
            $(document).ready(function(){
               /*$("a.process_order").click(function(e){
                   e.preventDefault();
                   $("form[name=proceed]").submit();
               }) */
            }); 
        </script>
        <?
    break;
        
    case "/range.php":
        ?>
        <script type="text/javascript">
            $(".product_info").hide(); 
            /*$(".products .c_25").hover(function(){
                $(this).find(".product_info").fadeIn();    
            },function(){
                $(this).find(".product_info").fadeOut();   
            })*/
        </script>
        <?
    break;
        
    case "/account-page.php":
        ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.2.0/zxcvbn.js"></script>
        <script type="text/javascript">
            
                var strength = {
                        0: "Worst",
                        1: "Bad",
                        2: "Weak",
                        3: "Good",
                        4: "Strong"
                }

                var password = document.getElementById('new_password');
                var meter = document.getElementById('password-strength-meter');
                var text = document.getElementById('password-strength-text');

                $('input[name=new_password]').keyup(function(){
                
                    var val = password.value;
                    var result = zxcvbn(val);

                    // Update the password strength meter
                    meter.value = result.score;

                    // Update the text indicator
                    if(val !== "") {
                        if(strength[result.score] == "Good" || strength[result.score] == "Strong"){
                            $('.profile button').prop('disabled', false);
                        } else {
                            $('.profile button').prop('disabled', true);
                        }
                        text.innerHTML = "Strength: " + "<strong>" + strength[result.score] + "</strong>" + "<span class='feedback'>" + result.feedback.warning + " " + result.feedback.suggestions + "</span"; 
                    }
                    else {
                        $('.profile button').prop('disabled', true);
                        text.innerHTML = "";
                    }
                });
             
        </script>
        <?
    break;
}
?>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#000000;"
    },
    "button": {
      "background": "#000000",
      "text": "#fff"
    }
  },
	"theme": "edgeless",
  
  "content": {
    "dismiss": "Accept &amp; Close",
    "link": "More Information",
    "href": "/privacy-policy"
  }
})});
</script>
<script type="text/javascript" src="https://api.feefo.com/api/javascript/ooddles-kitchen-co-uk" async></script>
