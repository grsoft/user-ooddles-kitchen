<div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1><? echo $xx->alias; ?></h1>
        <div class="c_75"><p><? echo $xx->introduction; ?></p></div>
        
    </div><!--close headline-->
    
</div><!--close banner-->

    <section>
       <?
        if(!empty($breadcrumbs)){
            $x=1;
            
            echo "<div id='breadcrumbs'>
                  <ul class='flex' itemscope itemtype='https://schema.org/BreadcrumbList'>";
            
            foreach($breadcrumbs as $b => $v){
                echo "<li itemprop='itemListElement' itemscope itemtype='https://schema.org/ListItem'>
                      <a href='{$v[link]}' title='{$v[name]}' itemprop='item'>
                      <meta itemprop='name' content='{$v[name]}' />
                      <meta itemprop='position' content='{$x}' />
                      {$v[name]}</a></li><li>&rang;</li>";
                $x++;
            }
            
            echo "</ul>
                  </div>";
        }
        ?>
</section>
<section class="blue_bg">
    <?
        // Get all subcategories attached to the category
            $ss = $db->prepare("SELECT * FROM subcategories WHERE status = ?");
            $ss->execute(array("Published"));
            if($ss->rowCount() > 0){
                echo "<div class='flex negative range_wrap'>";
                     while($r = $ss->fetchObject()){
                         echo "<div class='c_25'>
                                    <div class='inner'>
                                    <h2><a href='/{$xx->seo}/{$r->seo}' title='{$r->title}'>{$r->title}</a></h2>
                                    </div>
                                </div>";
                     }
                echo "</div>";
            }
        ?>
        
    </section>
<section>
    
    <? echo $xx->description; ?>
</section>
    