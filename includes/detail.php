<section class="blue_bg product light_blue">
        <div class="flex">
            <div class="c_50">
                  <? if(!empty($xx->image)){ 
                        echo "<div class='main_img'><img src='/images/products/{$xx->image}' alt='{$xx->title}' />";
                        if($xx->category_id == 1 && !empty($pet->name)){
                            echo "<div class='sticker'><img src='/images/bag-label.png' alt='Bag Label' /><span>{$pet->name}</span></div>";
                            
                        }
                        echo "</div>";
                    }
                    $sb = $db->prepare("SELECT b.title, b.image FROM product_benefit p LEFT JOIN benefits b ON p.benefit_id = b.id WHERE product_id = ?"); 
                    $sb->execute(array($xx->id));
                    if($sb->rowCount() > 0){
                ?>
                    <h5>Key Benefits</h5>
                    <div class="flex benefits">
                        <?
                            while($b = $sb->fetchObject()){
                                echo "<div class='c_15'><img src='/images/{$b->image}' title='{$b->title}' /></div>";
                            }
                        ?>
                    </div>
                <?  } ?>
                
            </div>
            
            <div class="c_50">
                <div class="details">
                    <h1><? echo $xx->title; ?></h1>
                    <p><? echo $xx->introduction; ?></p>
                    <a href="#" title="Read More About <? echo $xx->title; ?>" class="bold">Read More</a>
                    
                    
                    
                    <div class="purchase">
                        <div class="flex">
                            <?
                                $ss = $db->prepare("SELECT * FROM skus WHERE product_id = ? AND status = ?");
                                $ss->execute(array($xx->id, "Published"));
                                if($ss->rowCount() == 1){
                                    $s = $ss->fetchObject();
                                    ?>
                                    
                                    <div class="c_66">
                                        <span id="product"><? echo $s->id; ?></span>
                                        <h5 class="nub">One off order</h5>
                                        <!--<p class="bubble"><? //echo $s->weight.strtolower($s->unit); ?></p>-->
                                        <p class="price">&pound;<span><? echo number_format($s->base_price,2); ?></span></p><span id="base"><? echo $s->base_price; ?></span>
                                        <span id="weight"><? echo $s->weight; ?></span>
                                    <span id="min_days"><? echo round(floor((($s->weight*1000)/$per_day))); ?></span>
                                        <span id="min_cost"><? echo number_format(($s->base_price/round(floor((($s->weight*1000)/$per_day)))),2); ?></span>
                                        <span id="me"> Me Per 100G: <? echo $xx->me; ?> / Per KG: <? echo $carolie_per_kg; ?></span>
                                        <span id="grams"> Grams Per Day: <? echo $per_day; ?></span>
                                    </div>
                                    
                                    <div class="c_33">
                                        <div class="qty">
                                        <h5>Quantity</h5>
                                        <?
                                            if($s->stock > 0){
                                                ?>
                                        <div class="flex">
                                            <div><span class="nug" data-id="minus">-</span></div>
                                            <div>&nbsp;&nbsp;<span id="qty">0</span>&nbsp;&nbsp;</div>
                                            <div><span class="nug" data-id="plus">+</span></div>
                                        </div>
                                    <? }else{
                                            echo "Out of stock";
                                        }
                                    ?>
                                    </div><!--close qty-->
                                        </div>
                                    
                                    <div class="c_33"></div>
                                    <div class="c_100 breaker"></div>
                                    <div class="c_100">
                                        <h5>Subscribe and save</h5>
                                        <p>Best savings and never run out. Easy to cancel in your account.</p>
                                        
                                        <div class="flex negative repeat">
                                            <div class="c_33"><div class="inner"><a href="#" title="No Thanks" data-id="One off order" data-tag="">No Thanks</a></div></div>
                                            <div class="c_33"><div class="inner"><a href="#" title="Monthly" data-id="Auto Repeat Monthly" data-tag="monthly">Monthly</a></div></div>
                                            <div class="c_33"><div class="inner"><a href="#" title="Bi-Monthly" data-id="Auto Repeat Bi-Monthly" data-tag="bi-monthly">Bi-Monthly</a></div></div>
                                        </div>
                                    </div>
                                    <div class="c_100 breaker"></div>
                                    <div class="c_100"><p><form name="purchase" method="post" action="/actions.php?action=add-to-basket">
                                        <input name="auto" type="hidden" />
                                        <input name="sku" type="hidden" value="<? echo $s->id; ?>" />
                                        <input name="qty" type="hidden" />
                                        <input name="url" type="hidden" value="<? echo $_SERVER['REQUEST_URI']; ?>" />
                                        
                                        <input name="submit" type="submit" value="Add To Basket" /></p></div>
                            
                                    
                                   
                            <? if(!empty($pet->name)){
                                        
                                        
                                        ?><div class="c_100 breaker"></div>
                                            <div class="c_100">
                                        <h2><? echo $pet->name; ?>'s Feeding and Costs</h2>
                                        <p>The total weight of your order is <strong><span id="total_weight"><? echo $s->weight.strtolower($s->unit); ?></span></strong>. <br />
                                            Based on <strong><? echo $pet->name; ?>'s</strong> age, weight of <strong><? echo $pet->weight; ?>kg</strong> and activity level, 
                                            <? if($pet->gender == "Male"){ echo "he"; }else{ echo "she"; } ?> should be eating <strong><? echo $der; ?></strong> calories (approximately <strong><? echo $per_day; ?>g</strong>) of <? echo $xx->title; ?> per day. </p>
                                        <p>Your <? echo $xx->title; ?> order will last <strong><span id="min_day_calc"><? echo round(floor((($s->weight*1000)/$per_day))); ?></span> days</strong> at a cost of  <strong>&pound;<span id="min_cost_calc"><? echo number_format(($s->base_price/round(floor((($s->weight*1000)/$per_day)))),2); ?></span></strong> per day.</p>
                                    </div>
                        <? } ?>    
                                    <div class="c_100 breaker"></div>
                                    <div class="c_100 disclaimer">
                                        <p>Trade: Apply your Trade Code at checkout for your discount to be applied.</p>
                                        <p>Promo Code: If you have a promo code, please add at the checkout stage.</p>
                                        <p>Free DPD Delivery, small cost for Highlands and Channel Islands, this will show at the checkout.</p>
                                        </div><!--close c_100-->
                                    <?
                                }
                                
                            ?>
                            
                          
                            
                            
                        </div>
                        
                    </div><!--close purchase-->
                    
                    
                </div><!--close details-->
                
                
            </div>
            
        </div><!--close flex-->
        
    </section>
    <section>
      
        <ul class="flex tabs">
            <li class="active"><a href="#" title="<? echo $xx->title; ?> Key Benefits" data-id="description">Key Benefits</a></li>
            <li><a href="#" title="<? echo $xx->title; ?> Composition" data-id="composition">Composition</a></li>
            <li><a href="#" title="<? echo $xx->title; ?> Analytical Constituents" data-id="nutrition">Analytical Constituents</a></li>
            <li><a href="#" title="<? echo $xx->title; ?> Feeding Guide" data-id="guide">Feeding Guide</a></li>
        </ul>
        <div id="info">
            <div class="sector" id="description">
                <? echo $xx->description; ?>
            </div><!--close sector-->
            
            <div class="sector" id="composition">
                <div class="flex">
                    <div class="c_100">
                        <h3>The Superfoods</h3>
                    <?
                        // Are we linked to food?
                        $sf = $db->prepare("SELECT f.title, f.description FROM product_food p LEFT JOIN foods f ON p.food_id = f.id WHERE p.product_id = ? ORDER BY f.title ASC");
                        $sf->execute(array($xx->id));
                        if($sf->rowCount() > 0){
                            echo "<div class='flex ingredients'>";
                            while($f = $sf->fetchObject()){
                                echo "<div class='c_80'><h4>{$f->title}</h4>{$f->description}</div>";
                            }
                            echo "</div>";
                        }
                    ?></div>
                    <div class="c_100"><h3>Ingredients</h3><? echo $xx->composition; ?></div>
                </div>
            </div><!--close sector-->
            
           
            
            <div class="sector" id="nutrition">
                <div class="flex">
                    <div class="c_50"><? echo $xx->constituents; ?></div>
                    <div class="c_50"><? echo $xx->nutrition; ?></div>
                </div>
            </div><!--close sector-->
            
            <div class="sector" id="guide">
                <div class="flex">
                    <div class="c_50">
                        <div class="c_75">
                        <?
                        if(!empty($xx->feeding)){
                            echo str_replace("oodles.brainstormdevelopment", "www.ooddleskitchen", $xx->feeding);
                        }
                        ?>
                </div>
                    </div>
                    <div class="c_50">
                        <h3>Additional Information</h3>
                        <p>All dogs are different and the guidelines should be adapted to take into account breed, age, temperament and activity level of the individual dog. When changing foods please introduce gradually over a period of two weeks. Always ensure fresh, clean water is available.</p>
                    </div>
                </div><!--close flex-->
            </div><!--close sector-->
            
            
            
            
        </div><!--close info-->
        
        
        
    </section>