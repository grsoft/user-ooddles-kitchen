<section>
              <div class="introduction">
                <h1>Login to your O Hub</h1>
                <form name="login" method="post" action="/actions.php?action=login">
                    <input name="referral" type="hidden" value="<? echo $_SERVER['REQUEST_URI']; ?>" />
                    <div class="flex">
                        <div class="c_100">
                            <label>Email Address</label>
                            <input name="email" type="text" placeholder="Required" /></div>
                        
                        <div class="c_100">
                            <label>Password</label>
                            <input name="password" type="password" placeholder="Required" /></div>
                        
                        <div class="c_100"><input name="submit" type="submit" value="Login" /></div>
                    </div>
                </form>
            
                <h3>Forgot password?</h3>
                <p>If you need to reset your password, please <a href="#" title="Reset Password" class="forgot">click here</a>.</p>
                  
                  <form name="password" method="post" action="/actions.php?action=password">
                      <input name="url" type="hidden" value="<? echo $_SERVER['REQUEST_URI']; ?>" />
                         <p>Enter your email address and we'll send you an email with a link to reset your password. <strong>Please check your spam folder</strong>.</p>
                      <input name="email" type="text" placeholder="Required" />
                      <p><input name="submit" type="submit" value="Reset" /></p>
                    </form>
                  </div><!--close introduction-->
        
                </section>