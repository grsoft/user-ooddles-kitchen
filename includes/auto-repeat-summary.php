<?

echo "<section>";

                ?>
                 <div class="flex negative">
                  <div class="c_50"><div class="inner">
                      <p class="total">Your Auto Repeat Order: <span>&pound;<? echo number_format($xx->total,2); ?></span></p>
                      
                      <p><? if(!empty($xx->name)){ echo "<strong>For:</strong> {$xx->name} <br />"; } ?>
                          <strong>Frequency:</strong> <? echo ucwords($xx->frequency); ?><br />
                          <strong>Next Due:</strong> <? echo date("D jS M, Y", strtotime($xx->next_due)); ?><br />
                          <strong>Status:</strong> <? echo $xx->status; ?></p>


                      <div class="flex negative summary">
                          

                      <div class="c_50"><div class="inner">
                      <h3>Shipping Details</h3>
                      <p><strong><? echo $delivery->d_name." ".$delivery->d_surname; ?></strong><br />
                          <? echo $delivery_address; ?></p>
                      <p><strong>Telephone:</strong> <? echo $delivery->d_telephone; ?><br />
                                      <strong>Email:</strong> <? echo $delivery->d_email; ?></p>
                      </div></div>
                          </div>

 

                      </div></div>
                      <div class="c_50"><div class="inner">
                           <table class="table-responsive-full order_table">
                    <thead>
                      <tr>
                        <th>Product</th>
                        <th>Description</th>
                        <th>Unit Price</th>
                          <th>Quantity</th>
                          <th>Subtotal</th>
                      </tr>
                    </thead>
                    <tbody>
                        <? 
                              $sd = $db->prepare("SELECT s.title, c.price, SUM(c.price) AS subtotal, c.quantity, p.title AS product_title, i.category, p.image, s.quantity, s.weight, s.unit
                                                  FROM auto_repeat_items c
                                                  LEFT JOIN auto_repeat t ON c.auto_id = t.id
                                                  LEFT JOIN skus s ON c.sku_id = s.id
                                                  LEFT JOIN products p ON s.product_id = p.id
                                                  LEFT JOIN categories i ON p.category_id = i.id
                                                  LEFT JOIN orders o ON t.order_id = o.id
                                                  WHERE c.auto_id = ?");
                              $sd->execute(array($xx->id));
                              while($d = $sd->fetchObject()){
                                  echo "<tr>
                                        <td>";
                                  if(!empty($d->image)){
                                      echo "<img src='/thumb.php?src=/images/products/{$d->image}&w=80&h=80&zc=2' alt='{$d->title}' />";
                                  }
                                  echo "</td>
                                        <td><strong>{$d->product_title}</strong><br />
                                        {$d->quantity} x {$d->weight}{$d->unit} {$d->title}</td>
                                        <td>&pound;".number_format($d->price,2)."</td>
                                        <td>{$d->quantity}</td>
                                        <td>&pound;".number_format($d->subtotal,2);
                                  if(!empty($d->auto_repeat)){
                                      echo "<br /><span class='repeat'>Auto Repeat {$d->auto_repeat}</span>";
                                  }
                                  echo "</td>
                                        </tr>";
                              }
                          ?>
                    </tbody>
                </table>
                          <?
                          switch($xx->status){
                              case "Active":
                                  ?>
                                  <p><a href="/o-hub/confirm/cancel-auto-repeat/<? echo $xx->id; ?>" title="Cancel Auto Repeat" class="btn brown check cancel">Cancel Auto Repeat</a></p>
                                  <?
                              break;
                              case "Inactive":
                                  ?>
                                  <p><a href="/o-hub/confirm/activate-auto-repeat/<? echo $xx->id; ?>" title="Activate Auto Repeat" class="btn brown check cancel">Activate Auto Repeat</a></p>
                                  <?
                              break;
                          }
                          ?>
                          
                         
                           
                          </div></div>
                  </div>
    <?            
    echo "</section>";
?>