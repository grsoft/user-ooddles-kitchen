<?
$imgs = array("header-lucy.jpg", "header-charlie.jpg");
    shuffle($imgs);
    
    $banners = array(array("image-treats.jpg", "shop"));
    shuffle($banners);
    
    // Mail header and footer
    $mail_header = "<body bgcolor='#efefef'>
                    <table cellpadding='0' cellspacing='0' width='650' align='center' style='background-color:#fff; font-family: Arial, Gill Sans, Geneva, Museo Sans, museo sans, museo-sans-rounded, Helvetica, Georgia, Arial, Helvetica, sans-serif; font-size:12px; padding:0px;'>
                    <tr>
                    <td style='padding:0px; text-align:center; background-color: #000; color: #ffffff; border-bottom:2px solid #fff;'><img src='".MAIN_SITE."/images/{$imgs[0]}?v=".rand()."' alt='Logo' width='100%' /></td>
                    </tr>";
    
    $mail_footer = "
                    </table>
                    <table cellpadding='0' cellspacing='0' width='650' align='center' style='background-color:#fff; font-family: Arial, Gill Sans, Geneva, Museo Sans, museo sans, museo-sans-rounded, Helvetica, Georgia, Arial, Helvetica, sans-serif; font-size:12px; padding:0px 0px; margin-top:0px;'>

                    <tr>
                    <td style='padding:30px 10px; text-align:center;border-top:2px solid #6c3727;'>

                    <p style='font-family: Arial, Gill Sans, Geneva, Museo Sans, museo sans, museo-sans-rounded, Helvetica, Georgia, Hevetica, Arial, sans-serif; font-size:11px;padding:0px; margin:0px; color:#6c3727;'><a href='".MAIN_SITE."' title='{$company->name}' style='text-decoration:none; font-size:16px; font-weight:700; color:#6c3727;'>www.ooddleskitchen.co.uk</a><br /><span style='text-decoration:none; font-size:16px; font-weight:700; color:#6c3727;'>Follow us</span><br />
                    <a href='{$company->facebook}' title='Facebook'><img src='".MAIN_SITE."/images/icon-facebook.png' width='25' /></a> &nbsp;&nbsp; <a href='{$company->instagram}' title='Instagram'><img src='".MAIN_SITE."/images/icon-instagram.png' width='25' /></a> &nbsp;&nbsp; <a href='{$company->pinterest}' title='Pinterest'><img src='".MAIN_SITE."/images/icon-pinterest.png' width='25' /></a><br /><br />
                    Ooddles Kitchen, Swanley Lane, Burland, Nantwich, Cheshire CW5 8BQ<br />
                    Email: <a href='mailto:{$company->email}' title='Email {$company->name}' style='color:#6c3727;'>{$company->email}</a><br />
                    Company Number: 11266020 &nbsp;&nbsp;&nbsp;&nbsp; Vat Number: 293067192</p></td>
                    </tr>
                    </table>
                    </body>";
?>