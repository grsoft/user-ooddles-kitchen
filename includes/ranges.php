<section class="blue_bg centre">
        <h2>Dog Food Made With Love</h2>
        <div class="flex negative options">
            <div class="c_33">
                <div class="inner">
                    <div class="block">
                        
                        <img src="<? echo $sirv; ?>/images/first.png?w=500&h=500" alt="Recipes" class="Sirv" />
                    </div>
                   
                    <ul>
                        <li> Free DPD Delivery, with tracking, and confirmed
                            1 hour delivery slot.(orders over &pound;50) </li>
<li>95% of Orders Arrive Next Day.</li>
<li> Customer Services Until 10pm 7 days a week</li>
<li>Ooddles Social - Competitions, Giveaways,
    Freebies, Spin To Win. Come Dine With
    Ooddles. Join In.</li>
<li>Auto Repeat Orders Optional. Enjoy extra
    discounts & never forget to order your food.</li>

                    </ul>
<p style="text-align: left; font-size:13px">*Small surcharge for highlands and Channel Islands</p>
                    
                </div>
            </div>
            <div class="c_33">
			<div class="inner">
			 <div class="block">
                    <a title="" class="btn brown centre">Two Ways To Order</a>
                    </div>

                    <div class="c_100 " id="middle_img">
                    <div class="flex middle_image">
                            <!-- left side  -->
                            <div class="middile-width">
                            <div class="inner">
                                        <div class="image_center">
                                        <img src="/images/pets/imgpsh_1.png"  alt="">
                                        </div>
                                        <h1>Not quite <br> sure which <br> food? Take <br> Our Wizard</h1>
                                        <div class="image_center">
                                        <img src="/images/pets/imgpsh_1.png"   alt="">
                                        </div>
                                        <a href="/get-ooddling" title="Read More" class="middle_button brown centre btn" data-id="more_about" style="margin-top: 8px";>Start<br> Ooddling</a>
                                        <div class="image_center">
                                        <img src="images/pets/imgpsh_1.png"   alt="">
                                        </div>
                            </div>
                            </div>
                
                       <!-- right side  -->
                            <div class="middile-width">
                            <div class="inner ">
                                        <div class="image_center">
                                        <img src="images/pets/imgpsh_1.png"   alt="">
                                        </div>
                                        <h1>Know what<br> you want <br>or just <br>browsing</h1>
                                        <div class="image_center">
                                        <img src="images/pets/imgpsh_1.png"   alt="">
                                        </div>
                                        <a href="/shop" title="Read More" class="middle_button brown centre btn"  data-id="more_about"
                                         style="margin-top: 8px";>  Shop<br> Now</a>
                                        <div class="image_center">
                                        <img src="images/pets/imgpsh_1.png"   alt="">
                                        </div>   
                            </div>
                            </div>
               
                    </div>
                    <span class="middle-text">
                        <h1 class="centre middle_heading">Order whatever you would<br> like or put on AUTO REPEAT <br>for Great Savings</h1>
                        </span>

                    <div class="block Login_button">
                            <a href="/o-hub"  class="btn  brown centre">Already Ooddling <br>Log in Here</a>
                            </div> 
                            
                    </div>
                
                </div></div>
            <!--<div class="c_25"><div class="inner">
                    <div class="block">
                        <img src="/images/flea-worming.jpg" alt="Flea and Wormer Subscriptions" /></div>
                <ul class="opts">
                    <?
                    $sc = $db->prepare("SELECT * FROM categories WHERE status = ? AND id >= ? ORDER BY id ASC");
                    $sc->execute(array("Published", 6));
                    while($c = $sc->fetchObject()){
                        //echo "<li><a href='/our-foods/{$c->seo}' title='{$c->category}'>{$c->category}</a></li>";
                    }
                    
                    ?>
                    
                    </ul>
                
                </div></div>-->
            <div class="c_33"><div class="inner">
                    <div class="block">
                        <img src="<? echo $sirv; ?>/images/last.png?w=500&h=500" alt="Guides" class="Sirv"  /></div>
                <ul>
                    <li> No matter which way you Ooddle, our foods
                        are grain-free, natural and packed full of
                        goodness.</li>
<li><strong>Dry</strong>- Ooddles Dry is as good as feeding raw.
    80/20 range, superfoods range and lots more.</li>
<li><strong>Wet</strong> - Complete food, with a high meat
    content. Cannageenan free.</li>
<li> <strong>Raw</strong>  - If you are a raw lover, Go bare with
    Ooddles, our complete Raw Meals with added
    superfoods.</li>
<li> Personalised and delivered.</li>
                </ul>
                

            </div></div>
            
            
        </div><!--close flex-->
    
        
    </section><!--close section-->
<!--<p class="start centre"><br /><a href="/get-ooddling" title="Start Ooddling" class="btn brown">Not sure which food or need a cost per day? Start Ooddling HERE</a><br /><br /></p>-->