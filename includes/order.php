<?
if($_GET['area'] == "orders"){
    echo "<section>";
}
// Get the customer
        $st = $db->prepare("SELECT * FROM customers WHERE id = ?");
        $st->execute(array($xx->customer_id));
        $customer = $st->fetchObject();
        
        $address = $customer->address.", ";
        if(!empty($cust->town)){
          $address .= $customer->town.", ";
        }
        $address .= $customer->city.", <br />".$customer->county." ".$customer->postcode."<br />".$customer->country;
        
        $st = $db->prepare("SELECT * FROM deliveries WHERE order_id = ?");
        $st->execute(array($xx->id));
        $delivery = $st->fetchObject();

    // The delivery address
        $delivery_address = $delivery->d_address.", ";
        if(!empty($delivery->d_address2)){
            $delivery_address .= $delivery->d_address2.", ";
        }
        $delivery_address .= $delivery->d_city.", <br />".$delivery->d_county." ".strtoupper($delivery->d_postcode)."<br />".$delivery->d_country;
        
        switch($xx->status){
                
            case "Unpaid":
                ?>
                <div class="flex empty">
                <div class="c_33">
                </div>
                <div class="c_66">
                    <div class="inner">
                    <h3>What's going on?</h3>
                    <p>Your order appears to be <strong>awaiting payment</strong>. <br />
                        Sometimes this is just down to a delay with the payment gateway sending back notification. <br />
                        Please wait a moment and refresh the page to see if it has come through.<br />
                    Alternatively, you can email us on <a href="mailto:<? echo $company->email; ?>" title="Email <? echo $company->name; ?>"><? echo $company->email; ?></a> and we'll be happy to assist you.</p>
                    </div>
                    </div>
                </div>
        <?   
            break;
            case "Paid":
                $trans = array('id' => $_GET['id'], 'affiliation' => $company->name, 'revenue' => $xx->total, 'shipping' => $xx->shipping, 'tax' => 0);
                
                ?>
               
                
  <div class="flex negative">
                  <div class="c_33"><div class="inner">
                      <p class="total">Your Order: <span>&pound;<? echo number_format(($xx->total-$xx->discount_total+$xx->shipping),2); ?></span><br />
                          Status: <span><? echo $xx->status; ?></span></p>


                      

                      </div></div>
      <div class="c_33"><div class="inner">
          <div class="flex negative summary">
                          <div class="c_100"><div class="inner">
                              <h3>Billing Details</h3>
                               <p><strong><? echo $customer->name." ".$customer->surname; ?></strong><br />
                                   <? echo $address; ?></p>
                              <p><strong>Telephone:</strong> <? echo $customer->telephone; ?><br />
                                              <strong>Email:</strong> <? echo $customer->email; ?></p>
                          </div></div>

                      
                          </div>
          </div></div>
      <div class="c_33"><div class="inner">
          <div class="flex negative summary">
                          <div class="c_100"><div class="inner">
                              <h3>Shipping Details</h3>
                      <p><strong><? echo $delivery->d_name." ".$delivery->d_surname; ?></strong><br />
                          <? echo $delivery_address; ?></p>
                      <p><strong>Telephone:</strong> <? echo $delivery->d_telephone; ?><br />
                                      <strong>Email:</strong> <? echo $delivery->d_email; ?></p>
                      </div></div>
                          </div>
          </div></div>
      
                      <div class="c_100"><div class="inner">
                           <table class="table-responsive-full order_table">
                    <thead>
                      <tr>
                        <th>Product</th>
                        <th>Description</th>
                        <th>Unit Price</th>
                          <th>Quantity</th>
                          <th>Subtotal</th>
                      </tr>
                    </thead>
                    <tbody>
                        <? 
                              $sd = $db->prepare("SELECT s.title, c.price, SUM(c.price) AS subtotal, COUNT(c.id) AS qty, p.title AS product_title, i.category, p.image, c.auto_repeat, s.quantity, s.weight, s.unit, c.discount, SUM(c.discount) AS overall_discount, s.product_code, c.price
                                                  FROM cart c
                                                  LEFT JOIN skus s ON c.sku_id = s.id
                                                  LEFT JOIN products p ON s.product_id = p.id
                                                  LEFT JOIN categories i ON p.category_id = i.id
                                                  LEFT JOIN orders o ON c.order_id = o.id
                                                  WHERE o.unique_id = ? AND o.status = ?
                                                  GROUP BY c.sku_id, c.auto_repeat");
                              $sd->execute(array($_GET['id'], $xx->status));
                              while($d = $sd->fetchObject()){
                                  
                                  $items[] = array('sku' => $d->product_code, 'name' => $d->title, 'category' => $d->category, 'price' => $d->price, 'quantity' => $d->qty);
		                          $gtins[] = '{"gtin":"'.$d->product_code.'"}';
                                  
                                  echo "<tr>
                                        <td data-label='Product'>";
                                  if(!empty($d->image)){
                                      echo "<img src='/thumb.php?src=/images/products/{$d->image}&w=80&h=80&zc=2' alt='{$d->title}' />";
                                  }
                                  echo "</td>
                                        <td data-label='Description'><strong>{$d->product_title}</strong><br />
                                        {$d->quantity} x {$d->weight}{$d->unit} {$d->title}</td>
                                        <td data-label='Price'>&pound;".number_format($d->price,2)."</td>
                                        <td data-label='Quantity'>{$d->qty}</td>
                                        <td data-label='Total'>";
                                  if($d->overall_discount > 0){
                                     echo "&pound;".number_format(($d->subtotal-$d->overall_discount),2)." <span class='linethrough'>&pound;".number_format($d->subtotal,2)."</span>"; 
                                  }else{
                                      echo "&pound;".number_format($d->subtotal,2);    
                                  }
                                  if(!empty($d->auto_repeat)){
                                      echo "<br /><span class='repeat'>Auto Repeat {$d->auto_repeat}</span>";
                                  }
                                  echo "</td>
                                        </tr>";
                              }
                          ?>
                    </tbody>
                </table>
                          <div class="totals">
                              <div class="flex negative">
                                  <div class="c_33"><div class="inner">
                              <p><strong>Subtotal:</strong> &pound;<? echo number_format(($xx->total-$xx->discount),2); ?><br />
                                  <strong>Discount:</strong> &pound;<? echo number_format($xx->discount_total,2); ?><br />
                                      <strong>Delivery:</strong> &pound;<? echo number_format($xx->shipping,2); ?><br />
                                      <strong>Total:</strong> &pound;<? echo number_format(($xx->total-$xx->discount_total+$xx->shipping),2); ?></p>
                                      </div></div>
                                  <div class="c_66"><div class="inner"><div class="help">
                              <p>If you need any help or assistance with your order, please email us on <a href="mailto:<? echo $company->email; ?>" title="Email <? echo $company->name; ?>"><? echo $company->email; ?></a>.</p>

                          </div><!--close help--></div></div>
                              </div>
                          </div><!--close totals-->
                          
                          <?php
                if($xx->tracked == "0000-00-00 00:00:00"){
                    print '<!-- https://www.staging.ooddleskitchen.co.uk/order/10099TI  --!>';
                    print '<!-- https://www.staging.ooddleskitchen.co.uk/order/31061RS  --!>';
                    ?>
                         <script>
                              ga('require', 'ecommerce');
                          <?php
                              echo getTransactionJs($trans);

                              foreach ($items as &$item) {
                                echo getItemJs($trans['id'], $item);
                              }
                              
                          ?>
                              ga('ecommerce:send');
                              <? 
                              // Update the order as tracked.
                              $st = $db->prepare("UPDATE orders SET tracked = ? WHERE unique_id = ?");
                              $st->execute(array(date("Y-m-d H:i:s"), $_GET['id']));
                              ?>
                          </script>
                          <? } ?>  
                          </div></div>
                  </div>
<?
               
            break;
                
            
                
        }
?>

<?php
    print '<!-- sharad -->'."\r\n";
    $sd = $db->prepare("SELECT s.title, c.price, SUM(c.price) AS subtotal, COUNT(c.id) AS qty, p.title AS product_title, i.category, p.image, c.auto_repeat, s.quantity, s.weight, s.unit, c.discount, SUM(c.discount) AS overall_discount, s.product_code, c.price
    FROM cart c
    LEFT JOIN skus s ON c.sku_id = s.id
    LEFT JOIN products p ON s.product_id = p.id
    LEFT JOIN categories i ON p.category_id = i.id
    LEFT JOIN orders o ON c.order_id = o.id
    WHERE o.unique_id = ? AND o.status = ?
    GROUP BY c.sku_id, c.auto_repeat");
    $sd->execute(array($_GET['id'], $xx->status));
    if($sd->rowCount() > 0){
        $purDtAray = [];
        $totalValue = 0;
        while($d = $sd->fetchObject()){
            $purDtAray[] = [
                'id' => $d->product_code,
                'name' => $d->title,
                'list_name' => $d->product_title,
                'brand' => 'Ooddles Kitchen',
                'category' => $d->category,
                'variant' => '',
                'list_position' => 1,
                'quantity' => $d->qty,
                "currency" => "GBP",
                'price' => $d->price
            ];
            $totalValue = $totalValue+($d->qty*$d->price);
        }

        print '<script>'."\r\n";
        print 'gtag(\'event\', \'purchase\', {
            "transaction_id": "'.$_GET['id'].'",
            "send_to": "G-WZG4K6HNT7",
            "currency": "GBP",
            "value": '.number_format($totalValue,2).',
            "items": '.json_encode($purDtAray,true).'
    });
    ga(\'purchase:send\');'."\r\n"; 
    print '</script>'."\r\n";   
    }




        

if($_GET['area'] == "orders"){
    echo "</section>";
}
?>


