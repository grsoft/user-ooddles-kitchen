<section class="footer">
        <div class="flex negative reverse">
            <div class="c_33"><div class="inner"><p>Contact us:<br />
                <a href="mailto:<? echo $company->email; ?>" title="Email <? echo $company->name; ?>">Ooddles Help</a><br />
                <a href="/frequently-asked-questions" title="<? echo $company->name; ?> FAQ">Frequently Asked Questions</a><br />
                <a href="/blog" title="Ooddles Blog">Ooddles Blog</a><br />
                <a href="/ooddles-business.php" title="Ooddles Business">Ooddles Business</a><br />
                <a href="https://www.feefo.com/en-GB/reviews/ooddles-kitchen-co-uk?displayFeedbackType=SERVICE&timeFrame=YEAR" title="Ooddles Reviews">Ooddles Reviews</a><br />
                <!-- <a href="/delivery-queries" title="<? //echo $company->name; ?> Delivery Queries">Delivery Queries</a><br />
                 <a href="/customer-services" title="<? //echo $company->name; ?> Customer Services">Customer Services</a><br />
                 <a href="/breeders-and-stockists" title="<? //echo $company->name; ?> Breeder Accounts &amp; Stockists">Breeder Accounts &amp; Stockists</a><br />
			 -->
                </div></div>
            
            
             <div class="c_33"><div class="inner">
             <p><img src="/images/feefo-banner.png" alt="Feefo" /></p>
                 <!--<p><a href="/breeders-and-stockists" title="<? //echo $company->name; ?> Breeder Accounts &amp; Stockists">Breeder Accounts &amp; Stockists</a><br /> -->
                 <!-- <a href="/frequently-asked-questions" title="<? //echo $company->name; ?> FAQ">Frequently Asked Questions</a><br />
                 <a href="/blog" title="Ooddles Woof Blog">Ooddles Woof Blog</a></p> -->
                 
                 
                </div></div>
            
            <div class="c_33"><div class="inner">
                 <p>Don't miss out on the latest news and offers</p>
                <p><a href="/sign-up" title="Signup" class="btn brown centre">Sign-up to our Ooddles Woof News</a></p>
                <h5 class="socials">Follow us 
                    <? 
                        if(!empty($company->facebook)){ echo "<a href='{$company->facebook}' title='Follow us on Facebook' target='_blank'><img src='/images/icon-facebook.png' alt='Facebook' /></a>"; } 
                        if(!empty($company->pinterest)){ echo "<a href='{$company->pinterest}' title='Follow us on Pinterest' target='_blank'><img src='/images/icon-pinterest.png' alt='Pinterest' /></a>"; } 
                        if(!empty($company->instagram)){ echo "<a href='{$company->instagram}' title='Follow us on Instagram' target='_blank'><img src='/images/icon-instagram.png' alt='Instagram' /></a>"; } 
                        if(!empty($company->twitter)){ echo "<a href='{$company->twitter}' title='Follow us on Twitter' target='_blank'><img src='/images/icon-twitter.png' alt='Twitter' /></a>"; } 
                    ?></h5>
                
                
                
                </div></div>
				
			<div class="c_66" style="background: #dfeff894; border-bottom: 8px solid #65be65;"><div class="inner">
			<p style="text-align: center">
				<u><a href="/privacy-policy"  title="<? echo $company->name; ?> Privacy Policy">Privacy Policy</a></u> &nbsp; | &nbsp;
                <u><a href="/cookie-policy" title="<? echo $company->name; ?> Cookie Policy">Cookie Policy</a></u> &nbsp; | &nbsp;
                <u><a href="/privacy-policy" title="<? echo $company->name; ?> Terms &amp; Conditions">Terms &amp; Conditions</a></u>
                <!--<u><a href="/modern-slavery" title="<? //echo $company->name; ?> Terms &amp; Conditions">Modern Slavery Statement</a></u></p> -->
				</div>
				</div>
            <div class="c_66"><div class="inner">
                <div class="flex">
                    <div class="c_40"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?>" class="logo" /></div>
                    <div class="c_15"><img src="/thumb.php?src=/images/logo-fediaf.png&w=400&h=400&zc=2" alt="Fediaf" /></div>
                    <div class="c_15"><a href="http://www.petcare.org.uk/" title="Pet Industry Federation" target="_blank"><img src="/thumb.php?src=/images/logo-pif.jpg&w=400&h=400&zc=2" alt="PIF" /></a></div>
                   <!-- <div class="c_15"><a href="https://www.pfma.org.uk/" title="Pet Food Manufacturers Association" target="_blank"><img src="/thumb.php?src=/images/logo-pfma.jpg&w=400&h=400&zc=2" alt="Pet Food Manufacturers Association" /><br /><span>Ooddles Partners Are PFMA Members.</span> </a></div>-->
                    
                </div>
                
                
                
                <p>Ooddles Kitchen is a registered trademark. Copyright 2018. <br />No content or images can be used without seeking written permission prior. 
                Technical Issues? Please <a href="mailto:<? echo $company->email; ?>?subject=Technical Issue" title="Report Here">Report Here</a></p></div></div>
                <div class="c_33"><div class="inner">
                <!-- <p><img src="images/feefo-banner.png" alt="Feefo" /></p> -->
                <p><img src="/images/payment-logos.jpg" alt="Payment Logos" width="200" /></p></div></div>
            
        
            
        </div><!--close flex-->
    
        <div itemscope itemtype="https://schema.org/LocalBusiness">
          <meta itemprop="name" content="<? echo $company->name; ?> Kitchen" />
          <meta itemprop="description" content="<? echo $company->meta_description; ?>" />
            <meta itemprop="telephone" content="<? echo $company->telephone; ?>" />
              <meta itemprop="email" content="<? echo $company->email; ?>" />
            <meta itemprop="url" content="<? echo MAIN_SITE; ?>" />
            <meta itemprop="image" content="<? echo MAIN_SITE; ?>/images/logo-horizontal.png" />
            <meta itemprop="sameAs" content="https://www.feefo.com/reviews/ooddles-kitchen-co-uk" />
          <div itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
            <meta itemprop="streetAddress" content="Swanley Lane" />
            <meta itemprop="addressLocality" content="Nantwich" />
            <meta itemprop="addressRegion" content="Cheshire" />
          </div>
              

        </div>
        <div itemscope itemtype="https://schema.org/Brand">
            <meta itemprop="name" content="<? echo $company->name; ?>" />
            <meta itemprop="image" content="<? echo MAIN_SITE; ?>/images/logo-horizontal.png" />
            <meta itemprop="slogan" content="<? echo $company->meta_title; ?>" />
        </div>
        
    </section>