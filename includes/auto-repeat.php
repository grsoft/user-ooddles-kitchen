<section>
    <div id="breadcrumbs">
        <ul class="flex">
            <li><a href="/o-hub" title="O Hub">O Hub</a></li>
            <li>&rang;</li>
            <li><a href="/o-hub/auto-repeat" title="Auto Repeat">Auto Repeat</a></li>
            
            
        </ul>
        </div>
                
    </section>
<img src="/images/ooddles-range.jpg" alt="Placeholder" class="placeholder" />
<section class="orders auto_repeat">
    <h2>Your current Auto Repeat summary</h2>
    <p>Here are your current auto repeat orders, detailing when they started and who they are for.</p>
    <table class="table-responsive-full">
      <thead>
        <tr>
          <th>Start</th>
          <th>Pet</th>
          <th>Total</th>
            <th>Frequency</th>
            <th>Next</th>
            <th>Status</th>
             <th class="actions">Action</th>
        </tr>
      </thead>
      <tbody>
          <?
            get_auto_repeat($db, array($customer->id, ""));
          ?>


      </tbody>
  </table>
    
</section>