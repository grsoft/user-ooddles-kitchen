 <section class="blue_bg product light_blue">
        <div class="flex">
            <div class="c_50">
                  <? if(!empty($xx->image)){ 
                        echo "<div class='main_img'><img src='/images/products/{$xx->image}' alt='{$xx->title}' />";
                        if($xx->category_id == 1 && !empty($pet->name)){
                            echo "<div class='sticker'><img src='/images/bag-label.png' alt='Bag Label' /><span>{$pet->name}</span></div>";
                            
                        }
                        echo "</div>";
                    }
                    $sb = $db->prepare("SELECT b.title, b.image FROM product_benefit p LEFT JOIN benefits b ON p.benefit_id = b.id WHERE product_id = ?"); 
                    $sb->execute(array($xx->id));
                    if($sb->rowCount() > 0){
                ?>
                    <h5>Key Benefits</h5>
                    <div class="flex benefits">
                        <?
                            while($b = $sb->fetchObject()){
                                echo "<div class='c_15'><img src='/images/{$b->image}' title='{$b->title}' /></div>";
                            }
                        ?>
                    </div>
                <?  }
                if($xx->category_id == 1 || $xx->category_id == 2 || $xx->category_id == 5){
                ?>
                
                <a href="/get-ooddling" title="Start Ooddling"><img data-src="<? echo $sirv; ?>/images/ooddles-the-wizard.jpg?w=850" alt="Blog" class="Sirv wizard" /></a>
                <? } ?>
                
            </div>
            
            <div class="c_50">
                <div class="details">
                    <h1><? echo $xx->title; ?></h1>
                    <p><? echo $xx->introduction;
                        
                        
                        ?></p>
                    <?
                    $ss = $db->prepare("SELECT * FROM skus WHERE product_id = ? AND status = ? AND sample = ?");
                        $ss->execute(array($xx->id, "Published", 1));
                        if($ss->rowCount() > 0){
                            echo "<h3>Sample packs are available for this product. Please select 3 x 100g below.</h3><br />";
                        }
                    ?>
                    <a href="#" title="Read More About <? echo $xx->title; ?>" class="bold more">Read More</a>
                        <?
                            $foods = explode(",", $pet->food_types);
                            //if(in_array(1,$foods) && in_array(2,$foods) && ($xx->category_id == 1 || $xx->category_id == 2)){
                                $notice = "<div class='mixed'>
                                    <h3>If You are Feeding Wet and Dry Mixed</h3>
                                 <p>We recommend you follow one of the below combinations.</p>
                                 <p>80% Dry and 20% Wet<br />
                                    50% Dry and 50% Wet</p>
 
                                <p>* Your dry food will last longer";
                                if($xx->category_id == 1){ $notice .= " than given above"; }
                                $notice .= "<br />
                                * Weigh 80% or 50% of the Dry food, and add the wet food.</p>
                                </div><!--close mixed-->";
                    
                            //}
                    
                    ?>
                    
                    
                    
                    
                    <div class="purchase">
                        <div class="flex">
                            <?
                                $ss = $db->prepare("SELECT * FROM skus WHERE product_id = ? AND status = ? ORDER BY sample ASC, base_price ASC");
                                $ss->execute(array($xx->id, "Published"));
                                $sku_count = $ss->rowCount();
                                if($sku_count == 1){
                                    $s = $ss->fetchObject();
                                    ?>
                                    
                                    <div class="c_66">
                                        <span id="product"><? echo $s->id; ?></span>
                                        <h5 class="nub">One off order</h5>
                                        <!--<p class="bubble"><? //echo $s->weight.strtolower($s->unit); ?></p>-->
                                        <p class="price">&pound;<span><? echo number_format($s->base_price,2); ?></span></p>
                                        <!--Base price:--> <span id="base"><? echo $s->base_price; ?></span>
                                        <!--Base Weight:--> <span id="weight"><? 
                                                    if($s->unit == "KG"){
                                                        $weight = (($s->weight*$s->quantity));
                                                        $sr_weight = (($s->weight*1000)*$s->quantity);
                                                    }else{
                                                        $weight = ($s->weight*$s->quantity);
                                                        $sr_weight = ($s->weight*$s->quantity);
                                                    } 
                                                    echo $weight; ?></span>
                                        <!--Min Days:--> <span id="min_days"><? echo round(floor(($weight/$per_day))); ?></span>
                                        <!--Min Cost:--> <span id="min_cost"><? echo number_format(($s->base_price/round(floor((($sr_weight*1000)/$per_day)))),2); ?></span>
                                        <span id="me"> Me Per 100G: <? echo $xx->me; ?> / Per KG: <? echo $carolie_per_kg; ?></span>
                                        <!--Grams Per Day:--> <span id="grams"><? echo $per_day; ?></span>
                                    </div>
                                    
                                    <div class="c_33">
                                        <div class="qty">
                                        <h5>Quantity</h5>
                                        <?
                                            if($s->stock > 0){
                                                ?>
                                        <div class="flex">
                                            <div><span class="nug" data-id="minus">-</span></div>
                                            <div>&nbsp;&nbsp;<span id="qty">1</span>&nbsp;&nbsp;</div>
                                            <div><span class="nug" data-id="plus">+</span></div>
                                        </div>
                                    <? }else{
                                            echo "Out of stock";
                                        }
                                    ?>
                                    </div><!--close qty-->
                                        </div>
                                    
                                    <div class="c_33"></div>
                                    <div class="c_100 breaker"></div>
                                    <div class="c_100">
                                        <h5>Subscribe and save <span id="saver"></span></h5>
                                        <p>Best savings and never run out. Easy to cancel in your account.</p>
                                        
                                        <div class="flex negative repeat">
                                            <div class="c_33"><div class="inner"><a href="#" title="No Thanks" data-id="One off order" data-tag="" data-percent="0">No<br />Thanks</a></div></div>
                                            <div class="c_33"><div class="inner"><a href="#" title="Monthly" data-id="Auto Repeat Monthly" data-tag="monthly" data-percent="<? echo $company->auto_repeat_saving; ?>">Auto Repeat<br />Monthly</a></div></div>
                                            <div class="c_33"><div class="inner"><a href="#" title="Bi-Monthly" data-id="Auto Repeat Bi-Monthly" data-tag="bi-monthly" data-percent="5.00">Auto Repeat<br />Bi-Monthly</a></div></div>
                                        </div>
                                    </div>
                                    <div class="c_100 breaker"></div>
                                    <div class="c_100"><p><form name="purchase" method="post" action="/actions.php?action=add-to-basket">
                                        <input name="auto" type="hidden" />
                                        <input name="auto_percent" type="hidden" />
                                        <input name="sku" type="hidden" value="<? echo $s->id; ?>" />
                                        <input name="qty" type="hidden" />
                                        <input name="url" type="hidden" value="<? echo $_SERVER['REQUEST_URI']; ?>" />
                                        <input name="pet_id" type="hidden" value="<? echo $pet->unique_id; ?>" />
                                        <input name="submit" type="submit" value="Add To Basket" /></p></div>
                            
                                    <?
                                }else{
                                    ?>
                                    <div class="c_45">
                                        <span id="product"></span>
                                        <h5>Description</h5>
                                        <!--<p class="bubble"><? //echo $s->weight.strtolower($s->unit); ?></p>-->
                                        
                                        <select name="product">
                                            <?
                                                while($s = $ss->fetchObject()){
                                                    if($s->unit == "KG"){
                                                        $weight = ((($s->weight*1000)*$s->quantity));
                                                        $sr_weight = ($s->weight*1000);
                                                    }else{
                                                        $weight = ($s->weight*$s->quantity);
                                                        $sr_weight = ($s->weight*$s->quantity);
                                                    }
                                                    echo "<option value='{$s->id}' data-id='{$s->base_price}' data-weight='".$weight."' data-sample='{$s->sample}'>{$s->title} ({$s->quantity} x ".number_format($s->weight,0).strtolower($s->unit).")</option>";
                                                }
                                    ?>
                                            
                                        </select>
                                        
                                    </div>
                                    <div class="c_20"><div class="saver"><h5>&nbsp;</h5>
                                        <p class="price">&pound;<span><? echo number_format($s->base_price,2); ?></span></p>
                                        
                                        <!--Base price:--> <span id="base"></span>
                                        <!--Base Weight:--> <span id="weight"></span>
                                        <!--Min Days:--> <span id="min_days"><? echo round(floor(($sr_weight/$per_day))); ?></span>
                                        <!--Min Cost:--> <span id="min_cost"><? echo number_format(($s->base_price/round(floor((($sr_weight)/$per_day)))),2); ?></span>
                                        <span id="me"> Me Per 100G: <? echo $xx->me; ?> / Per KG: <? echo $carolie_per_kg; ?></span>
                                        <!--Grams Per Day:--> <span id="grams"><? echo $per_day; ?></span>
                                        
                                        
                                        </div></div>
                                    <div class="c_33">
                                        <div class="qty">
                                        <h5>Quantity</h5>
                                        
                                        <div class="flex">
                                            <div><span class="nug" data-id="minus">-</span></div>
                                            <div>&nbsp;&nbsp;<span id="qty">1</span>&nbsp;&nbsp;</div>
                                            <div><span class="nug" data-id="plus">+</span></div>
                                        </div>
                                    
                                    </div><!--close qty-->
                                        </div>
                                    
                                    <div class="c_33"></div>
                                    <div class="c_100 breaker"></div>
                                    <div class="c_100">
                                        
                                        <h5>Subscribe and save <span id="saver"></span></h5>
                                        <p>Best savings and never run out. Easy to cancel in your account.</p>
                                        
                                        <div class="flex negative repeat">
                                            <div class="c_33"><div class="inner"><a href="#" title="No Thanks" data-id="One off order" data-tag="" data-percent="0">No<br />Thanks</a></div></div>
                                            <div class="c_33"><div class="inner"><a href="#" title="Monthly" data-id="Auto Repeat Monthly" data-tag="monthly" data-percent="<? echo $company->auto_repeat_saving; ?>">Auto Repeat<br />Monthly</a></div></div>
                                            <div class="c_33"><div class="inner"><a href="#" title="Bi-Monthly" data-id="Auto Repeat Bi-Monthly" data-tag="bi-monthly" data-percent="5.00">Auto Repeat<br />Bi-Monthly</a></div></div>
                                        </div>
                                    </div>
                                    <div class="c_100 breaker"></div>
                                    <div class="c_100"><p><form name="purchase" method="post" action="/actions.php?action=add-to-basket">
                                        <input name="auto" type="hidden" />
                                        <input name="auto_percent" type="hidden" />
                                        <input name="sku" type="hidden" value="<? echo $s->id; ?>" />
                                        <input name="qty" type="hidden" />
                                        <input name="url" type="hidden" value="<? echo $_SERVER['REQUEST_URI']; ?>" />
                                        <input name="pet_id" type="hidden" value="<? echo $pet->unique_id; ?>" />
                                        <input name="submit" type="submit" value="Add To Basket" /></p></div>
                            
                                    
                                   
                           <? } ?>
                            <? if(!empty($pet->name)){
                                        ?><div class="c_100 breaker"></div>
                                            <div class="c_100">
                                                <div id="costs">
                                        
                                            
                                            <?
                                            switch($xx->category_id){
                                                case 1: // Dry Food
                                                    if($months >= 12){
                                                    ?>
                                                        <h2><? echo $pet->name; ?>'s Feeding and Costs</h2>
                                                        <p>The total weight of your order is <strong><span id="total_weight"></span></strong>. <br />
                                                        Based on <strong><? echo $pet->name; ?>'s</strong> age, weight of <strong><? echo $pet->weight; ?>kg</strong> and activity level, 
                                                            <? if($pet->gender == "Male"){ echo "he"; }else{ echo "she"; } ?> should be eating <strong><? echo $der; ?></strong> calories per day (approximately <strong><span id="per_day"></span>g</strong>) of <strong><? echo $xx->title; ?></strong> per day. </p>
                                                        <p>Your <strong><? echo $xx->title; ?></strong> food will last <strong><span id="min_day_calc"></span> days</strong> at a cost of  <strong>&pound;<span id="min_cost_calc"></span></strong> per day.</p>
                                                        <? echo $notice; ?>
                                                        <p>Feeding guides are guides only. Based on the information you have provided us we give the average calories per day, please note you may need to adjust this based on your dog’s activity or weight changes and age.</p>
                                                        
                                                    <?
                                                    }else{
                                                        ?>
                                                    
                                                        <h2><? echo $pet->name; ?>'s Feeding and Costs</h2>
                                                        <p>The total weight of your order is <strong><span id="total_weight"></span></strong>. <br />
                                                        <!--Based on <strong><? //echo $pet->name; ?>'s</strong> age, weight of <strong><? //echo $pet->weight; ?>kg</strong> and potential breed size (<? //echo $pet->size; ?>), 
                                                            <? //if($pet->gender == "Male"){ echo "he"; }else{ echo "she"; } ?> should be eating approximately <strong><span id="per_day"></span>g</strong> of <strong><? //echo $xx->title; ?></strong> per day. </p>
                                                        <p>Your <strong><? //echo $xx->title; ?></strong> food will last <strong><span id="min_day_calc"></span> days</strong> at a cost of  <strong>&pound;<span id="min_cost_calc"></span></strong> per day.</p>-->
                                                            As your dog is under 12 months, please see each suitable product individual feeding guide.
                                                        <? echo $notice; ?>
                                                        <p>Feeding guides are guides only. Based on the information you have provided us we give the average calories per day, please note you may need to adjust this based on your dog’s activity or weight changes and age.</p>
                                                    
                                                        <?
                                                    }
                                                break;
                                                    
                                                case 2: // Wet Food
                                                    ?>
                                                    <p>Our Wet Food is perfect to add to dry food and a very popular way to feed your dog.</p> 
                                                    <? echo $notice; ?>
                                                    
                                                        <!--Based on <strong><? //echo $pet->name; ?>'s</strong> age, weight of <strong><? //echo $pet->weight; ?>kg</strong> and activity level, 
                                                            <? //if($pet->gender == "Male"){ echo "he"; }else{ echo "she"; } ?> should be eating <strong><? //echo $der; ?></strong> calories per day (approximately <strong><span id="per_day"></span>g</strong>) of <strong><? //echo $xx->title; ?></strong> per day. </p>
                                                        <p>Your <strong><? //echo $xx->title; ?></strong> food will last <strong><span id="min_day_calc"></span> days</strong> at a cost of  <strong>&pound;<span id="min_cost_calc"></span></strong> per day.</p>
                                                        
                                                        <p>Feeding guides are guides only. Based on the information you have provided us we give the average calories per day, please note you may need to adjust this based on your dog’s activity or weight changes and age.</p>-->
                                                        
                                                    <?
                                                break;

                                                case 5: // Raw Food
                                                    ?>
                                                        <h2><? echo $pet->name; ?>'s Feeding and Costs</h2>
                                                        <p>The total weight of your order is <strong><span id="total_weight"></span></strong>. <br />
                                                        Based on <strong><? echo $pet->name; ?>'s</strong> age and weight of <strong><? echo $pet->weight; ?>kg</strong> <? if($pet->gender == "Male"){ echo "he"; }else{ echo "she"; } ?> should be eating an average of <strong><span id="per_day"><? echo $per_day; ?></span>g</strong> of <strong><? echo $xx->title; ?></strong> per day. </p>
                                                        <p>Your <strong><? echo $xx->title; ?></strong> food will last <strong><span id="min_day_calc"></span> days</strong> at a cost of  <strong>&pound;<span id="min_cost_calc"></span></strong> per day.</p>
                                                    <?
                                                break;
                                            }
                                            ?>
                        
                        </div><!--close costs-->
                                    </div>
                        <? } ?>    
                                    <div class="c_100 breaker"></div>
                                    <div class="c_100 disclaimer">
                                        <p>Trade: Apply your Trade Code at checkout for your discount to be applied.</p>
                                        <p>Promo Code: If you have a promo code, please add at the checkout stage.</p>
                                        <p>Free DPD Delivery, small cost for Highlands and Channel Islands, this will show at the checkout.</p>
                                        </div><!--close c_100-->
                            
                        </div>
                        
                    </div><!--close purchase-->
                    
                    
                </div><!--close details-->
                
                
            </div>
            
        </div><!--close flex-->
        
    </section>
    <section id="more">
      
        <ul class="flex tabs">
            <li class="active"><a href="#" title="<? echo $xx->title; ?> Key Benefits" data-id="description">Key Benefits</a></li>
            <li><a href="#" title="<? echo $xx->title; ?> Composition" data-id="composition">Composition</a></li>
            <li><a href="#" title="<? echo $xx->title; ?> Analytical Constituents" data-id="nutrition">Analytical Constituents</a></li>
            <li><a href="#" title="<? echo $xx->title; ?> Feeding Guide" data-id="guide">Feeding Guide</a></li>
        </ul>
        <div id="info">
            <div class="sector" id="description">
                <? echo $xx->description; ?>
            </div><!--close sector-->
            
            <div class="sector" id="composition">
                <div class="flex">
                    <div class="c_100">
                        <h3>The Superfoods</h3>
                    <?
                        // Are we linked to food?
                        $sf = $db->prepare("SELECT f.title, f.description FROM product_food p LEFT JOIN foods f ON p.food_id = f.id WHERE p.product_id = ? ORDER BY f.title ASC");
                        $sf->execute(array($xx->id));
                        if($sf->rowCount() > 0){
                            echo "<div class='flex ingredients'>";
                            while($f = $sf->fetchObject()){
                                echo "<div class='c_80'><h4>{$f->title}</h4>{$f->description}</div>";
                            }
                            echo "</div>";
                        }
                    ?></div>
                    <div class="c_100"><? echo $xx->composition; ?></div>
                </div>
            </div><!--close sector-->
            
           
            
            <div class="sector" id="nutrition">
                <div class="flex">
                    <div class="c_100"><? echo $xx->constituents; ?></div>
                    <div class="c_100"><? echo $xx->nutrition; ?></div>
                </div>
            </div><!--close sector-->
            
            <div class="sector" id="guide">
                <div class="flex">
                    <div class="c_100">
                        <?
                        if(!empty($xx->feeding)){
                            echo str_replace("oodles.brainstormdevelopment", "www.ooddleskitchen", $xx->feeding);
                        }
                        ?>
                
                    </div>
                    <div class="c_100">
                        <h3>Additional Information</h3>
                        <p>All dogs are different and the guidelines should be adapted to take into account breed, age, temperament and activity level of the individual dog. When changing foods please introduce gradually over a period of two weeks. Always ensure fresh, clean water is available.</p>
                    </div>
                </div><!--close flex-->
            </div><!--close sector-->
            
            
            
            
        </div><!--close info-->
        
        
        
    </section>