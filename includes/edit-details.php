<section>
    <div id="breadcrumbs">
        <ul class="flex">
            <li><a href="/o-hub" title="O Hub">O Hub</a></li>
            <li>&rang;</li>
            <li><a href="/o-hub/edit-details" title="Orders">Edit Details</a></li>
            
            
        </ul>
        </div>
                
    </section>
<section class="orders guest_check">
    <form name="checkout" method="post" action="/actions.php?action=update-details">
    <div class="flex negative">
        <div class="c_50"><div class="inner">
    <h2>Your Details</h2>
                <p>Please complete all required fields.</p>
                <div class="flex">
                            <div class="c_50">
                                <label>First Name</label>
                                <input name="name" type="text" value="<? if(!empty($_SESSION['name'])){ echo $_SESSION['name']; }else{ echo $customer->name; } ?>" tabindex="1" id="b_first" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Surname</label>
                                <input name="surname" type="text" value="<? if(!empty($_SESSION['surname'])){ echo $_SESSION['surname']; }else{ echo $customer->surname; } ?>" tabindex="2" id="b_surname" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Telephone Number</label>
                                <input name="telephone" type="text" value="<? if(!empty($_SESSION['telephone'])){ echo $_SESSION['telephone']; }else{ echo $customer->telephone; } ?>" tabindex="3" placeholder="Required" /></div>
                            
                            <div class="c_50">
                                <label>Email Address</label>
                                <input name="email" type="text" value="<? if(!empty($_SESSION['email'])){ echo $_SESSION['email']; }else{ echo $customer->email; } ?>" tabindex="4" id="b_email" placeholder="Required" /></div>
                    
                    
                            <div class="c_100">
                                <h2>Mailing List?</h2>
                                <p><input name="mailing" type="checkbox" value="1" <? if($customer->mailing_list == 1){ echo "checked='checked'"; } ?> /> Stay in touch with news and offers from <? echo $company->name; ?></p>
                            </div>
                </div>
                </div></div><!--close c_50-->
        <div class="c_50"><div class="inner">
                
                <h2>Billing Details</h2>
                <p>&nbsp;</p>
                        <div class="flex">
                            
                            
                            <div class="c_50">
                                <label>Address 1</label>
                                <input name="address" type="text" value="<? if(!empty($_SESSION['address'])){ echo $_SESSION['address']; }else{ echo $customer->address; } ?>" tabindex="5" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>Address 2</label>
                                <input name="town" type="text" value="<? if(!empty($_SESSION['town'])){ echo $_SESSION['town']; }else{ echo $customer->town; } ?>" tabindex="6"  /></div>
                            <div class="c_50">
                                <label>City</label>
                                <input name="city" type="text" value="<? if(!empty($_SESSION['city'])){ echo $_SESSION['city']; }else{ echo $customer->city; } ?>" tabindex="7" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>County</label>
                                <input name="county" type="text" value="<? if(!empty($_SESSION['county'])){ echo $_SESSION['county']; }else{ echo $customer->county; } ?>" tabindex="8" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>Postcode</label>
                                <input name="postcode" type="text" value="<? if(!empty($_SESSION['postcode'])){ echo $_SESSION['postcode']; }else{ echo strtoupper($customer->postcode); } ?>" tabindex="9" placeholder="Required" /></div>
                            <div class="c_50">
                                <label>Country</label>
                                <select name="country"><option value="">Required</option>
                    <? 
                        $sb = $db->prepare("SELECT * FROM countries ORDER BY country ASC"); 
                        $sb->execute();
                        while($b = $sb->fetchObject()){
                            if($customer->country == $b->country){
                                echo "<option value='{$b->country}' selected>{$b->country}</option>";
                            }else{
                                echo "<option value='{$b->country}'>{$b->country}</option>";
                            }
                            
                        }
                    ?>
                    </select></div>
                            </div><!--close flex-->
            
            </div></div><!--close c_50-->
        </div>
        <p><input name='submit' type='submit' value='Update Details' /></p>
        </form>
    
</section>