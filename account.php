<? include "header.php"; ?>
<div id="banner" class="half">
     <a href="/" title="<? echo $company->name; ?> Logo"><img src="/images/logo-horizontal.png" alt="<? echo $company->name; ?> Logo" class="logo" /></a>
    <? include "includes/nav.php"; ?>
    
    <div class="headline centre">
        <h1>Your O Hub</span></span></h1>
        
    </div><!--close headline-->
    
</div><!--close banner-->
            <?
            
            if($logged_in == 2){
                ?>
                <section>
                    <div class="flex negative ohub">
                        <div class="c_33">
                            <div class="inner" style="height:390px" ><h5>Your Details</h5>
                            <p><strong><? echo $customer->name; ?> <? echo $customer->surname; ?></strong><br />
                                <strong>Email:</strong> <? echo $customer->email;
                                if(!empty($customer->telephone)){
                                echo "<br /><strong>Telephone:</strong> ".$customer->telephone; }
                                ?></p>
                                <?
                                if(!empty($customer->address)){
                                    echo "<p>{$customer->address}<br />
                                    {$customer->town}, {$customer->city}<br />
                                    {$customer->county} {$customer->postcode}</p>";
                                }
                                if($customer->mailing_list == 1){
                                    echo "<p><strong>You are currently on our mailing list</strong></p>";
                                }
                
                                ?>
                                <p><a href="/o-hub/edit-details" title="Edit Details" class="btn brown"style="margin-top: 58px;">Edit Details</a></p>
                                <p><a href="/o-hub/edit-password" title="Edit Password" class="btn brown">Edit Password</a></p>
                            </div>
                        </div>
                        <div class="c_33">
                            <div class="inner" style="height:390px"><h5>Your Pets</h5>
                                 <table class="table-responsive-full account_table">
                                  <thead>
                                    <tr>
                                      <th>Pet</th>
                                        <th>Breed</th>
                                        <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      <?
                                        $sq = $db->prepare("SELECT * FROM pets WHERE customer_id = ? AND inactive != ?");
                                        $sq->execute(array($customer->id,1));
                                        if($sq->rowCount() > 0){
                                            while($q = $sq->fetchObject()){
                                                echo "<tr>
                                                      <td data-label='Name'>{$q->name}</td>
                                                      <td data-label='Breed'>";
                                                if(!empty($q->breed)){
                                                    echo $q->breed;    
                                                }else{
                                                    echo $q->type_id;
                                                }
                                                echo "</td>
                                                      <td data-label='Action'><a href='/o-hub/pets/{$q->unique_id}' title='View {$q->name}' class='btn blue'>View</a></td>
                                                      </tr>";    
                                            }
                                        }else{
                                            echo "<tr>
                                                  <td colspan='3'>You have no pets</td>
                                                  </tr>";
                                        }
                                        
                                       
                                      ?>
                                    
                                   
                                  </tbody>
                              </table>
                                <p><a href="/actions.php?action=add-pet" title="Add A Dog" class="btn brown" style="margin-top: 68px;">Add A Dog</a></p>
                                
                                
                            </div>
                        </div>
                        <div class="c_33">
                            <div class="inner" style="height:390px"><h5>Latest Orders</h5>
                            <table class="table-responsive-full account_table">
                                  <thead>
                                    <tr>
                                      <th>Date</th>
                                      <th>Amount</th>
                                        <th>Status</th>
                                         <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      <?
                                        get_orders($db, array($customer->id, 2));
                                      ?>
                                    
                                   
                                  </tbody>
                              </table>
                                <p><a href="/o-hub/orders" title="View All Orders">View all orders</a></p>
                                
                                <p><a href="/o-hub/ranges" title="Place a new order" class="btn brown">Place a new order</a></p>
                            
                            </div>
                        </div>
                        
                        
                         <div class="c_33">
                            <div class="inner repeater " style="height:590px"><h5>Auto Repeat Orders</h5>
                                
                                <img src="/images/sleepy.jpg" alt="Sleepy" class="placeholder" />
                                <?
                                $st = $db->prepare("SELECT a.start_date, a.next_due, p.name FROM auto_repeat a LEFT JOIN pets p ON a.pet_id = p.unique_id WHERE a.customer_id = ? AND a.status = ?");
                                $st->execute(array($customer->id, "Active"));
                                if($st->rowCount() > 0){
                                    $r = $st->fetchObject();
                                    echo "<p><span class='bold'>Next due";
                                    if(!empty($r->name)){
                                        echo " for {$r->name} ";
                                    }
                                    echo "on</span><br />
                                          ".date("D jS M, Y", strtotime($r->next_due))."</p>
                                          <a href='/o-hub/auto-repeat' title='Update or Cancel Auto Repeat'>Update or Cancel Auto Repeat</a>";
                                }else{
                                    echo "<p style='margin: 47px 0px;'>You do not have any auto repeat orders.</p>";
                                }
                                ?>
                                
                            
                                
                            </div>
                        </div>
                        
                         <div class="c_33">
                            <div class="inner ranges" style="height:590px;background-color: #69c3e8;">
                                
                                <div class="range_header">
                                <h5>Our Ranges</h5>
                                    </div><!--close range_header-->
                               
                                <a href="/o-hub/ranges" title="Shop Now"><img src="/images/ranges.jpg" alt="Our Ranges" class="placeholder" /></a>
                               <div class="range_header2">
                                <p style="padding-top: 20px;">We have a large variety of dry, wet and raw food, as well as many tasty treats for your dog.</p>
                                <p><a href="/o-hub/ranges" title="Shop Now" class="send btn " style="margin-top: 30px;color:white;">Shop Now</a></p>
                            </div>
                                
                            </div>
                        </div>
                        
                        <div class="c_33">
                            <div class="inner ranges" style="height:590px;background-color: #69c3e8;">
                                
                                <div class="range_header">
                                <h5>Ooddles Woof Blog</h5>
                                    </div><!--close range_header-->
                               
                                <a href="/blog" title="Blog"><img src="/images/blog.jpg" alt="Blog" class="placeholder" /></a>
                                <div class="range_header2">
                                <p style="padding-top: 20px;">Get all the latest Ooddles news from our Ooddles Woof Blog</p>
                                <p><a href="/blog" title="Read More" class="send btn " style="margin-top: 35px; color:white;">Read More</a></p>
                                </div>
                                
                            </div>
                        </div>
                        
                        
                        
                        
                        <div class="c_33">
                            <div class="inner" style="height: 357px;"><h5>Your Promo Codes &amp; My Rewards</h5>
                                <?
                                $sd = $db->prepare("SELECT * FROM discount_codes WHERE customer_id = ?");
                                $sd->execute(array($customer->id));
                                if($sd->rowCount() == 0){
                                    echo "<p style='margin-top: 95px;'><strong>You currently have no discount codes.</strong></p>";    
                                }else{
                                    ?>
                                    <table class="table-responsive-full">
                                  <thead>
                                    <tr>
                                      <th>Code</th>
                                      <th>Percentage</th>
                                        <th>Status</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      <?
                                        while($d = $sd->fetchObject()){
                                            echo "<tr>
                                                  <td>{$d->discount_code}</td>
                                                  <td>{$d->percentage}%</td>
                                                  <td>";
                                            if($d->redeemed == 1){
                                                echo "Redeemed";
                                            }else{
                                                echo "Available";
                                            }
                                            echo "</td>
                                                  </tr>";
                                        }
                                      ?>
                                    
                                   
                                  </tbody>
                              </table>
                                    <?
                                }
                                ?>
                                
                            
                                
                            </div>
                        </div>



                        <div class="c_33">
                        <div class="inner" style="background-color: #69c3e8;">
                        <a href="/come-dine-with-ooddles"> <img src="/images/Asset-ohub.png"  alt="" style="background-size: cover; width:100%; height: 350px;"></a>
                        </div>
                        </div>
                        <!-- <div class="c_33">
                            <div class="inner"style="height: 357px;">
                                <div class="flex"> -->
                                   
                                    
                                    <div class="c_33 ">
                                        <div class="inner refer">
                                            <h5>Refer a Dog</h5>
                                            <p>Just enter the email  of the dog's owner and enjoy <strong>great discounts</strong></p>
                                    
                                                <form name="email" method="post" action="/actions.php?action=refer">
                                                    <input name="url" type="hidden" value="<? echo $_SERVER['REQUEST_URI']; ?>" />
                                                    <input name="name" type="text" placeholder="Name" /><br /><br />
                                                    <input name="email" type="text" placeholder="Email" />
                                                    <p style="margin-top: 20px;"><input name="submit" type="submit" value="Send" />
                                                </form>
                                                       
                                           <!-- <p>Or simply hand out your unique referral link:</p>
                                            <p><strong><? 
                                                //echo $customer->referral_code; ?></strong></p>-->
                                            </div>
                                        </div>
                                    <!-- </div>
                                            
                                            
                                
                                
                                
                            </div> -->
                        <!-- </div> -->
                        
                       
                       
                    </div>
                </section>
                <?
            }else{
                include "includes/login.php";
            }
            

          if($logged_in != 2){ 
              include "includes/ranges.php";
              ?>
              <h3 class="subtitle">Personalised Premium Dog Food - Delivered Directly To Your Door</h3>
              <?
              include "includes/hub.php"; 
          } ?>
    
<? 
include "includes/company.php";
include "footer.php"; ?>