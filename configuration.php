<?
session_start();
if(substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
header('Content-type: text/html; charset=utf-8');

/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

define("MAIN_SITE", "https://www.ooddleskitchen.co.uk");
define("URL", $_SERVER['HTTP_HOST']);
define("PAGE", $_SERVER['PHP_SELF']);

// Mobile Detection
require_once 'classes/mobile.detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

// The connection


switch(URL){
	default:
		define("HOST", "localhost:3306");
		define("USER", "ooddles");
		define("PASS", "Drqu@ngo99_");
		define("DATABASE", "main");
		try {
			$db = new PDO("mysql:host=".HOST.";dbname=".DATABASE, USER, PASS);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		} 
		catch(PDOException $err){
			$error = $err->getMessage();
		}
		
	break;
	
}

define("NEWS", "/images/news/");
define("PRODUCTS", "/images/products/");
define("THUMBS", "/images/thumbs/");
define("IMAGES", "/images/");

define("INVOICES", "/var/www/vhosts/ooddleskitchen.co.uk/httpdocs/invoices/");
define("ROOT", "/var/www/vhosts/ooddleskitchen.co.uk/httpdocs/");

define("VAT_RATE", 20);
define("VAT_CALC", 1.2);

define("STATE", "LIVE");

// The company details
$st = $db->prepare("SELECT *
                    FROM company");
$st->execute();
$company = $st->fetchObject();

include "functions.php";

$genders = array("Male", "Female");
$answers = array("Yes", "No");
$food_types = array("Dry" => array("Dry Food Only", "dry-food.png"), "Wet" => array("Wet Food Only", "wet-food.png"), "Dry and Wet" => array("Dry and Wet Mixed", "wet-and-dry-food.png"), "Bare Oodles" => array("Raw", "wet-and-dry-food.png"), "Treats" => array("Healthy Treats/Chews", "wet-and-dry-food.png"));

$birth_days = range(1,31);
$birth_months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$site_key = "6Lfc5cwZAAAAAOFdLti7e4SHhjf7stu6AI9NNJzI";
$secret_key = "6Lfc5cwZAAAAABzRz9X6uBIU28RuPdq4Py0kTDJh";


if($logged_in != 2){
    $sc = $db->prepare("SELECT * FROM orders WHERE session_id = ? AND status = ?");
    $sc->execute(array(session_id(), "Unpaid"));
    if($sc->rowCount() > 0){
        $c = $sc->fetchObject();
        $cart_id = $c->id;
        $cart_hash = $c->unique_id;
        $sf = $db->prepare("SELECT SUM(price) AS total, COUNT(id) AS qty, SUM(discount) AS discount FROM cart WHERE order_id = ?");
        $sf->execute(array($c->id));
        $cart = $sf->fetchObject();
    }
}else{
    $sc = $db->prepare("SELECT * FROM orders WHERE customer_id = ? AND status = ?");
    $sc->execute(array($customer->id, "Unpaid"));
    if($sc->rowCount() > 0){
        $c = $sc->fetchObject();
        $cart_id = $c->id;
        $cart_hash = $c->unique_id;
        $sf = $db->prepare("SELECT SUM(price) AS total, COUNT(id) AS qty, SUM(discount) AS discount FROM cart WHERE order_id = ?");
        $sf->execute(array($c->id));
        $cart = $sf->fetchObject();
    }
}


$ship_rate = 4.95;
$min_spend = 50;

$raw_percent = 3;

$stripe_endpoint = "https://api.stripe.com";

$stripe_key = "pk_live_jNrJfCEWemQHaMcqMtJi0X7o";
$stripe_secret_key = "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";

$stripe_shipping = "prod_IncCS1IgE5S1Ur";

$bitly = "226a10980395a11dd09fc4f5246861f36fae37e9";

$panels = array(array("/breeders-and-stockists", "/images/tab-breeders.jpg"),array("/get-social", "/images/tab-social.jpg"),array("/blog", "/images/tab-blog.jpg"),array("/shop", "/images/tab-treats.jpg"),array("/rewards", "/images/tab-refer.jpg"),array("/how-it-works", "/images/tab-works.jpg"));
shuffle($panels);

$sirv = "https://ooddles.sirv.com";

$breadcrumbs = array();
$breadcrumbs[] = array("name" => "Home", "link" => "/");
?>