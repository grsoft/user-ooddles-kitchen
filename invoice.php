<?
include "configuration.php";
include "vendor/autoload.php";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// reference the Dompdf namespace
    use Dompdf\Dompdf;
    use Dompdf\Options;



// instantiate and use the dompdf class
    $options = new Options();
    $options->setIsRemoteEnabled(true);
    $options->setDefaultFont('Helvetica');

    $dompdf = new Dompdf($options); 

   //echo "SELECT * FROM orders WHERE unique_id = '".$_GET['id']."' "; exit;

    $ro = $db->prepare("SELECT * FROM orders WHERE unique_id = ? ");
    $ro->execute(array($_GET['id']));
    $order = $ro->fetchObject();

// Customer and delivery details
   // echo "SELECT *           FROM customers            WHERE id ='".$order->customer_id."' ";exit;
	$st = $db->prepare("SELECT *
						FROM customers
						WHERE id = ? ");
	$st->execute(array($order->customer_id));
	$customer = $st->fetchObject();

	$address = $customer->name." ".$customer->surname;
    if(!empty($customer->company)){
        $address .= ", ".$customer->company;
    }
    $address .= "<br />".$customer->address."<br />";
	if(!empty($customer->town)){
		$address .= $customer->town.", ";
	}
	$address .= $customer->city."<br />".$customer->county." ".strtoupper($customer->postcode)."<br />".$customer->country."<br /><br /><strong>Telephone:</strong> ".$customer->telephone."<br /><strong>Email:</strong> ".$customer->email;

//echo "SELECT * FROM deliveries WHERE order_id ='".$order->id."'";
    $st = $db->prepare("SELECT * FROM deliveries WHERE order_id = ?");
    $st->execute(array($order->id));
    $delivery = $st->fetchObject();

// The delivery address
    $delivery_address = $delivery->d_name." ".$delivery->d_surname."<br />".$delivery->d_address.", <br />";
    if(!empty($delivery->d_address2)){
        $delivery_address .= $delivery->d_address2.", ";
    }
    $delivery_address .= $delivery->d_city.", <br />".$delivery->d_county." ".strtoupper($delivery->d_postcode)."<br />".$delivery->d_country."<br /><br /><strong>Telephone:</strong> ".$delivery->d_telephone."<br /><strong>Email:</strong> ".$delivery->d_email;
//echo "SELECT * FROM orders  WHERE customer_id='".$customer->id."' ";exit;
 $orderCount=$db->prepare("SELECT * FROM orders  WHERE customer_id='".$customer->id."' ");
  $orderCount->execute();
  //$cout=$orderCount->rowCount();

  if($orderCount->rowCount()==1){
    $cout=$orderCount->rowCount()."st";
    //$cout.="1st";
  }else{
    $cout=$orderCount->rowCount();
  }

    $html ="<!doctype html>
            <html>
            <head>
            <meta charset='UTF-8'>
            <title>Invoice</title>
            <style>
                
                @page { margin: 0in; padding:0px; }
                body {
                  font-family: Helvetica, sans-serif;
                  font-weight: 300;
                  color: #653a2b;
                  font-size:11px;
                  width:100%;
                  height:950px;
                  padding:0px;
                  margin:0px;
                  
                }
            </style>
            </head>

            <body>
            <img src='https://www.ooddleskitchen.co.uk/images/invoice-header.jpg' style='position: absolute; top:0px; left:0px; z-index:1; width:100%;' />
            
            <div style='position:absolute; top:240px; left:0px; z-index:99; width:100%; font-family: Helvetica;'>
            
                <div style='display:block; margin:0px 20px;'>
                    <div style='width:32%; display:inline-block; vertical-align:top;'>
                        <h1 style='font-size:14px; margin-top:0px; padding-top:0px; margin-bottom:10px; padding-bottom:0px; font-family: Helvetica; font-weight:700; text-transform:uppercase;'>Invoice</h1>
                        <p style='padding-top:0px; margin-top:0px; font-family: Helvetica; font-weight:normal;'><strong>Created:</strong> ".date("D jS F, Y", strtotime($order->created_on))."<br /><strong>Invoice Number:</strong> {$order->unique_id} </p>

                          <br><br><br>
                        <h1 style='font-size:14px; margin-top:10px; padding-top:0px; font-family: Helvetica; font-weight:700; '>TO : ".$cout."</h1>


                        </div>

                    <div style='width:32%; display:inline-block; vertical-align:top;'>
                        <h2 style='font-size:14px; margin-top:0px; padding-top:0px; font-family: Helvetica; font-weight:700; text-transform:uppercase; margin-bottom:10px; padding-bottom:0px;'>Billing Address</h2>
                        <p style='padding-top:0px; margin-top:0px; font-family: Helvetica; font-weight:normal;'>{$address}</p></div>
                    <div style='width:32%; display:inline-block; vertical-align:top;'>
                        <h2 style='font-size:14px; margin-top:0px; padding-top:0px; font-family: Helvetica; font-weight:700; text-transform:uppercase; margin-bottom:10px; padding-bottom:0px;'>Shipping Address</h2>
                        <p style='padding-top:0px; margin-top:0px; font-family: Helvetica; font-weight:normal;'>{$delivery_address}</p>
                        </div>
                </div>
                
        <span style='font-size:20px; text-transform:uppercase;  font-family: Helvetica;'><h1 style='font-size:16px; margin-top:30px; margin-bottom:20px; padding-bottom:0px; padding-left:20px; font-weight:700;'>Order Detail<br></h1></span>
		 <div style='padding-left:20px; padding-right:20px;'>
    <table cellpadding='0' cellspacing='0' style='width:100%;'>";


$st = $db->prepare("SELECT s.title, c.basket_data,c.price, SUM(c.price) AS subtotal, COUNT(c.id) AS qty, p.title AS product_title, i.category, p.image, c.auto_repeat, s.quantity, s.weight, s.unit, c.discount, SUM(c.discount) AS overall_discount, t.name
                    FROM cart c
                    LEFT JOIN skus s ON c.sku_id = s.id
                    LEFT JOIN products p ON s.product_id = p.id
                    LEFT JOIN categories i ON p.category_id = i.id
                    LEFT JOIN orders o ON c.order_id = o.id
                    LEFT JOIN pets t ON c.pet_id = t.unique_id
                    WHERE o.unique_id = ? AND o.status = ?
                    GROUP BY c.sku_id, c.auto_repeat");
$st->execute(array($_GET['id'], "Paid"));
if($st->rowCount() == 0){
    $html .= "<tr>
              <td style='padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #ddd; font-family: Helvetica; font-weight:normal; font-size:0.6em;' colspan='3'><p>Sorry, we can't find this summary</p><br></td>
              </tr>";
}else{
    
   
    $html .= "<tr>
              <th style='text-align:left;font-family: Helvetica; font-weight:bold; font-size:11px; padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #ddd;padding-right:10px; text-transform:uppercase;'>Description</th>
              <th style='text-align:left;font-family: Helvetica; font-weight:bold; font-size:11px; padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #ddd;padding-right:10px; text-transform:uppercase;'>Dog</th>
              <th style='text-align:left;font-family: Helvetica; font-weight:bold; font-size:11px; padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #ddd;padding-right:10px; text-transform:uppercase;'>Quantity</th>
              <th style='text-align:left;font-family: Helvetica; font-weight:bold; font-size:11px; padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #ddd;padding-right:10px; text-transform:uppercase;'>Num Items</th>
              <th style='text-align:left;font-family: Helvetica; font-weight:bold; font-size:11px; padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #ddd;padding-right:10px; text-transform:uppercase;'>Total</th>
              </tr>";
    $num = array();
    while($r = $st->fetchObject()){
        $val = ($r->qty*$r->quantity);
        $num[] = $val;
        $test='';

        if($r->basket_data){

                    $restdata=unserialize($r->basket_data);

                    foreach ($restdata as $key => $value) {
                      //print_r($value);   exit;               

                     $test.=",".$value['product'];
                      
                    }

                  }else{
                  	$test=$r->title;
                  }
        $html .= "<tr>
                  <td style='padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #ddd; font-family: Helvetica; font-weight:normal; font-size:11px;'>".$test." @ &pound;".number_format($r->price,2)."</td>
                  <td style='padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #ddd; font-family: Helvetica; font-weight:normal; font-size:11px;'>{$r->name}</td>
                  <td style='padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #ddd; font-family: Helvetica; font-weight:normal; font-size:11px;'>{$r->qty}</td>
                  <td style='padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #ddd; font-family: Helvetica; font-weight:normal; font-size:11px;'>{$val}</td>
                  <td style='padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #ddd; font-family: Helvetica; font-weight:normal; font-size:11px;'>&pound;".number_format($r->subtotal,2)."</td>
                  </tr>";
    }    
}

$subtotal = round((($order->total-$order->discount_total)/VAT_CALC),2);
$shipping = round(($order->shipping/VAT_CALC),2);
$vat_sum = round(((($order->total-$order->discount_total)+$order->shipping)/VAT_CALC),2);
$vat = round((($order->total-$order->discount_total)+$order->shipping),2) - $vat_sum;
$html .= "<tr>
		  <td colspan='5'>&nbsp;</td>
		  </tr>
		  <tr>
		  <td colspan='3' class='cell deliver no_b'>&nbsp;</td>
		  <td colspan='1' class='cell deliver' style='padding-top:5px; padding-bottom:8px; text-align:right; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px; text-transform:uppercase; padding-right:20px;'>Number of Items</td>
		  <td class='deliver cell' style='padding-top:5px; padding-bottom:8px; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px;  text-transform:uppercase;'>".array_sum($num)."</td>
		  </tr>
          <tr>
		  <td colspan='3' class='cell deliver no_b'>&nbsp;</td>
		  <td colspan='1' class='cell deliver' style='padding-top:5px; padding-bottom:8px; text-align:right; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px; text-transform:uppercase; padding-right:20px;'>Discount</td>
		  <td class='deliver cell' style='padding-top:5px; padding-bottom:8px; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px;  text-transform:uppercase;'>&pound;".number_format($order->discount_total, 2)."</td>
		  </tr>
		  <tr>
		  <td colspan='3' class='cell deliver no_b'>&nbsp;</td>
		  <td colspan='1' class='cell deliver' style='padding-top:5px; padding-bottom:8px; text-align:right; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px; text-transform:uppercase; padding-right:20px;'>Delivery</td>
		  <td class='deliver cell' style='padding-top:5px; padding-bottom:8px; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px;  text-transform:uppercase;'>&pound;".number_format($order->shipping, 2)."</td>
		  </tr>
          <tr>
		  <td colspan='3' class='cell deliver no_b'>&nbsp;</td>
		  <td colspan='1' class='cell deliver' style='padding-top:5px; padding-bottom:8px; text-align:right; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px; text-transform:uppercase; padding-right:20px;'>Sub Total</td>
		  <td class='deliver cell' style='padding-top:5px; padding-bottom:8px; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px;  text-transform:uppercase;'>&pound;".number_format($subtotal, 2)."</td>
		  </tr>
		  <tr>
		  <td colspan='3' class='cell deliver no_b'>&nbsp;</td>
		  <td colspan='1' class='cell deliver no_b' style='padding-top:5px; padding-bottom:8px; text-align:right; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px; text-transform:uppercase; padding-right:20px;'>VAT @ ".VAT_RATE."%</td>
		  <td class='deliver cell no_b' style='padding-top:5px; padding-bottom:8px; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px;  text-transform:uppercase;'>&pound;".number_format($vat,2)."</td>
		  </tr>
		  <tr>
		  <td colspan='3' class='cell deliver no_b'>&nbsp;</td>
		  <td colspan='1' class='cell deliver total' style='padding-top:5px; padding-bottom:8px; text-align:right; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px; text-transform:uppercase; padding-right:20px;'>Total</td>
		  <td class='deliver cell total' style='padding-top:5px; padding-bottom:8px; border-bottom:1px solid #000; font-family: Helvetica; font-weight:normal; font-size:11px;  text-transform:uppercase;'>&pound;".number_format(round(($subtotal+$shipping+$vat),2), 2)."</td>
		  </tr>	";
		 
$html .= "</table>

          
</div>
            </div>
            
            <img src='https://www.ooddleskitchen.co.uk/images/invoice-footer.jpg' style='position: absolute; bottom:0px; left:0px; z-index:1; width:100%;' />";

    $html .= "</body>
              </html>";

if($_GET['type'] == "dl"){

    
    $dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
    $dompdf->render();

    $link = $order->unique_id."-invoice.pdf";

// Output the generated PDF to Browser
    $dompdf->stream($link);
    
}else{
    echo $html;
}
?>