<?
include "check-login.php";
include "pdf/dompdf_config.inc.php";

/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

$ro = $db->prepare("SELECT * FROM orders WHERE unique_id = ?");
$ro->execute(array($_GET['id']));
$order = $ro->fetchObject();



// Customer and delivery details
	$st = $db->prepare("SELECT *
						FROM customers
						WHERE id = ?");
	$st->execute(array($order->customer_id));
	$customer = $st->fetchObject();

	$address = $customer->name." ".$customer->surname;
    if(!empty($customer->company)){
        $address .= ", ".$customer->company;
    }
    $address .= "<br />".$customer->address."<br />";
	if(!empty($customer->address2)){
		//$address .= $customer->address2."<br />";
	}
	$address .= $customer->city."<br />".strtoupper($customer->postcode)."<br />".$customer->country."<br /><strong>Telephone:</strong> (+".$customer->country_code.") ".$customer->telephone."<br /><strong>Email:</strong> ".$customer->email;


	


$html = "<style type='text/css'>
         body {
            background-image:url(https://oodles.brainstormdevelopment.co.uk/images/bg-invoice.jpg);
            background-size: 100% auto;
            }
         </style>
         <div style='position:relative; z-index:1;top:0px; left:0px; height:100px;'><span style='font-size:1.9em; text-transform:uppercase;font-family:Helvetica;'><img src='".MAIN_SITE."/images/logo.png' width='300' style='margin-top:0px;' /> </span> 
         <div style='display:block; width:100%; position:absolute; z-index:2; top:0px; right:0px; text-align:right; '><span style='font-size:1.9em; text-transform:uppercase;font-family:Helvetica; '>Invoice</span><br /><span style='font-size:0.6em;font-family:Helvetica;'><strong>Date</strong>: ".date("d-m-Y", strtotime($order->created_on))."<br /><strong>Invoice Number</strong>: ".$order->unique_id."</span></div>
         
		 <div style='clear:both; border-bottom:1px solid #ccc; height:1px; padding-top:55px;'>&nbsp;</div>
		 
		 <div style='position:relative; z-index:3; left:0px; font-family:Helvetica, Arial; font-size:0.6em; margin-top:20px; width:100%;'><strong>Billing Address</strong><br />".$address."
		 
		 </div>
		 <div style='position:relative; top:0px; z-index:4; padding-top:50px;'><span style='font-size:1.2em; text-transform:uppercase;font-family:Helvetica; '><strong>Order Detail</strong></span><br /><br /><br />
		 <table cellpadding='0' cellspacing='0' width='100%'>
		<tr>
			<th style='text-align:left;font-family:Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:10px;padding-bottom:5px;border-bottom:1px solid #000;padding-right:10px;'>DATE</th>
			<th style='text-align:left;font-family:Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:10px;padding-bottom:5px;border-bottom:1px solid #000;padding-right:10px;' colspan='2'>ITEM</th>
        </tr>";

$st = $db->prepare("SELECT s.title, c.price, SUM(c.price) AS subtotal, COUNT(c.id) AS qty, p.title AS product_title, i.category, p.image, c.auto_repeat, s.quantity, s.weight, s.unit, c.discount, SUM(c.discount) AS overall_discount
                    FROM cart c
                    LEFT JOIN skus s ON c.sku_id = s.id
                    LEFT JOIN products p ON s.product_id = p.id
                    LEFT JOIN categories i ON p.category_id = i.id
                    LEFT JOIN orders o ON c.order_id = o.id
                    WHERE o.unique_id = ? AND o.status = ?
                    GROUP BY c.sku_id, c.auto_repeat");
$st->execute(array($_GET['id'], $xx->status));
if($st->rowCount() == 0){
    $html .= "<tr>
          <td style='padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #efefef;font-family:Helvetica; font-size:0.6em;'><p>Sorry, we can't find this summary</p></td>
          </tr>";
}else{
    
   
    $html .= "<tr>
              <th style='text-align:left;font-family:Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:10px;padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #efefef;padding-right:10px;'>Description</th>
              <th style='text-align:left;font-family:Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:10px;padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #efefef;padding-right:10px;'>Quantity</th>
              <th style='text-align:left;font-family:Gotham, Helvetica Neue, Helvetica, Arial, sans-serif; font-size:10px;padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #efefef;padding-right:10px;'>Total</th>
              </tr>";
    while($r = $st->fetchObject()){
        $html .= "<tr>
                  <td style='padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #efefef;font-family:Helvetica; font-size:0.6em;'>{$i->description} @ &pound;".number_format($i->price,2)."</td>
                  <td style='padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #efefef;font-family:Helvetica; font-size:0.6em;'>{$i->quantity}</td>
                  <td style='padding-top:5px; padding-bottom:5px; border-bottom:1px dotted #efefef;font-family:Helvetica; font-size:0.6em;'>&pound;".number_format($i->total,2)."</td>
                  </tr>";
    }    
}

$subtotal = round((($order->total-$order->discount)/VAT_CALC),2);
$shipping = round(($order->shipping/VAT_CALC),2);
$vat_sum = round(((($order->total-$order->discount)+$order->shipping)/VAT_CALC),2);
$vat = round((($order->total-$order->discount)+$order->shipping),2) - $vat_sum;
$html .= "<tr>
		  <td colspan='3'>&nbsp;</td>
		  </tr>
		  <tr>
		  <td colspan='1' class='cell deliver no_b'>&nbsp;</td>
		  <td colspan='1' class='cell deliver' style='padding-top:5px; padding-bottom:8px; text-align:right; border-bottom:1px solid #000;font-family:Helvetica; font-size:0.6em; text-transform:uppercase; padding-right:20px;'><strong>Discount</strong></td>
		  <td class='deliver cell' style='padding-top:5px; padding-bottom:8px; border-bottom:1px solid #000;font-family:Helvetica; font-size:0.6em; text-transform:uppercase;'>&pound;".number_format($order->discount, 2)."</td>
		  </tr>
		  <tr>
		  <td colspan='1' class='cell deliver no_b'>&nbsp;</td>
		  <td colspan='1' class='cell deliver' style='padding-top:5px; padding-bottom:8px; text-align:right; border-bottom:1px solid #000;font-family:Helvetica; font-size:0.6em; text-transform:uppercase; padding-right:20px;'><strong>Sub Total</strong></td>
		  <td class='deliver cell' style='padding-top:5px; padding-bottom:8px; border-bottom:1px solid #000;font-family:Helvetica; font-size:0.6em; text-transform:uppercase;'>&pound;".number_format($subtotal, 2)."</td>
		  </tr>
		  </tr>
		  
		  
		  <tr>
		  <td colspan='1' class='cell deliver no_b'>&nbsp;</td>
		  <td colspan='1' class='cell deliver no_b' style='padding-top:5px; padding-bottom:8px; text-align:right; border-bottom:1px solid #000;font-family:Helvetica; font-size:0.6em; text-transform:uppercase; padding-right:20px;'><strong>VAT @ ".VAT_RATE."%</strong></td>
		  <td class='deliver cell no_b' style='padding-top:5px; padding-bottom:8px; border-bottom:1px solid #000;font-family:Helvetica; font-size:0.6em; text-transform:uppercase;'>&pound;".number_format($vat,2)."</td>
		  </tr>
		  
		  
		  <tr>
		  <td colspan='1' class='cell deliver no_b'>&nbsp;</td>
		  <td colspan='1' class='cell deliver total' style='padding-top:5px; padding-bottom:8px; text-align:right; border-bottom:1px solid #000;font-family:Helvetica; font-size:0.6em; text-transform:uppercase; padding-right:20px;'><strong>Total</strong></td>
		  <td class='deliver cell total' style='padding-top:5px; padding-bottom:8px; border-bottom:1px solid #000;font-family:Helvetica; font-size:0.6em; text-transform:uppercase;'>&pound;".number_format(round(($subtotal+$shipping+$vat),2), 2)."</td>
		  </tr>	";
		 
$html .= "</table>";
if($order->status == 1){
	$html .= "<h1 style='font-family:Helvetica; font-size:1em; text-transform:uppercase; font-weight:normal; padding-top:10px;'><strong>Payment</strong></h1><p style='font-family:Helvetica, Arial; font-size:0.6em; padding-top:0px; margin-bottom:20px;'>To pay this invoice please go to:<br />
	<a href='".MAIN_SITE."/resume/{$order->unique_id}'>".MAIN_SITE."/resume/{$order->unique_id}</a></p>";
}
		
$html .= "<h1 style='font-family:Helvetica; font-size:1em; text-transform:uppercase; font-weight:normal; padding-top:10px;'><strong>Contact</strong></h1>
         <p style='font-family:Helvetica, Arial; font-size:0.6em; padding-top:0px; margin-bottom:20px;'>If you have any problems with your order please contact us by <br />email at <a href='mailto:".$company->email."' title='Email' style='color:#000; font-weight:bold; text-decoration:none;'>".$company->email."</a> stating the reference number <strong>".$order->unique_id."</strong>.</p>
		 
		 </div>
		 <div style='position:absolute; bottom:0px; left:0px; color:#aaa; text-align:left; width:100%; font-family:Helvetica, Arial; font-size:0.5em; padding:20px 0px; border-top:1px solid #ccc;'>".$company->address."<br />Email: ".$company->email."&nbsp;&nbsp;&nbsp;&nbsp;Telephone: ".$company->telephone."</div>";


if($_GET['type'] == "dl"){
    $pdf = new DOMPDF();
    $pdf->load_html($html);
    $pdf->render();
    $output = $pdf->output();
    $link = $order->unique_id."-invoice.pdf";
    
    //file_put_contents($link, $output);
    $pdf->stream($link);
    
}else{
    echo $html;
}



?>